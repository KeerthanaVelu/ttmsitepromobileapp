﻿// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var objectTypes = {
    account: 'account_',
    assetowner: 'assetowner_',
    rca: 'rca_',
    contractor: 'contractor_',
    tenantbranch: 'tenantbranch_',
    truck: 'truck_',
    staff: 'staff_',
    job: 'job_',
    qdata: 'qdata_',
    teammember: 'teammember_',
    timelog: 'timelog_',
    jobactivity: 'jobactivity_',
    siteactivity: 'siteactivity_',
    sitehazardassessment: 'sitehazardassessment_',
    onsiterecord: 'onsiterecord_',
    endShift: 'endshift_',
    truckaudit: 'truckaudit_',
    stopbreaktimetimer: 'stopbreaktime_timer_',
    agency: 'agency_',
    agencystaff: 'agencystaff_',
    breaktimeupdate: 'breaktimeupdate_',
    starttrucksshift: 'starttrucksshift_',
    siteRecordpdflog: 'siteRecordpdflog_',
    updateJobStatus: 'updateJobStatus_',
    truckPrecheckRecord: 'truckPrecheckRecord_',
    geargeftonsiterecord:'geargeftonsiterecord_'
};
var siteprodb = null;
var mainApp = angular.module('mainApp', ['ionic', 'angular-linq', 'ngAnimate', 'angularMoment', 'timer', 'ngResource', 'ngCordova', 'SignalR', 'TruckAudit', 'ngSanitize', 'google.places'])
    .config(['$stateProvider', function ($stateProvider) {
        $stateProvider.state('app', {
            url:'/app',
            template: '<ion-nav-view></ion-nav-view>',
            //template: '<ion-nav-view><ion-view></ion-view></ion-nav-view>',
            controller: 'AppCtrlr'
        }).state('app.login', {
            url: '/login',
            //url: '/',
            templateUrl: 'views/login.html',
            controller: 'LoginCtrlr',
            params: { mode: null }
        }).state('app.jobs', {
            url: '/jobs',
            templateUrl: 'views/jobs.html',
            controller: 'JobsCtrlr'
        }).state('app.jobdetail', {
            url: '/jobs/{id}',
            templateUrl: 'views/jobdetailsV2.html',
            controller: 'JobDetailsCtrlr',
            //cache: false,
            params: {id: null, showAddActivity: false}
        }).state('app.newjob', {
            url: '/newjob',
            templateUrl: 'views/newjob.html',
            controller: 'NewJobCtrlrV2'
        }).state('app.generalinfo', {
            url: '/generalinfo/{id}',
            templateUrl: 'views/generalinfoV2.html',
            controller: 'GeneralInfoCtrlr'
        }).state('app.jobinfo', {
            url: '/jobinfo/{id}',
            templateUrl: 'views/jobinfo.html',
            controller: 'JobInfoCtrlr'
        }).state('app.generalnotes', {
            url: '/generalnotes/{id}',
            templateUrl: 'views/generalnotesV2.html',
            controller: 'GeneralNotesCtrlr'
        }).state('app.parties', {
            url: '/parties/{id}',
            templateUrl: 'views/partiesV2.html',
            controller: 'PartiesCtrlr'
        }).state('app.team', {
            url: '/team/{id}',
            templateUrl: 'views/teamV3.html',
            controller: 'TeamCtrlr',
            params: {isJobArchived: false}
        }).state('teammember', {
            url: '/teammember',
            templateUrl: 'views/teammember.html',
            controller: 'TeamMemberCtrlr'
        }).state('newteammember', {
            url: '/newteammember',
            templateUrl: 'views/newteammember.html',
            controller: 'NewTeamMemberCtrlr'
        }).state('app.teammemberlog', {
            url: '/jobs/{id}/teammemberlog/{teamMemberId}',
            templateUrl: 'views/teammemberlogV2.html',
            controller: 'TeamMemberLogCtrlr',
            params: {isJobArchived: false}
        }).state('app.staff', {
            url: '/staff',
            templateUrl: 'views/managestaffV2.html',
            controller: 'ManageStaffCtrlr'
        }).state('app.managestaff', {
            url: '/managestaff',
            templateUrl: 'views/managestaff.html',
            controller: 'ManageStaffCtrlr'
        }).state('app.staffform', {
            url: '/staffform',
            templateUrl: 'views/staffform.html',
            controller: 'StaffFormCtrlr',
            params: { mode: null }
        }).state('app.usersettings', {
            url: '/usersettings',
            templateUrl: 'views/usersettings.html',
            controller: 'UserSettingsCtrlr'
        }).state('tsls', {
            url: '/tsls',
            templateUrl: 'views/tsls.html',
            controller: 'TSLSCtrlr'
        }).state('actprestartprep', {
            url: '/actprestartprep',
            templateUrl: 'views/act-prestartprep.html',
            controller: 'ActPrestartPrepCtrlr'
        }).state('app.jobactivity', {
            url: '/jobs/{jobid}/jobactivity/{id}',
            templateUrl: 'views/jobactivity.html',
            controller: 'JobActivityCtrlr',
            params:{jobName: null, isJobArchived: false}
        }).state('acttraveltodepot', {
            url: '/acttraveltodepot',
            templateUrl: 'views/act-traveltodepot.html',
            controller: 'ActTravelToDepotCtrlr'
        }).state('acttraveltonextjob', {
            url: '/acttraveltonextjob',
            templateUrl: 'views/act-traveltonextjob.html',
            controller: 'ActTravelToNextJobCtrlr'
        }).state('app.sitehazardassessment', {
            url: '/jobs/{jobid}/sitehazardassessment/{id}',
            templateUrl: 'views/sitehazardassessment.html',
            controller: 'SiteHazardAssessmentCtrlr',
            params: {isJobArchived: false}
        }).state('app.siteactivity', {
            url: '/jobs/{jobid}/siteactivity/{id}',
            templateUrl: 'views/siteactivity.html',
            controller: 'SiteActivityCtrlr',
            params: { jobName: null, isJobArchived: false }              
        }).state('app.toolBoxBriefingRecord', {
            url: '/jobs/{jobid}/toolBoxBriefingRecord/{id}',
            templateUrl: 'views/choosetoolboxbriefing.html',
            controller: 'ToolBoxBriefingRecordCtrlr',
            params: { jobName: null, isJobArchived: false }
        }).state('app.tmponsiteassessment', {
            url: '/jobs/{jobid}/tmponsiteassessment/{id}',
            templateUrl: 'views/tmponsiteassessment.html',
            controller: 'TMPOnSiteAssessmentCtrlr',
            cache: false,
            params: {jobName: null}
        }).state('actsiteamendment', {
            url: '/actsiteamendment',
            templateUrl: 'views/act-siteamendment.html',
            controller: 'ActSiteAmendmentCtrlr'
        }).state('actsiteretrieval', {
            url: '/actsiteretrieval',
            templateUrl: 'views/act-siteretrieval.html',
            controller: 'ActSiteRetrievalCtrlr'
        }).state('app.onsiterecord', {
            url: '/jobs/{jobid}/onsiterecord/{id}',
            templateUrl: 'views/onsiterecord.html',
            controller: 'OnSiteRecordCtrlr',
            params:{jobName: null, isJobArchived: false}
        }).state('app.gearleftonositerecord', {
            url: '/jobs/{jobid}/gearleftonsiterecord/{id}',
            templateUrl: 'views/gearleftonsiterecord.html',
            controller: 'GearLeftOnSiteRecordCtrlr',
            params:{jobName: null, isJobArchived: false}
        }).state('app.onsiterecorditem', {
            url: '/jobs/{jobid}/onsiterecord/{id}/onsiterecorditem',
            templateUrl: 'views/onsiterecorditem.html',
            controller: 'OnSiteRecordItemCtrlr',
            params: { runGetPhoto: false, jobName: null, itemIndex: null, isJobArchived: false, isComplete: false }
        }).state('actwaiting', {
            url: '/actwaiting',
            templateUrl: 'views/act-waiting.html',
            controller: 'ActWaitingCtrlr'
        }).state('actworkspaceactive', {
            url: '/actworkspaceactive',
            templateUrl: 'views/act-workspaceactive.html',
            controller: 'ActWorkSpaceActiveCtrlr'
        }).state('acttruckaudit', {
            url: '/acttruckaudit',
            templateUrl: 'views/act-truckaudit.html',
            controller: 'ActTruckAuditCtrlr'
        });
    }])
    .config(['$httpProvider', function ($httpProvider) {
        $httpProvider.interceptors.push(function ($q, $rootScope) {
            return {
                'responseError': function (error) {
                    //console.log('response error interceptor');
                    //console.log(error);
                    if (error.status == 401) {
                        $rootScope.$broadcast('unathorizedaccess', { mode: 'relogin' });
                    }
                    return $q.reject(error);
                }
            };
        });
    }])
.config(['$provide', function ($provide) {
    $provide.decorator("$q", function ($delegate) {
        function seqAll(promises) {
            //create a defer, which will return promise with setting resolve/reject
            var deferred = $delegate.defer();
            //create an array of results to store results of promises
            var results = [];
            var j = 0;
            recursive(promises[j]);
            //create a recursive function, which loops through all the promises one after the another
            function recursive(promise) {
                j++;
                promise.then(function (data) {
                    // when success, push the data to results & again go for next promise else set defer resovle to array of data collected
                    results.push(data);
                    if (j < promises.length) {
                        recursive(promises[j]);
                    } else {
                        deferred.resolve(results);
                    }
                }, function (error) {
                    // If promise got failed reject it & return from recursive loop of promises
                    deferred.reject('promises[' + (j - 1) + ']' + ' rejected with status: ' + error.status);
                    return;
                });
            }
            return deferred.promise;
        }
        $delegate.seqAll = seqAll;
        return $delegate;
    })
}])
.run(['$ionicPlatform', '$state', '$rootScope', '$filter', 'DBSvc', 'AccountSvc', 'WebApiQSvc', 'JobsSvc', '$ionicLoading', 'TrucksSvc', 'SyncService',
 function ($ionicPlatform, $state, $rootScope, $filter, DBSvc, AccountSvc, WebApiQSvc, JobsSvc, $ionicLoading, TrucksSvc, SyncService) {
    //$ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>', hideOnStateChange: true });    
    $ionicPlatform.ready(function () {
        /*
        $ionicPlatform.registerBackButtonAction(function (event) {
            console.log('[212]:');
            console.log(event);
            event.preventDefault();
            event.stopPropagation();
        }, 101);
        */
        
         if (window.cordova) {
                    cordova.plugins.backgroundMode.setDefaults({ title: 'Site Pro', text: 'Site Pro is running in the background.' });
                    cordova.plugins.backgroundMode.enable();
         }
        /*
        if (window.cordova && window.cordova.plugins.Keyboard) {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            window.cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            // Don't remove this line unless you know what you are doing. It stops the viewport
            // from snapping when text inputs are focused. Ionic handles this internally for
            // a much nicer keyboard experience.
            window.cordova.plugins.Keyboard.disableScroll(true);
        }
        if (window.StatusBar) {
            window.StatusBar.styleDefault();
            window.StatusBar.show();
            window.StatusBar.overlaysWebView(true);
        }
        */
        $rootScope.ShowActivity = function (id) {
            switch (id) {
                case 1:
                    return 'Travel to Site';
                case 2:
                    return 'Site Hazard Assessment';
                case 3:
                    return 'Site Deployment';
                case 4:

                    break;
                case 5:
                    return 'Site Retrieval';
                case 6:

                    break;
                case 7:
                    return 'Site Check';
                case 8:

                    break;
                case 9:
                    return 'Travel to Depot';
            }
        };

        document.addEventListener("online", function(){
             $rootScope.$broadcast('online');
        }, false);
        document.addEventListener("offline", function(){
            $rootScope.$broadcast('offline');
        }, false);
        
        DBSvc.initPouchDB().then(function(response){
            $state.go('app');
        });
    });    
}]);
mainApp.controller('CrossCheckToggleCtrlr', ['$scope', function ($scope) {
    console.log($scope.checked);
    $scope.toggle = function (value) {
        $scope.checked = value;
    };
}]);
// mainApp.factory('$exceptionHandler',['$log', function($log){
//     return function myErrorHandler(error, cause){
//         console.log(error);
//         console.log(cause);
//     };
// }]);
mainApp.factory('TSLModal', ['ModalSvc', function (ModalSvc) {
    var service = { showTSL: showTSL };
    return service;
    
    function showTSL() {
        return ModalSvc.show('views/addtsl.html', 'AddTSLCtrlr as vm', {});
    }
}]);
mainApp.factory('ModalFactory', ['ModalSvc', function (ModalSvc) {
    var service = {
        showClosureTypes: showClosureTypes,
        showTSL: showTSL,
        showTrucksSelection: showTrucksSelection,
        showTransferJobView: showTransferJobView,
        showChooseTeamMembers: chooseTeamMembers,
        newTeamMember: newTeamMember,
        chooseActivity: chooseActivity,
        selectAgencyStaff: selectAgencyStaff,
        chooseTruck: chooseTruck,
        ToolBoxBrief: ToolBoxBrief
    };
    return service;

    function ToolBoxBrief(addedClosureTypes, closureTypesOption) {
        return ModalSvc.show('views/choosetoolboxbriefing.html','ChooseToolboxbriefCtrlr as vm', { addedClosureTypes: addedClosureTypes, closureTypesOption: closureTypesOption });
    }
    function showClosureTypes(addedClosureTypes, closureTypesOption) {
        return ModalSvc.show('views/chooseclosuretype.html', 'ChooseClosureTypeCtrlr as vm', { addedClosureTypes: addedClosureTypes , closureTypesOption: closureTypesOption } );
    }
    function showTSL(isFirstTsl, tsl) {
        return ModalSvc.show('views/addtsl.html', 'AddTSLCtrlr as vm', {isFirstTsl: isFirstTsl, tsl: tsl});
    }
    function showTrucksSelection(trucks) {
      return ModalSvc.show('views/choose-trucks.html', 'ChooseTrucksCtrlr as vm', { selectedTrucks: trucks }, {hardwareBackButtonClose:true});
    }
    function showTransferJobView(job, branches) {
        return ModalSvc.show('views/transfer-job.html', 'TransferJobCtrlr as vm', { job: job, branches: branches });
    }
    function chooseTeamMembers(currentMembers){
        return ModalSvc.show('views/chooseteammembers.html', 'ChooseTeamMembersCtrlr as vm', { currentMembers: currentMembers });
    }
    function newTeamMember(){
        return ModalSvc.show('views/newteammemberV2.html', 'NewTeamMemberCtrlr as vm', {});
    }
    function chooseActivity(activityStates, teamStarted){
        return ModalSvc.show('views/chooseactivity.html', 'ChooseActivityCtrlr as vm', {activityStates: activityStates, teamStarted: teamStarted});
    }
    function selectAgencyStaff(currentTeamMembers){
        return ModalSvc.show('views/selectAgencyStaff.html', 'SelectAgencyStaffCtrl as vm', { currentTeamMembers: currentTeamMembers});
    }
    function chooseTruck(){
        return ModalSvc.show('views/choose-truck.html', 'ChooseTruckCtrlr as vm', {}, {hardwareBackButtonClose: true});
    }
}]);
mainApp.factory('ModalSvc', ['$ionicModal', '$rootScope', '$q', '$injector', '$controller', '$ionicPlatform', function ($ionicModal, $rootScope, $q, $injector, $controller, $ionicPlatform) {
    return { show: show };

    function show(templateUrl, controller, parameters, options) {
        var defer = $q.defer(), ctrlInstance, modalScope = $rootScope.$new(), thisScopeId = modalScope.$id, 
        defaultOptions = {
            animation: 'slide-in-up',
            focusFirstInput: false,
            backdropClickToClose: true,
            hardwareBackButtonClose: false,
            modalCallback: null
        };

        options = angular.extend({}, defaultOptions, options);
        console.log('[344]: ' + options);
        $ionicModal.fromTemplateUrl(templateUrl, {
            scope: modalScope,
            animation: options.animation,
            focusFirstInput: options.focusFirstInput,
            backdropClickToClose: options.backdropClickToClose,
            hardwareBackButtonClose: options.hardwareBackButtonClose
        }).then(function (modal) {
            $ionicPlatform.onHardwareBackButton(function(){
                console.log('[353]:');
            });
            modalScope.modal = modal;
            modalScope.openModal = function () {
                modalScope.modal.show();
            };
            modalScope.closeModal = function (result, closureTypesOption) {
                defer.resolve(result, closureTypesOption);
                modalScope.modal.hide();
            };
            modalScope.$on('modal.hidden', function (thisModal) {
                if(thisModal.currentScope){
                    var modalScopeId = thisModal.currentScope.$id;
                    if (thisScopeId == modalScopeId) {
                        defer.resolve(null);
                        _cleanup(thisModal.currentScope);
                    }
                }   
            });

            var locals = {'$scope': modalScope, 'parameters': parameters};
            var ctrlEval = _evalController(controller);
            ctrlInstance = $controller(controller, locals);
            if(ctrlEval.isControllerAs){
                ctrlInstance.openModal = modalScope.openModal;
                ctrlInstance.closeModal = modalScope.closeModal;
            }
            modalScope.modal.show().then(function () {
                modalScope.$broadcast('modal.afterShow', modalScope.modal);
            });
            if (angular.isFunction(options.modalCallback)) {
                options.modalCallback(modal);
            }
        }, function (error) {
            defer.reject(error);
        });

        return defer.promise;
    }

    function _cleanup(scope) {
        scope.$destroy();
        if(scope.modal)
            scope.modal.remove();
    }

    function _evalController(ctrlName) {
        var result = { isControllerAs: false, controllerName: '', propName: '' };
        var fragments = (ctrlName || '').trim().split(/\s+/);
        result.isControllerAs = fragments.length === 3 && (fragments[1] || '').toLowerCase() === 'as';
        if(result.isControllerAs){
            result.controllerName = fragments[0];
            result.propName = fragments[2];
        }
        else
            result.controllerName = ctrlName;

        return result;
    }
}]);
mainApp.filter('correctImgSrc', function(){
    return function(imgSrc){         
        return window.WkWebView.convertFilePath(imgSrc);        
    }
});