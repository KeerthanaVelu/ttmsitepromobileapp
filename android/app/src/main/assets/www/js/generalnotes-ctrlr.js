﻿mainApp.controller('GeneralNotesCtrlr', ['$scope', '$state', '$ionicPopup', '$ionicPlatform', '$cordovaCamera', '$cordovaFile', '$cordovaCapture', '$ionicLoading', 'JobsSvc', '$window', '$q', '$linq', 'VoiceToTextSvc', 'PhotoSvc', function ($scope, $state, $ionicPopup, $ionicPlatform, $cordovaCamera, $cordovaFile, $cordovaCapture, $ionicLoading, JobsSvc, $window, $q, $linq, VoiceToTextSvc, PhotoSvc) {
    $scope.fields = JobsSvc.job.generalNotes || { photos: [], audioNotes: [] };
    $scope.basePhotos = angular.copy($scope.fields.photos);
    $scope.baseAudioNotes = angular.copy($scope.fields.audioNotes);
    $scope.baseNotes = $scope.fields.notes;
    $scope.voiceToText = function () {
        console.log('voiceToText()');
        VoiceToTextSvc.Capture().then(function (result) {
            console.log('voiceToText() success');
            console.log(result);
            if ($scope.fields.notes == null)
                $scope.fields.notes = '';
            if ($scope.fields.notes.length > 0)
                $scope.fields.notes += ' ' + result[0];
            else
                $scope.fields.notes += result[0];
        }, function (error) {
            console.log('voiceToText() error');
            console.log(error);
            alert(error);
        });                
    };
    $scope.capturePhoto = function () {   
        PhotoSvc.getPhoto().then(function (photo) {
            console.log(photo);
            $scope.fields.photos.push(photo);
        }, function(error){
            alert('Something went wrong while getting a photo.');
        });        
    };
    $scope.captureAudio = function () {
        if (!$scope.fields.audioNotes)
            $scope.fields.audioNotes = [];

        var options = { limit: 1 };
        navigator.device.capture.captureAudio(function (audioData) {
            // Success! Audio data is here
            var audioPath = audioData[0].fullPath;
            var directory = getDirectory(audioPath);
            var sourceFile = audioData[0].name;
            var newFilename = getRandomFilename();
            if ($scope.fields.audioNotes.length >= 0) {
                newFilename = $scope.fields.audioNotes.length.toString() + newFilename + sourceFile.slice(sourceFile.lastIndexOf('.'));
            }
            //$cordovaFile.moveFile(directory, sourceFile, cordova.file.dataDirectory, newFilename).then(function (result) {
            $cordovaFile.copyFile(directory, sourceFile, cordova.file.dataDirectory, newFilename).then(function (result) {
                var notes = { path: result.nativeURL, created: new Date() };
                $scope.fields.audioNotes.push(notes);
            }, function (error) {
                console.log(error);
                alert(getFileError(error));
            });
        }, function (err) {
            console.log(err);
            alert(getMediaCaptureError(err));
        }, options);
    };
    /*
    $scope.captureAudio = function () {
        if(!$scope.fields.audioNotes)
            $scope.fields.audioNotes = [];

        var options = { limit: 1 };
        //navigator.device.capture.captureAudio(options).then(function (audioData) {
        $cordovaCapture.captureAudio(options).then(function (audioData) {
            // Success! Audio data is here
            var audioPath = audioData[0].fullPath;            
            var directory = getDirectory(audioPath);
            var sourceFile = audioData[0].name;
            var newFilename = getRandomFilename();
            if ($scope.fields.audioNotes.length >= 0) {
                newFilename = $scope.fields.audioNotes.length.toString() + newFilename + sourceFile.slice(sourceFile.lastIndexOf('.'));
            }            
            //$cordovaFile.moveFile(directory, sourceFile, cordova.file.dataDirectory, newFilename).then(function (result) {
            $cordovaFile.copyFile(directory, sourceFile, cordova.file.dataDirectory, newFilename).then(function (result) {
                var notes = { path: result.nativeURL, created: new Date() };
                $scope.fields.audioNotes.push(notes);
            }, function (error) {
                console.log(error);
                alert(getFileError(error));
            });            
        }, function (err) {            
            console.log(err);
            alert(getMediaCaptureError(err));
        });
    };
    */
    $scope.back = function () {
        if (!angular.equals($scope.basePhotos, $scope.fields.photos) || !angular.equals($scope.baseAudioNotes, $scope.fields.audioNotes) || ($scope.fields.notes != null && $scope.fields.notes != $scope.baseNotes)) {
            var forUpload = [];
            var basePhotos = $linq.Enumerable().From($scope.basePhotos);
            var baseAudioNotes = $linq.Enumerable().From($scope.baseAudioNotes);
            for (var i = 0; i < $scope.fields.photos.length; i++) {
                if (basePhotos.FirstOrDefault(null, 'f => f.fileName == "' + $scope.fields.photos[i].fileName + '"') == null)
                    forUpload.push($scope.fields.photos[i]);
            }
            for (var i = 0; i < $scope.fields.audioNotes.length; i++) {
                if (baseAudioNotes.FirstOrDefault(null, 'f => f.fileName == "' + $scope.fields.audioNotes[i].fileName + '"') == null)
                    forUpload.push($scope.fields.audioNotes[i]);
            }

            var filesQ = [];
            if (forUpload.length > 0) {                
                for (var i = 0; i < forUpload.length; i++) {
                    filesQ.push(JobsSvc.addToQ({ id: (new $window.ObjectId()).toString(), isFileUpload: true, path: forUpload[i].path }));
                }                
            }
            $q.all(filesQ).then(function (results) {
                $scope.fields.jobId = JobsSvc.job.id;
                $scope.fields.isGeneralNotes = true;
                JobsSvc.updateGeneralNotes($scope.fields);
                $state.go('jobdetails');
            });
        }
        else
            $state.go('jobdetails');
    };
    $scope.removePhoto = function (index) {
        $scope.fields.photos.splice(index, 1);
    };
    var media = null;
    $scope.audioNotesPlaying = -1;
    $scope.playAudioNotes = function (path, index) {
        var subPath = path.replace('file://', '');
        if (media == null) {
            media = new Media(subPath, function () {
            }, function (error) {
                switch (error) {
                    case MediaError.MEDIA_ERR_ABORTED:
                        alert('MediaError.MEDIA_ERR_ABORTED');
                        break;
                    case MediaError.MEDIA_ERR_NETWORK:
                        alert('MediaError.MEDIA_ERR_NETWORK');
                        break;
                    case MediaError.MEDIA_ERR_DECODE:
                        alert('MediaError.MEDIA_ERR_DECODE');
                        break;
                    case MediaError.MEDIA_ERR_NONE_SUPPORTED:
                        alert('MediaError.MEDIA_ERR_NONE_SUPPORTED');
                        break;
                }
            }, function (mediaStatus) {
                switch (mediaStatus) {
                    case Media.MEDIA_NONE:
                        //alert('Media.MEDIA_NONE');
                        break;
                    case Media.MEDIA_STARTING:
                        //alert('Media.MEDIA_STARTING');
                        break;
                    case Media.MEDIA_RUNNING:
                        //mediaStatus.MEDIA_RUNNING
                        $scope.audioNotesPlaying = index;
                        $scope.$apply();
                        break;
                    case Media.MEDIA_PAUSED:
                        break;
                    case Media.MEDIA_STOPPED:
                        if (media != null) {
                            media.release();
                            media = null;
                        }
                        //media = null;
                        $scope.audioNotesPlaying = -1;
                        $scope.$apply();
                        break;
                }
            });

            media.play();
        }
        else {
            media.stop();
            media.release();
            media = null;
            $scope.AudioNotesPlaying = -1;
        }
    };
    $scope.removeAudioNotes = function (audioPath, index) {
        var audioFileName = audioPath.substr(audioPath.lastIndexOf('/') + 1);
        var confirmPopup = $ionicPopup.confirm({
            title: 'Site Pro',
            template: 'Do you want to remove this voice notes?<br/><strong>' + audioFileName + '</strong>'
        });
        confirmPopup.then(function (res) {
            if (res) {
                $ionicLoading.show({ template: "Removing..." });
                window.resolveLocalFileSystemURL(audioPath, function (fileEntry) {
                    fileEntry.remove(function (result) {
                        $scope.fields.audioNotes.splice(index, 1);
                        //$scope.Item.AudioNotes.splice(index, 1);
                        $ionicLoading.hide();
                    });
                }, function (error) {
                    console.log(error);
                    console.log(getFileError(error));
                    $scope.fields.audioNotes.splice(index, 1);
                    //alert("An error occured while removing the file. Please try again.");
                    $ionicLoading.hide();
                });
            }
        });
    };
}]);