﻿mainApp.controller('ClosureTypeCtrlr', ['$scope', '$state', '$ionicPopup', 'ClosureTypeSvc', 'JobsSvc', 'parameters', function ($scope, $state, $ionicPopup, ClosureTypeSvc, JobsSvc, parameters) {
    $scope.$on('modal.shown', function(){
        //alert('ClosureTypeCtrlr: modal.shown');
        //if ($scope.fields.forSiteRetrieval) {
        //    $scope.ClosureTypes.splice(3, 1);
        //    $scope.ClosureTypes.splice(3, 1);
        //}        
    });
    $scope.ClosureTypes = ClosureTypeSvc.GetAll();
    $scope.IsOtherType = false;
    $scope.OtherType = { Selected: false, Value: null };
    $scope.addedClosureTypes = angular.copy(parameters);
    $scope.DoneClosureTypes = function () {                
        if ($scope.addedClosureTypes.length == 0 && !$scope.OtherType.Selected) {
            $ionicPopup.alert({ title: "Closure Type", template: 'Please choose closure type(s).' });
            return;
        }        

        if ($scope.OtherType.Selected && ($scope.OtherType.Value == null || $scope.OtherType.Value.length == 0)) {
            $ionicPopup.alert({ title: "Closure Type", template: 'Please specify other type.' });
            return;
        }
        if ($scope.OtherType.Value != null && $scope.OtherType.Value.length > 0) {
            $scope.addedClosureTypes.push($scope.OtherType.Value);
        }
        $scope.closeModal($scope.addedClosureTypes);
        //$scope.$emit('closuretypedone', { forSiteRetrieval: true });
        
    };
    $scope.ToggleClosure = function (closure) {
        if ($scope.addedClosureTypes.indexOf(closure) >= 0)
            $scope.addedClosureTypes.splice($scope.ClosureTypes.indexOf(closure), 1);
        else
            $scope.addedClosureTypes.push(closure);
    };
    $scope.CancelClosureTypes = function () {
        //$scope.$emit('closuretypecancelled', {});
        $scope.addedClosureTypes = 
        $scope.closeModal(null);
    };
    $scope.SetTitle = function () {
        switch (JobsSvc.job.CurrentActivity.activityId) {
            case 2:
            case 5:
            case 7:
                return "Active Closure Type";
                break;
            case 8:
            case 8.1:
                return "Unattended Closure Type";
                break;
        }        
    };
    //$scope.ClosureUnattended = $scope.fields.forSiteRetrieval ? 'Unattended ' : '';
}]);