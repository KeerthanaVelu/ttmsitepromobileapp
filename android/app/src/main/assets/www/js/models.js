﻿var dbVersion = 1;
var Job = function () {
    this.id = null;    
    this.jobName = null;
    this.reference = null;
    this.description = null;
    this.address = null;
    this.parties = { rcaId: null, contractorId: null, contractorBranchId: null, projectManagerId: null, assetOwnerId: null };
    this.activities = [];
    this.teamMembers = [];
    //this.team = [];
    this.isJob = true;
    this.createdOn = null;
    this.modifiedOn = null;
    this.generalNotes = { notes: null, photos: [], audioNotes: [] };
    this.siteRecordPDFLog = [];
};
//official
var NewJobVM = {
    id: null,
    rId: null,
    //tempId: null,
    tenantId: null,
    branchId: null,
    assignedUserId: null,
    //isJob: true,
    jobName: null,
    reference: null,
    tmpName: null,
    description: null,
    address: null,
    poNumber: null,
    parties: { rcaId: null, contractorId: null, contractorBranchId: null, projectManagerId: null, assetOwnerId: null, onsiteContact: null, onsiteContactNum: null },
    trucks: [],
    teamMembers: [],
    roadLevels: [],
    activities: [],
    generalNotes: { notes: null, photos: [], audioNotes: [] },
    siteRecordPDFLog: []
    //isServerSynced: false
};
/* version 1
var siteCheckItems = [
    { id: 1, itemName: 'Sign Position / Visibility / Upright', checked: false, checkedWithIssues: false, photos: [], notes: '' },
    { id: 2, itemName: 'Sign / Delineation Condition', checked: false, checkedWithIssues: false, photos: [], notes: '' },
    { id: 3, itemName: 'Delineation Position / Spacing / Tapers', checked: false, checkedWithIssues: false, photos: [], notes: '' },
    { id: 4, itemName: 'Vests / Hardhats / Safety Glasses Used & Correct', checked: false, checkedWithIssues: false, photos: [], notes: '' },
    { id: 5, itemName: 'Pedestrians? Cyclists? (width(m))', checked: false, checkedWithIssues: false, photos: [], notes: '' },
    { id: 6, itemName: 'Property Access', checked: false, checkedWithIssues: false, photos: [], notes: '' },
    { id: 7, itemName: 'Safety Zones Clear', checked: false, checkedWithIssues: false, photos: [], notes: '' },
    { id: 8, itemName: 'Arrow Board & Beacon Lights', checked: false, checkedWithIssues: false, photos: [], notes: '' },
    { id: 9, itemName: 'Traffic Flow', checked: false, checkedWithIssues: false, photos: [], notes: '' }
];
*/
//version 2
var siteCheckItems = [
    { id: 1, itemName: 'High-visibility garment worn by all?', checked: false, checkedWithIssues: false, photos: [], notes: '' },
    { id: 2, itemName: 'Signs positioned as per TMP?', checked: false, checkedWithIssues: false, photos: [], notes: '' },
    { id: 3, itemName: 'Conflicting signs covered?', checked: false, checkedWithIssues: false, photos: [], notes: '' },
    { id: 4, itemName: 'Correct delineation as per TMP?', checked: false, checkedWithIssues: false, photos: [], notes: '' },
    { id: 5, itemName: 'Lane widths appropriate?', checked: false, checkedWithIssues: false, photos: [], notes: '' },
    { id: 6, itemName: 'Appropriate positive TTM used?', checked: false, checkedWithIssues: false, photos: [], notes: '' },
    { id: 7, itemName: 'Footpath standards met?', checked: false, checkedWithIssues: false, photos: [], notes: '' },
    { id: 8, itemName: 'Cycle lane standards met?', checked: false, checkedWithIssues: false, photos: [], notes: '' },
    { id: 9, itemName: 'Traffic flows OK?', checked: false, checkedWithIssues: false, photos: [], notes: '' },
    { id: 10, itemName: 'Adequate property access?', checked: false, checkedWithIssues: false, photos: [], notes: '' },
    { id: 11, itemName: 'Barrier deflection area is clear? (Refer to Barrier design statement)', checked: false, checkedWithIssues: false, photos: [], notes: '' },
    { id: 12, itemName: 'Add others as required', checked: false, checkedWithIssues: false, photos: [], notes: '' }
];

var Branch = function () {
    this.Id = null;
    this.Branch = null;
    this.Staff = [];
};
var Staff = {
    id: null,
    tenantId: null,
    branchId: null,    
    fullName: null,
    phone: null,
    qualification: null,
    notes: null,
    photo: null
};
// var Staff = function () {
//     this.id = null;
//     this.branchId = null;
//     this.fullName = null;
//     this.phone = null;
//     this.qualification = null;
//     this.notes = null;
//     this.photo = null;
//     this.active = false;
//     this.archived = false;
// };

var TimeLogType = ["Start Time", "Break Start Time", "Break End Time", "End Time"];
var TimeLogTypeConst = { StartTime: {value: 0, name: "Start Time"}, BreakStartTime: {value: 1, name: "Break Start Time"},
     BreakEndTime: {value: 2, name: "Break End Time"}, EndTime: {value: 3, name: "End Time"} };
let TimeLogTypeEnum = {
    STARTTIME: 0,
    BREAKTIMESTART: 1,
    BREAKTIMEEND: 2,
    ENDTIME: 3,
    props: {
         0: {name: 'Start Time'},
         1: {name: 'Break Time Start'},
         2: {name: 'Break Time End'},
         3: {name: 'End Time'},
    }
};
let DataActionEnum = { CREATE: 0, UPDATE: 1, DELETE: 2 };
let ActivityActionEnum = { START: 0, STOP: 1, UPDATE: 2 };
/*
Travel to Site
Site Deployment
Site Hazard Assessment
Waiting
Work Space Active - Concurrent
Site Check - multiple
Site Amendment - multiple
Site Retrieval
Travel to Depot
*/
var JobActivities = [ 
    /*{ Id: 14, Activity: 'Start Shift' },*/ { Id: 10, Activity: 'Pre-Start Truck Check' }, { Id: 1, Activity: 'Travel to Site' }, /*{Id: 16, Activity: 'TMP On-Site Assessment' } ON-HOLD FOR A FEW WEEKS,*/ /*{ Id: 3, Activity: 'Site Hazard Assessment' },*/
    { Id: 2, Activity: 'Site Deployment' }, { Id: 6, Activity: 'On-Site Record' }, /*{Id: 15, Activity: 'Short Form Audit'},*/ /*{ Id: 5, Activity: 'Work Space Active' Removed it as Andrew asked to remove it },*/ { Id: 7, Activity: 'Site Amendment Record' },
    { Id: 8, Activity: 'Site Retrieval Record' }, { Id: 12, Activity: 'Travel to Next Job' }, { Id: 9, Activity: 'Travel to Depot' }, { Id: 17, Activity: 'Truck Pre-check Record' }, { Id: 18, Activity: 'ToolBox Briefing Record' }, { Id: 19, Activity: 'Gear Left On Site Record' }, { Id: 13, Activity: 'End of Shift' }
];
let ActivityTypeEnum = {
    PreStartTruckCheck: 10,TruckPrecheckRecord: 17, TravelToSite: 1, TMPOnSiteAssessment: 16, SiteHazardAssessment: 3,
    SiteDeployment: 2, ToolBoxBriefingRecord: 18, GearLeftOnSiteRecord: 19, OnSiteRecord: 6/*Site Check*/, ShortFormAudit: 15, WorkSpaceActive: 5, SiteAmendmentRecord: 7,
    SiteRetrievalRecord: 8, TravelToNextJob: 12, TravelToDepot: 9, EndOfShift: 13, StartOfShift: 14, StartOfShift: 14, ManageTeam: -1,
    props: {
        14: { name: 'Start of Shift' }, 10: { name: 'Pre-Start Truck Check' }, 17: { name: 'Truck Pre-check Record' },1: { name: 'Travel to Site' }, 16: { name: 'TMP On-Site Assessment' }, 3: { name: 'Site Hazard Assessment' },
        2: { name: 'Site Deployment' }, /*18: { name: 'ToolBox Briefing Record' },*/ 6: { name: 'On-Site Record' }/*Site Check*/, 15: { name: 'Short Form Audit' }, 5: { name: 'Work Space Active' },
        7: { name: 'Site Amendment Record' }, 8: { name: 'Site Retrieval Record' }, 12: { name: 'Travel to Next Job' },
        9: { name: 'Travel to Depot' }, 19: { name: 'Gear Left On Site Record' },  13: { name: 'End of Shift' }
    }
};

var ClosureTypes = ['Berm', 'Bi-directional Contraflow', 'Cycle Lane', 'Detour', 'Footpath', 'Lane Drop', 'Manually Controlled Alternating Flow', 'Manually Controlled Road Closure (eg. Tree Felling)', 'Rolling Block', 'Semi-Static', 'Shoulder', 'Traffic Signal Controlled Alternating'];

let TeamMemberVM = {
    id: null,
    rId: null,
    staffId: null,
    jobId: null,
    rJobId: null,
    tenantId: null,
    endOfShiftTime: null,
    timeLogs: []
};

let TeamMemberDto = { staffId: null, tenantId: null };

let TimeLogVM = { id: null, teamMemberId: null, rTeamMemberId: null, jobId: null, rJobId: null, timeLogType: 0, time: new Date(), duration: 0, isPaid: false };

var activityMode = { start: 0, stop: 1 };

//1(TravelToSite),9(TravelToDepot),12(TravelToNextJob),13(EndOfShift)
let JobActivityVM = {
    endCoordinates: null,
    startCoordinates: null,
    unAttended: false,
    id: null,
    rId: null,
    jobId: null,
    activityType: null,
    startTime: null,
    endTime: null,
    complete: false,
    truckNumber: null,
    isCancelled: false,
    notes: null,
    mode: 0
}
let TruckPrecheckRecordVM = {
    endCoordinates: null,
    startCoordinates: null,
    unAttended: false,
    id: null,
    rId: null,
    jobId: null,
    activityType: null,
    startTime: null,
    endTime: null,
    complete: false,
    truckNumber: null,
    isCancelled: false,
    notes: null,
    mode: 0,
    //truckReport: {},
    isCamera: false,
    isReverseCamera: false,
    HubExpiry: null,
    HuboReading: 0,
    CofExpiry: null,
    RegoExpiry: null,
    isAlarms: false,
    isAssistDevices: false,
    isEngineoilwater: false,
    isTyres: false,
    isFireExtinguisher: false,
    isFirstAidKit: false,
    isYardbroom: false,
    isPailofsoapywater_brush: false,
    isAmber: false,
    isAlllights: false,
    isTruckMirrors: false,
    isWheel: false,
    isWaterCoolerFull: false,
    isNotes: null
}
//site hazard assessment
let SiteHazardAssessmentVM = {
    endCoordinates: null,
    startCoordinates: null,
    unAttended: false,
    id: null,
    rId: null,
    jobId: null,
    activityType: null,
    startTime: null,
    endTime: null,
    complete: false,
    truckNumber: null,
    isCancelled: false,
    notes: null,
    mode: 0, //0=start,1=stop
    description: null
}
//2[SiteDeployment],5[WorkSpaceActive],7[SiteAmendment],8[SiteRetrieval]
let SiteActivityVM = {
    id: null,
    rId: null,
    jobId: null,
    activityType: null,
    startTime: null,
    endTime: null,
    endCoordinates: null,
    startCoordinates: null,
    isCancelled: false,
    notes: null, 
    unAttended: false,
    addedClosureTypes: [],
    tsls: []
}
let ToolBoxBriefVM = {
    id: null,
    rId: null,
    jobId: null,
    activityType: null,
    startTime: null,
    endTime: null,
    endCoordinates: null,
    startCoordinates: null,
    isCancelled: false,
    notes: null,
    unAttended: false,
    testcommunication: false,
    ppe: false,
    isfootpath: false,
    isExcessWeight: false,
    isStepOffTruck: false
}
//onsiterecord aka site check
let OnSiteRecordVM = {
    startCoordinates: null,
    id: null,
    rId: null,
    jobId: null,
    activityType: null,
    startTime: null,
    isCancelled: false,
    notes: null,
    siteCheckItems: []
};
//onsiterecord aka site check
let GearLeftOnSiteRecordVM = {
    startCoordinates: null,
    id: null,
    rId: null,
    jobId: null,
    activityType: null,
    startTime: null,
    isCancelled: false,
    notes: null,
    level1Items: [],
    level2Items: [],
    accessoriesItems: []
};

let RoadLevels = [{ name: 'LR', checked: false }, { name: 'LV', checked: false }, { name: 'L1', checked: false }, { name: 'L2', checked: false }, { name: 'L2LS', checked: false }, { name: 'L3', checked: false }];

let RUNMODEENUM = {local:0, test:1, prod:2};
let RUNMODE = RUNMODEENUM.prod;

//TMP On-Site Assessment
var AssessmentResult = {'good': '1', 'mitigated': '2', 'unsuitable': '3'};
var CheckpointResult = { empty: 1, good: 2, unsuitable: 3, mitigated: 4 };
var CheckingPoints = [
    {
        category: 'Road level',
        pointsToConsider: 'Does the TMP road level match the actual road level?',
        result: CheckpointResult.empty,
        comment: ''
    },
    {    
        category: 'Shape',
        pointsToConsider: 'Does the TMP Designs cater for',
        result: CheckpointResult.empty,
        comment: '',
        subpoints:[
            {
                pointsToConsider: 'all Intersections',
                result: CheckpointResult.empty
            },
            {
                pointsToConsider: 'all Vertical Curves (hills and dips)',
                result: CheckpointResult.empty
            },
            {
                pointsToConsider: 'all Horizontal Curves (corners)',
                result: CheckpointResult.empty
            },
            {
                pointsToConsider: 'Sufficient advance warning from each approach',
                result: CheckpointResult.empty
            }
        ]
    },
    {    
        category: 'Direction and protection',
        pointsToConsider: 'Direction and protection<br />Is there:',
        result: CheckpointResult.empty,
        comment: '',
        subpoints:[
            {
                pointsToConsider: 'sufficient length to place the planned direction and protection',
                result: CheckpointResult.empty
            },
            {
                pointsToConsider: 'sufficient [Lane Width(s)] left once the planned direction and protection is in place',
                uiText: 'sufficient [Lane Width(s)] left once the planned direction and protection is in place LINK THIS AS A POPUP TSL (Minimum Width) TABLE',
                result: CheckpointResult.empty
            },
            {
                pointsToConsider: "Is there adequate 'sign visibility' for all signs, thresholds and cone tapers?",
                result: CheckpointResult.empty
            },
            {
                pointsToConsider: 'sufficient room to accommodate the required positive traffic control',
                result: CheckpointResult.empty
            }
        ]
    },
    {    
        category: 'Proposed speed restrictions',
        pointsToConsider: '',
        result: CheckpointResult.empty,
        comment: '',
        subpoints:[
            {
                pointsToConsider: 'Is a TSL required?',
                uiText: 'Is a TSL required? LINK THIS TO TSL Matrix',
                result: CheckpointResult.empty
            },
            {
                pointsToConsider: 'Is there an approved TSL in the TMP that matches the Matrix Recommendation?',
                result: CheckpointResult.empty
            }
        ]
    },
    {    
        category: 'Plant and equipment',
        pointsToConsider: 'Will all the plant, equipment, and planned activity fit within the designated working space?',
        result: CheckpointResult.empty,
        comment: ''
    },
    {    
        category: 'Personal safety',
        pointsToConsider: '',
        result: CheckpointResult.empty,
        comment: '',
        subpoints:[
            {
                pointsToConsider: 'Are all workers able to carry out all their work within the designated working space, and if not, will they be covered by the rules for inspections?',
                result: CheckpointResult.empty
            },
            {
                pointsToConsider: 'If covered by the inspection rules, is there an inspection diagram for this in the TMP?',
                result: CheckpointResult.empty
            }
        ]
    },
    {    
        category: 'Layout diagrams',
        pointsToConsider: '',
        result: CheckpointResult.empty,
        comment: '',
        subpoints:[
            {
                pointsToConsider: 'Is the diagram referenced in the TMP?',
                result: CheckpointResult.empty
            },
            {
                pointsToConsider: 'Does the diagram(s) match the written section of the TMP?',
                result: CheckpointResult.empty
            }
        ]
    },
    {    
        category: 'RCA Notification',
        pointsToConsider: 'Has the RCA been notified?',
        result: CheckpointResult.empty,
        comment: ''
    }
];

let Assessment = {
    startTime: null,
    startCoordinates: null,
    endTime: null,
    endCoordinates: null,
    recipients: [],
    assessmentResult: null,
    createdOn: new Date(),
    details: {
        tmpReferenceNo: '',
        tmpStillCurrent: false,
        tmdNos: '',
        tmpExpiry: null,
        daysUntilTmpExpired: 0,
        location: '',
        rpFrom: '',
        rpTo: '',
        carNumber: '',
        carExpiry: null,
        daysUntilExpired: 0,
    },
    checkingPoints: CheckingPoints
}

/*
agency model >>
agencyAcroNym:'ag13'
agencyName:'Agency 20210313'
branchId:'57bb87b68f4ea736eca46189'
branchName:'Northland'
contactName:'Contact 1'
id:'604ceec48ac9d5215c96466f'
isArchived:false
phone:'23578235'
tenantId:'53d5bae2197e6b07bc66ad5d'

agency staff model >>
agencyAcroNym:null
agencyId:'604ceec48ac9d5215c96466f'
agencyName:null
fullName:'Jose Rizal'
id:'604ceee88ac9d5215c964675'
isArchived:false
mobile:'0649636'
notes:'Noli me Tangere'
qualification:'Hero'
staffType:'TC Class 1 driver'
*/