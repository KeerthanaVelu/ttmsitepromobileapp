﻿mainApp.controller('DummyCtrlr', ['$scope', '$state', 'JobsSvc', '$ionicPlatform', '$ionicModal', '$ionicLoading', '$linq', 'SiteCheckAlarmSvc', 'ModalFactory', 'TrucksSvc', 'TruckAuditSvc', '$ionicPopup', function ($scope, $state, JobsSvc, $ionicPlatform, $ionicModal, $ionicLoading, $linq, SiteCheckAlarmSvc, ModalFactory, TrucksSvc, TruckAuditSvc, $ionicPopup) {
    
    function getTruckNames(truckIds) {
        var truckNames = [];
        if (truckIds) {
            for (var i = 0; i < truckIds.length; i++) {
                var truck = TrucksSvc.getById($scope.job.trucks[i]);
                if (truck)
                    truckNames.push(truck.truckNumber + ' - ' + truck.truckRego);
            }
        }

        return truckNames;
    }
    
    $ionicModal.fromTemplateUrl('views/addactivityselection.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        $scope.modal = modal;
    });
    $scope.$on('$destroy', function () {
        $scope.modal.remove();
    });
    
    $scope.$on('$ionicView.beforeEnter', function () {
        //disabled the items(1/8/9) in the Activities selection modal
        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>' });
        //$scope.Activities = angular.copy(JobActivities);
        $scope.Activity = null;
        $scope.job = JobsSvc.job;
        $scope.activities = $linq.Enumerable().From($scope.job.activities).Where('w => w.cancelled != true').ToArray();
        $scope.truckNames = getTruckNames($scope.job.trucks);
        $ionicLoading.hide();
    });
    
    $scope.$on('$ionicView.loaded', function () {
        $scope.Activities = angular.copy(JobActivities);
        activities = $linq.Enumerable().From(JobsSvc.job.activities).Where('w => w.cancelled != true').ToArray();
        var siteDeployment = $linq.Enumerable().From(activities).FirstOrDefault(null, 'f => f.activityId == 2');
        var siteCheck = $linq.Enumerable().From(activities).FirstOrDefault(null, 'f => f.activityId == 6');
        if (siteDeployment != null) {
            if (siteCheck == null) {
                SiteCheckAlarmSvc.alarmNow();
                SiteCheckAlarmSvc.start();
            }
            else {
                var diff = moment().diff(moment(siteCheck.startTime), 'minutes');
                console.log('loaded: diff = ' + diff);
                if (diff >= 110) {
                    SiteCheckAlarmSvc.alarmNow();
                }
                SiteCheckAlarmSvc.start();
            }
        }
    });
    
    $scope.$on('$ionicView.enter', function () {
        if (JobsSvc.showAddActivity)
            $scope.AddActivitySelection();

        $ionicLoading.hide();
    });
    $scope.$on('unathorizedaccess', function (event, data) {
        $scope.CloseActivitySelection();
    });
    
    $scope.Activity = null;
    $scope.AddActivity = function (activity) {
    console.log("2");
        if (activity.locked)
            return;

        $scope.CloseActivitySelection();
        
        switch (activity.Id) {
            case 1:
                JobsSvc.job.activityId = 1;
                JobsSvc.job.CurrentActivity = { id: null, activityId: 1, startTime: null, endTime: null, complete: false, truckNumber: null, cancelled: false, notes: null };
                $state.go('acttraveltosite');
                break;
            case 2:
                JobsSvc.job.activityId = 2;
                JobsSvc.job.CurrentActivity = { id: null, activityId: 2, startTime: null, endTime: null, addedClosureTypes: [], tsls: [], complete: false, notes: null };
                $state.go('actsitedeployment');
                break;
            case 3:
                JobsSvc.job.activityId = 3;
                JobsSvc.job.CurrentActivity = { id: null, activityId: 3, description: null, startTime: null, endTime: null, addedClosureTypes: [], tsls: [], complete: false, notes: null };
                $state.go('actsitehazardassessment');
                break;
            case 4:
                JobsSvc.job.activityId = 4;
                JobsSvc.job.CurrentActivity = { id: null, activityId: 4, startTime: null, endTime: null, complete: false, notes: null };
                $state.go('actwaiting');
                break;
            case 5:
                JobsSvc.job.activityId = 5;
                JobsSvc.job.CurrentActivity = { id: null, activityId: 5, startTime: null, endTime: null, complete: false, notes: null };
                $state.go('actworkspaceactive');
                break;
            case 6:
                JobsSvc.job.activityId = 6;
                JobsSvc.job.CurrentActivity = { id: null, activityId: 6, complete: false, siteCheckItems: [], notes: null };
                angular.copy(siteCheckItems, JobsSvc.job.CurrentActivity.siteCheckItems);
                $state.go('actsitecheck');
                break;
            case 7:
                JobsSvc.job.activityId = 7;
                JobsSvc.job.CurrentActivity = { id: null, activityId: 7, startTime: null, endTime: null, addedClosureTypes: [], tsls: [], complete: false, notes: null };
                $state.go('actsiteamendment');
                break;
            case 8:
                JobsSvc.job.activityId = 8;
                JobsSvc.job.CurrentActivity = { id: null, activityId: 7, startTime: null, endTime: null, addedClosureTypes: [], tsls: [], complete: false, notes: null };
                $state.go('actsiteretrieval');
                break;
            case 9:
                JobsSvc.job.activityId = 9;
                JobsSvc.job.CurrentActivity = { id: null, activityId: 1, startTime: null, endTime: null, complete: false, notes: null };
                $state.go('acttraveltodepot');
                break;
            case 10:
                JobsSvc.job.activityId = 10;
                TruckAuditSvc.start(JobsSvc.job.jobName).then(function (result) {
                });
                TruckAuditSvc.complete().then(function (result) {
                    if (result) {
                        result.isTruckAudit = true;
                        result.activityId = 10;
                        result.startTime = new Date();
                        JobsSvc.AddSendActivity(result);
                    }
                });
                break;
            case 11:
                JobsSvc.job.CurrentActivity = { id: null, activityId: activity.Id, startTime: null, endTime: null, complete: false, notes: null };
                $state.go('actprestartprep');
                break;
            case 12:
                JobsSvc.job.CurrentActivity = { id: null, activityId: activity.Id, startTime: null, endTime: null, complete: false, notes: null };
                $state.go('acttraveltonextjob');
                break;
            case 13:
                //$ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>' });
                endOfShift(activity);
                //var activity = { id: (new $window.ObjectId()).toString(), jobId: $scope.job.id, activityId: activity.Id, startTime: new Date(), endTime: null, complete: true, notes: null, mode = 0 };
                /*
                JobsSvc.endOfShift(activity).then(function () {
                    //$ionicLoading.hide();
                    //$scope.modal.hide();
                }, function (error) {
                    console.log(error);
                    //$scope.modal.hide();
                    //$ionicLoading.hide();
                    alert('Something went wrong.');
                    });
                */
                break;
                
            default:

        }
        
    };
    function endOfShift(activity) {
        //console.log(activity.Id);
        //console.log($scope.job.id);
        console.log('endOfShift()');
        var endOfShiftActivity = { id: null, jobId: $scope.job.id, activityId: activity.Id, startTime: new Date(), endTime: null, complete: true };//, notes: null, mode = 0 };
        //var sample = { jobId: $scope.job.id, activityId: activity.Id };        
    }
    /*
    $scope.AddActivitySelection = function () {
        if ($scope.job.trucks.length == 0) {
            $ionicPopup.alert({ title: 'Trucks', template: 'Please choose trucks before starting an activity.' });
            return;
        }

        $scope.modal.show();
    };
    $scope.CloseActivitySelection = function () {
        $scope.modal.hide();
    };

    $scope.Parties = function () {
        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>' });
        $state.go('parties');
    };
    $scope.Team = function () {
        JobsSvc.job.teamChanged = false;
        $state.go('team');
    };
    $scope.GeneralInfo = function () {
        $state.go('generalinfo');
    };
    $scope.done = function () {
        //do housekeeping
        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>' });
        JobsSvc.save().then(function (result) {
            $ionicLoading.hide();
            JobsSvc.job = null;
            SiteCheckAlarmSvc.stop();
            $state.go('jobs');
        }, function (error) {
            console.log(error);
            $ionicLoading.hide();
            alert('an error occured while saving the data');
        });
    };

    $scope.ActivityDetails = function (activity) {
        switch (activity.activityId) {
            case 1:
                JobsSvc.job.CurrentActivity = activity;
                $state.go('acttraveltosite');
                break;
            case 2:
                JobsSvc.job.activityId = 2;
                JobsSvc.job.CurrentActivity = activity;
                $state.go('actsitedeployment');
                break;
            case 3:
                JobsSvc.job.activityId = 3;
                JobsSvc.job.CurrentActivity = activity;
                $state.go('actsitehazardassessment');
                break;
            case 4:
                JobsSvc.job.activityId = 4;
                JobsSvc.job.CurrentActivity = activity;
                $state.go('actwaiting');
                break;
            case 5:
                JobsSvc.job.CurrentActivity = activity;
                $state.go('actworkspaceactive');
                break;
            case 6:
                JobsSvc.job.CurrentActivity = activity;
                $state.go('actsitecheck');
                break;
            case 7:
                //JobsSvc.job.activityId = 7;
                JobsSvc.job.CurrentActivity = activity;
                $state.go('actsiteamendment');
                break;
            case 8:
                JobsSvc.job.activityId = 8;
                JobsSvc.job.CurrentActivity = activity;
                $state.go('actsiteretrieval');
                break;
            case 9:
                JobsSvc.job.CurrentActivity = activity;
                $state.go('acttraveltodepot');
                break;
        }
    };
    $scope.showDetails = false;
    $scope.toggleDetails = function () {
        $scope.showDetails = !$scope.showDetails;
    };
    $scope.generalNotes = function () {
        $state.go('generalnotes');
    };
    $scope.testSiteCheckAlarm = function () {
        SiteCheckAlarmSvc.start();
    };
    $scope.chooseTrucks = function () {
        //alert('choose trucks');
        if ($scope.job.trucks == null)
            $scope.job.trucks = [];

        ModalFactory.showTrucksSelection($scope.job.trucks).then(function (result) {
            console.log(result);
            if (result) {
                //set the trucks
                $scope.job.trucks = result.trucks;
                $scope.truckNames = getTruckNames($scope.job.trucks);
                //send changes to server
                JobsSvc.save().then(function (result) {
                    JobsSvc.updateTrucks($scope.job);
                });
            }
        }, function (error) {
            console.log(error);
        });
    };
    */
}]);