﻿function fileErrorHandler(e) {
    var msg = '';
    console.log("error code: " + e.code);
    switch (e.code) {
        case FileError.QUOTA_EXCEEDED_ERR:
            msg = 'QUOTA_EXCEEDED_ERR';
            break;
        case FileError.NOT_FOUND_ERR:
            msg = 'NOT_FOUND_ERR';
            break;
        case FileError.SECURITY_ERR:
            msg = 'SECURITY_ERR';
            break;
        case FileError.INVALID_MODIFICATION_ERR:
            msg = 'INVALID_MODIFICATION_ERR';
            break;
        case FileError.INVALID_STATE_ERR:
            msg = 'INVALID_STATE_ERR';
            break;
        case FileError.ENCODING_ERR:
            msg = 'ENCODING_ERR';
            break;
        default:
            msg = 'Unknown Error';
            break;
    };

    console.log('Error: ' + msg);
}
function getFileError(error) {
    switch (error.code) {
        case FileError.NOT_FOUND_ERR:
            return 'FileError.NOT_FOUND_ERR';
        case FileError.SECURITY_ERR:
            return 'FileError.SECURITY_ERR';
        case FileError.ABORT_ERR:
            return 'FileError.ABORT_ERR';
        case FileError.NOT_READABLE_ERR:
            return 'FileError.NOT_READABLE_ERR';
        case FileError.ENCODING_ERR:
            return 'FileError.ENCODING_ERR';
        case FileError.NO_MODIFICATION_ALLOWED_ERR:
            return 'FileError.NO_MODIFICATION_ALLOWED_ERR';
        case FileError.INVALID_STATE_ERR:
            return 'FileError.INVALID_STATE_ERR';
        case FileError.SYNTAX_ERR:
            return 'FileError.SYNTAX_ERR';
        case FileError.INVALID_MODIFICATION_ERR:
            return 'FileError.INVALID_MODIFICATION_ERR';
        case FileError.QUOTA_EXCEEDED_ERR:
            return 'FileError.INVALID_MODIFICATION_ERR';
        case FileError.TYPE_MISMATCH_ERR:
            return 'FileError.TYPE_MISMATCH_ERR';
        case FileError.PATH_EXISTS_ERR:
            return 'FileError.PATH_EXISTS_ERR';
        default:
            return 'unknown file error';
    }
}
function LeadZero(duration, limit) {
    if (duration < 10)
        return "0" + duration;

    return duration;
}

function getFileName(path) {
    return path.substring(path.lastIndexOf('/') + 1, path.length);
}

function getDirectory(path) {
    return path.substring(0, path.lastIndexOf('/'));
}

function getMediaCaptureError(err) {
    switch (err.code) {
        case CaptureError.CAPTURE_INTERNAL_ERR:
            return 'CAPTURE_INTERNAL_ERR';
        case CaptureError.CAPTURE_APPLICATION_BUSY:
            return 'CAPTURE_APPLICATION_BUSY';
        case CaptureError.CAPTURE_INVALID_ARGUMENT:
            return 'CAPTURE_INVALID_ARGUMENT';
        case CaptureError.CAPTURE_NO_MEDIA_FILES:
            return 'CAPTURE_NO_MEDIA_FILES';
        case CaptureError.CAPTURE_PERMISSION_DENIED:
            return 'CAPTURE_PERMISSION_DENIED';
        case CaptureError.CAPTURE_NOT_SUPPORTED:
            return 'CAPTURE_NOT_SUPPORTED';
    }
}
function getRandomFilename() {
    return chance.string({ pool: 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789', length: 15 });
}
function getCaptureError(error) {
    switch (error) {
        case CaptureError.CAPTURE_INTERNAL_ERR:
            return 'CaptureError.CAPTURE_INTERNAL_ERR';
        case CaptureError.CAPTURE_APPLICATION_BUSY:
            return 'CaptureError.CAPTURE_APPLICATION_BUSY';
        case CaptureError.CAPTURE_INVALID_ARGUMENT:
            return 'CaptureError.CAPTURE_INVALID_ARGUMENT';
        case CaptureError.CAPTURE_NO_MEDIA_FILES:
            return 'CaptureError.CAPTURE_NO_MEDIA_FILES';
        case CaptureError.CAPTURE_PERMISSION_DENIED:
            return 'CaptureError.CAPTURE_PERMISSION_DENIED';
        case CaptureError.CAPTURE_NOT_SUPPORTED:
            return 'CaptureError.CAPTURE_NOT_SUPPORTED:'
        default:
            return JSON.parse(error).error_message;
    }
}

function getErrorMessage(error) {
    if(error.status == -1)
        return error.xhrStatus;
    else if(error.status >= 400 && error.status <= 600){
        if(error.data && error.data.message)
            return error.data.message;
        if(error.data && error.data.error_description)
            return error.data.error_description;
        if(error.data.error === "invalid_grant")
            return "Unauthorized credentials";
    }
    else
        return error.statusText;
}

function getFileExtension(fileName){
    let slugs = fileName.split('.');
    return slugs[slugs.length - 1];
}

function getMimeType(fileExtension){
    if(fileExtension === 'pdf')
        return 'application/pdf';
    if(fileExtension === 'jpg' || fileExtension === 'jpeg')
        return 'image/jpeg';
    if(fileExtension === 'png')
        return 'image/png';
    if(fileExtension === 'doc')
        return 'application/msword';
    if(fileExtension === 'docx')
        return 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
    if(fileExtension === 'xls')
        return 'application/vnd.ms-excel';
    if(fileExtension === 'xlsx')
        return 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';

    return '';
}

var geocoder;
function GetAddress(latlang, success) {
    if (!geocoder)
        initGeocoder();

    if (geocoder) {
        geocoder.geocode({ 'location': latlang }, function (results, status) {            
            if (status === google.maps.GeocoderStatus.OK) {
                if (results.length > 0) {
                    success(results);
                } else {
                    success(null);
                }
            } else {
                success(null);
            }
        });
    }
    else
        success(null);
}

function initGeocoder() {
    console.log('initGeocoder');
    try{
        geocoder = new google.maps.Geocoder();    
    }
    catch(error){
        geocoder = null;
    }
}

function isNullOrWhiteSpace(value){
    return value === undefined || value === null || value.length == 0 || value.trim().length == 0;
}