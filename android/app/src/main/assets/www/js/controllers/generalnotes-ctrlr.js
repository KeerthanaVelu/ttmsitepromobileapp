﻿mainApp.controller('GeneralNotesCtrlr', ['$scope', '$ionicPopup', '$cordovaFile', '$ionicLoading', 'JobsSvc', 'VoiceToTextSvc', 'PhotoSvc', '$stateParams', '$ionicHistory', '$cordovaMedia', '$state',
 function ($scope, $ionicPopup, $cordovaFile, $ionicLoading, JobsSvc, VoiceToTextSvc, PhotoSvc, $stateParams, $ionicHistory, $cordovaMedia, $state) {
    $scope.$on('$ionicView.enter', function(){
        $ionicLoading.show({ template: '<ion-spinner></ion-spinner>'});
        JobsSvc.getGeneralNotes($stateParams.id).then(function(response){
            $scope.fields = response;
        }, function(error){
            alert('Something went wrong.');
        }).finally(function(){
            $ionicLoading.hide();
        });
    });


//#region     
    $scope.voiceToText = function () {
        console.log('voiceToText()');
        VoiceToTextSvc.Capture().then(function (result) {
            console.log('voiceToText() success');
            console.log(result);
            if ($scope.fields.notes == null)
                $scope.fields.notes = '';
            if ($scope.fields.notes.length > 0)
                $scope.fields.notes += ' ' + result[0];
            else
                $scope.fields.notes += result[0];
        }, function (error) {
            console.log('voiceToText() error');
            console.log(error);
            alert(error);
        });                
    };
    $scope.capturePhoto = function () {
        if(!$scope.fields.photos)
            $scope.fields.photos = [];
        PhotoSvc.getPhoto().then(function (photo) {
            console.log(photo);
            $scope.fields.photos.push(photo.path);
        }, function(error){
            alert('Something went wrong while getting a photo.');
        });        
    };
    $scope.captureAudio = function () {
        if (!$scope.fields.audioNotes)
            $scope.fields.audioNotes = [];

        var options = { limit: 1 };
        navigator.device.audiorecorder.recordAudio(function (audio) {
            //create new file name
            //move file to app directory with new name
            //save only name
            let audioData = JSON.parse(audio);
            let fullPath = 'file://' + audioData.full_path;
            console.log(fullPath);
            window.resolveLocalFileSystemURL(fullPath, function (resolvedUrl) {
                console.log(resolvedUrl);
                var newFilename = (new ObjectId()).toString() + '.' + fullPath.split('.').pop();
                $cordovaFile.moveFile(getDirectory(fullPath), audioData.file_name, cordova.file.dataDirectory, newFilename).then(function (newAudio) {
                    console.log(newAudio);
                    $scope.fields.audioNotes.push(newAudio.nativeURL);
                }, function (error) {
                    if (error.code == FileError.ENCODING_ERR)
                        alert('ENCODING_ERR');
                    console.log(error);
                });
            }, function(error){
                console.log(error);
            });                    
        }, function (error) {
            console.log(error);
            alert(getCaptureError(error));
        });        
    };
    
    $scope.back = function () {
        //$ionicHistory.goBack();
        $state.go('app.jobdetail', {id: $stateParams.id, showAddActivity: false});
    };
     $scope.removePhoto = function (index) {

         var photoPath = $scope.fields.photos[index];
         var photoFileName = photoPath.substr(photoPath.lastIndexOf('/') + 1);
         var confirmPopup = $ionicPopup.confirm({
             title: 'Site Pro',
             template: 'Do you want to remove this photo?<br/><strong>' + photoFileName + '</strong>'
         });
         confirmPopup.then(function (res) {
             if (res) {
                 $scope.fields.photos.splice(index, 1);
             }
         });

    };
     var media = null;
    $scope.audioNotesPlaying = -1;    
     $scope.playAudioNotes = function (audio, index) {
        var audioPath = audio.replace('file://', '');
        //media = $cordovaMedia.newMedia(audioPath);

        if (media == null) {
            media = new Media(audioPath, function () {
            }, function (error) {
                switch (error) {
                    case MediaError.MEDIA_ERR_ABORTED:
                        alert('MediaError.MEDIA_ERR_ABORTED');
                        break;
                    case MediaError.MEDIA_ERR_NETWORK:
                        alert('MediaError.MEDIA_ERR_NETWORK');
                        break;
                    case MediaError.MEDIA_ERR_DECODE:
                        alert('MediaError.MEDIA_ERR_DECODE');
                        break;
                    case MediaError.MEDIA_ERR_NONE_SUPPORTED:
                        alert('MediaError.MEDIA_ERR_NONE_SUPPORTED');
                        break;
                }
            }, function (mediaStatus) {
                switch (mediaStatus) {
                    case Media.MEDIA_NONE:
                        //alert('Media.MEDIA_NONE');
                        break;
                    case Media.MEDIA_STARTING:
                        //alert('Media.MEDIA_STARTING');
                        break;
                    case Media.MEDIA_RUNNING:
                        //mediaStatus.MEDIA_RUNNING
                        console.log(index);
                        $scope.audioNotesPlaying = index;
                        $scope.$apply();
                        break;
                    case Media.MEDIA_PAUSED:
                        break;
                    case Media.MEDIA_STOPPED:
                        if (media != null) {
                            media.release();
                            media = null;
                        }
                        //media = null;
                        $scope.audioNotesPlaying = -1;
                        $scope.$apply();
                        break;
                }
            });

            media.play();
        }
        else {
            media.stop();
            media.release();
            media = null;
            $scope.AudioNotesPlaying = -1;
        }

        //currentAudio.play();
    };
    $scope.removeAudioNotes = function (index) {
        var audioPath = $scope.fields.audioNotes[index];
        var audioFileName = audioPath.substr(audioPath.lastIndexOf('/') + 1);
        var confirmPopup = $ionicPopup.confirm({
            title: 'Site Pro',
            template: 'Do you want to remove this voice notes?<br/><strong>' + audioFileName + '</strong>'
        });
        confirmPopup.then(function (res) {
            if (res) {
                $scope.fields.audioNotes.splice(index, 1);
            }
        });
    };
    $scope.done = function () {
        if($scope.fields){
            $ionicLoading.show({ template: '<ion-spinner></ion-spinner>'});        
            JobsSvc.updateGeneralNotes($scope.fields.id, $scope.fields).then(function (response){            
            }, function (error) {
                console.log(error);
                alert('Something went wrong.');
            }).finally(function(){
                $ionicLoading.hide();
                $state.go('app.jobdetail', {id: $stateParams.id, showAddActivity: false});
                //$ionicHistory.goBack();
            });
        }
    };
 
//#endregion
}]);