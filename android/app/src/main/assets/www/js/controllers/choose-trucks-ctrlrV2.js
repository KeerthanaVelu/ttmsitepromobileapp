﻿mainApp.controller('ChooseTrucksCtrlr', ['$scope', 'TrucksSvc', '$linq', '$ionicPopup', '$ionicLoading', 'parameters', 'AccountSvc', function ($scope, TrucksSvc, $linq, $ionicPopup, $ionicLoading, parameters, AccountSvc) {
    $scope.$on('modal.shown', function(event){
        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>'});
        AccountSvc.getBranchesV2().then(function(branches){
            $scope.branches = branches;
            $scope.option.branchId = AccountSvc.getBranchIdV2();
            TrucksSvc.getAll().then(function(trucks){                
                $scope.tenantTrucks = trucks;
                setSelectedTrucks();
                $ionicLoading.hide();
            }, function(error){
                console.log(error);
                $ionicLoading.hide();
            });
        });
    });
    function setSelectedTrucks() {        
        for (var i = 0; i < $scope.tenantTrucks.length; i++) {
            var truck = $scope.tenantTrucks[i];
            var truckSelected = $linq.Enumerable().From($scope.selectedTrucks).FirstOrDefault(null, 'f => f == "' + truck.id + '"');
            if (truckSelected != null)
                truck.checked = true;
        }

        $scope.selectedCount = $scope.selectedTrucks.length;
    }
    $scope.selectedTrucks = parameters.selectedTrucks || [];
    $scope.selectedCount = 0;
    $scope.option = {search:null};
    $scope.cancel = function () {
        $scope.closeModal(null);
    };
    $scope.done = function () {        
        var selectedTrucks = $linq.Enumerable().From($scope.tenantTrucks).Where('w => w.checked == true').ToArray();
        var truckIds = $linq.Enumerable().From(selectedTrucks).Select('s => s.id').ToArray();
        var truckNames = $linq.Enumerable().From(selectedTrucks).Select('s => (s.truckNumber + " - " + s.truckRego)').ToArray();
        $scope.closeModal({ trucks: truckIds, truckNames: truckNames });
    };
    $scope.addTruck = function (truck) {
        //console.log(truck);        
        if (truck.checked)
            $scope.selectedCount++;
        else
            $scope.selectedCount--;
            
        if ($scope.selectedCount > 6) {
            truck.checked = false;
            $scope.selectedCount--;
        }
    };
    $scope.refreshTrucks = function () {
        //return;
        if (navigator.connection.type == 'none') {
            $ionicPopup.alert({ title: 'Connection Problem', template: 'Internet connection not available. Check your connection & try again.' });
            return;
        }

        $ionicLoading.show({ template: '<ion-spinner></ion-spinner>', hideOnStateChange: true });
        TrucksSvc.refreshData().then(function (result) {
            //$scope.tenantTrucks = angular.copy(TrucksSvc.getAll());
            //setSelectedTrucks();            
        }, function (error) {
            $ionicPopup.alert({ title: 'Site Pro', template: 'Unable to load new data.' });
        }).finally(function () {
            TrucksSvc.getAll().then(function(trucks){                
                $scope.tenantTrucks = trucks;
                setSelectedTrucks();
                $ionicLoading.hide();
            }, function(error){
                console.log(error);
                $ionicLoading.hide();
            });
            $ionicLoading.hide();
        });
    };
}]);