﻿mainApp.controller('SiteActivityCtrlr', ['$scope', '$state', 'JobsSvc', 'LocationSvc', '$ionicLoading', 'ModalFactory', 'SiteCheckAlarmSvc', 'VoiceToTextSvc', '$ionicPopup', '$stateParams', '$linq', '$filter', function ($scope, $state, JobsSvc, LocationSvc, $ionicLoading, ModalFactory, SiteCheckAlarmSvc, VoiceToTextSvc, $ionicPopup, $stateParams, $linq, $filter) {
    $scope.$on('$ionicView.enter', function () {
        console.log($stateParams);
        $ionicLoading.show({ template: '<ion-spinner></ion-spinner>' });
        $scope.isJobArchived = $stateParams.isJobArchived;
        $scope.showTSL = true;
        JobsSvc.getActivity($stateParams.jobid, $stateParams.id).then(function(response){
            $scope.activity = response;
            $scope.activityHeaders = {};
            $scope.activityHeaders.name = ActivityTypeEnum.props[$scope.activity.activityType].name;
            $scope.activityHeaders.jobName = $stateParams.jobName;

            if ($scope.activity.activityType === 8)
                $scope.showTSL = false;

            if(!$scope.activity.isCancelled && $scope.activity.startTime && !$scope.activity.endTime)
                $scope.$broadcast('timer-start');
            else{
                let startTime = moment($scope.activity.startTime);
                let endTime = moment($scope.activity.endTime);
                var diff = endTime.valueOf() - startTime.valueOf();
                $scope.activitySummary = {};
                $scope.activitySummary.totalDuration = LeadZero(moment.duration(diff).hours()) + ":" + LeadZero(moment.duration(diff).minutes()) + ":" + LeadZero(moment.duration(diff).seconds());
            }
            $scope.btns.saveNotesEnabled = false;
            /*
            if(!$scope.activity.endTime){
                if($scope.activity.activityType === ActivityTypeEnum.SiteAmendmentRecord || $scope.activity.activityType === ActivityTypeEnum.SiteRetrievalRecord){
                    SiteCheckAlarmSvc.pause();
                    //get TSLs from last site deployment <<<<<                
                    JobsSvc.get($stateParams.jobid).then(function(job){
                        if(job){
                            $scope.job = job;
                            let actTypeNow = $scope.activity.activityType;
                            //var lastActType = actTypeNow === ActivityTypeEnum.SiteAmendmentRecord ? ActivityTypeEnum.SiteDeployment : actTypeNow === ActivityTypeEnum.SiteRetrievalRecord ? ActivityTypeEnum.SiteAmendmentRecord : actTypeNow;
                            var lastSiteDeploymentRetrieval = $linq.Enumerable().From(job.activities).LastOrDefault(null, 'f => (f.activityType === ' + ActivityTypeEnum.SiteDeployment + ' || f.activityType === ' + ActivityTypeEnum.SiteAmendmentRecord + ') && f.tsls && f.tsls.length > 0');//lastActType);
                            if(lastSiteDeploymentRetrieval){
                                console.log('[30]');
                                console.log(lastSiteDeploymentRetrieval.tsls);
                                if(lastSiteDeploymentRetrieval.tsls){
                                    $scope.activity.tsls = lastSiteDeploymentRetrieval.tsls.slice(0);
                                    $ionicLoading.show({ template: '<ion-spinner></ion-spinner>' });
                                    JobsSvc.updateActivityV2($stateParams.jobid, $scope.activity).then(function(response){
                                        $ionicLoading.hide();
                                    });
                                }
                            }
                        }
                    });
                    //>>>>>
                }
            }
            */
            $ionicLoading.hide();
        });
    });

    $scope.tsls = [];
    
    $scope.stop = function () {
        $ionicLoading.show({ template: '<ion-spinner></ion-spinner>' });
        var posOptions = { timeout: 5000, enableHighAccuracy: true };
        var coordinates = null;

        LocationSvc.getCurrentPosition(posOptions).then(function (position) {
            console.log(position);
            coordinates = position;
        }, function (error) {
            console.log(error);
            coordinates = error;
        }).finally(function () {
            $ionicLoading.hide();
            $scope.activity.endTime = new Date();
            if (coordinates.coords) {
                $scope.activity.endCoordinates = { latitude: coordinates.coords.latitude, longitude: coordinates.coords.longitude };
            }
            $scope.activity.action = ActivityActionEnum.STOP;
            JobsSvc.stopActivity($stateParams.jobid, $scope.activity).then(function(response){
                if($scope.activity.activityType === ActivityTypeEnum.SiteDeployment){
                    SiteCheckAlarmSvc.start();
                }                
                if($scope.activity.activityType === ActivityTypeEnum.SiteRetrievalRecord){
                    SiteCheckAlarmSvc.stop();
                }
                $ionicLoading.hide();
                $state.go('app.jobdetail', {id: $stateParams.jobid, showAddActivity: true});
            });
        });
    };
    $scope.addTSL = function () {
        //TSLSvc.AddTSL($scope);
        let isFirstTsl = false;
        if($scope.activity.tsls == null || $scope.activity.tsls.length === 0)
            isFirstTsl = true;

        ModalFactory.showTSL(isFirstTsl).then(function (result) {
            if (result) {
                $scope.activity.tsls.push(result[0]);
                $scope.activity.tsls.push(result[1]);
                $scope.activity.action = ActivityActionEnum.UPDATE;
                $ionicLoading.show({ template: '<ion-spinner></ion-spinner>' });
                JobsSvc.updateActivityV2($stateParams.jobid, $scope.activity).then(function(response){
                    $ionicLoading.hide();
                });
            }
        });
    };
    $scope.back = function () {
        $state.go('app.jobdetail', {id: $stateParams.jobid, showAddActivity: false});
        //$ionicHistory.goBack();
    };

    $scope.editClosureTypes = function () {
        ModalFactory.showClosureTypes($scope.activity.addedClosureTypes, $scope.activity.closureTypesOption).then(function (result) {
            if(result){
                $scope.activity.addedClosureTypes = result.closureTypes;
                $scope.activity.closureTypesOption = result.closureTypesOption;
                $scope.activity.action = ActivityActionEnum.UPDATE;
                $ionicLoading.show({ template: '<ion-spinner></ion-spinner>' });
                JobsSvc.updateActivityV2($stateParams.jobid, $scope.activity).then(function(response){
                    $ionicLoading.hide();
                });
            }
        });
    };

    $scope.cancel = function () {
        var confirmPopup = $ionicPopup.confirm({
            title: 'Site Pro',
            template: 'Are you sure you want to cancel this activity?',
            okText: 'Yes',
            cancelText: 'No'
        });

        confirmPopup.then(function (res) {
            if (res) {
                JobsSvc.cancelActivity($stateParams.jobid, $scope.activity.id).then(function(response){
                    SiteCheckAlarmSvc.resume();
                    $ionicLoading.hide();
                    $state.go('app.jobdetail', {id: $stateParams.jobid, showAddActivity: true});
                    //$state.go('app.jobdetail', {id: $stateParams.jobid});
                });
            }
        });
    };
    $scope.voiceToText = function () {
        VoiceToTextSvc.Capture().then(function (result) {
            console.log(result);
            if ($scope.activity.notes == null)
                $scope.activity.notes = '';
            if ($scope.activity.notes.length > 0)
                $scope.activity.notes += ' ' + result[0];
            else
                $scope.activity.notes += result[0];
        }, function (error) {
            console.log(error);
        });
    };

    $scope.btns = { saveNotesEnabled: false };
    $scope.enableSaveNotesBtn = function(){
        if(!$scope.btns.saveNotesEnabled)
            $scope.btns.saveNotesEnabled = true;
    };
    $scope.saveNotes = function(){
        $ionicLoading.show({ template: '<ion-spinner></ion-spinner>' });
        JobsSvc.updateActivityV2($stateParams.jobid, $scope.activity).then(function(response){
            $ionicLoading.hide();
        });
    };

    $scope.tslRemoved = function(tsl){
        let tempTslRemoved = new Date();
        let confirm = $ionicPopup.confirm({ title: 'Site Pro', template: "Please confirm removal time: <br/>" + $filter('date')(tempTslRemoved, 'hh:mm a dd-MM-yyyy'), cancelText: 'No', okText: 'Yes' });
        confirm.then(function(confirm){
            if(confirm){
                tsl.tslRemoved = tempTslRemoved;
                $ionicLoading.show({ template: '<ion-spinner></ion-spinner>' });
                JobsSvc.updateActivityV2($stateParams.jobid, $scope.activity).then(function(response){
                    $ionicLoading.hide();
                });
                /*
                JobsSvc.saveJob($scope.job).then(function(){
                    $ionicLoading.hide();
                });
                */
            }
        });
    };

    $scope.removeTsl = function(tsl){
        let confirm = $ionicPopup.confirm({ title: 'Site Pro', template: "Do you want to remove this TSL?", cancelText: 'No', okText: 'Yes' });
        confirm.then(function(confirm){
            if(confirm){
                let index = $scope.activity.tsls.indexOf(tsl);
                if(index >= 0){
                    $scope.activity.tsls.splice(index, 1);
                    $ionicLoading.show({ template: '<ion-spinner></ion-spinner>' });
                    JobsSvc.updateActivityV2($stateParams.jobid, $scope.activity).then(function(response){
                        $ionicLoading.hide();
                    });
                }
            }
        });
    };

    $scope.editTsl = function(index, tsl){

        if (index !== null && index !== undefined) {
            $scope.selectedIndex = index;
            $scope.selectedtsls = [];
            $scope.selectedtsls.push($scope.activity.tsls[index]);
            $scope.selectedtsls.push($scope.activity.tsls[index + 1]);

            let tempTsl = angular.copy($scope.selectedtsls);

            ModalFactory.showTSL(index === 0, tempTsl).then(function (result) {
                if (result) {
                    $scope.activity.tsls[$scope.selectedIndex] = result[0];
                    $scope.activity.tsls[$scope.selectedIndex + 1] = result[1];
                    $ionicLoading.show({ template: '<ion-spinner></ion-spinner>' });
                    JobsSvc.updateActivityV2($stateParams.jobid, $scope.activity).then(function (response) {
                        $ionicLoading.hide();
                    });
                }
            });
        }
    };
}]);

mainApp.controller('ToolBoxBriefingRecordCtrlr', ['$scope', '$state', 'JobsSvc', 'LocationSvc', '$ionicLoading', 'ModalFactory', 'SiteCheckAlarmSvc', 'VoiceToTextSvc', '$ionicPopup', '$stateParams', '$linq', '$filter', function ($scope, $state, JobsSvc, LocationSvc, $ionicLoading, ModalFactory, SiteCheckAlarmSvc, VoiceToTextSvc, $ionicPopup, $stateParams, $linq, $filter) {
    $scope.$on('$ionicView.enter', function () {
        console.log($stateParams);
        $ionicLoading.show({ template: '<ion-spinner></ion-spinner>' });
        $scope.isJobArchived = $stateParams.isJobArchived;
        $scope.showTSL = true;
        JobsSvc.getActivity($stateParams.jobid, $stateParams.id).then(function (response) {
            $scope.activity = response;
            $scope.activityHeaders = {};
            $scope.activityHeaders.name = ActivityTypeEnum.props[$scope.activity.activityType].name;
            $scope.activityHeaders.jobName = $stateParams.jobName;

            if ($scope.activity.activityType === 8)
                $scope.showTSL = false;

            if (!$scope.activity.isCancelled && $scope.activity.startTime && !$scope.activity.endTime)
                $scope.$broadcast('timer-start');
            else {
                let startTime = moment($scope.activity.startTime);
                let endTime = moment($scope.activity.endTime);
                var diff = endTime.valueOf() - startTime.valueOf();
                $scope.activitySummary = {};
                $scope.activitySummary.totalDuration = LeadZero(moment.duration(diff).hours()) + ":" + LeadZero(moment.duration(diff).minutes()) + ":" + LeadZero(moment.duration(diff).seconds());
            }
            $scope.btns.saveNotesEnabled = false;
            $ionicLoading.hide();
        });
    });

    $scope.tsls = [];

    $scope.stop = function () {
        $ionicLoading.show({ template: '<ion-spinner></ion-spinner>' });
        var posOptions = { timeout: 5000, enableHighAccuracy: true };
        var coordinates = null;

        LocationSvc.getCurrentPosition(posOptions).then(function (position) {
            console.log(position);
            coordinates = position;
        }, function (error) {
            console.log(error);
            coordinates = error;
        }).finally(function () {
            $ionicLoading.hide();
            $scope.activity.endTime = new Date();
            if (coordinates.coords) {
                $scope.activity.endCoordinates = { latitude: coordinates.coords.latitude, longitude: coordinates.coords.longitude };
            }
            $scope.activity.action = ActivityActionEnum.STOP;
            JobsSvc.stopActivity($stateParams.jobid, $scope.activity).then(function (response) {
                if ($scope.activity.activityType === ActivityTypeEnum.SiteDeployment) {
                    SiteCheckAlarmSvc.start();
                }
                if ($scope.activity.activityType === ActivityTypeEnum.SiteRetrievalRecord) {
                    SiteCheckAlarmSvc.stop();
                }
                $ionicLoading.hide();
                $state.go('app.jobdetail', { id: $stateParams.jobid, showAddActivity: true });
            });
        });
    };
    $scope.addTSL = function () {
        //TSLSvc.AddTSL($scope);
        let isFirstTsl = false;
        if ($scope.activity.tsls == null || $scope.activity.tsls.length === 0)
            isFirstTsl = true;

        ModalFactory.showTSL(isFirstTsl).then(function (result) {
            if (result) {
                $scope.activity.tsls.push(result[0]);
                $scope.activity.tsls.push(result[1]);
                $scope.activity.action = ActivityActionEnum.UPDATE;
                $ionicLoading.show({ template: '<ion-spinner></ion-spinner>' });
                JobsSvc.updateActivityV2($stateParams.jobid, $scope.activity).then(function (response) {
                    $ionicLoading.hide();
                });
            }
        });
    };
    $scope.back = function () {
        $state.go('app.jobdetail', { id: $stateParams.jobid, showAddActivity: false });
        //$ionicHistory.goBack();
    };

    $scope.editClosureTypes = function () {
        ModalFactory.showClosureTypes($scope.activity.addedClosureTypes, $scope.activity.closureTypesOption).then(function (result) {
            if (result) {
                $scope.activity.addedClosureTypes = result.closureTypes;
                $scope.activity.closureTypesOption = result.closureTypesOption;
                $scope.activity.action = ActivityActionEnum.UPDATE;
                $ionicLoading.show({ template: '<ion-spinner></ion-spinner>' });
                JobsSvc.updateActivityV2($stateParams.jobid, $scope.activity).then(function (response) {
                    $ionicLoading.hide();
                });
            }
        });
    };

    $scope.cancel = function () {
        var confirmPopup = $ionicPopup.confirm({
            title: 'Site Pro',
            template: 'Are you sure you want to cancel this activity?',
            okText: 'Yes',
            cancelText: 'No'
        });

        confirmPopup.then(function (res) {
            if (res) {
                JobsSvc.cancelActivity($stateParams.jobid, $scope.activity.id).then(function (response) {
                    SiteCheckAlarmSvc.resume();
                    $ionicLoading.hide();
                    $state.go('app.jobdetail', { id: $stateParams.jobid, showAddActivity: true });
                    //$state.go('app.jobdetail', {id: $stateParams.jobid});
                });
            }
        });
    };
    $scope.voiceToText = function () {
        VoiceToTextSvc.Capture().then(function (result) {
            console.log(result);
            if ($scope.activity.notes == null)
                $scope.activity.notes = '';
            if ($scope.activity.notes.length > 0)
                $scope.activity.notes += ' ' + result[0];
            else
                $scope.activity.notes += result[0];
        }, function (error) {
            console.log(error);
        });
    };

    $scope.btns = { saveNotesEnabled: false };
    $scope.enableSaveNotesBtn = function () {
        if (!$scope.btns.saveNotesEnabled)
            $scope.btns.saveNotesEnabled = true;
    };
    $scope.saveNotes = function () {
        $ionicLoading.show({ template: '<ion-spinner></ion-spinner>' });
        JobsSvc.updateActivityV2($stateParams.jobid, $scope.activity).then(function (response) {
            $ionicLoading.hide();
        });
    };

    $scope.tslRemoved = function (tsl) {
        let tempTslRemoved = new Date();
        let confirm = $ionicPopup.confirm({ title: 'Site Pro', template: "Please confirm removal time: <br/>" + $filter('date')(tempTslRemoved, 'hh:mm a dd-MM-yyyy'), cancelText: 'No', okText: 'Yes' });
        confirm.then(function (confirm) {
            if (confirm) {
                tsl.tslRemoved = tempTslRemoved;
                $ionicLoading.show({ template: '<ion-spinner></ion-spinner>' });
                JobsSvc.updateActivityV2($stateParams.jobid, $scope.activity).then(function (response) {
                    $ionicLoading.hide();
                });
                /*
                JobsSvc.saveJob($scope.job).then(function(){
                    $ionicLoading.hide();
                });
                */
            }
        });
    };

    $scope.removeTsl = function (tsl) {
        let confirm = $ionicPopup.confirm({ title: 'Site Pro', template: "Do you want to remove this TSL?", cancelText: 'No', okText: 'Yes' });
        confirm.then(function (confirm) {
            if (confirm) {
                let index = $scope.activity.tsls.indexOf(tsl);
                if (index >= 0) {
                    $scope.activity.tsls.splice(index, 1);
                    $ionicLoading.show({ template: '<ion-spinner></ion-spinner>' });
                    JobsSvc.updateActivityV2($stateParams.jobid, $scope.activity).then(function (response) {
                        $ionicLoading.hide();
                    });
                }
            }
        });
    };

    $scope.editTsl = function (index, tsl) {

        if (index !== null && index !== undefined) {
            $scope.selectedIndex = index;
            $scope.selectedtsls = [];
            $scope.selectedtsls.push($scope.activity.tsls[index]);
            $scope.selectedtsls.push($scope.activity.tsls[index + 1]);

            let tempTsl = angular.copy($scope.selectedtsls);

            ModalFactory.showTSL(index === 0, tempTsl).then(function (result) {
                if (result) {
                    $scope.activity.tsls[$scope.selectedIndex] = result[0];
                    $scope.activity.tsls[$scope.selectedIndex + 1] = result[1];
                    $ionicLoading.show({ template: '<ion-spinner></ion-spinner>' });
                    JobsSvc.updateActivityV2($stateParams.jobid, $scope.activity).then(function (response) {
                        $ionicLoading.hide();
                    });
                }
            });
        }
    };
}]);