﻿mainApp.controller('ActSiteCheckItemCtrlr', ['$scope', '$state', '$ionicPlatform', 'JobsSvc', '$cordovaCamera', '$cordovaFile', '$filter', '$ionicPopup', 'PhotoSvc', function ($scope, $state, $ionicPlatform, JobsSvc, $cordovaCamera, $cordovaFile, $filter, $ionicPopup, PhotoSvc) {
    $scope.$on('$ionicView.enter', function () {
        $scope.fields = JobsSvc.job.siteCheckItem;
        $scope.fields.complete = JobsSvc.job.CurrentActivity.complete;
        $scope.checkedWithIssuesStyle = $scope.fields.checkedWithIssues ? { color: "#ef473a" } : {};
        $scope.checkedStyle = $scope.fields.checked ? { color: "#33cd5f" } : {};
        $scope.jobName = JobsSvc.job.jobName;
        if ($scope.fields.getPhoto){
            $scope.GetPhoto();
        }
    });

    $scope.Done = function () {
        $state.go('actsitecheck');
    };
    $scope.GetPhoto = function () {        
            $scope.photoSource = null;
            PhotoSvc.getPhoto().then(function (photo) {
                console.log(photo);
                $scope.fields.photos.push(photo);
            }, function(error){
                alert('Something went wrong while getting a photo.');
            });         
    };
    $scope.setCheckedWithIssues = function () {
        $scope.fields.checkedWithIssues = true;
        $scope.fields.checked = false;
        $scope.checkedWithIssuesStyle = { color: "#ef473a" };
        $scope.checkedStyle = {};
    };
    $scope.setChecked = function () {
        $scope.fields.checked = true;
        $scope.fields.checkedWithIssues = false;
        $scope.checkedWithIssuesStyle = {};
        $scope.checkedStyle = { color: "#33cd5f" };
    };
    $scope.removePhoto = function(index){
        $scope.fields.photos.splice(index, 1);
    };
}]);