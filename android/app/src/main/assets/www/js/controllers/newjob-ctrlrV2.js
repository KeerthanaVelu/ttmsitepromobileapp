﻿mainApp.controller('NewJobCtrlrV2', ['$scope', '$state', '$ionicPopup', '$ionicLoading', '$q', 'RCASvc', 'JobsSvc', 'ContractorSvc', 'AssetOwnerSvc', '$resource', '$http', 'AccountSvc', 'ModalFactory', 'ContractorsSvc', 'AssetOwnersSvc', '$linq',
                            function ($scope, $state, $ionicPopup, $ionicLoading, $q, RCASvc, JobsSvc, ContractorSvc, AssetOwnerSvc, $resource, $http, AccountSvc, ModalFactory, ContractorsSvc, AssetOwnersSvc, $linq) {

    $scope.$on('$ionicView.enter', function () {
        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>'});

        RCASvc.getAll().then(function(response){
            $scope.RCAs = response;
            $scope.job = angular.copy(NewJobVM);
            $scope.job.tenantId = AccountSvc.getTenantIdV2();
            $scope.job.branchId = AccountSvc.getBranchIdV2();
            $scope.job.assignedUserId = AccountSvc.getUserIdV2();
            //$scope.job.jobName = "Test Job " + (new Date()).valueOf();//remove when deploying to test or production
            $scope.truckNames = [];
            $scope.roadLevels = angular.copy(RoadLevels);
            $scope.job.expectedDaysDuration = 1;
            $ionicLoading.hide();
        });

        $("#signature_pad_new").empty();
        $('#signature_pad_new').jSignature({ height: 249, width: window.innerWidth - 37 });
        $('#signature_pad_new').jSignature('clear');
   
    });
    
    function valid() {
        var valid = true;
        var message = '';
        if ($scope.job.jobName == null || $scope.job.jobName.length == 0)
            message += "Job Name is required.<br/>";        
        
        if (message.length > 0) {
            $ionicPopup.alert({ title: "Error", template: message });
            valid = false;
        }

        return valid;
    }

    $scope.gplacesOptions = {componentRestrictions: {country:'nz'}, types:['geocode']};

    $scope.done = function () {
        if (!valid())
            return;

        if ($scope.job.trucks == null || $scope.job.trucks.length == 0) {
            $ionicPopup.alert({ title: 'Site Pro', template: 'Please choose 1 or more trucks.' });
            return;
        }

        if ($scope.job.fences == true && (($scope.job.fencesQuantity == 0 || $scope.job.fencesQuantity == undefined) || ($scope.job.fencesPrice == 0 || $scope.job.fencesPrice == undefined))) {

            $ionicPopup.alert({ title: 'Site Pro', template: 'Fences/Gates is checked but quantity OR price are not added. Please add them and try again!' });
            return;
        }

        let selectedRoadLevels = $linq.Enumerable().From($scope.roadLevels).Where('w => w.checked == true').Select('s => s.name').ToArray();
        $scope.job.roadLevels = selectedRoadLevels;
        $scope.job.IsCreatedFromApp = true;
        $ionicLoading.show({ template: '<ion-spinner></ion-spinner>'});

        if($scope.job.address && $scope.job.address.formatted_address)
            $scope.job.address = $scope.job.address.formatted_address;

        if ($scope.job.id == null) {
            //add user as default team member and stms            
            let teamMember = angular.copy(TeamMemberVM);
            teamMember.id = (new ObjectId()).toString();
            teamMember._id = objectTypes.teammember + teamMember.id;
            teamMember.staffId = AccountSvc.getUserIdV2();
            teamMember.tenantId = AccountSvc.getTenantIdV2();
            teamMember.isSTMS = true;
            $scope.job.teamMembers.push(teamMember);

            let signatureDataBase30 = $('#signature_pad_new').jSignature('getData', 'base30');
            $scope.job.parties.onsiteContactSignature = signatureDataBase30[1];
            
            JobsSvc.addV2($scope.job).then(function(response){
                $state.go('app.jobdetail', {id: response});
            }, function(error){
                console.log(error);
                alert('Something went wrong. We apologize for the inconvenience.');
            }).finally(function(response){
                console.log(response);
                $ionicLoading.hide();
            });
        }
    };
    $scope.Cancel = function () {
        $state.go('app.jobs');
    };
    $scope.getOtherParties = function(rcaId){
        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>'});
        $q.all({contractors: ContractorsSvc.getAllByRca(rcaId), assetOwners: AssetOwnersSvc.getAllByRca(rcaId)}).then(function(response){
            $scope.contractors = response.contractors;
            $scope.assetOwners = response.assetOwners;
            $ionicLoading.hide();            
        });
    };        
    
    $scope.GetBranches = function (contractorId) {
        if (contractorId == null) {
            $scope.contractorBranches = null;
            $scope.projectManagers = null;
            return;
        }
        var contractor = $linq.Enumerable().From($scope.contractors).FirstOrDefault(null, 'f => f.id == "' + contractorId + '"');
        $scope.contractor = contractor;
        $scope.contractorBranches = contractor.branches;
    };

    $scope.GetProjectManagers = function (contractorBranchId) {
        if (contractorBranchId == null) {
            $scope.projectManagers = null;
            return;
        }

        var contractorBranch = $linq.Enumerable().From($scope.contractorBranches).FirstOrDefault(null, 'f => f.id == "' + contractorBranchId + '"');
        $scope.projectManagers = contractorBranch.projectManagers;
    };


    $scope.GetProjectManagersNumber = function (projectManagerId) {
        if (projectManagerId == null) {
            $scope.projectManagerNumber = "";
            return;
        }
        var projectManagerNumber = $linq.Enumerable().From($scope.projectManagers).FirstOrDefault(null, 'f => f.id == "' + projectManagerId + '"');
        $scope.job.parties.projectManagerNumber = projectManagerNumber.mobile;
    };



    $scope.GetAssetOwners = function (rcaId) {
        if(rcaId != null)
            $scope.assetOwners = AssetOwnerSvc.GetAllByRCA(rcaId);
    };

    $scope.setTestParties = function () {
        var defer = $q.defer();

        $scope.RCAs = RCASvc.GetAll();

        var rca = $scope.RCAs[0];
        $scope.rcaId = rca.id;
        $scope.GetOtherParties(rca.id);
        $scope.contractors = ContractorSvc.GetAllByRCA(rca.id);
        $scope.contractor = $scope.contractors[0];
        $scope.contractorBranches = $scope.contractors[0].branches;
        $scope.contractorBranch = $scope.contractorBranches[0];
        $scope.GetProjectManagers($scope.contractorBranch);
        $scope.job.projectManagerId = $scope.projectManagers[0].id;
        $scope.job.assetOwnerId = $scope.assetOwners[0].id;
        defer.resolve(true);

        return defer.promise;
    };

    $scope.chooseTrucks = function () {
        if ($scope.job.trucks == null)
            $scope.job.trucks = [];

        ModalFactory.showTrucksSelection($scope.job.trucks).then(function (result) {
            if (result) {
                //set the trucks
                let trucks = [];
                result.trucks.forEach(id => {
                    trucks.push({truckId: id, useType: null});
                });
                $scope.job.trucks = trucks;
                $scope.truckNames = result.truckNames;
            }
        }, function (error) {
            console.log(error);
        });
    };

    $scope.clearSignaturePad = function () {
        $('#signature_pad_new').jSignature('clear');
    };
}]);