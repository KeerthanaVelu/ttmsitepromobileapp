﻿mainApp.controller('TeamMemberCtrlr', ['$scope', '$state', '$ionicModal', '$linq', 'JobsSvc', 'StaffSvc', 'AccountSvc', function ($scope, $state, $ionicModal, $linq, JobsSvc, StaffSvc, AccountSvc) {
    $ionicModal.fromTemplateUrl('views/findteammember.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        $scope.modal = modal;
    });    

    $scope.$on('$ionicView.enter', function () {
        loadTeam();
    });

    function loadTeam() {
        var staff = StaffSvc.get();
        var memberIds = $linq.Enumerable().From(JobsSvc.job.team).Select('val, i => val.memberId ').ToArray();
        $scope.members = [];
        $scope.members = StaffSvc.getByIds(memberIds);
        angular.forEach($scope.members, function (value, key) {
            var branch = $linq.Enumerable().From(AccountSvc.branches).FirstOrDefault(null, 'f => f.id == "' + value.branchId + '"');
            if (branch != null)
                value.branchName = branch.branch;
        });
    };
    $scope.loadTeam = function () {
        loadTeam();
    };
    $scope.find = function () {
        $scope.modal.show();
    };
    $scope.new = function () {
        $state.go('newteammember');
    };
    $scope.cancel = function () {
        $state.go('team');
    };
    $scope.done = function () {
        $state.go('team');
    };
    $scope.hide = function () {
        $scope.modal.hide();
    };
}]);

mainApp.controller('FindTeamMemberCtrlr', ['$scope', '$linq', 'StaffSvc', 'JobsSvc', 'AccountSvc', '$ionicLoading', function ($scope, $linq, StaffSvc, JobsSvc, AccountSvc, $ionicLoading) {
    $scope.$on('modal.shown', function () {
        $scope.loadStaff();
        $ionicLoading.hide();
    });
    $scope.search = { fullName: '' };
    $scope.branchId = '';
    $scope.close = function () {
        $scope.hide();
        $scope.loadTeam();
    };
    $scope.branches = AccountSvc.getBranches();
    //$scope.members = angular.copy(StaffSvc.get());
    $scope.loadStaff = function () {
        $scope.members = angular.copy(StaffSvc.get());//StaffSvc.getAllInactive();        
        //var selectedStaffIds = $linq.Enumerable().From(JobsSvc.job.teamMembers).Select('s => s.staffId').ToArray();
        var openMembers = $linq.Enumerable().From(JobsSvc.job.teamMembers).Select('s => s.endOfShift == null').ToArray();
        var selectedStaffIds = $linq.Enumerable().From(openMembers).Select('s => s.staffId').ToArray();
        angular.forEach(selectedStaffIds, function (value, key) {
            console.log(value);
            var exist = $linq.Enumerable().From($scope.members).FirstOrDefault(null, 'f => f.id == "' + value + '"');
            if(exist != null){
                var existIndex = $scope.members.indexOf(exist);
                if(existIndex >= 0)
                    $scope.members.splice(existIndex, 1);
            }
        });        
    };
    $scope.addMember = function (member) {
        if (JobsSvc.job.teamMembers == null)
            JobsSvc.job.teamMembers = [];
        member.active = true;

        var timeLogStart = null;
        if (JobsSvc.job.activities && JobsSvc.job.activities.length > 0) {
            timeLogStart = { isTeamMemberTimelog: true, jobId: JobsSvc.job.id, staffMemberId: member.id, timeLogType: 0, time: new Date() };
            //timeLogStart = { jobId: JobsSvc.job.id, timeLogType: 0, time: new Date() };
        }

        //var newTeamMember = { staffMemberId: member.id, timeLogs: [] };
        var newTeamMember = { staffId: member.id, timeLogs: [] };
        if(timeLogStart != null)
            newTeamMember.timeLogs.push(timeLogStart);
        JobsSvc.job.teamMembers.push(newTeamMember);
        $scope.members.splice($scope.members.indexOf(member), 1);        
        JobsSvc.job.teamChanged = true;
        JobsSvc.save();
    };
    $scope.getBranchName = function (member) {
        var branch = $linq.Enumerable().From($scope.branches).FirstOrDefault(null, 'f => f.id == "' + member.branchId + '"');
        return branch != null ? branch.branch : '';
    };
}]);