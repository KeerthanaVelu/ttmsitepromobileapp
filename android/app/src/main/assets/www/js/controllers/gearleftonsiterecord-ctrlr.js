﻿mainApp.controller('GearLeftOnSiteRecordCtrlr', ['$scope', '$state', '$ionicPopup', 'JobsSvc', '$linq', 'UploadSvc', '$ionicLoading', 'LocationSvc', 'WebApiQSvc', '$q', '$stateParams', 'DBSvc', 'SiteCheckAlarmSvc', 'VoiceToTextSvc', '$ionicHistory',  'TempSvc',
    function ($scope, $state, $ionicPopup, JobsSvc, $linq, UploadSvc, $ionicLoading, LocationSvc, WebApiQSvc, $q, $stateParams, DBSvc, SiteCheckAlarmSvc, VoiceToTextSvc, $ionicHistory, TempSvc) {

    $scope.$on('$ionicView.enter', function () {
        if(!$scope.activity){
            $ionicLoading.show({ template: '<ion-spinner></ion-spinner>' });
            $scope.isJobArchived = $stateParams.isJobArchived;
            JobsSvc.getActivity($stateParams.jobid, $stateParams.id).then(function(response){
                console.log('[8]:');
                console.log(response);
                $scope.activity = response;
                $scope.activityHeaders = {};
                $scope.activityHeaders.name = ActivityTypeEnum.props[$scope.activity.activityType].name;
                $scope.activityHeaders.jobName = $stateParams.jobName;
                $scope.isReadonly = $scope.activity.endTime !=null ? true :false;
                $ionicLoading.hide();
            });
        }
    });
        $scope.AlertMessage = function () {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Site Pro',
                template: 'Description and Qty is required',
                okText: 'Yes',
                cancelText: 'No'
            });
            confirmPopup.then(function (res) {
                if (res) {

                }
            });
        };
        $scope.addLevel1 = function () {
            var newAddress = { description: '', qty: 0 };
            if ($scope.activity.level1Items.length > 0) {
                let Valid = false;
                $scope.activity.level1Items.forEach(function (el) {
                    if (Valid == false) {
                        if ((el.description == "" || el.description == null) || (el.qty == null || el.qty == "" || el.qty == 0)) {
                            Valid = true; 
                        }
                    }                   
                });
                if (Valid == false) {
                    $scope.activity.level1Items.push(newAddress);
                } else {
                    $scope.AlertMessage();
                }
            } else {
                $scope.activity.level1Items.push(newAddress);
            }
                      
        }
        $scope.addLevel2 = function () {
            var newAddress = { description: '', qty: 0 };
            if ($scope.activity.level2Items.length > 0) {
                let Valid = false;
                $scope.activity.level2Items.forEach(function (el) {
                    if (Valid == false) {
                        if ((el.description == "" || el.description == null) || (el.qty == null || el.qty == "" || el.qty == 0)) {
                            Valid = true;
                        }
                    }
                });
                if (Valid == false) {
                    $scope.activity.level2Items.push(newAddress);
                } else {
                    $scope.AlertMessage();
                }
            } else {
                $scope.activity.level2Items.push(newAddress);
            }          
        }
        $scope.addAccessories = function () {
            var newAddress = { description: '', qty: 0 };
            if ($scope.activity.accessoriesItems.length > 0) {
                let Valid = false;
                $scope.activity.accessoriesItems.forEach(function (el) {
                    if (Valid == false) {
                        if ((el.description == "" || el.description == null) || (el.qty == null || el.qty == "" || el.qty == 0)) {
                            Valid = true;
                        }
                    }
                });
                if (Valid == false) {
                    $scope.activity.accessoriesItems.push(newAddress);
                } else {
                    $scope.AlertMessage();
                }
            } else {
                $scope.activity.accessoriesItems.push(newAddress);
            } 
        }

    $scope.done = function () {
        $ionicLoading.show({ template: '<ion-spinner></ion-spinner>' });
        var posOptions = { timeout: 5000, enableHighAccuracy: true };
        var coordinates = null;

        LocationSvc.getCurrentPosition(posOptions).then(function (position) {
            //console.log(position);
            coordinates = position;
        }, function (error) {
            console.log(error);
            coordinates = error;
        }).finally(function () {
            console.log($scope.activity);
            $ionicLoading.hide();
            if (coordinates.coords) {
                $scope.activity.endCoordinates = { latitude: coordinates.coords.latitude, longitude: coordinates.coords.longitude };
            }
            let Valid = false;
            if ($scope.activity.level1Items.length == 0 && $scope.activity.level2Items.length == 0 && $scope.activity.accessoriesItems.length == 0)
                Valid = true;

            $scope.activity.level1Items.forEach(function (el) {
                if (Valid == false) {
                    if ((el.description == "" || el.description == null) || (el.qty == null || el.qty == "" || el.qty == 0)) {
                        Valid = true;
                    }
                }
            });
            $scope.activity.level2Items.forEach(function (el) {
                if (Valid == false) {
                    if ((el.description == "" || el.description == null) || (el.qty == null || el.qty == "" || el.qty == 0)) {
                        Valid = true;
                    }
                }
            });
            $scope.activity.accessoriesItems.forEach(function (el) {
                if (Valid == false) {
                    if ((el.description == "" || el.description == null) || (el.qty == null || el.qty == "" || el.qty == 0)) {
                        Valid = true;
                    }
                }
            });
            if (Valid == false) {
                $scope.activity.endTime = new Date();
                JobsSvc.stopActivity($stateParams.jobid, $scope.activity).then(function (response) {
                    if ($scope.activity.activityType === ActivityTypeEnum.OnSiteRecord) {
                        SiteCheckAlarmSvc.reset();
                    }
                    $ionicLoading.hide();
                    $state.go('app.jobdetail', { id: $stateParams.jobid, showAddActivity: true });
                    //$ionicHistory.goBack();
                });
            } else {
                $scope.AlertMessage();
            }            
        });        
    };
    $scope.cancel = function () {
        var confirmPopup = $ionicPopup.confirm({
            title: 'Site Pro',
            template: 'Are you sure you want to cancel this activity?',
            okText: 'Yes',
            cancelText: 'No'
        });

        confirmPopup.then(function (res) {
            if (res) {    
                JobsSvc.cancelActivity($stateParams.jobid, $scope.activity.id).then(function(response){
                    $ionicLoading.hide();
                    $state.go('app.jobdetail', {id: $stateParams.jobid, showAddActivity: true});
                });                
            }
        });
    };

    $scope.back = function () {
        $state.go('app.jobdetail', {id: $stateParams.jobid, showAddActivity: false});
    };  
}]);