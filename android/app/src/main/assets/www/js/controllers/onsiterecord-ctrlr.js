﻿mainApp.controller('OnSiteRecordCtrlr', ['$scope', '$state', '$ionicPopup', 'JobsSvc', '$linq', 'UploadSvc', '$ionicLoading', 'LocationSvc', 'WebApiQSvc', '$q', '$stateParams', 'DBSvc', 'SiteCheckAlarmSvc', 'VoiceToTextSvc', '$ionicHistory',  'TempSvc',
                                function ($scope, $state, $ionicPopup, JobsSvc, $linq, UploadSvc, $ionicLoading, LocationSvc, WebApiQSvc, $q, $stateParams, DBSvc, SiteCheckAlarmSvc, VoiceToTextSvc, $ionicHistory, TempSvc) {
    $scope.$on('$ionicView.enter', function () {
        if(!$scope.activity){
            $ionicLoading.show({ template: '<ion-spinner></ion-spinner>' });
            $scope.isJobArchived = $stateParams.isJobArchived;
            JobsSvc.getActivity($stateParams.jobid, $stateParams.id).then(function(response){
                console.log('[8]:');
                console.log(response);
                $scope.activity = response;
                $scope.activityHeaders = {};
                $scope.activityHeaders.name = ActivityTypeEnum.props[$scope.activity.activityType].name;
                $scope.activityHeaders.jobName = $stateParams.jobName;
                $ionicLoading.hide();
            });
        }
    });

    $scope.itemDetails = function (item) {
        TempSvc.item = item;
        let ndx = $scope.activity.siteCheckItems.indexOf(item);
        $state.go('app.onsiterecorditem', {id: $stateParams.id, jobid: $stateParams.jobid, jobName: $stateParams.jobName, itemIndex: ndx, isJobArchived: $scope.isJobArchived, isComplete: $scope.activity.endTime != null});
    };
    $scope.done = function () {
        $ionicLoading.show({ template: '<ion-spinner></ion-spinner>' });
        var posOptions = { timeout: 5000, enableHighAccuracy: true };
        var coordinates = null;

        LocationSvc.getCurrentPosition(posOptions).then(function (position) {
            //console.log(position);
            coordinates = position;
        }, function (error) {
            console.log(error);
            coordinates = error;
        }).finally(function () {
            console.log($scope.activity);
            $ionicLoading.hide();
            $scope.activity.endTime = new Date();
            if (coordinates.coords) {
                $scope.activity.endCoordinates = { latitude: coordinates.coords.latitude, longitude: coordinates.coords.longitude };
            }
            JobsSvc.stopActivity($stateParams.jobid, $scope.activity).then(function(response){
                if($scope.activity.activityType === ActivityTypeEnum.OnSiteRecord){
                    SiteCheckAlarmSvc.reset();
                }
                $ionicLoading.hide();
                $state.go('app.jobdetail', {id: $stateParams.jobid, showAddActivity: true});
                //$ionicHistory.goBack();
            });
        });        
    };
    $scope.cancel = function () {
        var confirmPopup = $ionicPopup.confirm({
            title: 'Site Pro',
            template: 'Are you sure you want to cancel this activity?',
            okText: 'Yes',
            cancelText: 'No'
        });

        confirmPopup.then(function (res) {
            if (res) {    
                JobsSvc.cancelActivity($stateParams.jobid, $scope.activity.id).then(function(response){
                    $ionicLoading.hide();
                    $state.go('app.jobdetail', {id: $stateParams.jobid, showAddActivity: true});
                });                
            }
        });
    };
    
    $scope.back = function () {
        $state.go('app.jobdetail', {id: $stateParams.jobid, showAddActivity: false});
        //$ionicHistory.goBack();
    };
    
    $scope.getPhoto = function (item) {
        TempSvc.item = item;
        let ndx = $scope.activity.siteCheckItems.indexOf(item);
        $state.go('app.onsiterecorditem', { jobid: $stateParams.jobid, id: $stateParams.id, jobName: $stateParams.jobName, itemIndex: ndx});
    };
    
    $scope.setCheckedWithIssues = function (item) {
        item.checkedWithIssues = true;
        item.checked = false;
        item.notApplicable = false;
    };
    $scope.setChecked = function (item) {
        item.checked = true;
        item.checkedWithIssues = false;
        item.notApplicable = false;
    };
    $scope.setNotApplicable = function(item){
        item.notApplicable = true;
        item.checked = false;
        item.checkedWithIssues = false;
    }
    
    $scope.voiceToText = function () {
        VoiceToTextSvc.Capture().then(function (result) {
            console.log(result);
            if ($scope.activity.notes == null)
                $scope.activity.notes = '';
            if ($scope.activity.notes.length > 0)
                $scope.activity.notes += ' ' + result[0];
            else
                $scope.activity.notes += result[0];
        }, function (error) {
            console.log(error);
        });        
    };
}]);