﻿mainApp.controller('ChooseTruckCtrlr', ['$scope', 'TrucksSvc', '$linq', '$ionicPopup', '$ionicLoading', 'parameters', 'AccountSvc', function ($scope, TrucksSvc, $linq, $ionicPopup, $ionicLoading, parameters, AccountSvc) {
    $scope.$on('modal.shown', function(event){
        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>'});
        AccountSvc.getBranchesV2().then(function(branches){
            $scope.branches = branches;
            $scope.option.branchId = AccountSvc.getBranchIdV2();
            TrucksSvc.getAll().then(function(trucks){                
                $scope.tenantTrucks = trucks;
                $ionicLoading.hide();
            }, function(error){
                console.log(error);
                $ionicLoading.hide();
            });
        });
    });
    $scope.selectedTrucks = parameters.selectedTrucks || [];
    $scope.selectedCount = 0;
    $scope.option = {search:null};
    $scope.cancel = function () {
        $scope.closeModal(null);
    };
   $scope.chooseTruck = function(truck){
        $scope.closeModal(truck);
   };
    $scope.refreshTrucks = function () {
        //return;
        $ionicLoading.show({ template: '<ion-spinner></ion-spinner>', hideOnStateChange: true });
        TrucksSvc.refreshData().then(function (result) {
            //$scope.tenantTrucks = angular.copy(TrucksSvc.getAll());
            TrucksSvc.getAll().then(function(trucks){                
                $scope.tenantTrucks = trucks;
                $ionicLoading.hide();
            }, function(error){
                console.log(error);
                $ionicLoading.hide();
            });
        }, function (error) {
            $ionicPopup.alert({ title: 'Site Pro', template: 'Unable to refresh trucks data.' });
        }).finally(function () {
            $ionicLoading.hide();
        });
    };
}]);