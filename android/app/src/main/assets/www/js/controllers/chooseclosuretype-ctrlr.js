﻿mainApp.controller('ChooseClosureTypeCtrlr', ['$scope', '$state', '$ionicPopup', 'ClosureTypeSvc', 'JobsSvc', 'parameters', '$linq', function ($scope, $state, $ionicPopup, ClosureTypeSvc, JobsSvc, parameters, $linq) {
    $scope.$on('modal.shown', function(){
        let closureTypes = ClosureTypeSvc.GetAll();
        closureTypes.push('Other Type');
        let closureTypesList = [];
        let addedClosureTypes = parameters.addedClosureTypes;
        let closureTypesOptionParam = parameters.closureTypesOption;
        closureTypes.forEach(item => {
            let selected = false;
            if(addedClosureTypes)
                selected = $linq.Enumerable().From(addedClosureTypes).Contains(item);
            if(item == 'Other Type')
                closureTypesList.push({name: item, selected: false, otherType: null});
            else
                closureTypesList.push({ name: item, selected: selected });
        });
        
        $scope.closureTypes = closureTypesList;
        $scope.UnattendedSelection = false;
        $scope.NightSelection = false;
        if (closureTypesOptionParam == true || closureTypesOptionParam === "Unattended") {
            $scope.UnattendedSelection = true;
            $scope.NightSelection = false;
        } else if (closureTypesOptionParam === "Night") {
            $scope.UnattendedSelection = false;
            $scope.NightSelection = true;
        }

        $scope.closureTypesOptionList = [{ Selected: $scope.UnattendedSelection, Value: "Unattended" }, { Selected: $scope.NightSelection, Value: "Night" }];
    });
    
    $scope.IsOtherType = false;
    $scope.OtherType = { Selected: false, Value: null };
    
    $scope.closureTypesOption = "";
    $scope.addedClosureTypes = angular.copy(parameters);

    //if (closureTypesOption === true) {
    //    for (var x of $scope.closureTypesOptionList) {
    //        if (x.Value === "Unattended") {
    //            x.Selected = true;
    //        }
    //    }
    //}

    $scope.done = function () {

        let closureTypeOptionList = $linq.Enumerable().From($scope.closureTypesOptionList).Where('w => w.Selected == true').ToArray();
        if (closureTypeOptionList.length > 0) {
            $scope.closureTypesOption = closureTypeOptionList[0].Value;
        }

        let selected = $linq.Enumerable().From($scope.closureTypes).Where('w => w.selected == true').ToArray();
        if(selected.length == 0){
            $ionicPopup.alert({ title: "Closure Type", template: 'Please choose closure type(s).' });
            return;
        }
        let otherType = $linq.Enumerable().From(selected).FirstOrDefault(null, 'w => w.name == "Other Type"');
        if (otherType && !otherType.otherType) {
            $ionicPopup.alert({ title: "Closure Type", template: 'Please specify other type.' });
            return;
        }
        let closureTypes = $linq.Enumerable().From(selected).Select('s => s.name').ToArray();
        
        if(otherType){
            let oTNdx = closureTypes.indexOf('Other Type');
            closureTypes.splice(oTNdx, 1);
            closureTypes.push(otherType.otherType);
        }
        $scope.closeModal({ closureTypes: closureTypes, closureTypesOption: $scope.closureTypesOption });
    };
    $scope.ToggleClosure = function (closure) {
        if ($scope.addedClosureTypes.indexOf(closure) >= 0)
            $scope.addedClosureTypes.splice($scope.ClosureTypes.indexOf(closure), 1);
        else
            $scope.addedClosureTypes.push(closure);
    };
    $scope.cancel = function () {        
        $scope.closeModal(null);
    };
    $scope.SetTitle = function () {
        switch (JobsSvc.job.CurrentActivity.activityId) {
            case 2:
            case 5:
            case 7:
                return "Active Closure Type";
                break;
            case 8:
            case 8.1:
                return "Unattended Closure Type";
                break;
        }        
    };
    $scope.setOtherClosureType = function(closure){
        if(closure.otherType.length > 0)
            closure.selected = true;
        else closure.selected = false;            
    };

    $scope.closureTypesOptionChange = function (item) {
        for (var x of $scope.closureTypesOptionList) {
            if (x.Value === item.Value) {
                x.Selected = item.Selected;
            } else if (item.Selected === true && x.Value !== item.Value) {
                x.Selected = false;
            }
        }
    };

    
}]);

mainApp.controller('ChooseToolboxbriefCtrlr', ['$scope', '$state', '$ionicPopup', 'ClosureTypeSvc', 'JobsSvc', 'parameters', '$linq', '$ionicLoading', 'LocationSvc',
    function ($scope, $state, $ionicPopup, ClosureTypeSvc, JobsSvc, parameters, $linq, $ionicLoading, LocationSvc) {
    $scope.$on('modal.shown', function () {

        $scope.testcommunication = true;
        $scope.ppe = false;
        $scope.isfootpath = false;
        $scope.isfootpath = false;
        //$scope.isfootpath = false;

        $scope.Instructions = {
            testcommunication: true,
            ppe : false,
            isfootpath : false,
            isExcessWeight : false,
            isStepOffTruck : false,
        };
    });

    $scope.IsOtherType = false;
    $scope.OtherType = { Selected: false, Value: null };

    $scope.closureTypesOption = "";
    $scope.addedClosureTypes = angular.copy(parameters);

    $scope.done = function () {
        $ionicLoading.show({ template: '<ion-spinner></ion-spinner>' });
        var posOptions = { timeout: 5000, enableHighAccuracy: true };
        var coordinates = null;

        LocationSvc.getCurrentPosition(posOptions).then(function (position) {
            //console.log(position);
            coordinates = position;
        }, function (error) {
            console.log(error);
            coordinates = error;
        }).finally(function () {
            console.log($scope.Instructions);
            $ionicLoading.hide();
            $scope.activity.endTime = new Date();
            //if (coordinates.coords) {
            //    $scope.activity.endCoordinates = { latitude: coordinates.coords.latitude, longitude: coordinates.coords.longitude };
            //}
            //JobsSvc.stopActivity($stateParams.jobid, $scope.activity).then(function (response) {
            //    if ($scope.activity.activityType === ActivityTypeEnum.OnSiteRecord) {
            //        SiteCheckAlarmSvc.reset();
            //    }
            //    $ionicLoading.hide();
            //    $state.go('app.jobdetail', { id: $stateParams.jobid, showAddActivity: true });
            //    //$ionicHistory.goBack();
            //});
        });
       // $scope.closeModal({ closureTypesOption: $scope.Instructions });
    };
    $scope.ToggleClosure = function (closure) {
        if ($scope.addedClosureTypes.indexOf(closure) >= 0)
            $scope.addedClosureTypes.splice($scope.ClosureTypes.indexOf(closure), 1);
        else
            $scope.addedClosureTypes.push(closure);
    };
    $scope.cancel = function () {
        $scope.closeModal(null);
    };
    $scope.SetTitle = function () {
        switch (JobsSvc.job.CurrentActivity.activityId) {
            case 2:
            case 5:
            case 7:
                return "Active Closure Type";
                break;
            case 8:
            case 8.1:
                return "Unattended Closure Type";
                break;
        }
    };
    $scope.setOtherClosureType = function (closure) {
        if (closure.otherType.length > 0)
            closure.selected = true;
        else closure.selected = false;
    };

    $scope.closureTypesOptionChange = function (item) {
        for (var x of $scope.closureTypesOptionList) {
            if (x.Value === item.Value) {
                x.Selected = item.Selected;
            } else if (item.Selected === true && x.Value !== item.Value) {
                x.Selected = false;
            }
        }
    };


}]);