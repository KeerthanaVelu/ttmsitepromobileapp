mainApp.controller('JobDetailsCtrlr', ['$scope', '$stateParams', '$state', '$q', 'JobsSvc', '$ionicLoading', 'TrucksSvc', '$linq', 'ModalFactory', 'LocationSvc', '$ionicPopup', 'TruckAuditSvc', 'TruckReportSvc', 'SiteCheckAlarmSvc', '$ionicPopover', 'ReportingService', 'AccountSvc', 'RemoteUrls', '$ionicModal', '$filter', '$ionicActionSheet', 'StaffSvc', '$resource', 'ContractorsSvc',
    function ($scope, $stateParams, $state, $q, JobsSvc, $ionicLoading, TrucksSvc, $linq, ModalFactory, LocationSvc, $ionicPopup, TruckAuditSvc, TruckReportSvc, SiteCheckAlarmSvc, $ionicPopover, ReportingService, AccountSvc, RemoteUrls, $ionicModal, $filter, $ionicActionSheet, StaffSvc, $resource, ContractorsSvc){
    function getTruckNames() {
        var truckNames = [];
        if ($scope.job.trucks != null && $scope.job.trucks.length > 0) {
            $scope.job.trucks.forEach(truckId => {
                TrucksSvc.get(truckId).then(function(truck){
                    if(truck)
                        truckNames.push(truck.truckNumber + ' - ' + truck.truckRego);
                });                
            });   
        }

        return truckNames;
    }

    function getTruckNames(ids){
        let defer = $q.defer();
        let truckNames = [];
        async.eachSeries(ids, function(id, callback){
            TrucksSvc.get(id).then(function(truck){
                if(truck)
                    truckNames.push(truck.truckNumber + '-' + truck.truckRego);
                callback();
            });
        }).then(function(){
            defer.resolve(truckNames);            
        });

        return defer.promise;
    }

    function getTruckIds(){
        return $linq.Enumerable().From($scope.job.trucks).Select('s => s.truckId').ToArray();
    }

    function siteCheckAlarm(){
        if($scope.job.activities){
            let lastAct = $scope.job.activities[$scope.job.activities.length - 1];
            if(lastAct && lastAct.activityType === ActivityTypeEnum.SiteDeployment){
                if (lastAct.endTime != null) {
                    var diff = moment().diff(moment(lastAct.endTime), 'minutes');
                    //if (diff >= 1) {//test
                    if (diff >= 110) {
                        SiteCheckAlarmSvc.alarmNow();
                        SiteCheckAlarmSvc.start();
                    }
                }
            }
        }
    }

    function getJob(jobId){
        let defer = $q.defer();
        $ionicLoading.show({ template: '<ion-spinner></ion-spinner>'});
        JobsSvc.get(jobId).then(function(job){
            if(job === null){
                defer.resolve(true);
                return;
            }
            console.log(job);
            $scope.job = job;
            if($scope.job.activities){
                var activities = $linq.Enumerable().From($scope.job.activities).Where('w => w.isCancelled == false').ToArray().reverse();
                $scope.job.activities = activities;
            }

             $scope.timeLogsTemplate = "";
            if ($scope.job.teamMembers.length > 0) {
                $scope.job.teamMembers.forEach(teamMember => {

                    var timeLogsStart = $linq.Enumerable().From(teamMember.timeLogs).Where('w => w.timeLogType == 0').ToArray().reverse();
                    var timeLogsEnd = $linq.Enumerable().From(teamMember.timeLogs).Where('w => w.timeLogType == 3').ToArray().reverse();

                    if (timeLogsStart.length > 0) {
                        $scope.timeLogsTemplate += '<div class ="item"><strong>The Team Duration</strong><div>Start Time <span class="pull-right">' + $filter('date')(timeLogsStart[0].time, 'hh:mm a dd-MM-yyyy') + '</span></div>';
                    }
                    if (timeLogsEnd.length > 0) {
                        $scope.timeLogsTemplate += '<div>End Time <span class="pull-right">' + (timeLogsEnd == null ? 'Ongoing' : $filter('date')(timeLogsEnd[0].time, 'hh:mm a dd-MM-yyyy')) + '</span></div></div>';
                    } else {
                        $scope.timeLogsTemplate += '</div>';
                    }

                });

            }
            
            getTruckNames(getTruckIds()).then(function(truckNames){
                $scope.truckNames = truckNames;
                //site check alarm
                siteCheckAlarm();
                $ionicLoading.hide();
                defer.resolve(true);
            });
        }, function(error){            
            $ionicLoading.hide();  
            alert('Something went wrong. We apoligize for the inconvenience.');
            defer.resolve(error);
        });

        return defer.promise;
    }

    $ionicPopover.fromTemplateUrl('views/jobmenuoptions.html', {
        scope: $scope
    }).then(function (popover) {
        $scope.jobMenuOptions = popover;
    });

    $scope.$on('$ionicView.enter', function(event){
        getJob($stateParams.id).then(function(){
            if($stateParams.showAddActivity){
                $scope.addActivity();
            }
        });
    });

    $scope.showMenuOptions = function($event){
        $scope.jobMenuOptions.show($event);
    }
    $scope.defaultOnsiteRecordPdfFilename = 'onsiterecord.pdf';
    $scope.generateOnsiteRecordPdf = function () {
        $scope.jobMenuOptions.hide();

        StaffSvc.getAll().then(function (staffMembers) {
            //console.log(staffMembers);
            $scope.staffList = staffMembers;
            let staff = $linq.Enumerable().From($scope.staffList).FirstOrDefault(null, 'f => f.id == "' + AccountSvc.getUserIdV2() + '"');
            var authHeader = { 'Authorization': 'Bearer ' + AccountSvc.getAccessTokenV2() };
            var jobsResource = $resource(RemoteUrls.rootPath +'/api/tenants/:tenantId/staff/:id?api-version=2.0', {}, {
            'get': { method: 'GET', url: '', isArray: false, headers: authHeader, params: { tenantId : '@tenantId', id: '@id' } }
            });
            jobsResource.get({ tenantId: staff.tenantId, id: staff.id }, function (result) {
            staff.signiture = result.signiture;
            staff.isSignApp = result.isSignApp;

             /* generate pdf using url. */
                        //generate the site check rows
                        var gearLeftOnSiteRecord = $linq.Enumerable().From($scope.job.activities).Where('w => w.isCancelled == false && w.activityType == ' + ActivityTypeEnum.GearLeftOnSiteRecord).ToArray();
                        var onsiteRecords = $linq.Enumerable().From($scope.job.activities).Where('w => w.isCancelled == false && w.activityType == ' + ActivityTypeEnum.OnSiteRecord).ToArray();
                        var onsiteRecords = onsiteRecords.reverse();
                        let tempActivities = $scope.job.activities ? $scope.job.activities.reverse() : [];
                        let actsWithTsls = $linq.Enumerable().From(tempActivities).Where('f => f.activityType === ' + ActivityTypeEnum.SiteDeployment + ' || f.activityType === ' + ActivityTypeEnum.SiteAmendmentRecord +
                            ' || f.activityType === ' + ActivityTypeEnum.SiteRetrievalRecord).ToArray();

                        //get only the tsls of last deployment/amendment/retrieval
                        let tsls = [];
                        if (actsWithTsls.length > 0)
                            tsls = actsWithTsls[actsWithTsls.length - 1].tsls;

                        let rps = '';
                        rps += $scope.job.rpFrom ? 'From: ' + $scope.job.rpFrom : '';
                        rps += $scope.job.rpTo ? ' To: ' + $scope.job.rpTo : '';
                        let onsiteContactSignature = '';
                        if ($scope.job.parties.onsiteContactSignature && $scope.job.parties.onsiteContactSignature.length > 0) {
                            $('#onsitecontact_signature').jSignature({ height: 249, width: window.innerWidth - 37 });
                            $('#onsitecontact_signature').jSignature('importData', 'data:image/jsignature;base30,' + $scope.job.parties.onsiteContactSignature);
                            onsiteContactSignature = $('#onsitecontact_signature').jSignature('getData');
                            //console.log(base64Data);
                        }
                        let stmsSignature = AccountSvc.get().signatureData;
                        if (stmsSignature) {
                            $('#onsitecontact_signature').jSignature('importData', 'data:image/jsignature;base30,' + stmsSignature);
                            stmsSignature = $('#onsitecontact_signature').jSignature('getData');
                }
                var ClientContract = '';
                if ($scope.job.parties.rcaId) {
                    ContractorsSvc.getAllByRca($scope.job.parties.rcaId).then(function (contractors) {
                        $scope.contractors = contractors;
                        var contractor = $linq.Enumerable().From($scope.contractors).FirstOrDefault(null, 'f => f.id == "' + $scope.job.parties.contractorId + '"');
                        ClientContract = contractor != null ? contractor.contractorName  : ""
                    });
                }
                resource = $resource('', null, { 'GetJob': { url: RemoteUrls.getJob, method: 'Get', headers: authHeader, params: { id: '@id' } } });
                resource.GetJob({ id: $scope.job.rId }).$promise.then(function (response) {
                    let truckReport = $linq.Enumerable().From(response.activities).Where('w => w.isCancelled == false && w.truckId != null && w.activityType == ' + ActivityTypeEnum.TruckPrecheckRecord).ToArray();
                    console.log(truckReport, "truckReport");
                    ReportingService.generateOnsiteRecord(onsiteRecords, tsls, $scope.job.reference, $scope.job.address, rps, $scope.job.parties.onsiteContact, onsiteContactSignature, stmsSignature, $scope.job.id, staff, $scope.job.createdOn, truckReport, gearLeftOnSiteRecord, ClientContract);
                }, function (error) {
                    console.log(error);
                });

                        
            }, function (error) {
            console.log(error,"-error-")
            });
        });
    }

    $scope.downloadTmpFiles = function(event){
        event.preventDefault();
        $scope.jobMenuOptions.hide();
        let btnList = [];
        
        if($scope.job.tmpFiles.length > 0){
            $scope.job.tmpFiles.forEach(file => {
                btnList.push({ text: $scope.filenameSplit(file, 0), filename: file });
            });            
        }
        
        //btnList.push({ label: 'Cancel', icon: 'ion-close-round' });

        $ionicActionSheet.show({
            titleText: 'TMP Files',
            cssClass: 'custom-action-sheet',
            buttons: btnList,
            cancelText: 'Cancel',
            cancel: function () {
                // add cancel code..
            },
            buttonClicked: function (btnIndex) {
                console.log(btnIndex);
                if (btnIndex < btnList.length) {
                    let file = btnList[btnIndex];
                    console.log(file.filename);
                    $scope.downloadTmp(file.filename);
                }
            }
        });

        //ons.openActionSheet({
        //    title: 'TMP Files',
        //    cancelable: true,
        //    buttons: btnList
        //}).then(function(btnIndex){
        //    console.log(btnIndex);
        //    if(btnIndex < btnList.length - 1){
        //        let file = btnList[btnIndex];
        //        $scope.downloadTmp(file);
        //    }            
        //});
    };

    $scope.filenameSplit = function (fullfilename, nb) {

        let splitfilename = fullfilename.split('-fnseparator-');
        let filename = splitfilename[nb];

        if (filename == null || filename.length <= 0)
            return fullfilename;
        else
            return filename;
    }

    $scope.downloadTmp = function(file){
        //event.preventDefault();
        $scope.jobMenuOptions.hide();
        $ionicLoading.show({ template: '<ion-spinner></ion-spinner>'});
        var fileTransfer = new FileTransfer();
        var source = encodeURI(RemoteUrls.tmpDownloadPathRoot + file);//$scope.job.tmpFile);
        var target = cordova.file.dataDirectory + file;//$scope.job.tmpFile;
        var localDownloadFolder = 'file:///storage/emulated/0/Download/';//might need to accommodate iOS in the future
        fileTransfer.download(
            source, target,
            function(entry) {
                console.log("download complete: " + entry.toURL());
                console.log(entry);
                window.resolveLocalFileSystemURL(localDownloadFolder, function(dirEntry){
                    var permissions = cordova.plugins.permissions;
                    permissions.requestPermission(permissions.WRITE_EXTERNAL_STORAGE, function(status){
                        console.log(status);
                        entry.copyTo(dirEntry, $scope.job.tmpFile, function(copiedFile){
                            console.log(copiedFile);
                            $ionicLoading.hide();
                            alert('TMP file is downloaded to your Download folder.');
                            cordova.plugins.fileOpener2.showOpenWithDialog(
                                copiedFile.nativeURL,
                                getMimeType(getFileExtension(copiedFile.name)),
                                {
                                    error : function(e) { console.log(e); },
                                    success : function () {},
                                    position : [0, 0]
                                }
                            );
                        }, function(error){
                            $ionicLoading.hide();
                            console.log(error);
                        });
                    }, function(error){
                        $ionicLoading.hide();
                        console.log('error: ');
                        console.log(error);
                        alert('Unable to access local storage.');
                    });                    
                })
            },
            function(error) {
                $ionicLoading.hide();
                console.log("download error source " + JSON.stringify(error));
                alert('Problem while downloading file.');
            },
            false            
        );
    };

    $scope.done = function(){
        $state.go('app.jobs');
    };

    $scope.generalInfo = function(){
        console.log($stateParams);
        $state.go('app.generalinfo', {id: $scope.job.id});
    };

    $scope.chooseTrucks = function () {
        if($scope.job.archived)
            return;
        if ($scope.job.trucks == null)
            $scope.job.trucks = [];

        ModalFactory.showTrucksSelection(getTruckIds()).then(function (result) {
            //console.log(result);
            if (result) {
                let trucks = [];
                result.trucks.forEach(id => {
                    trucks.push({truckId: id, useType: null});
                });
                $scope.job.trucks = trucks;
                $ionicLoading.show({ template: '<ion-spinner></ion-spinner>'});
                getTruckNames(getTruckIds()).then(function(truckNames){
                    $scope.truckNames = truckNames;
                    //update trucks?
                    JobsSvc.updateTrucksV2($scope.job.id, $scope.job.trucks).then(function(response){
                        $ionicLoading.hide();
                    });                    
                });
            }
        }, function (error) {
            //console.log(error);
        });
    };

    $scope.parties = function () {
        console.log($stateParams);
        $state.go('app.parties', {id: $scope.job.id});
    };

    $scope.team = function () {
        //if($scope.job.archived)
            //return;
        $state.go('app.team', {id: $scope.job.id, isJobArchived: $scope.job.archived});
    };

    $scope.generalNotes = function(){
        console.log($stateParams);
        $state.go('app.generalnotes', { id: $scope.job.id });
    };

    function confirmUnattendedSite(activityType){

    }

    function createSiteActivity(closureTypesOption){
        let defer = $q.defer();

        ModalFactory.showClosureTypes(null, closureTypesOption).then(function (response) {
            if(response){
                let result = angular.copy(SiteActivityVM);
                result.addedClosureTypes = response.closureTypes;
                result.closureTypesOption = response.closureTypesOption;
                //console.log(response);
                defer.resolve(result);
            }
            else
                defer.resolve(null);
        });

        return defer.promise;
        }
        function createToolBoxBriefActivity(closureTypesOption) {
            let defer = $q.defer();

            ModalFactory.ToolBoxBrief(null, closureTypesOption).then(function (response) {
                console.log(response);
                if (response) {
                    let result = angular.copy(ToolBoxBriefVM);
                    //result.addedToolBoxBrief = response.closureTypes;
                    //result.closureTypesOption = response.closureTypesOption;
                    //console.log(response);
                    defer.resolve(result);
                }
                else
                    defer.resolve(null);
            });

            return defer.promise;
        }

    function createSiteAmendment(){
        let defer = $q.defer();
        //let confirmUnattended = ons.notification.confirm({ title: 'Site Pro', message: 'Will there be an unattended site?', cancelable: true, buttonLabels:['Yes', 'No'] });
        let confirmUnattended = $ionicPopup.confirm({ title: 'Site Pro', template: "Will there be an unattended site?", cancelText: 'No', okText: 'Yes'});
        confirmUnattended.then(function(confirmUnattendedSite){
            if (typeof confirmUnattendedSite === 'boolean' && $scope.job.activities !== null ){
                //get closure types from last site deployment
                let activities = $scope.job.activities.slice().reverse();
                var lastSiteDeployment = $linq.Enumerable().From(activities).LastOrDefault(null, 'f => f.activityType === ' + ActivityTypeEnum.SiteDeployment)
                var existingClosureTypes = null;
                if(lastSiteDeployment && lastSiteDeployment.addedClosureTypes)
                    existingClosureTypes = lastSiteDeployment.addedClosureTypes;

                var lastDeployment = $linq.Enumerable().From(activities).LastOrDefault(null, 'f => f.activityType === 2 && f.tsls && f.tsls.length > 0');                    
                var lastTsls = null;
                if(lastDeployment && lastDeployment.tsls){
                    lastTsls = lastDeployment.tsls;
                }

                $scope.optionList = [{ addedClosureTypes: existingClosureTypes }, { closureTypesOption: confirmUnattendedSite}];

                ModalFactory.showClosureTypes(existingClosureTypes, confirmUnattendedSite).then(function (response) {
                //ModalFactory.showClosureTypes(null).then(function (response) {
                    if(response){
                        let result = angular.copy(SiteActivityVM);
                        result.addedClosureTypes = response.closureTypes;
                        result.closureTypesOption = response.closureTypesOption;
                        result.unattended = confirmUnattendedSite;// == 0 ? true : false;
                        if(lastTsls)
                            result.tsls = lastTsls;
                        defer.resolve(result);
                    }
                    else
                    defer.resolve(null);
                });    
            }
            else
                defer.resolve(null);
        });

        return defer.promise;
    }
    
    function createSiteRetrievalRecord(){
        function endWorkspaceActive(isActive, jobId, workspaceactive){
            let defer = $q.defer();
            //let posOptions = { timeout: 5000, enableHighAccuracy: true };
            if(!isActive)
                defer.resolve(true);
            else{
                $ionicLoading.show({ template: '<ion-spinner></ion-spinner>' });
                LocationSvc.getCurrentPosition().then(function (position) {
                    //console.log(position);
                    workspaceactive.endCoordinates = { latitude: position.coords.latitude, longitude: position.coords.longitude };
                }, function (error) {
                    //console.log(error);
                    defer.resolve(true);
                }).finally(function () {
                    workspaceactive.endTime = new Date();
                    JobsSvc.stopActivity(jobId, workspaceactive).then(function(response){
                        defer.resolve(true);
                        $ionicLoading.hide();
                    });
                });
            }

            return defer.promise;
        }

        let defer = $q.defer();
        let seqData = {};     
        let tasks = async.seq(function(result, callback){
                let workspaceactive = $linq.Enumerable().From($scope.job.activities).LastOrDefault(null, 'f => f.activityType == 5 && f.endTime == null');
                seqData.workSpaceActivity = workspaceactive;
                if(workspaceactive)
                    callback(null, workspaceactive);
                else
                    callback(null, false);
            },function(workspaceactive, callback){
                if(workspaceactive){
                    let confirmWorkspace = $ionicPopup.confirm({ title: 'Site Pro', template: 'Is your workspace still active?', cancelText: 'No', okText: 'Yes' });
                    confirmWorkspace.then(function(stillActive){
                        if(typeof stillActive === "boolean"){
                            seqData.stillActive = stillActive;                            
                            callback(null, true);
                        }
                        else
                            callback('cancelled', null);
                    });
                }
                else{
                    callback(null, false);
                }          
            }, function(workspaceconfirm, callback){
                //let confirmUnattended = ons.notification.confirm({ title: 'Site Pro', message: 'Will there be an unattended site?', cancelable: true, buttonLabels:['Yes', 'No'] });
                let confirmUnattended = $ionicPopup.confirm({ title: 'Site Pro', template: 'Will there be an unattended site?', cancelText: 'No', okText: 'Yes' });
                confirmUnattended.then(function(isconfirmed){
                    if(typeof isconfirmed === 'boolean'){
                        if(isconfirmed){
                            seqData.unattended = true;
                            callback('siteamendment', seqData);
                        }
                        else if(!isconfirmed){
                            callback(null, seqData);
                        }
                    }
                    else
                        callback('cancelled', seqData);                    
                    /*
                    if(isconfirmed){
                        seqData.unattended = true;
                        callback('siteamendment', seqData);
                    }
                    else if(!isconfirmed)
                        callback(null, seqData);                    
                    else
                        callback('cancelled', seqData);
                    */
                });
            }
            /*
            , function(unattendedconfirm, callback){
                let confirmComplete = $ionicPopup.confirm({ title: 'Site Pro', template: 'Is this job 100% complete ready for final sign-off or will it continue another day?', cancelText: 'No', okText: 'Yes' });
                confirmComplete.then(function (iscomplete) {                    
                    if(typeof iscomplete === "boolean"){
                        seqData.isCompleteConfirmed = iscomplete;
                        callback(null, seqData);                        
                    }
                    else
                        callback('cancelled', seqData);
                });      
            }
            */
        );
        tasks({}, function(confirmRes, result){
            $('body').removeClass('modal-open');
            if(confirmRes == 'cancelled'){
                defer.resolve(null);
            }
            else if(confirmRes == 'siteamendment'){
                let result = angular.copy(SiteActivityVM);
                result.activityType = ActivityTypeEnum.SiteAmendmentRecord;
                result.unattended = seqData.unattended;
                defer.resolve(result);
            }
            else{                
                endWorkspaceActive(result.stillActive, $scope.job.id, result.workSpaceActivity).then(function(response){
                    let newActivity = angular.copy(SiteActivityVM);
                    newActivity.activityType = ActivityTypeEnum.SiteRetrievalRecord;

                    let activities = $scope.job.activities.slice().reverse();
                    var lastClosureTypes = $linq.Enumerable().From(activities).LastOrDefault(null, 'f => f.addedClosureTypes && f.addedClosureTypes.length > 0');                    
                    if(lastClosureTypes && lastClosureTypes.addedClosureTypes){
                        newActivity.addedClosureTypes = lastClosureTypes.addedClosureTypes;
                    }

                    var lastTsls = $linq.Enumerable().From(activities).LastOrDefault(null, 'f => f.tsls && f.tsls.length > 0');                    
                    if(lastTsls && lastTsls.tsls){
                        newActivity.tsls = lastTsls.tsls;
                    }

                    JobsSvc.completeJobV2($scope.job).then(function(response){
                        defer.resolve(newActivity);
                    });
                    //newActivity.complete = result.isCompleteConfirmed;
                    /*                    
                    if(result.isCompleteConfirmed){
                        JobsSvc.completeJobV2($scope.job).then(function(response){
                            defer.resolve(newActivity);
                        });                        
                    }
                    else
                        defer.resolve(newActivity);
                    */
                });
            }
        });

        return defer.promise;
    }

    function createEndOfShift(){
        function addEndTimeLog(lastLogType, member){
            let subDefer = $q.defer();

            if(lastLogType === TimeLogTypeEnum.ENDTIME){
                subDefer.resolve(true);
            }
            else{
                let newLog = angular.copy(TimeLogVM);        
                newLog.jobId = $scope.job.id;
                newLog.rJobId = $scope.job.rId;
                newLog.teamMemberId = member.id;
                newLog.rTeamMemberId = member.rId;
                newLog.time = endOfShiftTime;
                newLog.timeLogType = TimeLogTypeEnum.ENDTIME;
                JobsSvc.addTimeLog(newLog).then(function(response){                    
                    subDefer.resolve(true);
                }, function(error){
                    subDefer.reject(error);
                });
            }
            return subDefer.promise;
        }
        function addDefaultStms(activeTeamMembers){
            let funcDefer = $q.defer();

            let stmsExist = $linq.Enumerable().From(activeTeamMembers).FirstOrDefault(null, 'f => f.staffId == "' + AccountSvc.getUserIdV2() + '"');
            if(activeTeamMembers.length == 0 || !stmsExist){
                let newTeamMember = angular.copy(TeamMemberVM);                
                newTeamMember.id = (new ObjectId()).toString();
                newTeamMember._id = objectTypes.teammember + newTeamMember.id;
                newTeamMember.staffId = AccountSvc.getUserIdV2();
                newTeamMember.jobId = $scope.job.id;
                newTeamMember.tenantId = AccountSvc.getTenantIdV2();
                JobsSvc.addTeamMembers($scope.job.id, [newTeamMember]).then(function(response){
                    funcDefer.resolve(newTeamMember);
                });
            }
            else
                funcDefer.resolve(null);

            return funcDefer.promise;
        }
        let defer = $q.defer();


        // let confirm = $ionicPopup.confirm({ title: 'Site Pro', template: 'Are you sure you want to end this shift?', cancelText: 'No', okText: 'Yes' });
        // confirm.then(function(isconfirmed){            
        //     if(isconfirmed){
                //get time for end of shift
                let tempTime = new Date();
                let endOfShiftTime = new Date(tempTime.getFullYear(), tempTime.getMonth(), tempTime.getDate(), tempTime.getHours(), tempTime.getMinutes(), 0);
                let coordinates = null;
                //var posOptions = { timeout: 5000, enableHighAccuracy: true };
                $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>'});
                LocationSvc.getCurrentPosition().then(function (position) {
                    console.log(position);
                    coordinates = position;
                }, function (error) {
                    console.log(error);
                    coordinates = error;
                }).finally(function () {            
                    JobsSvc.getAllTeam($scope.job.id).then(function(teamMembers){
                        addDefaultStms(teamMembers).then(function(defaultStms){
                            if(defaultStms)
                                teamMembers.push(defaultStms);

                            async.eachSeries(teamMembers, function(member, callback){
                                console.log('[302]:');
                                console.log(member);                    
                                let lastLog = member.timeLogs && member.timeLogs.length ? member.timeLogs[member.timeLogs.length-1] : {timeLogType: null};
                                addEndTimeLog(lastLog.timeLogType, member).then(function(){
                                    member.endOfShiftTime = endOfShiftTime;
                                    JobsSvc.endShiftTeamMember(member).then(function(response){
                                        callback();
                                    });
                                });                    
                            }).then(function(error){
                                if(!error){
                                    let activity = angular.copy(JobActivityVM);
                                    activity.activityType = ActivityTypeEnum.EndOfShift;
                                    activity.jobId = $scope.job.id;
                                    activity.endTime = endOfShiftTime;
                                    if (coordinates.coords) {
                                        activity.endCoordinates = { latitude: coordinates.coords.latitude, longitude: coordinates.coords.longitude };
                                    }
                                    JobsSvc.addSendActivity(activity.jobId, activity).then(function(response){
                                        JobsSvc.archiveJob(activity.jobId).then(function(){
                                            $ionicLoading.hide();
                                            defer.resolve(true);
                                        });                                     
                                    });                        
                                }
                            });
                        });
                    });            
                });
            // }
            // else
            //     defer.resolve(null);
        //});

        return defer.promise;
    }

    function createTruckAudit(activityId){
        let defer = $q.defer();
        TrucksSvc.getAll().then(function(trucks){
            //$scope.trucks = trucks;
            TruckAuditSvc.start(trucks, $scope.job.jobName, null).then(function(audit){
                if(audit){
                    console.log(audit);
                    $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>'});
                    TrucksSvc.saveAudit(audit).then(function(auditId){
                        defer.resolve(auditId);
                        //$ionicLoading.hide();
                        $state.go('app.jobdetail', {id: $scope.job.id});
                    });
                }
                else
                    defer.resolve(null);
                // else
                //     $state.go('app.jobdetail', {id: $scope.job.id});
            });
        });        
        return defer.promise;
    }
        function createTruckReport(activityId){
        let defer = $q.defer();
        TrucksSvc.getAll().then(function(trucks){
            //$scope.trucks = trucks;
            
            TruckReportSvc.start(trucks, $scope.job.jobName, $scope.job.id, null).then(function (truckreport){
                if (truckreport) {
                    truckreport.id = activityId;
                    $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>' });

                    var posOptions = { timeout: 5000, enableHighAccuracy: true };
                    var coordinates = null;

                    LocationSvc.getCurrentPosition(posOptions).then(function (position) {
                        //console.log(position);
                        coordinates = position;
                    }, function (error) {
                        console.log(error);
                        coordinates = error;
                    }).finally(function () {
                        $scope.activity = truckreport;
                        $ionicLoading.hide();
                        if (coordinates.coords) {
                            $scope.activity.endCoordinates = { latitude: coordinates.coords.latitude, longitude: coordinates.coords.longitude };
                        }
                        $scope.activity.endTime = new Date();
                        JobsSvc.stopActivity($scope.job.id, $scope.activity).then(function (response) {
                           
                            $ionicLoading.hide();
                            $state.go('app.jobdetail', { id: $scope.job.id, showAddActivity: true });
                            //$ionicHistory.goBack();
                        });
                    }); 


                    //TrucksSvc.truckreportSave($scope.job.id,truckreport).then(function(auditId){
                    //    defer.resolve(auditId);
                    //    //$ionicLoading.hide();
                    //    $state.go('app.jobdetail', {id: $scope.job.id});
                    //});
                }
                else
                    defer.resolve(null);
                // else
                //     $state.go('app.jobdetail', {id: $scope.job.id});
            });
        });
        return defer.promise;
    }
    function stopTruckAuditActivity(truckAuditId, activityId){
        let defer = $q.defer();
        $ionicLoading.show({ template: '<ion-spinner></ion-spinner>' });
        //var posOptions = { timeout: 5000, enableHighAccuracy: true };
        var coordinates = null;

        LocationSvc.getCurrentPosition().then(function (position) {
            console.log(position);
            coordinates = position;
        }, function (error) {
            console.log(error);
            coordinates = error;
        }).finally(function () {
            JobsSvc.getActivity($scope.job.id, activityId).then(function(activity){
                activity.endTime = new Date();
                //TrucksSvc.getAudit(truckAuditId).then(function(truckAudit){
                    //console.log(truckAudit);
                    //if(truckAudit){
                        activity.truckAuditId = truckAuditId;
                    //}
                    if (coordinates.coords) {
                        activity.endCoordinates = { latitude: coordinates.coords.latitude, longitude: coordinates.coords.longitude };
                    }            
                    JobsSvc.stopActivity($scope.job.id, activity).then(function(response){
                        $ionicLoading.hide();
                        defer.resolve(true);
                    });
                //});                
            });            
        });

        return defer.promise;
    }

    function createActivity(activityType){
        let defer = $q.defer();
        switch(activityType){
            case ActivityTypeEnum.TravelToSite:
            case ActivityTypeEnum.TravelToDepot:
            case ActivityTypeEnum.TravelToNextJob:
            case ActivityTypeEnum.WorkSpaceActive:
            case ActivityTypeEnum.PreStartTruckCheck:
                let newJobActivity = angular.copy(JobActivityVM);
                newJobActivity.activityType = activityType;
                defer.resolve(newJobActivity);
                break;
            case ActivityTypeEnum.TruckPrecheckRecord:
                let newJobActivity1 = angular.copy(TruckPrecheckRecordVM);
                            console.log(newJobActivity1,"newJobActivity1");
                            newJobActivity1.activityType = activityType;
                            defer.resolve(newJobActivity1);
                            break;
            case ActivityTypeEnum.SiteDeployment:            
                createSiteActivity().then(function(activity){
                    if(activity)
                        activity.activityType = activityType;
                    defer.resolve(activity);
                });
                break;
            case ActivityTypeEnum.ToolBoxBriefingRecord:
                createToolBoxBriefActivity().then(function (activity) {
                    if (activity)
                        activity.activityType = activityType;
                    defer.resolve(activity);
                });
                break;
            case ActivityTypeEnum.SiteAmendmentRecord:
                createSiteAmendment().then(function(activity){
                    if(activity){
                        activity.activityType = activityType;
                        defer.resolve(activity);
                    }
                    else
                        defer.resolve(null);
                });
                break;
            case ActivityTypeEnum.SiteHazardAssessment:
                let newShaActivity = angular.copy(SiteHazardAssessmentVM);
                newShaActivity.activityType = activityType;
                defer.resolve(newShaActivity);
                break;
            case ActivityTypeEnum.OnSiteRecord:
                let newOsrActivity = angular.copy(OnSiteRecordVM);
                angular.copy(siteCheckItems, newOsrActivity.siteCheckItems);
                newOsrActivity.activityType = activityType;
                defer.resolve(newOsrActivity);
                break;
            case ActivityTypeEnum.GearLeftOnSiteRecord:
                let newGearOsrActivity = angular.copy(GearLeftOnSiteRecordVM);
                //angular.copy(Level1Items, newGearOsrActivity.Level1Items);
                newGearOsrActivity.activityType = activityType;
                defer.resolve(newGearOsrActivity);
                break;
            case ActivityTypeEnum.SiteRetrievalRecord:
                createSiteRetrievalRecord().then(function(activity){
                    if(activity == null){
                        defer.resolve(null);
                        return;
                    }
                    if(activity.activityType == ActivityTypeEnum.SiteAmendmentRecord){
                        $ionicPopup.alert({title: 'Site Pro', template: 'Please state what closure types you are leaving out.'}).then(function(){
                            createSiteActivity(activity.unattended).then(function(sarActivity){
                                if (sarActivity) {
                                    sarActivity.activityType = ActivityTypeEnum.SiteAmendmentRecord;
                                    sarActivity.unattended = activity.unattended;
                                    defer.resolve(sarActivity);
                                }
                            });
                        });
                    }                        
                    else{
                        activity.activityType = activityType;
                        defer.resolve(activity);
                    }
                });
                break;
            case ActivityTypeEnum.EndOfShift:
                createEndOfShift().then(function(activity){
                    if(activity){
                        activity.activityType = activityType;
                        defer.resolve(activity);
                    }
                    else
                        defer.resolve(null);
                });
                break;
            case ActivityTypeEnum.TMPOnSiteAssessment:
                //let assessment = angular.copy(Assessment);
                //assessment.activityType = activityType;
                defer.resolve({});
                break;
            case ActivityTypeEnum.ShortFormAudit:
                alert('TODO');
                defer.resolve(null);
                break;
        }
        return defer.promise;
    }

    function startActivity(activityType) {
        $ionicLoading.show({ template: '<ion-spinner></ion-spinner>' });
        //var posOptions = { timeout: 5000, enableHighAccuracy: true };
        var coordinates = null;

        LocationSvc.getCurrentPosition().then(function (position) {
            //console.log(position);
            coordinates = position;
        }, function (error) {
            console.log(error);
            coordinates = error;
        }).finally(function () {
            $ionicLoading.hide();
            createActivity(activityType).then(function(activity){
                if(activity){
                    activity.jobId = $scope.job.id;
                    //activity.activityType = activityType;
                    activity.startTime = new Date();
                    if (coordinates.coords) {
                        activity.startCoordinates = { latitude: coordinates.coords.latitude, longitude: coordinates.coords.longitude };
                    }
                    JobsSvc.addSendActivity(activity.jobId, activity).then(function(activityId){
                        switch(activityType){
                            case ActivityTypeEnum.TravelToSite:
                            case ActivityTypeEnum.TravelToDepot:
                            case ActivityTypeEnum.TravelToNextJob:
                            case ActivityTypeEnum.WorkSpaceActive:                            
                                $state.go('app.jobactivity', {id: activityId, jobid: $scope.job.id, jobName: $scope.job.jobName});
                                break;
                            case ActivityTypeEnum.SiteDeployment:
                            case ActivityTypeEnum.SiteAmendmentRecord:
                            case ActivityTypeEnum.SiteRetrievalRecord:
                                $state.go('app.siteactivity', {id: activityId, jobid: $scope.job.id, jobName: $scope.job.jobName});
                                break;
                            case ActivityTypeEnum.ToolBoxBriefingRecord:
                                $state.go('app.siteactivity', { id: activityId, jobid: $scope.job.id, jobName: $scope.job.jobName });
                                break;
                            case ActivityTypeEnum.SiteHazardAssessment:
                                $state.go('app.sitehazardassessment', {id: activityId, jobid: $scope.job.id, jobName: $scope.job.jobName});
                                break;
                            case ActivityTypeEnum.OnSiteRecord:
                                $state.go('app.onsiterecord', {id: activityId, jobid: $scope.job.id, jobName: $scope.job.jobName});
                                break;
                            case ActivityTypeEnum.GearLeftOnSiteRecord:
                                $state.go('app.gearleftonositerecord', { id: activityId, jobid: $scope.job.id, jobName: $scope.job.jobName });
                                break;
                            case ActivityTypeEnum.EndOfShift:
                                //refresh job data
                                $state.go('app.jobdetail', {id: $scope.job.id});
                                break;
                            case ActivityTypeEnum.PreStartTruckCheck:
                            //case ActivityTypeEnum.TruckPrecheckRecord:
                                createTruckAudit().then(function(auditId){
                                    console.log(auditId);
                                    //stop activity
                                    if(auditId)
                                        stopTruckAuditActivity(auditId, activityId).then(function(){
                                            $state.go('app.jobdetail', {id: $scope.job.id, showAddActivity: true});
                                        });
                                    else{//cancelled
                                        JobsSvc.cancelActivity($scope.job.id, activityId).then(function(response){
                                            $ionicLoading.hide();
                                            $state.go('app.jobdetail', {id: $scope.job.id, showAddActivity: true});
                                        });
                                    }
                                });
                                break;
                            case ActivityTypeEnum.TruckPrecheckRecord:
                                createTruckReport(activityId).then(function(auditId){
                                                                console.log(auditId);
                                                                //stop activity
                                                                //if(auditId)
                                                                //    stopTruckAuditActivity(auditId, activityId).then(function(){
                                                                //        $state.go('app.jobdetail', {id: $scope.job.id, showAddActivity: true});
                                                                //    });
                                    if (!auditId)//cancelled
                                    {
                                        JobsSvc.cancelActivity($scope.job.id, activityId).then(function (response) {
                                            $ionicLoading.hide();
                                            $state.go('app.jobdetail', { id: $scope.job.id, showAddActivity: true });
                                        });
                                    }
                                                            });
                                                            break;
                        }                        
                    });           
                }
                else{
                    $scope.addActivity();
                }
            });            
        });
    };    

   function teamMembersHasNotStarted() {
        if ($scope.job.teamMembers.length == 0)
            return true;

        
        let openTeamMembers = $linq.Enumerable().From($scope.job.teamMembers).Where('w => !w.endOfShiftTime && w.dataMode != ' + DataActionEnum.DELETE).ToArray();
        if (openTeamMembers.length == 0)
            return true;
        
        let hasNoTimelogs = $linq.Enumerable().From(openTeamMembers).Any('a => !a.timeLogs || a.timeLogs.length == 0');
        if(hasNoTimelogs)
            return true;

        return false;
    }

    function hasAnyTeamMemberStarted(){        
        let lastLogs = $linq.Enumerable().From($scope.job.teamMembers).Select(s => { 
            return { id: s.id, lastLog: (s.timeLogs ? s.timeLogs[s.timeLogs.length -1] : null) }
        }).ToArray();
        console.log(lastLogs);
        let anyLogNotEnded = $linq.Enumerable().From(lastLogs).Any('a => a.lastLog && a.lastLog.timeLogType < 3');
        return anyLogNotEnded;
    };

    function promptTeamTimeNotStarted(){
        let defer = $q.defer();
        if(teamMembersHasNotStarted()){            
            let teamTimeOption = $ionicPopup.show({
                title: 'Site Pro', template: "You haven't started your team's time yet.",
                buttons: [
                    { text: 'Go Back', type: 'button-positive', onTap: function (e) { return true; } },
                    { text: 'Continue', type: 'button-stable', onTap: function (e) { return false; }}
                ]
            });
            teamTimeOption.then(function (goBack) {
                if (typeof goBack === "boolean") {
                    defer.resolve(goBack);                    
                }
                else
                    defer.resolve(null);
            });
        }
        else
            defer.resolve(false);        

        return defer.promise;
    }

    function getCompletedActivities(){
        let defer = $q.defer();
        async.seq(function(args, callback){
            let activities = $linq.Enumerable().From($scope.job.activities).Where('w => w.endTime').Select('s => s.activityType').Distinct().ToArray();
            callback(null, activities);
        })({}, function(error, result){            
            defer.resolve(result);
        });

        return defer.promise;
    }

    $scope.addActivityModal = null;
    $scope.addActivityModalClose = function(){
        $scope.addActivityModal.hide();
        $scope.addActivityModal.remove();
    };
    $scope.selectedActivity = function(activityType){
        if(activityType && activityType === ActivityTypeEnum.ManageTeam){
            $scope.addActivityModalClose();
            $scope.team();
        }
        else if(activityType && activityType === ActivityTypeEnum.EndOfShift){
            let confirm = $ionicPopup.confirm({ title: 'Site Pro', template: 'Are you sure you want to end this shift?', cancelText: 'No', okText: 'Yes' });            
            confirm.then(function(isconfirmed){            
                $scope.addActivityModalClose();
                if(isconfirmed){                           
                    createActivity(activityType).then(function(activity){
                        if(activity){
                            getJob($stateParams.id).then(function(){
                                $ionicPopup.alert({title: 'Site Pro', template: 'This shift has been sent to your ops manager and will be archived on your phone and deleted after two weeks.'}).then(function(){
                                    $scope.done();
                                });
                            });
                        }
                    });
                }
            });            
        }
        else if(activityType === ActivityTypeEnum.TMPOnSiteAssessment){
            $scope.addActivityModalClose();
            $state.go('app.tmponsiteassessment', {id: 'new', jobid: $scope.job.id, jobName: $scope.job.jobName});
        }
        else if(activityType === ActivityTypeEnum.StartOfShift){
            $scope.addActivityModalClose();
            $ionicLoading.show({ template: '<ion-spinner></ion-spinner>' });
            JobsSvc.startTeamShift($scope.job.id).then(function(){
                getJob($stateParams.id).then(function(){
                    $scope.addActivity();
                    $ionicLoading.hide();                                    
                });
            });
        }
        else{
            $scope.addActivityModalClose();
            startActivity(activityType);
        }
    };
    $scope.addActivity = function(){
    console.log("3");
        let completedActivities = $linq.Enumerable().From($scope.job.activities).Where('w => w.endTime').Select('s => s.activityType').Distinct().ToArray();        
        $scope.activitiesSelection = angular.copy(JobActivities);
        $scope.activitiesSelection.unshift({Id: -1, Activity: 'Manage Team'});
        let hasShiftStarted = hasAnyTeamMemberStarted();
        $scope.activitiesSelection.forEach(activity => {
            if($linq.Enumerable().From(completedActivities).Contains(activity.Id)){
                activity.statusStyle = 'activity-done';
                if(activity.Id === ActivityTypeEnum.OnSiteRecord)
                    activity.statusStyle = 'activity-done-repeat';
                else if(activity.Id === ActivityTypeEnum.StartOfShift)
                    activity.disabled = true;
            }
            
            if(activity.Id != ActivityTypeEnum.ManageTeam && !hasShiftStarted)
                activity.disabled = true;
            else
                activity.disabled = false;
        });
        
        $ionicModal.fromTemplateUrl('views/addactivityselection2.html', {scope: $scope, Animation: 'slide-up'}).then(function(modal){
            $scope.addActivityModal = modal;
            $scope.addActivityModal.show();
        });
        return;
        
            ModalFactory.chooseActivity(completedActivities, hasAnyTeamMemberStarted()).then(function(activityType){
                if(activityType && activityType === ActivityTypeEnum.ManageTeam){
                    $scope.team();
                    //return;
                }
                else if(activityType && activityType === ActivityTypeEnum.EndOfShift){
                    let confirm = $ionicPopup.confirm({ title: 'Site Pro', template: 'Are you sure you want to end this shift?', cancelText: 'No', okText: 'Yes' });
                    
                    //confirm.then(function(isconfirmed){            
                        //if(isconfirmed){
                            /*                            
                            createActivity(activityType).then(function(activity){
                                if(activity){
                                    getJob($stateParams.id).then(function(){
                                        $ionicPopup.alert({title: 'Site Pro', template: 'This shift has been sent to your ops manager and will be archived on your phone and deleted after two weeks.'}).then(function(){
                                            $scope.done();
                                        });
                                    });
                                }
                            });
                            */
                        //}
                        //confirm.close();
                    //});
                    
                }
                else if(activityType === ActivityTypeEnum.TMPOnSiteAssessment){
                    $state.go('app.tmponsiteassessment', {id: 'new', jobid: $scope.job.id, jobName: $scope.job.jobName});
                }
                else if(activityType === ActivityTypeEnum.StartOfShift){
                    $ionicLoading.show({ template: '<ion-spinner></ion-spinner>' });
                    JobsSvc.startTeamShift($scope.job.id).then(function(){
                        getJob($stateParams.id).then(function(){
                            $scope.addActivity();
                            $ionicLoading.hide();                                    
                        });
                    });
                }
                else//(activityType)
                    startActivity(activityType);
            }, function(error){
                console.error(error);
            });
        //});
    };

    $scope.addActivityORIG = function(){
        promptTeamTimeNotStarted().then(function(goBackToTeam){
            if(goBackToTeam === true)
                $scope.team();
            else if(goBackToTeam === false){
                //console.log('ok!');
                //console.log($scope.job.activities);
                getCompletedActivities().then(function(completedActivities){
                    ModalFactory.chooseActivity(completedActivities).then(function(activityType){
                        if(activityType && activityType === ActivityTypeEnum.EndOfShift){
                            createActivity(activityType).then(function(activity){
                                getJob($stateParams.id).then(function(){
                                    $ionicPopup.alert({title: 'Site Pro', template: 'This shift has been sent to your ops manager and will be archived on your phone and deleted after two weeks.'}).then(function(){
                                        $scope.done();
                                    });
                                });
                            });
                        }
                        else if(activityType === ActivityTypeEnum.TMPOnSiteAssessment){
                            $state.go('app.tmponsiteassessment', {id: 'new', jobid: $scope.job.id, jobName: $scope.job.jobName});
                        }
                        else if(activityType === ActivityTypeEnum.StartOfShift){
                            $ionicLoading.show({ template: '<ion-spinner></ion-spinner>' });
                            JobsSvc.startTeamShift($scope.job.id).then(function(){
                                getJob($stateParams.id).then(function(){
                                    $scope.addActivity();
                                    $ionicLoading.hide();                                    
                                });
                            });
                        }
                        else if(activityType)
                            startActivity(activityType);
                    });
                });
            }
        });
    };

    
    function getActivityPath(activity){
        switch(activity.activityType){
            case ActivityTypeEnum.TravelToSite:
            case ActivityTypeEnum.TravelToDepot:
            case ActivityTypeEnum.TravelToNextJob:
            case ActivityTypeEnum.WorkSpaceActive:
            case ActivityTypeEnum.TruckPrecheckRecord:
                return 'app.jobactivity';
            case ActivityTypeEnum.SiteDeployment:
            case ActivityTypeEnum.SiteAmendmentRecord:
            case ActivityTypeEnum.SiteRetrievalRecord:
                return 'app.siteactivity';
            case ActivityTypeEnum.ToolBoxBriefingRecord:
                return 'app.toolBoxBriefingRecord';
            case ActivityTypeEnum.OnSiteRecord:
                return 'app.onsiterecord';
            case ActivityTypeEnum.GearLeftOnSiteRecord:
                return 'app.gearleftonositerecord';
            case ActivityTypeEnum.TMPOnSiteAssessment:
                return 'app.tmponsiteassessment';
            case ActivityTypeEnum.SiteHazardAssessment:
                return 'app.sitehazardassessment';
        }
    }

    $scope.showActivity = function(activity){
        if(activity.activityType === ActivityTypeEnum.EndOfShift)
            return;
        if(activity.activityType === ActivityTypeEnum.PreStartTruckCheck){
            //load truck audit for viewing only
            TrucksSvc.getAll().then(function(trucks){
                TrucksSvc.getAudit(activity.truckAuditId).then(function(truckAudit){
                    if(truckAudit){
                        TruckAuditSvc.start(trucks, $scope.job.jobName, truckAudit, true).then(function(audit){
                            $state.go('app.jobdetail', {id: $scope.job.id});
                        });
                    }
                });
            });        
            
            return;
        }
        if (activity.activityType === ActivityTypeEnum.TruckPrecheckRecord) {
            console.log(activity, "--activity--");
            console.log($scope.job.id, "--$scope.job.id--");
            //load truck audit for viewing only
            TrucksSvc.getAll().then(function (trucks) {
                TruckReportSvc.start(trucks, $scope.job.jobName, $scope.job.id, activity, true).then(function (audit) {
                    //$state.go('app.truckreport');
                });
                //TrucksSvc.getTruckReportId($scope.job.id, activity.id).then(function (truckReports) {
                //    console.log(truckReports, "truckReport");
                //    if (truckReports) {
                //        TruckReportSvc.start(trucks, $scope.job.jobName, $scope.job.id,truckReports, true).then(function (audit) {
                //           //$state.go('app.truckreport');
                //        });
                //    }
                //});
            });

            return;
        }
        $state.go(getActivityPath(activity), {id: activity.id, jobid: $scope.job.id, jobName: $scope.job.jobName, isJobArchived: $scope.job.archived});
    };
    
    $scope.jobInfo = function(){
        $state.go('app.jobinfo', {id: $scope.job.id});
    };

}]);