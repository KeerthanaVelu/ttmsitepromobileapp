﻿mainApp.controller('SiteHazardAssessmentCtrlr', ['$scope', '$state', 'JobsSvc', 'LocationSvc', '$ionicLoading', 'WebApiQSvc', '$q', '$ionicPopup', 'TrucksSvc', '$linq', 'VoiceToTextSvc', '$stateParams', '$ionicHistory',  function ($scope, $state, JobsSvc, LocationSvc, $ionicLoading, WebApiQSvc, $q, $ionicPopup, TrucksSvc, $linq, VoiceToTextSvc, $stateParams, $ionicHistory) {
    $scope.$on('$ionicView.enter', function () {        
        //get activity
        console.log($stateParams);
        $ionicLoading.show({ template: '<ion-spinner></ion-spinner>' });
        $scope.isJobArchived = $stateParams.isJobArchived;
        JobsSvc.getActivity($stateParams.jobid, $stateParams.id).then(function(response){
            if(response){
                $scope.activity = response;
                $scope.activityHeaders = {};
                $scope.activityHeaders.name = ActivityTypeEnum.props[$scope.activity.activityType].name;
                $scope.activityHeaders.jobName = $stateParams.jobName;
                if(!$scope.activity.isCancelled && $scope.activity.startTime && !$scope.activity.endTime)
                    $scope.$broadcast('timer-start');
                else{
                    let startTime = moment($scope.activity.startTime);
                    let endTime = moment($scope.activity.endTime);
                    var diff = endTime.valueOf() - startTime.valueOf();
                    $scope.activitySummary = {};
                    $scope.activitySummary.totalDuration = LeadZero(moment.duration(diff).hours()) + ":" + LeadZero(moment.duration(diff).minutes()) + ":" + LeadZero(moment.duration(diff).seconds());
                }
            }
            $ionicLoading.hide();
        });
    });
    
    $scope.stop = function () {
        $ionicLoading.show({ template: '<ion-spinner></ion-spinner>' });
        var posOptions = { timeout: 5000, enableHighAccuracy: true };
        var coordinates = null;

        LocationSvc.getCurrentPosition(posOptions).then(function (position) {
            console.log(position);
            coordinates = position;
        }, function (error) {
            console.log(error);
            coordinates = error;
        }).finally(function () {
            $ionicLoading.hide();
            $scope.activity.endTime = new Date();
            if (coordinates.coords) {
                $scope.activity.endCoordinates = { latitude: coordinates.coords.latitude, longitude: coordinates.coords.longitude };
            }            
            JobsSvc.stopActivity($stateParams.jobid, $scope.activity).then(function(response){
                $ionicLoading.hide();
                $state.go('app.jobdetail', {id: $stateParams.jobid, showAddActivity: true});
            });
        });
    };
    $scope.back = function () {
        $state.go('app.jobdetail', {id: $stateParams.jobid, showAddActivity: false});
        //$ionicHistory.goBack();
    };

    $scope.cancel = function () {
        //send cancel command to cancel this activity
        var confirmPopup = $ionicPopup.confirm({
            title: 'Site Pro',
            template: 'Are you sure you want to cancel this activity?',
            okText: 'Yes',
            cancelText: 'No'
        });

        confirmPopup.then(function (res) {
            if (res) {
                JobsSvc.cancelActivity($stateParams.jobid, $scope.activity.id).then(function(response){
                    $ionicLoading.hide();
                    $state.go('app.jobdetail', {id: $stateParams.jobid, showAddActivity: true});
                });
            }
        });
    };
    $scope.voiceToText = function () {
        VoiceToTextSvc.Capture().then(function (result) {
            console.log(result);
            if ($scope.activity.notes == null)
                $scope.activity.notes = '';
            if ($scope.activity.notes.length > 0)
                $scope.activity.notes += ' ' + result[0];
            else
                $scope.activity.notes += result[0];
        }, function (error) {
            console.log(error);
        });        
    };
}]);