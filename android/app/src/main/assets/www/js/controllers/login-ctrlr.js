﻿mainApp.controller('LoginCtrlr', ['$scope', '$state', '$ionicLoading', 'AccountSvc', 'JobsSvc', 'WebApiQSvc', 'DBSvc', '$cordovaDialogs', 'TrucksSvc', '$stateParams', '$ionicHistory', '$ionicPopup', 'SyncService', function ($scope, $state, $ionicLoading, AccountSvc, JobsSvc, WebApiQSvc, DBSvc, $cordovaDialogs, TrucksSvc, $stateParams, $ionicHistory, $ionicPopup, SyncService) {
    console.log('LoginCtrlr');
    $scope.loginCredentials = { email: '', password: '' };
    $scope.login = function () {

        if (navigator.connection.type == 'none') {
            $ionicPopup.alert({ title: 'Connection Problem', template: 'Internet connection not available. Check your connection & try again.' });
            return;

        }
        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>' });
        if ($stateParams.mode == 'relogin') {
            AccountSvc.relogin($scope.loginCredentials.email, $scope.loginCredentials.password).then(function (response) {
                WebApiQSvc.init(response.token);
                WebApiQSvc.startDataService();
                $ionicLoading.hide();
                $ionicHistory.goBack();
            }, function (error) {   
                console.log(error);
                $ionicLoading.hide();
                if (error.status == 400)
                    $cordovaDialogs.alert('Invalid login.', 'Site Pro');
                else
                    $cordovaDialogs.alert('Unable to connect to server.', 'Site Pro');
            });
        }
        else {
            AccountSvc.login($scope.loginCredentials.email, $scope.loginCredentials.password).then(function (response) {
                $ionicLoading.hide();
                console.log('LoginCtrlr: AccountSvc.login');
                console.log(response);
                $scope.$emit('loginsuccess', response);                
            }, function (error) {
                console.log(error);
                $ionicLoading.hide();
                if (error.status == 400)
                    $cordovaDialogs.alert('Invalid login.', 'Site Pro');
                else
                    $cordovaDialogs.alert('Unable to connect to server.', 'Site Pro');
            });
        }
    };
    $scope.forgotPassword = function(){
        var prompt = $ionicPopup.prompt({title: 'Forgot Password?', template: 'Please enter your email to recover your password.',
            okText: 'Send', inputPlaceholder: 'username@email.com', inputType: 'email'});
        prompt.then(function(email){
            console.log(email);
            if (email) {

                if (navigator.connection.type == 'none') {
                    $ionicPopup.alert({ title: 'Connection Problem', template: 'Internet connection not available. Check your connection & try again.' });
                    return;
                }

                $ionicLoading.show({
                    template: '<ion-spinner icon="spiral" class="spinner-positive"></ion-spinner>'
                });
                
                AccountSvc.forgotPassword(email).then(function(){
                    $ionicLoading.hide();
                    $ionicPopup.alert({title: 'Forgot Password?', template: 'Please check your email for the details.'});
                }, function(error){
                    $ionicLoading.hide();
                    $ionicPopup.alert({title: 'Forgot Password?', template: 'An error occcured while processing the request.'});
                });
                
            }
            else{
                $ionicPopup.alert({title: 'Forgot Password?', template: 'Email is required.'});
            }
        });
    }
}]);