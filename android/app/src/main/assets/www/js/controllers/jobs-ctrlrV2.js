﻿mainApp.controller('JobsCtrlr', ['$scope', '$state', '$ionicPopover', 'JobsSvc', '$ionicLoading', '$q', 'AccountSvc', '$linq', 'ApiSvc', 'LocationSvc', '$ionicPopup', '$ionicActionSheet', '$ionicModal', 'ModalFactory', 'TransferJobSvc', 'TenantSvc', 'VersionSvc', function ($scope, $state, $ionicPopover, JobsSvc, $ionicLoading, $q, AccountSvc, $linq, ApiSvc, LocationSvc, $ionicPopup, $ionicActionSheet, $ionicModal, ModalFactory, TransferJobSvc, TenantSvc, VersionSvc) {
    function getLatestVersion(){
        console.log('getLatestVersion()');
        VersionSvc.getLatest().then(function(result){
            console.log('[5]');
            console.log(result);
            $scope.latestVersion = result;
        }, function(error){
            console.log(error);
        });
    }    
    $scope.currentVersion = VersionSvc.currentVersion;
    $ionicPopover.fromTemplateUrl('views/adminoptions.html', {
        scope: $scope
    }).then(function (popover) {
        $scope.popover = popover;
    });
    $ionicPopover.fromTemplateUrl('views/jobitemoptions.html', {
        scope: $scope
    }).then(function (popover) {
        $scope.jobItemOptions = popover
    });
    $scope.$on('$ionicView.loaded', function (event) {
        $scope.userInfo = AccountSvc.getUserInfo();
        getLatestVersion();
    });

    $scope.$on('$ionicView.enter', function(event){
        $scope.changeTab($scope.activeTab);
    });
    
    $scope.$on('jobReceived', function (event) {
        $scope.changeTab($scope.activeTab); 
    });

    $scope.$on('mobileJobTransferReceived', function (event, data) {
        var job = $linq.Enumerable().From($scope.jobs).FirstOrDefault(null, 'f => f.id == "' + data.jobId + '"');
        var index = $scope.jobs.indexOf(job);
        if (index >= 0) {
            $scope.jobs.splice(index, 1);
            JobsSvc.save().then(function () {
            }, function (error) {
                console.log(error);
                alert('An error occured.');
            });
        }
    });

    function formatShiftDates(shifts) {
        for (var i = 0; i < shifts.length; i++) {
            let shift = shifts[i];
            console.log(shift.shiftDate);
            shift.dayGroup = shift.shiftDate;
            shift.shiftDate = moment(shift.shiftDate, 'DD-MM-YYYY').format('dddd Do MMMM YY');
        }
    }

    function loadJobsByDay(){
        $ionicLoading.show({ template: '<ion-spinner></ion-spinner>' });
        JobsSvc.getAllByExpectedStartDate().then(function(jobs){
            $scope.jobs = jobs;
            formatShiftDates($scope.jobs);
            console.log('[51]');
            console.log($scope.jobs);
        }, function(){
            loadJobsByDay();
        }).finally(function(){
            $ionicLoading.hide();
        });
    }

    function loadJobs() {
        $ionicLoading.show({ template: '<ion-spinner></ion-spinner>' });
        JobsSvc.getAllV2().then(function(jobs){
            $scope.jobs = jobs;
        }, function(){
            loadJobs();
        }).finally(function(){
            $ionicLoading.hide();
        });
    }

    function loadArchivedJobs(){
        $ionicLoading.show({ template: '<ion-spinner></ion-spinner>' });
        //remove archived jobs that are 2 weeks old        
        JobsSvc.getArchivedJobsByDate().then(function(jobs){
            $scope.jobs = jobs;
            formatShiftDates($scope.jobs);
        }).finally(function(){
            $ionicLoading.hide();
        });
    }

    $scope.jobs = [];
    $scope.adminOptions = function ($event) {
        $scope.popover.show($event);
    };
    $scope.manageStaff = function () {
        $scope.popover.hide();
        $state.go('app.staff');
    };
    $scope.searchJobs = '';
    $scope.newJob = function () {
        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>', hideOnStateChange: true });
        $state.go('app.newjob');
    };
    $scope.loadJob = function(id){        
        $state.go('app.jobdetail', {id: id, showAddActivity: false});
    };
    $scope.confirmLoadJob = function(job){
        if(job.expectedStartDay !== moment().format('DD-MM-YYYY')){
            //let confirmOpenJob = ons.notification.confirm({ title: 'Site Pro', message: "The expected start date is not today's date. Are you sure you want to open this shift now?", cancelable: true, buttonLabels:['Open Job', 'Exit'] });
            let confirmOpenJob = $ionicPopup.confirm({ title: 'Site Pro', template: "The expected start date is not today's date. Are you sure you want to open this shift now?", cancelText: 'Exit', okText: 'Open Job' });
            confirmOpenJob.then(function(confirm){
                console.log(confirm);
                if(confirm){
                    $scope.loadJob(job.id);
                }
            });
        }
        else
            $scope.loadJob(job.id);
    };
    $scope.delete = function (job, event) {
        event.stopPropagation();
        event.preventDefault();
        var confirmPopup = $ionicPopup.confirm({
            title: 'Site Pro',
            template: 'Do you want to delete this job? <br/>' + job.jobName,
            okText: 'Yes',
            cancelText: 'No'
        });

        confirmPopup.then(function (res) {
            if (res) {
                $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>', hideOnStateChange: true });
                JobsSvc.updateJobStatus(job.id).then(function (response) {
                    JobsSvc.delete(job.id).then(function (response) {
                        console.log(response);
                        $ionicLoading.hide();
                        $scope.changeTab($scope.activeTab);
                    }, function (error) {
                        $ionicLoading.hide();
                        console.log(error);
                        alert('An error occured.');
                    });
                }, function (error) {
                    $ionicLoading.hide();
                }).finally(function () {
                    $ionicLoading.hide();
                });
            }
        });
    };
    
    $scope.transferJobModal = function (job) {        
        var confirmPopup = $ionicPopup.confirm({
            title: 'Site Pro',
            template: 'Have you briefed the receiving STMS? <br/>Job Name: ' + job.jobName,
            okText: 'Yes',
            cancelText: 'No'
        });
        confirmPopup.then(function (result) {
            if (result) {
                //load the branches/STMS
                $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>', hideOnStateChange: true });
                TenantSvc.getInfo(AccountSvc.getTenantId()).then(function (tenantInfo) {
                    console.log(tenantInfo);
                    $ionicLoading.hide();                    
                    TransferJobSvc.transferForm(job, tenantInfo.branches).then(function (data) {
                        console.log(data);
                        if (data) {
                            TransferJobSvc.transfer(data).then(function (transferResult) {
                                console.log(transferResult);
                            }, function (error) {
                                console.log(error);
                                $ionicPopup.alert({ title: 'Site Pro', template: "Something went wrong." });
                            });
                        }
                    }, function (error) {
                        console.log(error);
                        $ionicLoading.hide();
                        $ionicPopup.alert({ title: 'Site Pro', template: "Something went wrong." });
                    });                    
                }, function (error) {
                    $ionicLoading.hide();
                    console.log(error);
                    $ionicPopup.alert({ title: 'Site Pro', template: "Can't connect to server." });
                });                
            };
        });        
    };
    $scope.showItemOptions = function (job, event) {        
        event.stopPropagation();
        event.preventDefault();

        $ionicActionSheet.show({
            cssClass: 'job-item-actionsheet',
            buttons: [
                //{ text: 'Transfer' }
            ],
            destructiveText: 'Delete',
            cancelText: 'Cancel',
            cancel: function () {
                // add cancel code..
            },
            buttonClicked: function (index) {
                if (index == 0) {
                    alert('TODO');                    
                    //$scope.transferJobModal(job);
                }
                return true;
            },
            destructiveButtonClicked: function () {
                $scope.delete(job, event);
                return true;
            }
        });
    };
    $scope.logout = function(){
        $scope.popover.hide();
        $state.go('app.login');
    };
    
    $scope.activeTab = 1;
    $scope.changeTab = function(tabIndex){
        $scope.activeTab = tabIndex;
        if($scope.activeTab === 1){
            //loadJobs();
            loadJobsByDay();
        }
        else{
            loadArchivedJobs();
        }
    };

    $scope.userSettings = function(){
        $scope.popover.hide();
        $state.go('app.usersettings');
    };
    $scope.latestVersion = null;
    $scope.showLatestVersion = function(hidePopup){
        if($scope.popover._isShown)
            $scope.popover.hide();
        let template = 'Current Version: ' + $scope.currentVersion;
        if($scope.hasNewVersion()){
            template += "<br/>New Version: " + $scope.latestVersion;        
            template += "<br/>Please check your Google Play Store app to install the latest version.";
        }
        $ionicPopup.alert({title: 'Site Pro', template: template}).then(function(){
            $scope.popover.hide();
        });        
    };
    $scope.hasNewVersion = function(){
        if($scope.latestVersion == null)
            return false;

        return $scope.currentVersion != $scope.latestVersion;
    } 
}]);
