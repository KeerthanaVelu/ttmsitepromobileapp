﻿mainApp.controller('StaffFormCtrlr', ['$scope', '$state', 'StaffSvc', 'AccountSvc', '$ionicPopup', '$ionicLoading', '$stateParams', 'PhotoSvc', '$cordovaFile', 
function ($scope, $state, StaffSvc, AccountSvc, $ionicPopup, $ionicLoading, $stateParams, PhotoSvc, $cordovaFile) {
    $scope.$on('$ionicView.enter', function (event) {
        console.log($stateParams);
        $scope.mode = $stateParams.mode;
        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>'});        
        AccountSvc.getBranchesV2().then(function(branches){
            $scope.branches = branches;
            //$scope.teamMember = angular.copy(Staff);
            $ionicLoading.hide();
        }, function(error){
            console.log(error);
            $ionicLoading.hide();
        });
        
    });

    $scope.$on('$ionicView.loaded', function(event){
        console.log('loaded');
    });

    function valid() {
        var errors = [];
        if ($scope.staff.fullName == null || $scope.staff.fullName.length == 0)
            errors.push("Please enter a Full Name.");
        if ($scope.staff.branchId == null || $scope.staff.branchId.length == 0)
            errors.push("Please choose a Branch.");

        if (errors.length > 0) {
            var errorMessage = '';
            for (var i = 0; i < errors.length; i++) {
                errorMessage += errors[i];
                if (i < errors.length)
                    errorMessage += "<br/>";
            }

            $ionicPopup.alert({ title: 'Site Pro', template: errorMessage });
        }

        return errors.length <= 0;
    }

    $scope.staff = StaffSvc.staffMember || angular.copy(Staff);
    $scope.done = function () {
        alert('todo');
        return;
        if (!valid())

            return;

        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>' });
        if ($scope.staff.id == null) {
            $scope.staff.id = (new ObjectId()).toString();
            $scope.staff.tenantId = AccountSvc.getTenantId();
            //StaffSvc.staff.push($scope.staff);
            StaffSvc.addStaff($scope.staff).then(function () {
                $ionicLoading.hide();
                $state.go('managestaff');
            });
        }
        else {
            $scope.staff.tenantId = AccountSvc.getTenantId();            
            StaffSvc.updateStaff($scope.staff).then(function () {
                $ionicLoading.hide();
                $state.go('managestaff');
            });
        }
    };
    $scope.addPhoto = function () {
        PhotoSvc.getPhoto().then(function (photo) {
            console.log(photo);
            $cordovaFile.readAsDataURL(getDirectory(photo.path), getFileName(photo.path)).then(function (result) {
                $scope.staff.photo = result.replace('data:image/jpeg;base64,', '');
            });
        }, function(error){
            alert('Something went wrong while getting a photo.');
        });
        /*
        $ionicPlatform.ready(function () {
            var options = {
                quality: 75,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.CAMERA,
                allowEdit: false,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 256,
                targetHeight: 256,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: true,
                correctOrientation: true
            };

            $cordovaCamera.getPicture(options).then(function (result) {
                var photo = result;
                $scope.staff.photo = photo;
            }, function (error) {
                console.log(error);
                alert(error);
            });            
        });
        */
    };
    $scope.cancel = function () {
        $state.go('app.staff');
        //$state.go('managestaff');
    };
}]);