﻿mainApp.controller('ActTravelToSiteCtrlr', ['$scope', '$state', '$window', 'JobsSvc', 'LocationSvc', '$ionicLoading', 'WebApiQSvc', '$q', '$ionicPopup', 'TrucksSvc', '$linq', 'VoiceToTextSvc', function ($scope, $state, $window, JobsSvc, LocationSvc, $ionicLoading, WebApiQSvc, $q, $ionicPopup, TrucksSvc, $linq, VoiceToTextSvc) {
    $scope.truckNumber = null;

    function getTruckNumber() {
        var popup = $ionicPopup.show({
            template: 'Choose a truck.<br/><select ng-options="truck.id as (truck.truckNumber + \' - \' + truck.truckRego) for truck in trucks" ng-model="fields.truckId" class="trucks-option">' +
              '<option value="">Please select...</option></select>',
            title: 'Site Pro',
            scope: $scope,
            buttons: [
                {
                    text: 'Cancel', onTap: function () {
                        return null;
                    }
                },
                {
                    text: 'Ok', type: 'button-positive', onTap: function (e) {
                        if (!$scope.fields.truckId)
                            e.preventDefault();
                        else
                            return $scope.fields.truckId;
                    }
                }
            ]
        });

        return popup;
    }

    $scope.$on('$ionicView.enter', function () {
        $scope.fields = JobsSvc.job.CurrentActivity;
        $scope.truckNumber = JobsSvc.job.CurrentActivity.truckNumber;
        $scope.trucks = TrucksSvc.getAll();
        $scope.jobName = JobsSvc.job.jobName;
        if ($scope.fields.complete) {
            var diff = $scope.fields.endTime.valueOf() - $scope.fields.startTime.valueOf();
            $scope.TotalDuration = LeadZero(moment.duration(diff).hours()) + ":" + LeadZero(moment.duration(diff).minutes()) + ":" + LeadZero(moment.duration(diff).seconds());
        }
        else {
            if ($scope.fields.id == null)                
                $scope.Start();
            else
                $scope.$broadcast('timer-start');
        }
    });

    $scope.Start = function () {
        $ionicLoading.show({ template: '<ion-spinner></ion-spinner>' });
        var posOptions = { timeout: 5000, enableHighAccuracy: true };
        var coordinates = null;

        LocationSvc.getCurrentPosition(posOptions).then(function (position) {
            console.log(position);
            coordinates = position;
        }, function (error) {
            console.log(error);
            coordinates = error;
        }).finally(function () {
            $ionicLoading.hide();
            console.log(coordinates);

            $scope.fields.startTime = new Date();            
            $scope.$broadcast('timer-start');
            var activity = { id: (new $window.ObjectId()).toString(), jobId: JobsSvc.job.id, activityId: 1, startTime: $scope.fields.startTime, complete: false, startCoordinates: {}, endCoordinates: {}, mode: 0, cancelled: JobsSvc.job.CurrentActivity.cancelled, truckNumber: $scope.truckNumber, truckId: $scope.fields.truckId, notes: null };

            if (coordinates.coords) {
                activity.startCoordinates = { latitude: coordinates.coords.latitude, longitude: coordinates.coords.longitude };
            }
            
            $scope.fields = JobsSvc.job.CurrentActivity = activity;
            JobsSvc.AddSendActivity(JobsSvc.job.CurrentActivity);
            
            if (JobsSvc.job.team != null) {
                for (var i = 0; i < JobsSvc.job.team.length; i++) {
                    var member = JobsSvc.job.team[i];
                    if (member.timeLogs == null)
                        member.timeLogs = [];
                    member.jobId = JobsSvc.job.id;
                    member.timeLogs.push({ jobId: JobsSvc.job.id, staffMemberId: member.staffMemberId, timeLogType: 0, time: new Date() });
                }
                WebApiQSvc.Send({ isTeamMembers: true, team: JobsSvc.job.team });
            }            
        });
    };
    
    $scope.Stop = function () {
        $ionicLoading.show({ template: '<ion-spinner></ion-spinner>' });
        var posOptions = { timeout: 5000, enableHighAccuracy: true };
        var coordinates = null;

        LocationSvc.getCurrentPosition(posOptions).then(function (position) {
            console.log(position);
            coordinates = position;
        }, function (error) {
            console.log(error);
            coordinates = error;
        }).finally(function () {
            $ionicLoading.hide();
            JobsSvc.job.CurrentActivity.endTime = new Date();
            JobsSvc.job.CurrentActivity.complete = true;
            $scope.fields = JobsSvc.job.CurrentActivity;

            if (coordinates.coords) {
                JobsSvc.job.CurrentActivity.endCoordinates = { latitude: coordinates.coords.latitude, longitude: coordinates.coords.longitude };
            }
            JobsSvc.job.CurrentActivity.mode = 1;
            JobsSvc.UpdateActivity(JobsSvc.job.CurrentActivity);
            JobsSvc.showAddActivity = true;
            $scope.Done();
        });
    };
    $scope.Done = function () {
        $state.go('jobdetails');
    };

    $scope.Cancel = function () {
        //send cancel command to cancel this activity
        var confirmPopup = $ionicPopup.confirm({
            title: 'Site Pro',
            template: 'Are you sure you want to cancel this activity?',
            okText: 'Yes',
            cancelText: 'No'
        });

        confirmPopup.then(function (res) {
            if (res) {
                JobsSvc.job.CurrentActivity.cancelled = true;
                WebApiQSvc.Send({ cancelActivity: true, jobId: JobsSvc.job.id, activityId: JobsSvc.job.CurrentActivity.id });
                $state.go('jobdetails');
            }
        });

        /*JobsSvc.job.CurrentActivity.cancelled = true;
        WebApiQSvc.Send({ cancelActivity: true, jobId: JobsSvc.job.id, activityId: JobsSvc.job.CurrentActivity.id });
        $state.go('jobdetails');*/
    };
    $scope.voiceToText = function () {
        VoiceToTextSvc.Capture().then(function (result) {
            console.log(result);
            if ($scope.fields.notes == null)
                $scope.fields.notes = '';
            if ($scope.fields.notes.length > 0)
                $scope.fields.notes += ' ' + result[0];
            else
                $scope.fields.notes += result[0];
        }, function (error) {
            console.log(error);
        });
        /*
        ionic.Platform.ready(function () {
            if (navigator.connection.type == 'none') {
                $ionicPopup.alert({ title: 'Site Pro', template: 'Internet connection not available.' });
                return;
            }

            if (device.platform == 'Android') {                
                navigator.SpeechRecognizer.startRecognize(function (result) {
                //window.plugins.speechrecognizer.startRecognize(function (result) {
                    if ($scope.fields.notes == null)
                        $scope.fields.notes = '';                        
                    if ($scope.fields.notes.length > 0)
                        $scope.fields.notes += ' ' + result[0];
                    else
                        $scope.fields.notes += result[0];

                    $scope.$apply();
                }, function (error) {
                    console.log(error);
                }, 1, "Speak Now", "en-NZ");
            }
            else{
                var recognition = new SpeechRecognition();
                recognition.onresult = function (event) {
                    if (event.results && event.results.length > 0) {
                        if ($scope.fields.notes == null)
                            $scope.fields.notes = '';

                        if ($scope.fields.notes.length > 0)
                            $scope.fields.notes += ' ' + event.results[0][0].transcript;
                        else
                            $scope.fields.notes += event.results[0][0].transcript;

                        $scope.$apply();
                    }
                };
                recognition.onend = function () {
                    $ionicLoading.hide();
                };
                recognition.onerror = function (error) {
                    $ionicLoading.hide();
                };
                recognition.onnomatch = function () {
                    $ionicLoading.hide();
                };
                recognition.start();
            }
        });
        */
    };
}]);