﻿mainApp.controller('NewTeamMemberCtrlr', ['$scope', '$state', '$ionicPlatform', '$ionicPopup', 'JobsSvc', 'StaffSvc', '$cordovaCamera', 'AccountSvc', 'PhotoSvc', '$cordovaFile', '$ionicLoading', function ($scope, $state, $ionicPlatform, $ionicPopup, JobsSvc, StaffSvc, $cordovaCamera, AccountSvc, PhotoSvc, $cordovaFile, $ionicLoading) {    
    $scope.$on('modal.shown', function(event){
        console.log('modal.shown');
        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>'});
        AccountSvc.getBranchesV2().then(function(branches){
            $scope.branches = branches;
            $scope.teamMember = angular.copy(Staff);
            $ionicLoading.hide();
        }, function(error){
            console.log(error);
            $ionicLoading.hide();
        });
    });
   
    $scope.back = function () {
        $scope.closeModal(null);
    };

    function valid() {
        var errors = [];
        if ($scope.teamMember.fullName == null || $scope.teamMember.fullName.length == 0)
            errors.push("Please enter a Full Name.");
        if ($scope.teamMember.branchId == null || $scope.teamMember.branchId.length == 0)
            errors.push("Please choose a Branch.");

        if (errors.length > 0) {
            var errorMessage = '';
            for (var i = 0; i < errors.length; i++) {
                errorMessage += errors[i];
                if (i < errors.length)
                    errorMessage += "<br/>";
            }

            $ionicPopup.alert({ title: 'Site Pro', template: errorMessage });
        }

        return errors.length <= 0;
    }

    $scope.done = function () {
        if (!valid()) {
            return;
        }
        //save to db
        //return id
        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>'});
        StaffSvc.add($scope.teamMember).then(function(response){
            $scope.closeModal(response);
        }, function(error){
            console.log(error);
            alert('Something went wrong. We apologize for the inconvenience.');
        }).finally(function(response){
            console.log(response);
            $ionicLoading.hide();
        });
    };
    $scope.addPhoto = function () {
        PhotoSvc.getPhoto().then(function (photo) {
            console.log(photo);
            $cordovaFile.readAsDataURL(getDirectory(photo.path), getFileName(photo.path)).then(function (result) {
                //console.log(result);
                $scope.teamMember.photo = result.replace('data:image/jpeg;base64,', '');
            });            
        }, function(error){
            alert('Something went wrong while getting a photo.');
        });
        /*
        $ionicPlatform.ready(function () {
            var options = {
                quality: 75,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.CAMERA,
                allowEdit: false,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 256,
                targetHeight: 256,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: true,
                correctOrientation: true
            };

            $cordovaCamera.getPicture(options).then(function (result) {                
                var photo = result;
                $scope.teamMember.photo = photo;
                //$scope.$apply();
            }, function (error) {
                // error
                alert(error);
            });
        });
        */
    };    
}]);