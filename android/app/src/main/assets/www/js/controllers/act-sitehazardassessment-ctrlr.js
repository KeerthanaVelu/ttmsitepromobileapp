﻿mainApp.controller('ActSiteHazardAssessmentCtrlr', ['$scope', '$state', '$window', 'JobsSvc', '$ionicLoading', 'LocationSvc', 'WebApiQSvc', 'VoiceToTextSvc', '$ionicPopup', function ($scope, $state, $window, JobsSvc, $ionicLoading, LocationSvc, WebApiQSvc, VoiceToTextSvc, $ionicPopup) {
    $scope.$on('$ionicView.enter', function () {
        $scope.fields = JobsSvc.job.CurrentActivity;
        $scope.oldDescription = $scope.fields.description;
        $scope.jobName = JobsSvc.job.jobName;
        if ($scope.fields.complete) {
            var diff = $scope.fields.endTime.valueOf() - $scope.fields.startTime.valueOf();
            $scope.TotalDuration = LeadZero(moment.duration(diff).hours()) + ":" + LeadZero(moment.duration(diff).minutes()) + ":" + LeadZero(moment.duration(diff).seconds());
        }
        else {
            if($scope.fields.id == null)
                $scope.Start();
            else
                $scope.$broadcast('timer-start');            
        }
    });
    
    $scope.Started = false;
    $scope.HasStarted = false;
    $scope.Done = function () {
        $state.go('jobdetails');
    };
    $scope.Start = function () {
        $ionicLoading.show({ template: '<ion-spinner></ion-spinner>' });
        var posOptions = { timeout: 5000, enableHighAccuracy: true };
        var coordinates = null;
        
        LocationSvc.getCurrentPosition(posOptions).then(function (position) {
            console.log(position);
            coordinates = position;
        }, function (error) {
            console.log(error);
            coordinates = error;
        }).finally(function () {
            $ionicLoading.hide();
            $scope.fields.startTime = new Date();
            //$scope.fields.startTime = moment().subtract(503, 'seconds').toDate();//simulate previous time
            //$scope.fields.startTime = new Date();
            $scope.HasStarted = true;
            $scope.$broadcast('timer-start');
            var activity = { id: (new $window.ObjectId()).toString(), jobId: JobsSvc.job.id, activityId: 3, description: $scope.fields.description, startTime: $scope.fields.startTime, endTime: $scope.fields.endTime, complete: false, startCoordinates:{}, endCoordinates: {}, mode: 0, notes: $scope.fields.notes };

            if(coordinates.coords)
                activity.startCoordinates = { latitude: coordinates.coords.latitude, longitude: coordinates.coords.longitude };
            //activity.startCoordinates.latitude = -37.0 + Number(Math.random().toFixed(6));
            //activity.startCoordinates.longitude = 174.0 + Number(Math.random().toFixed(6));

            $scope.fields = JobsSvc.job.CurrentActivity = activity;        
            JobsSvc.AddSendActivity(JobsSvc.job.CurrentActivity);
        });
    };
    $scope.Stop = function () {
        $ionicLoading.show({ template: '<ion-spinner></ion-spinner>' });
        var posOptions = { timeout: 5000, enableHighAccuracy: true };
        var coordinates = null;
        
        LocationSvc.getCurrentPosition(posOptions).then(function (position) {
            console.log(position);
            coordinates = position;
        }, function (error) {
            console.log(error);
            coordinates = error;
        }).finally(function () {
            $ionicLoading.hide();
            JobsSvc.job.CurrentActivity.endTime = new Date();
            JobsSvc.job.CurrentActivity.complete = true;

            if(coordinates.coords)
                JobsSvc.job.CurrentActivity.endCoordinates = { latitude: coordinates.coords.latitude, longitude: coordinates.coords.longitude };
            //JobsSvc.job.CurrentActivity.endCoordinates.latitude = -37.0 + Number(Math.random().toFixed(6));
            //JobsSvc.job.CurrentActivity.endCoordinates.longitude = 174.0 + Number(Math.random().toFixed(6));
            
            JobsSvc.job.CurrentActivity.mode = 1;
            $scope.HasStarted = false;
            $scope.Started = !$scope.Started;
            JobsSvc.UpdateActivity(JobsSvc.job.CurrentActivity);
            JobsSvc.showAddActivity = true;
            $scope.Done();
        });
    };
    $scope.back = function () {        
        $state.go('jobdetails');
    };
    $scope.cancel = function () {
        var confirmPopup = $ionicPopup.confirm({
            title: 'Site Pro',
            template: 'Are you sure you want to cancel this activity?',
            okText: 'Yes',
            cancelText: 'No'
        });

        confirmPopup.then(function (res) {
            if (res) {
                JobsSvc.job.CurrentActivity.cancelled = true;
                WebApiQSvc.Send({ cancelActivity: true, jobId: JobsSvc.job.id, activityId: JobsSvc.job.CurrentActivity.id });
                $state.go('jobdetails');
            }
        });
        /*JobsSvc.job.CurrentActivity.cancelled = true;
        WebApiQSvc.Send({ cancelActivity: true, jobId: JobsSvc.job.id, activityId: JobsSvc.job.CurrentActivity.id });
        $state.go('jobdetails');*/
    };
    $scope.voiceToText = function () {
        VoiceToTextSvc.Capture().then(function (result) {
            console.log(result);
            if ($scope.fields.notes == null)
                $scope.fields.notes = '';
            if ($scope.fields.notes.length > 0)
                $scope.fields.notes += ' ' + result[0];
            else
                $scope.fields.notes += result[0];
        }, function (error) {
            console.log(error);
        });        
    };
}]);