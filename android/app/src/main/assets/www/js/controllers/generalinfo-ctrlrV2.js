﻿mainApp.controller('GeneralInfoCtrlr', ['$scope', '$ionicHistory', 'JobsSvc', '$linq', '$state', '$stateParams', '$ionicLoading', function ($scope, $ionicHistory, JobsSvc, $linq, $state, $stateParams, $ionicLoading) {
    $scope.$on('$ionicView.enter', function(){
        $ionicLoading.show({ template: '<ion-spinner></ion-spinner>'});
        JobsSvc.getGeneralInfo($stateParams.id).then(function(response){
            console.log(response);
            $scope.generalInfo = response;
            setRoadLevels();
        }, function(error){
            alert('Something went wrong.');
        }).finally(function(){
            $ionicLoading.hide();
        });
    });

    function setRoadLevels(){
        if($scope.generalInfo.roadLevels != null){
            $scope.roadLevels.forEach(item => {
                if($scope.generalInfo.roadLevels.indexOf(item.name) >= 0)
                    item.checked = true;
            });
        }
    }

    $scope.back = function () {
        $state.go('app.jobdetail', {id: $stateParams.id, showAddActivity: false});
        //$ionicHistory.goBack();
    };
    $scope.roadLevels = angular.copy(RoadLevels);//[{ name: 'LR', checked: false }, { name: 'LV', checked: false }, { name: 'L1', checked: false }, { name: 'L2', checked: false }, { name: 'L2LS', checked: false }, { name: 'L3', checked: false }];
    $scope.roadLevelsChanged = function () {
        $scope.job.roadLevels = $linq.Enumerable().From($scope.roadLevels).Where('w => w.checked  == true').Select('s => s.name').ToArray();
        console.log($scope.job.roadLevels);
    };
    $scope.done = function () {
        $ionicLoading.show({ template: '<ion-spinner></ion-spinner>'});
        let selectedRoadLevels = $linq.Enumerable().From($scope.roadLevels).Where('w => w.checked == true').Select('s => s.name').ToArray();
        $scope.generalInfo.roadLevels = selectedRoadLevels;        
        JobsSvc.updateGeneralInfoV2($scope.generalInfo).then(function (response){
        }, function (error) {
            console.log(error);
            alert('Something went wrong.');
        }).finally(function(){
            $ionicLoading.hide();
            //$ionicHistory.goBack();
            $state.go('app.jobdetail', {id: $stateParams.id, showAddActivity: false});
        });
    };
}]);