﻿mainApp.controller('ActSiteCheckCtrlr', ['$scope', '$state', '$ionicPopup', '$window', 'JobsSvc', 'TSLSvc', '$linq', 'UploadSvc', '$cordovaDialogs', '$ionicLoading', 'LocationSvc', 'WebApiQSvc', '$q', 'DBSvc', 'SiteCheckAlarmSvc', 'VoiceToTextSvc', function ($scope, $state, $ionicPopup, $window, JobsSvc, TSLSvc, $linq, UploadSvc, $cordovaDialogs, $ionicLoading, LocationSvc, WebApiQSvc, $q, DBSvc, SiteCheckAlarmSvc, VoiceToTextSvc) {
    $scope.$on('$ionicView.enter', function () {
        //if (JobsSvc.job.siteCheckItem != null)
        //    return;
        $scope.jobName = JobsSvc.job.jobName;
        if (JobsSvc.job.CurrentActivity.id == null) {
            //$scope.fields.SiteCheckTime = new Date();
            $scope.fields.startTime = new Date();
            //angular.copy(siteCheckItems, $scope.siteCheckItems);
            $scope.siteCheckItems = JobsSvc.job.CurrentActivity.siteCheckItems;
            var siteDeployment = $linq.Enumerable().From(JobsSvc.job.activities).FirstOrDefault(null, 'f => f.activityId == 2');
            if (siteDeployment && siteDeployment.TSLs)
                $scope.TSLs = siteDeployment.TSLs;
        }
        else {
            $scope.fields = JobsSvc.job.CurrentActivity;
            $scope.siteCheckItems = $scope.fields.siteCheckItems;
            $scope.TSLs = $scope.fields.TSLs;
        }

        for (var i = 0; i < $scope.siteCheckItems.length; i++) {
            $scope.siteCheckItems[i].checkedWithIssuesStyle = $scope.siteCheckItems[i].checkedWithIssues ? { color: "#ef473a" } : {};
            $scope.siteCheckItems[i].checkedStyle = $scope.siteCheckItems[i].checked ? { color: "#33cd5f" } : {};
        }

    });

    $scope.fields = { startTime: null };
    $scope.siteCheckItems = [];
    $scope.tsls = [];

    $scope.CheckItem = function (item) {
        JobsSvc.job.siteCheckItem = item;
        JobsSvc.job.siteCheckItem.getPhoto = false;
        $state.go('actsitecheckitem');
    };
    $scope.AddTSL = function () {
        TSLSvc.AddTSL($scope);
    };
    $scope.CancelTSL = function () {
        TSLSvc.Cancel();
    };
    $scope.Done = function () {
        $ionicLoading.show({ template: '<ion-spinner></ion-spinner>' });
        var posOptions = { timeout: 5000, enableHighAccuracy: true };
        var coordinates = null;
        
        LocationSvc.getCurrentPosition(posOptions).then(function (position) {
            //console.log(position);
            coordinates = position;
        }, function (error) {
            console.log(error);
            coordinates = error;
        }).finally(function () {
            $ionicLoading.hide();
            var activity = {
                id: (new $window.ObjectId()).toString(), jobId: JobsSvc.job.id, activityId: 6, startTime: $scope.fields.startTime, siteCheckItems: $scope.siteCheckItems, TSLs: $scope.tsls, complete: true, startCoordinates: {}, endCoordinates: {}, mode: 0, notes: $scope.fields.notes
            };

            if(coordinates.coords)
                activity.startCoordinates = { latitude: coordinates.coords.latitude, longitude: coordinates.coords.longitude };

            JobsSvc.job.siteCheckItem = null;
            JobsSvc.showAddActivity = true;
            //send all the photos first before sending the activity data
            //on the server side order the activities by date modified
            //on the webapisvc
            // skip sending the activity when the photo uploads are not yet complete
            var paths = $linq.Enumerable().From(activity.siteCheckItems).SelectMany('sm => sm.photos').Select('s => s.path').ToArray();
            if (paths.length > 0) {
                var imagesQ = [];
                for (var i = 0; i < paths.length; i++) {
                    imagesQ.push(JobsSvc.addToQ({ id: (new $window.ObjectId()).toString(), isFileUpload: true, path: paths[i] }));                    
                }

                $q.all(imagesQ).then(function (results) {
                    //DBSvc.getAllQData().then(function(data){
                    //    console.log(data);
                    //});
                    console.log('JobsSvc.AddSendActivity(activity);');
                    JobsSvc.AddSendActivity(activity);
                    $state.go('jobdetails');
                });
            }
            else{
                console.log('JobsSvc.AddSendActivity(activity);');
                JobsSvc.AddSendActivity(activity);
                $state.go('jobdetails');
            }
            SiteCheckAlarmSvc.reset();
        });
    };
    $scope.Cancel = function () {
        var confirmPopup = $ionicPopup.confirm({
            title: 'Site Pro',
            template: 'Are you sure you want to cancel this activity?',
            okText: 'Yes',
            cancelText: 'No'
        });

        confirmPopup.then(function (res) {
            if (res) {                
                $state.go('jobdetails');
            }
        });
        //$state.go('jobdetails');
    };
    $scope.back = function () {
        $state.go('jobdetails');
    };
    $scope.GetPhoto = function (item) {
        JobsSvc.job.siteCheckItem = item;
        JobsSvc.job.siteCheckItem.getPhoto = true;
        $state.go('actsitecheckitem');
    };
    $scope.setCheckedWithIssues = function (item) {
        item.checkedWithIssues = true;
        item.checked = false;
        item.checkedWithIssuesStyle = { color: "#ef473a" };
        item.checkedStyle = {};
        //$ionicPopup.prompt({
        //    title: 'Site Pro',
        //    template: 'Notes',
        //    inputType: 'textarea'
        //}).then(function (result) {
        //    if (result && result.length > 0) {
        //        item.notes = result;
        //        item.checkedWithIssues = true;
        //        item.checked = false;
        //        item.checkedWithIssuesStyle = { color: "#ef473a" };
        //        item.checkedStyle = {};
        //    }
        //});       
    };
    $scope.setChecked = function (item) {
        item.checked = true;
        item.checkedWithIssues = false;
        item.checkedWithIssuesStyle = {};
        item.checkedStyle = { color: "#33cd5f" };
    };
    $scope.voiceToText = function () {
        VoiceToTextSvc.Capture().then(function (result) {
            console.log(result);
            if ($scope.fields.notes == null)
                $scope.fields.notes = '';
            if ($scope.fields.notes.length > 0)
                $scope.fields.notes += ' ' + result[0];
            else
                $scope.fields.notes += result[0];
        }, function (error) {
            console.log(error);
        });        
    };
}]);