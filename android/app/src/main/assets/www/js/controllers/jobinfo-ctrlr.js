﻿mainApp.controller('JobInfoCtrlr', ['$scope', '$state', '$stateParams', 'JobsSvc', 'RCASvc', 'ContractorsSvc', 'AssetOwnersSvc', '$ionicLoading', '$linq', '$q', '$ionicHistory', '$timeout', function ($scope, $state, $stateParams, JobsSvc, RCASvc, ContractorsSvc, AssetOwnersSvc, $ionicLoading, $linq, $q, $ionicHistory, $timeout) {
    $scope.$on('$ionicView.beforeEnter', function(){
                
    });
    
    $scope.$on('$ionicView.enter', function () {
        $ionicLoading.show({ template: '<ion-spinner></ion-spinner>'});
        let seqTasks = async.seq(function(callback){
                RCASvc.getAll().then(function(rcas){
                    $scope.rcas = rcas;
                    callback(null, rcas);
                }, function(error){
                    callback(error, null);
                });
            },
            function(rcas, callback){
                JobsSvc.getJobInfo($stateParams.id).then(function(jobInfo){
                    console.log(jobInfo);
                    callback(null, jobInfo);
                }, function(error){
                    callback(error, null);
                });
            },
            function(jobInfo, callback){
                if(jobInfo.parties.rcaId){
                    ContractorsSvc.getAllByRca(jobInfo.parties.rcaId).then(function(contractors){
                        $scope.contractors = contractors;
                            $scope.getBranches(jobInfo.parties.contractorId);
                            if(jobInfo.parties.projectManagerId){
                                $scope.getProjectManagers(jobInfo.parties.contractorBranchId);
                            }
                        callback(null, jobInfo);
                    });
                }
                else
                    callback(null, jobInfo);
            },
            function(jobInfo, callback){
                if(jobInfo.parties.rcaId){
                    AssetOwnersSvc.getAllByRca(jobInfo.parties.rcaId).then(function(assetOwners){
                        $scope.assetOwners = assetOwners;
                        callback(null, jobInfo);
                    });
                }
                else
                    callback(null, jobInfo);
            }
        );
        seqTasks(function(error, jobInfo){
            $scope.jobInfo = jobInfo;
            setRoadLevels();
            //onsite contact signature
            $('#signature_pad').jSignature({height: 249, width: window.innerWidth - 37});
            if ($scope.jobInfo.parties.onsiteContactSignature && $scope.jobInfo.parties.onsiteContactSignature.length > 0) {

                $timeout(function () {
                    $('#signature_pad').jSignature('importData', 'data:image/jsignature;base30,' + $scope.jobInfo.parties.onsiteContactSignature);
                }, 100);
            }

            $ionicLoading.hide();
        });        
    });

    $scope.gplacesOptions = {componentRestrictions: {country:'nz'}, types:['geocode']};

    function setRoadLevels(){
        $scope.roadLevels = angular.copy(RoadLevels);
        if($scope.jobInfo.roadLevels != null){
            $scope.roadLevels.forEach(item => {
                if($scope.jobInfo.roadLevels.indexOf(item.name) >= 0)
                    item.checked = true;
            });
        }
    }

    $scope.getOtherParties = function(rcaId){
        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>'});
        $q.all({contractors: ContractorsSvc.getAllByRca(rcaId), assetOwners: AssetOwnersSvc.getAllByRca(rcaId)}).then(function(response){
            $scope.contractors = response.contractors;
            $scope.assetOwners = response.assetOwners;
        }).finally(function(){
            $ionicLoading.hide();
        });
    };

    $scope.getBranches = function (contractorId) {
        if (contractorId == null) {
            $scope.contractorBranches = null;
            $scope.projectManagers = null;
            return;
        }
        var contractor = $linq.Enumerable().From($scope.contractors).FirstOrDefault(null, 'f => f.id == "' + contractorId + '"');
        $scope.contractorBranches = contractor && contractor.branches ? contractor.branches : [];
    };

    $scope.getProjectManagers = function (contractorBranchId) {
        if (contractorBranchId == null) {
            $scope.projectManagers = null;
            return;
        }

        var contractorBranch = $linq.Enumerable().From($scope.contractorBranches).FirstOrDefault(null, 'f => f.id == "' + contractorBranchId + '"');
        $scope.projectManagers = contractorBranch.projectManagers;
    };

    $scope.GetProjectManagersNumber = function (projectManagerId) {
        if (projectManagerId == null) {
            $scope.projectManagerNumber = "";
            return;
        }
        var projectManagerNumber = $linq.Enumerable().From($scope.projectManagers).FirstOrDefault(null, 'f => f.id == "' + projectManagerId + '"');
        $scope.jobInfo.parties.projectManagerNumber = projectManagerNumber.mobile;
    };

    $scope.done = function(){
        $ionicLoading.show({ template: '<ion-spinner></ion-spinner>'});
        let selectedRoadLevels = $linq.Enumerable().From($scope.roadLevels).Where('w => w.checked == true').Select('s => s.name').ToArray();
        $scope.jobInfo.roadLevels = selectedRoadLevels;

        let signatureDataBase30 = $('#signature_pad').jSignature('getData', 'base30');
        $scope.jobInfo.parties.onsiteContactSignature = signatureDataBase30[1];

        if($scope.jobInfo.address && $scope.jobInfo.address.formatted_address)
            $scope.jobInfo.address = $scope.jobInfo.address.formatted_address;
        JobsSvc.updateJobInfo($scope.jobInfo).then(function(response){
            $state.go('app.jobdetail', {id: $stateParams.id, showAddActivity: false});
        }, function(error){
            console.log(error);
            alert('Something went wrong.');
        }).finally(function(){
            $ionicLoading.hide();
        });
    };

    $scope.back = function(){
        $state.go('app.jobdetail', {id: $stateParams.id, showAddActivity: false});
    };

    $scope.clearSignaturePad = function(){
        $('#signature_pad').jSignature('clear');
    };
}]);