mainApp.controller('ChooseTeamMembersCtrlr', ['$scope', '$ionicLoading', 'StaffSvc', 'AccountSvc', '$linq', 'parameters', '$ionicPlatform', function ($scope, $ionicLoading, StaffSvc, AccountSvc, $linq, parameters, $ionicPlatform){
    function loadStaff(){
        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>'});
        AccountSvc.getBranchesV2().then(function(branches){
            console.log(branches);
            $scope.branches = branches;
            $scope.branchId = AccountSvc.getBranchIdV2() || '';
            StaffSvc.getAll().then(function(staffMembers){                
                console.log(staffMembers);
                let currentTeamMembersLinq = $linq.Enumerable().From(parameters.currentMembers);
                let alreadyAddedStaff = [];
                staffMembers.forEach(item => {
                    let exist = currentTeamMembersLinq.FirstOrDefault(null, 'f => f.staffId == "' + item.id + '"');
                    if(exist)
                        alreadyAddedStaff.push(item);
                });
                staffMembers = $linq.Enumerable().From(staffMembers).Except(alreadyAddedStaff).ToArray();
                $scope.staffMembers = staffMembers;
                console.log($scope.staffMembers);
                console.log('[21]');
                console.log(AccountSvc.getUserIdV2());
                let owner = $linq.Enumerable().From($scope.staffMembers).FirstOrDefault(null, 'f => f.userId == "' + AccountSvc.getUserIdV2() + '"');
                if(owner)
                    owner.isMe = true;
                let ownerBianca = $linq.Enumerable().From($scope.staffMembers).FirstOrDefault(null, 'f => f.fullName == "Bianca Mackintosh"');
                let multiBianca = $linq.Enumerable().From($scope.staffMembers).Where('f => f.fullName == "Bianca Mackintosh"').ToArray();
                console.log('[27]');
                console.log(owner);
                console.log(ownerBianca);
                console.log(multiBianca);
                $ionicLoading.hide();                
            });
        });
    }

    $scope.$on('modal.shown', function(){
        loadStaff();
        /*        
        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>'});
        AccountSvc.getBranchesV2().then(function(branches){
            console.log(branches);
            $scope.branches = branches;
            $scope.branchId = AccountSvc.getBranchIdV2() || '';
            StaffSvc.getAll().then(function(staffMembers){                
                console.log(staffMembers);
                let currentTeamMembersLinq = $linq.Enumerable().From(parameters.currentMembers);
                let alreadyAddedStaff = [];
                staffMembers.forEach(item => {
                    let exist = currentTeamMembersLinq.FirstOrDefault(null, 'f => f.staffId == "' + item.id + '"');
                    if(exist)
                        alreadyAddedStaff.push(item);
                });
                staffMembers = $linq.Enumerable().From(staffMembers).Except(alreadyAddedStaff).ToArray();
                $scope.staffMembers = staffMembers;
                console.log($scope.staffMembers);
                console.log('[21]');
                console.log(AccountSvc.getUserIdV2());
                let owner = $linq.Enumerable().From($scope.staffMembers).FirstOrDefault(null, 'f => f.userId == "' + AccountSvc.getUserIdV2() + '"');
                if(owner)
                    owner.isMe = true;
                let ownerBianca = $linq.Enumerable().From($scope.staffMembers).FirstOrDefault(null, 'f => f.fullName == "Bianca Mackintosh"');
                let multiBianca = $linq.Enumerable().From($scope.staffMembers).Where('f => f.fullName == "Bianca Mackintosh"').ToArray();
                console.log('[25]');
                console.log(owner);
                console.log(ownerBianca);
                console.log(multiBianca);
                $ionicLoading.hide();                
            });
        });
        */        
    });

    $scope.teamMembers = [];
    $scope.cancel = function(){
        $scope.closeModal($scope.teamMembers);
    };

    $ionicPlatform.onHardwareBackButton(function () {
        $scope.closeModal($scope.teamMembers);
    });

    $scope.addMember = function(staff){
        console.log(staff);
        if ($scope.teamMembers.indexOf((staff.userId === null || staff.userId === undefined || staff.userId === "") ? staff.id : staff.userId) < 0){
            $scope.teamMembers.push((staff.userId === null || staff.userId === undefined || staff.userId === "") ? staff.id : staff.userId);
            let index = $scope.staffMembers.indexOf(staff);
            $scope.staffMembers.splice(index, 1);
        }
    };
    $scope.refreshData = function(){
        //alert('refresh!');
        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>'});
        StaffSvc.refreshData().then(function(staff){
            $ionicLoading.hide();
            loadStaff();
        }, function(error){
            $ionicLoading.hide();
            ons.notification.alert({title: 'Site Pro', message: 'Unable to refresh data.'});
        });        
    };
}]);