mainApp.controller('SelectAgencyStaffCtrl', ['$ionicPlatform', '$scope', 'AgenciesSvc', 'AccountSvc', '$ionicLoading', '$q', '$linq', 'parameters', function ($ionicPlatform, $scope, AgenciesSvc, AccountSvc, $ionicLoading, $q, $linq, parameters){
    console.log('[2]');
    console.log(parameters);
    function getAgencies(){
        let defer = $q.defer();

        async.parallel([
            function(callback){
                AccountSvc.getBranchesV2().then(function(branches){
                    callback(null, branches);
                });
            },
            function(callback){
                AgenciesSvc.getAgencies().then(function(agencies){
                    callback(null, agencies);
                });
            },
            function(callback){
                AgenciesSvc.getAgenciesStaff().then(function(agenciesStaff){
                    callback(null, agenciesStaff);
                });
            }
        ]).then(function(results){
            console.log('[17]:');
            console.log(results);
            $scope.branches = results[0];
            $scope.branchId = AccountSvc.getBranchIdV2() || '';
            $scope.agencies = results[1];
            $scope.agenciesStaff = results[2];
            console.log($scope.agenciesStaff);
            //remove current
            let qry = $linq.Enumerable().From($scope.agenciesStaff);
            let currentTms = qry.Where(staff => {
                return parameters.currentTeamMembers.indexOf(staff.id) > -1;
            }).ToArray();
            console.log('[30]');
            console.log($scope.agenciesStaff.length);
            $scope.agenciesStaff = qry.Except(currentTms).ToArray();
            console.log($scope.agenciesStaff.length);
            defer.resolve(true);
        }, function(error){
            defer.reject(error);
        });

        return defer.promise;
    }

    function loadAgencies(){
        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>' });
        getAgencies().then(function(){
            $ionicLoading.hide();
        });
    }

    $scope.$on('modal.shown', function(){
        loadAgencies();        
    });
    
    $scope.selectedAgencyStaff = [];
    $scope.selectStaff = function(staff){
        $scope.selectedAgencyStaff.push({staffId: staff.id, agencyId: staff.agencyId});
        let index = $scope.agenciesStaff.indexOf(staff);
        if(index >= 0)
            $scope.agenciesStaff.splice(index, 1);
    };

    $scope.done = function(){
        $scope.closeModal($scope.selectedAgencyStaff);
    };

    $ionicPlatform.onHardwareBackButton(function () {
        $scope.closeModal($scope.selectedAgencyStaff);
    });

    $scope.refreshData = function(){
        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>'});
        AgenciesSvc.refreshData().then(function(staff){
            $ionicLoading.hide();
            loadAgencies();
        }, function(error){
            $ionicLoading.hide();
            ons.notification.alert({title: 'Site Pro', message: 'Unable to refresh data.'});
        });        
    };
}]);