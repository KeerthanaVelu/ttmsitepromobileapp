mainApp.controller('TMPOnSiteAssessmentCtrlr',['$scope', '$state', '$stateParams', '$linq', '$ionicPopup', '$q', '$ionicLoading', '$ionicModal', 'LocationSvc', 'JobsSvc', function($scope, $state, $stateParams, $linq, $ionicPopup, $q, $ionicLoading, $ionicModal, LocationSvc, JobsSvc){
    $scope.$on('$ionicView.loaded', function(event ,data){
        console.log('view loaded');
        console.log($stateParams);
        $scope.jobName = $stateParams.jobName;
        if($stateParams.id === 'new'){
            $ionicLoading.show({ template: '<ion-spinner></ion-spinner>' });
            $scope.activity = angular.copy(JobActivityVM);
            $scope.activity.activityType = ActivityTypeEnum.TMPOnSiteAssessment;
            $scope.activity.jobId = $stateParams.jobid;
            $scope.activity.startTime = new Date();
            $scope.activity.assessment = angular.copy(Assessment);
            LocationSvc.getCurrentPosition().then(function (position) {
                if(position){
                    $scope.activity.assessment.startCoordinates = { latitude: position.coords.latitude, longitude: position.coords.longitude };
                }
            }, function (error) {
                console.log(error);            
            }).finally(function(){
                $ionicLoading.hide();
            });
        }
        else{
            //load activity details
            $ionicLoading.show({ template: '<ion-spinner></ion-spinner>' });
            JobsSvc.getActivity($stateParams.jobid, $stateParams.id).then(function(response){
                console.log(response);
                $scope.activity = response;
                if($scope.activity.assessment.details.tmpExpiry)
                    $scope.activity.assessment.details.tmpExpiry = new Date($scope.activity.assessment.details.tmpExpiry);
                if($scope.activity.assessment.details.carExpiry)
                    $scope.activity.assessment.details.carExpiry = new Date($scope.activity.assessment.details.carExpiry);
            }, function (error) {
                //console.log(error);
                $ionicPopup.alert('Error loading data.');
            }).finally(function(){
                $ionicLoading.hide();
            });
        }
    });
    $scope.$on('$ionicView.leave', function(event, data){
        console.log('leave');
    });
    $scope.$on('$ionicView.unloaded', function(){
        console.log('view unloaded');
    });
    $scope.cancel = function(){
        $state.go('app.jobdetail', {id: $stateParams.jobid, showAddActivity: true});
    };
    
    function getAllPoints(){
        var topPoints = $linq.Enumerable().From($scope.activity.assessment.checkingPoints).Where('w => w.subpoints == null').ToArray();
        var subPoints = $linq.Enumerable().From($scope.activity.assessment.checkingPoints).SelectMany('s => s.subpoints').ToArray();
        var allPoints = $linq.Enumerable().From(topPoints.concat(subPoints));
        return allPoints;
    };
    
    function countEmptyButtons() {
        var allPoints = getAllPoints();
        return allPoints.Count('c => c.result == 1');
    };

    function getAssessmentResult() {
        var result = null;
        var allPoints = getAllPoints();
        if(allPoints.Any('w => w.result == ' + CheckpointResult.mitigated))
            result = AssessmentResult.mitigated;
        else if(allPoints.Any('w => w.result == ' + CheckpointResult.unsuitable))
            result = AssessmentResult.unsuitable;
        else
            result = AssessmentResult.good;

        return result;
    };

    function isNotSuitable(){
        var allPoints = getAllPoints();
        return allPoints.Any('a => a.result == 3');
    };

    function getAdvisoryMessage(){
        let defer = $q.defer();
        $ionicPopup.prompt({
            title: 'TMP On-Site Assessment',
            templateUrl: 'advisoryMessage.html',
            cssClass: 'advisory-prompt',
            scope: $scope,
            buttons: [{
                text: 'Cancel',
                type: 'button-light',
                onTap: function(e) {
                  return 0;
                }
            },{
                text: 'OK',
                type: 'button-positive',
                onTap: function(e) {
                    return this.scope.activity.assessment.assessmentResult;
                }
            }]
        }).then(function(optionSelected){
            defer.resolve(optionSelected);                        
        });

        return defer.promise;
    }

    function alertUnsuitable(){
        let defer = $q.defer();
        if(isNotSuitable()){
            $ionicPopup.alert({title: 'TMP On-Site Assessment', template: 'Please call your Operations Manager immediately for instructions and support.'})
            .then(function(){
                defer.resolve(true);
            });
        }
        else defer.resolve(true);

        return defer.promise;
    }

    function confirmUnansweredQuestions(emptyButtons){
        let defer = $q.defer();
        if(emptyButtons > 0){
            var message = emptyButtons + ' questions remain unanswered.<br/>Are you sure you want to send?';
            $ionicPopup.confirm({title: 'TMP On-Site Assessment', template: message}).then(function(itsOkToSend){
                defer.resolve(itsOkToSend);
            });            
        }
        else defer.resolve(true);

        return defer.promise;
    }

    function selectEmailTargets() {
        $ionicLoading.show({template: '<ion-spinner></ion-spinner'});
        $ionicModal.fromTemplateUrl('selectEmailTargetsModal.html', {
            scope: $scope, animation: 'slide-in-up'
        }).then(function(modal){
            navigator.contacts.find([navigator.contacts.fieldType.displayName, navigator.contacts.fieldType.name, navigator.contacts.fieldType.emails], function (contacts) {
                $q.when($linq.Enumerable().From(contacts).Where('w => w.emails != null && w.emails.length > 0').Select('s => { name: s.displayName, email: s.emails[0].value, isAdded: false }').ToArray(), function(result){
                    console.log(result);
                    $scope.contacts = result;
                    $scope.recipients = [];
                    $ionicLoading.hide();                    
                    $scope.modal = modal;
                    $scope.modal.show();
                });            
            }, function (error) {
                console.log(error);
            });
        });        
    }    

    let otherTarget = {name: null, email: null, isNew: false};
    $scope.addOtherModel = angular.copy(otherTarget);
    $scope.recipients = [];
    $scope.addOtherTarget = function(){
        $scope.addOtherModel.isNew = true;
    };
    $scope.saveOtherTarget = function(){
        $scope.recipients.push($scope.addOtherModel);
        $scope.addOtherModel = angular.copy(otherTarget);
    };
    $scope.cancelOtherTarget = function(){
        $scope.addOtherModel = angular.copy(otherTarget);
    };
    
    $scope.addRecipient = function(recipient){
        let index = $scope.recipients.indexOf(recipient);
        if(index < 0){
            $scope.recipients.push(recipient);
            let contactsIndex = $scope.contacts.indexOf(recipient);
            if(contactsIndex >= 0)
                $scope.contacts.splice(contactsIndex, 1);
        }
    };
    $scope.removeRecipient = function(recipient){
        let index = $scope.recipients.indexOf(recipient);
        if(index >= 0){
            $scope.contacts.push(recipient);
            $scope.recipients.splice(index, 1);
        }
    };
    
    $scope.done = function(){
        var emptyButtons = countEmptyButtons();
        $scope.activity.assessment.assessmentResult = getAssessmentResult();        
        confirmUnansweredQuestions(emptyButtons).then(function(confirmed){
            if(confirmed){
                getAdvisoryMessage().then(function(advisorySelected){
                    if(advisorySelected > 0){
                        alertUnsuitable().then(function(){
                            console.log('send');
                            selectEmailTargets();
                        });
                    }
                });
            }
        });
    };

    $scope.sendAssessment = function(){
        if($scope.recipients.length > 0){
            $ionicLoading.show({template: '<ion-spinner></ion-spinner'});
            $scope.activity.endTime = new Date();
            $scope.activity.assessment.recipients = $scope.recipients;
            console.log($scope.activity);
            JobsSvc.addSendActivity($scope.activity.jobId, $scope.activity).then(function(){
                $scope.modal.hide();
                $ionicLoading.hide();
                $scope.cancel();
            });
        }
    };

    $scope.cancelSendAssessment = function(){
        $scope.modal.hide();
        $scope.cancel();
    };

    $scope.getCurrentAddress = function(){
        $ionicLoading.show({template: '<div style="text-align: center;">Finding current location<br/><ion-spinner></ion-spinner></div>'});
        LocationSvc.getCurrentAddress().then(function (address) {
            $scope.activity.assessment.details.location = address;
        }, function (error) {
            $ionicPopup.alert({title: 'TMP On-Site Assessment', template: 'Unable to find current location.'});
        }).finally(function(){
            $ionicLoading.hide();
        });
    };

    $scope.getExpiryDays = function(expiry, target){
        if($scope.activity.assessment != null){
            //console.log($scope.activity.assessment.Details[expiry]);
            var result = Math.ceil((moment($scope.activity.assessment.details[expiry]).diff(moment(), 'hours'))/24);
            $scope.activity.assessment.details[target] = result;
            return result;
        }
        return 0;
    };
}]);