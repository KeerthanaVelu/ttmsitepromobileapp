﻿mainApp.controller('NewJobCtrlr', ['$scope', '$state', '$ionicPopup', '$ionicLoading', '$q', 'RCASvc', 'JobsSvc', 'ContractorSvc', 'AssetOwnerSvc', '$resource', '$http', 'AccountSvc', 'ModalFactory', 'ContractorsSvc', 'AssetOwnersSvc', '$linq',
    function ($scope, $state, $ionicPopup, $ionicLoading, $q, RCASvc, JobsSvc, ContractorSvc, AssetOwnerSvc, $resource, $http, AccountSvc, ModalFactory, ContractorsSvc, AssetOwnersSvc, $linq) {

    $scope.$on('$ionicView.enter', function () {        
        //$scope.RCAs = RCASvc.GetAll();
        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>'});

        RCASvc.getAll().then(function(response){
            $scope.RCAs = response;
            $scope.job = angular.copy(NewJobVM);
            //$scope.job.id = (new ObjectId()).toString();
            $scope.job.tenantId = AccountSvc.getTenantId();
            $scope.job.branchId = AccountSvc.getBranchId();
            // $scope.job = JobsSvc.job || angular.copy(NewJobVM);
            // $scope.job.tenantId = AccountSvc.getTenantId();
            // $scope.job.branchId = AccountSvc.getBranchId();
            $scope.job.jobName = "Test Job " + (new Date()).valueOf();
            $scope.truckNames = [];
            $ionicLoading.hide();
        });

        // $scope.job = JobsSvc.job || angular.copy(NewJobVM);
        // $scope.job.tenantId = AccountSvc.getTenantId();
        // $scope.job.branchId = AccountSvc.getBranchId();
        // $scope.job.jobName = "Test Job " + (new Date()).valueOf();
        // $scope.truckNames = [];
        // $scope.$apply();
    });
    
    function valid() {
        var valid = true;
        var message = '';
        if ($scope.job.jobName == null || $scope.job.jobName.length == 0)
            message += "Job Name is required.<br/>";        
        
        if (message.length > 0) {
            $ionicPopup.alert({ title: "Error", template: message });
            valid = false;
        }

        return valid;
    }

    $scope.done = function () {
        if (!valid())
            return;

        if ($scope.job.trucks == null || $scope.job.trucks.length == 0) {
            $ionicPopup.alert({ title: 'Site Pro', template: 'Please choose 1 or more trucks.' });
            return;
        }

        $ionicLoading.show({ template: '<ion-spinner></ion-spinner>'});//, hideOnStateChange: true });

        if ($scope.job.id == null) {
            /*
            JobsSvc.job = $scope.job;
            JobsSvc.job.tempId = (new ObjectId()).toString();
            JobsSvc.job.jobName = $scope.job.jobName;
            JobsSvc.job.reference = $scope.job.reference;
            JobsSvc.job.description = $scope.job.description;
            JobsSvc.job.address = $scope.job.address;            
            JobsSvc.job.createdOn = moment.tz(moment(), 'NZ').toDate();
            JobsSvc.job.modifiedOn = moment.tz(moment(), 'NZ').toDate();
            JobsSvc.job.assignedUserId = AccountSvc.getUserId();
            JobsSvc.job.complete = false;
            JobsSvc.job.generalNotes = $scope.job.generalNotes;
            */
            JobsSvc.addV2($scope.job).then(function(response){
                $state.go('app.jobdetail', {id: response});
            }, function(error){
                console.log(error);
                alert('Something went wrong. We apologize for the inconvenience.');
            }).finally(function(response){
                console.log(response);
                $ionicLoading.hide();
            });
        }        

        //$state.go('jobdetails');
    };
    $scope.Cancel = function () {
        $state.go('app.jobs');
    };
    $scope.getOtherParties = function(rcaId){
        //contractors
        //asset owners
        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>'});
        // ContractorsSvc.getAllByRca(rcaId).then(function(contractors){
        //     $scope.contractors = contractors;
        //     $ionicLoading.hide();
        // });
        $q.all({contractors: ContractorsSvc.getAllByRca(rcaId), assetOwners: AssetOwnersSvc.getAllByRca(rcaId)}).then(function(response){
            $scope.contractors = response.contractors;
            $scope.assetOwners = response.assetOwners;
            $ionicLoading.hide();            
        });
    };    

    //#region
    /*
    $scope.GetOtherParties = function (rcaId) {
        if (rcaId == null) {
            $scope.contractors = null;
            $scope.contractor = null;
            $scope.contratorBranches = null;
            $scope.contractorBranch = null;
            $scope.projectManagers = null;
            $scope.assetOwners = null;
        }
        $scope.GetContractors(rcaId);
        $scope.GetAssetOwners(rcaId);
        $scope.rcaId = rcaId;
    };
    */
    //#endregion

    //#region
/* 
    $scope.GetContractors = function(rcaId){
        if(rcaId != null){
            $scope.contractors = ContractorSvc.GetAllByRCA(rcaId);
            $scope.contractorBranches = null;
            $scope.projectManagers = null;
        }
    };
 */    
    //#endregion
    $scope.GetBranches = function (contractorId) {
        if (contractorId == null) {
            $scope.contractorBranches = null;
            $scope.projectManagers = null;
            return;
        }
        var contractor = $linq.Enumerable().From($scope.contractors).FirstOrDefault(null, 'f => f.id == "' + contractorId + '"');
        $scope.contractor = contractor;
        $scope.contractorBranches = contractor.branches;
    };

    $scope.GetProjectManagers = function (contractorBranchId) {
        if (contractorBranchId == null) {
            $scope.projectManagers = null;
            return;
        }

        var contractorBranch = $linq.Enumerable().From($scope.contractorBranches).FirstOrDefault(null, 'f => f.id == "' + contractorBranchId + '"');
        $scope.projectManagers = contractorBranch.projectManagers;
    };

    $scope.GetAssetOwners = function (rcaId) {
        if(rcaId != null)
            $scope.assetOwners = AssetOwnerSvc.GetAllByRCA(rcaId);
        /*if (rcaId == null)
            $scope.assetOwners = null;
        else {
            $scope.assetOwners = AssetOwnerSvc.GetAllByRCA(rcaId);
        }*/
    };

    $scope.setTestParties = function () {
        var defer = $q.defer();

        $scope.RCAs = RCASvc.GetAll();

        var rca = $scope.RCAs[0];
        $scope.rcaId = rca.id;
        $scope.GetOtherParties(rca.id);
        $scope.contractors = ContractorSvc.GetAllByRCA(rca.id);
        $scope.contractor = $scope.contractors[0];
        $scope.contractorBranches = $scope.contractors[0].branches;
        $scope.contractorBranch = $scope.contractorBranches[0];
        $scope.GetProjectManagers($scope.contractorBranch);
        $scope.job.projectManagerId = $scope.projectManagers[0].id;
        $scope.job.assetOwnerId = $scope.assetOwners[0].id;
        defer.resolve(true);

        return defer.promise;
    };

    $scope.chooseTrucks = function () {
        if ($scope.job.trucks == null)
            $scope.job.trucks = [];

        ModalFactory.showTrucksSelection($scope.job.trucks).then(function (result) {
            if (result) {
                //set the trucks
                $scope.job.trucks = result.trucks;
                $scope.truckNames = result.truckNames;
            }
        }, function (error) {
            console.log(error);
        });
    };
}]);

/*
syncservice
    QObject:Job = db.getNextQ();
    if(QObject)
        QObject.sync()
            if(ok)
                postsynchandlerservice(QObject);
                loop
            

job.sync()
    sendtoserver()
    if(ok)
        set remoteId from server
        save to database    
        return ok


postsynchandlerservice.execute(QObject){
    if(QObject is type job)
        notify ui of creation
}

job page ui
    on notify ui of creation
        set server id


work on job with id as localId
put every data for syncing into the sync service
sync service will do the syncing via a queue
after a qdata is successfully sent to the server
do post sync operation like notify ui if any
remove it from the Q database
*/