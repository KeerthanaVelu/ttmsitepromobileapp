mainApp.controller('UserSettingsCtrlr', ['$scope', '$ionicLoading', '$state', 'AccountSvc', 'DBSvc', function ($scope, $ionicLoading, $state, AccountSvc, DBSvc){
    $scope.$on('$ionicView.loaded', function(){
        $scope.account = AccountSvc.get();
        $('#user_signature').jSignature({height: 249, width: window.innerWidth - 37});
        if ($scope.account.signatureData && $scope.account.signatureData.length > 0)
            $('#user_signature').jSignature('importData', 'data:image/jsignature;base30,' + $scope.account.signatureData);
    });

    $scope.changeSignature = false;
    $scope.clearSignaturePad = function(){
        $('#user_signature').jSignature('clear');
    };

    $scope.changeSignaturePad = function () {
        $scope.changeSignature = true;
    };

    $scope.back = function(){
        $state.go('app.jobs');
    };
    $scope.done = function(){
        //save settings
        $ionicLoading.show({ template: '<ion-spinner></ion-spinner>'});
        let signatureDataBase30 = $('#user_signature').jSignature('getData', 'base30');

        var signatireData = $('#user_signature').jSignature('getData');
        var userId = $scope.account.id;
        var signatureName = "User_" + (new ObjectId()).toString() + ".jpg";
        $scope.account.signiture = signatureName;
        $scope.account.signatureData = signatureDataBase30;

        if ($scope.changeSignature === true) {
            AccountSvc.update($scope.account).then(function () {
                $ionicLoading.hide();
                $state.go('app.jobs');
                AccountSvc.updateSignature(userId, signatireData, signatureName).then(function () {
                    $ionicLoading.hide();
                });
                DBSvc.updatesigniture($scope.account.id, signatureName).then(function (result) {

                        }, function (error) {
                            defer.reject(error);
                        });

            });
        } else {
            $state.go('app.jobs');
            $ionicLoading.hide();
        }


    };
}]);