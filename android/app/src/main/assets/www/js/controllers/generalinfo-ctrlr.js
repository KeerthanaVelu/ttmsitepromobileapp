﻿mainApp.controller('GeneralInfoCtrlr', ['$scope', '$ionicHistory', 'JobsSvc', '$linq', function ($scope, $ionicHistory, JobsSvc, $linq) {
    $scope.$on('$ionicView.loaded', function () {
        if ($scope.job.roadLevels) {
            for (var i = 0; i < $scope.roadLevels.length; i++) {
                var level = $scope.roadLevels[i];
                if ($scope.job.roadLevels.indexOf(level.name) >= 0)
                    level.checked = true;
            }
        }
    });
    $scope.job = angular.copy(JobsSvc.job);
    $scope.forms = {};
    $scope.back = function () {
        $ionicHistory.goBack();
    };
    $scope.roadLevels = [{ name: 'LR', checked: false }, { name: 'LV', checked: false }, { name: 'L1', checked: false }, { name: 'L2', checked: false }, { name: 'L2LS', checked: false }, { name: 'L3', checked: false }];
    $scope.roadLevelsChanged = function () {
        $scope.job.roadLevels = $linq.Enumerable().From($scope.roadLevels).Where('w => w.checked  == true').Select('s => s.name').ToArray();
        console.log($scope.job.roadLevels);
    };
    $scope.done = function () {
        var roadLevelsChanged = false;
        if (JobsSvc.job.roadLevels != $scope.job.roadLevels || (JobsSvc.job.roadLevels && $scope.job.roadLevels && JobsSvc.job.roadLevels.toString() != $scope.job.roadLevels))
            roadLevelsChanged = true;
        if ($scope.forms.generalInfoForm.$dirty || roadLevelsChanged) {
            JobsSvc.job.jobName = $scope.job.jobName;
            JobsSvc.job.reference = $scope.job.reference;
            JobsSvc.job.description = $scope.job.description;
            JobsSvc.job.address = $scope.job.address;
            JobsSvc.job.roadLevels = $scope.job.roadLevels;
            JobsSvc.job.poNumber = $scope.job.poNumber;
            var data = { isJobGeneralInfo: true, jobId: $scope.job.id, job: $scope.job };
            JobsSvc.updateGeneralInfo(data).then(function (result) {
                $ionicHistory.goBack();
            }, function (error) {
                console.log(error);
                alert('Something went wrong.');
            });
        }
    };
}]);