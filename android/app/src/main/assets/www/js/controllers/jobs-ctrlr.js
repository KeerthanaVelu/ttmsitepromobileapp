﻿mainApp.controller('JobsCtrlr', ['$scope', '$state', '$ionicPopover', 'JobsSvc', '$ionicLoading', '$q', 'AccountSvc', '$linq', 'ApiSvc', 'LocationSvc', '$ionicPopup', '$ionicActionSheet', '$ionicModal', 'ModalFactory', 'TransferJobSvc', 'TenantSvc', function ($scope, $state, $ionicPopover, JobsSvc, $ionicLoading, $q, AccountSvc, $linq, ApiSvc, LocationSvc, $ionicPopup, $ionicActionSheet, $ionicModal, ModalFactory, TransferJobSvc, TenantSvc) {
    $ionicPopover.fromTemplateUrl('views/adminoptions.html', {
        scope: $scope
    }).then(function (popover) {
        $scope.popover = popover;
    });
    $ionicPopover.fromTemplateUrl('views/jobitemoptions.html', {
        scope: $scope
    }).then(function (popover) {
        $scope.jobItemOptions = popover
    });
    $scope.$on('$ionicView.loaded', function (event) {
        console.log('JobsCtrlr -> $ionicView.loaded');
        $scope.userInfo = AccountSvc.getUserInfo();        
    });

    $scope.$on('$ionicView.enter', function(event){
        $ionicLoading.show({ template: '<ion-spinner></ion-spinner>' });
        JobsSvc.getAllV2().then(function(jobs){
            $scope.jobs = jobs;
        }).finally(function(){
            $ionicLoading.hide();
        });
    });

    //#region
    /* ORIGINAL
    $scope.$on('$ionicView.enter', function () {
        console.log('JobsCtrlr -> $ionicView.enter');
        $scope.Activity = null;
        $ionicLoading.show({ template: '<ion-spinner></ion-spinner>' });
        loadJobs().then(function (jobs) {
            $scope.jobs = jobs;
            $ionicLoading.hide();
            LocationSvc.getCurrentPosition({ timeout: 2000, enableHighAccuracy: true }).then(function () { });
        }, function (error) {
            console.log(error);
        });
    });
     */
    //#endregion

    $scope.$on('jobReceived', function (event) {
        //loadJobs();
        console.log('event: jobReceived');
        $ionicLoading.show({ template: '<ion-spinner></ion-spinner>' });
        JobsSvc.getAllV2().then(function(jobs){
            $scope.jobs = jobs;
        }).finally(function(){
            $ionicLoading.hide();
        });
    });

    $scope.$on('mobileJobTransferReceived', function (event, data) {
        var job = $linq.Enumerable().From($scope.jobs).FirstOrDefault(null, 'f => f.id == "' + data.jobId + '"');
        var index = $scope.jobs.indexOf(job);
        if (index >= 0) {
            $scope.jobs.splice(index, 1);
            JobsSvc.save().then(function () {
            }, function (error) {
                console.log(error);
                alert('An error occured.');
            });
        }
    });

    function loadJobs() {
        var defer = $q.defer();
        var jobs = JobsSvc.getAll();
        defer.resolve(jobs);
        return defer.promise;
    }

    $scope.jobs = [];
    $scope.adminOptions = function ($event) {
        $scope.popover.show($event);
    };
    $scope.manageStaff = function () {
        $scope.popover.hide();
        $state.go('app.staff');
    };
    $scope.searchJobs = '';
    $scope.newJob = function () {
        //$state.go('app.jobdetail', {id: 'response'});
        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>', hideOnStateChange: true });
        $state.go('app.newjob');
    };
    $scope.loadJob = function(id){
        $state.go('app.jobdetail', {id: id});
    };

    $scope.emergency = function () {
        $state.go('emergency');
    };    
    $scope.delete = function (job, event) {
        event.stopPropagation();
        event.preventDefault();
        var confirmPopup = $ionicPopup.confirm({
            title: 'Site Pro',
            template: 'Do you want to delete this job? <br/>' + job.jobName,
            okText: 'Yes',
            cancelText: 'No'
        });

        confirmPopup.then(function (res) {
            if (res) {
                var index = $scope.jobs.indexOf(job);
                if (index >= 0) {
                    $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>', hideOnStateChange: true });
                    $scope.jobs.splice(index, 1);
                    JobsSvc.save().then(function () {
                        $ionicLoading.hide();
                    }, function (error) {
                        $ionicLoading.hide();
                        console.log(error);
                        alert('An error occured.');
                    });
                }                
            }
        });
    };
    
    $scope.transferJobModal = function (job) {
        var confirmPopup = $ionicPopup.confirm({
            title: 'Site Pro',
            template: 'Have you briefed the receiving STMS? <br/>Job Name: ' + job.jobName,
            okText: 'Yes',
            cancelText: 'No'
        });
        confirmPopup.then(function (result) {
            if (result) {
                //load the branches/STMS
                $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>', hideOnStateChange: true });
                TenantSvc.getInfo(AccountSvc.getTenantId()).then(function (tenantInfo) {
                    console.log(tenantInfo);
                    $ionicLoading.hide();                    
                    TransferJobSvc.transferForm(job, tenantInfo.branches).then(function (data) {
                        console.log(data);
                        if (data) {
                            TransferJobSvc.transfer(data).then(function (transferResult) {
                                console.log(transferResult);
                            }, function (error) {
                                console.log(error);
                                $ionicPopup.alert({ title: 'Site Pro', template: "Something went wrong." });
                            });
                        }
                    }, function (error) {
                        console.log(error);
                        $ionicLoading.hide();
                        $ionicPopup.alert({ title: 'Site Pro', template: "Something went wrong." });
                    });                    
                }, function (error) {
                    $ionicLoading.hide();
                    console.log(error);
                    $ionicPopup.alert({ title: 'Site Pro', template: "Can't connect to server." });
                });                
            };
        });        
    };
    $scope.showItemOptions = function (job, event) {        
        event.stopPropagation();
        event.preventDefault();        
        $ionicActionSheet.show({
            cssClass: 'job-item-actionsheet',
            buttons: [
                { text: 'Transfer' }
            ],
            destructiveText: 'Delete',
            cancelText: 'Cancel',
            cancel: function () {
                // add cancel code..
            },
            buttonClicked: function (index) {
                if (index == 0) {
                    $scope.transferJobModal(job);
                }
                return true;
            },
            destructiveButtonClicked: function () {
                $scope.delete(job, event);
                return true;
            }
        });
    };
    $scope.logout = function(){
        $scope.popover.hide();
        $state.go('app.login');
    }; 
}]);
