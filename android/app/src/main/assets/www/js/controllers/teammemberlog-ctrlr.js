﻿mainApp.controller('TeamMemberLogCtrlr', ['$scope', '$state', 'JobsSvc', '$ionicLoading', '$stateParams', '$ionicPopup', '$q', 'BreakTimeOptions', function ($scope, $state, JobsSvc, $ionicLoading, $stateParams, $ionicPopup, $q, BreakTimeOptions) {
    function getTimeLogsList(){
        let results = [];
        $scope.teamMemberLogs.timeLogs.forEach(log => {
            results.push({logTypeName: TimeLogTypeEnum.props[log.timeLogType].name, time: log.time, logType: log.timeLogType});
        });
        $scope.timeLogsList = results.reverse();
    }

    function getTimeLogsSummary(){
        let workDuration = breakDuration = totalWorkTime = 0;
        let startTime, endTime, breakTimeStart, breakTimeEnd;
        $scope.teamMemberLogs.timeLogs.forEach(log => {
            if(log.timeLogType === TimeLogTypeEnum.STARTTIME)
                startTime = log.time;
            else if(log.timeLogType === TimeLogTypeEnum.ENDTIME){
                endTime = log.time;
                if(startTime){
                    let diff = moment(endTime).diff(startTime, 'minutes');
                    workDuration += diff;
                }
            }
            else if(log.timeLogType === TimeLogTypeEnum.BREAKTIMESTART)
                breakTimeStart = log.time;
            else if(log.timeLogType === TimeLogTypeEnum.BREAKTIMEEND){
                if(breakTimeStart){
                    breakTimeEnd = log.time;
                    let diff = moment(breakTimeEnd).diff(breakTimeStart, 'minutes');
                    breakDuration += diff;
                }
            }
        });
        //totalWorkTime = workDuration + breakDuration;
        totalWorkTime = workDuration + $scope.teamMemberLogs.totalBreakTimeMinutes;
        let workDurationHours, workDurationMins, totalWorkTimeHours, totalWorkTimeMins;
        workDurationHours = moment.duration(workDuration, 'minutes').hours();
        workDurationMins = moment.duration(workDuration, 'minutes').minutes();
        breakDurationHours = moment.duration(breakDuration, 'minutes').hours();
        breakDurationMins = moment.duration(breakDuration, 'minutes').minutes();
        totalWorkTimeHours = moment.duration(totalWorkTime, 'minutes').hours();
        totalWorkTimeMins = moment.duration(totalWorkTime, 'minutes').minutes();

        $scope.timeLogsSummary = {
            workDurationHours: workDurationHours,
            workDurationMins: workDurationMins,
            breakDurationHours: breakDurationHours,
            breakDurationMins: breakDurationMins,
            totalWorkTimeHours: totalWorkTimeHours,
            totalWorkTimeMins: totalWorkTimeMins
        };
    }

    function setButtonStates(){
        let lastLog = $scope.teamMemberLogs.timeLogs[$scope.teamMemberLogs.timeLogs.length - 1];
        if(lastLog){
            $scope.isWorkTime = lastLog.timeLogType < TimeLogTypeEnum.ENDTIME;
            $scope.isBreakTime = lastLog.timeLogType == TimeLogTypeEnum.BREAKTIMESTART;
        }
    }

    function getTeamMemberLogs(){
        let defer = $q.defer();
        //$ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>' });
        JobsSvc.getTeamMemberLogs($stateParams.id, $stateParams.teamMemberId).then(function(teamMemberLogs){
            console.log('[64]');
            console.log(teamMemberLogs);
            $scope.teamMemberLogs = teamMemberLogs;
            $scope.lastLog = $scope.teamMemberLogs.timeLogs.length > 0 ? $scope.teamMemberLogs.timeLogs[$scope.teamMemberLogs.timeLogs.length-1] : null;
            getTimeLogsSummary();    
            getTimeLogsList();
            setButtonStates();
            defer.resolve(true);
        }, function(error){
            defer.reject(error);
        });

        return defer.promise;
    }

    function newTimeLogORIG(){
        let newLog = angular.copy(TimeLogVM);        
        newLog.jobId = $scope.teamMemberLogs.jobId;
        newLog.rJobId = $scope.teamMemberLogs.rJobId;
        newLog.teamMemberId = $scope.teamMemberLogs.id;
        newLog.rTeamMemberId = $scope.teamMemberLogs.rId;
        return newLog;
    }

    $scope.$on('$ionicView.enter', function () {
        $scope.isJobArchived = $stateParams.isJobArchived;
        //getTeamMemberLogs();        
        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>' }).then(function(){
            getTeamMemberLogs().then(function(){
                //$ionicLoading.hide();
            }, function(error){
                alert('An error occured.');
            }).finally(function(){
                $ionicLoading.hide();
            });
        });
    });

    $scope.$on('timelogsChanged', function(){
        console.log('TeamMemberLogCtrlr -> timelogsChanged triggered');
        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>' }).then(function(){
            getTeamMemberLogs().then(function(){
                //$ionicLoading.hide();
            }, function(error){
                alert('An error occured.');
            }).finally(function(){
                $ionicLoading.hide();
            });
        });        
    });
    
    $scope.done = function () {
        $state.go('app.team',{id: $stateParams.id, isJobArchived: $scope.isJobArchived});
    };

    $scope.isBreakTime = false;
    $scope.isWorkTime = false;
    $scope.isProcessingShift = false;

    $scope.toggleWorkTime = function(){
        console.log('[109] toggleWorkTime()');
        console.log('$scope.isProcessingShift: ' + $scope.isProcessingShift);
        if(!$scope.isProcessingShift){            
            //$scope.isProcessingShift = true;
            let logType = -1;
            if ($scope.teamMemberLogs.timeLogs.length == 0) {
                logType = TimeLogTypeEnum.STARTTIME;
            }
            else {
                let lastLog = $scope.teamMemberLogs.timeLogs[$scope.teamMemberLogs.timeLogs.length - 1];
                if (lastLog.timeLogType < TimeLogTypeEnum.ENDTIME)
                    logType = TimeLogTypeEnum.ENDTIME;
                else
                    logType = TimeLogTypeEnum.STARTTIME;
            }

            if (logType === TimeLogTypeEnum.ENDTIME) {
                //let confirm = ons.notification.confirm({title: 'Site Pro', message: 'You are about to end the shift for this person, are you sure?', cancelable: true, buttonLabels:['Yes', 'No'] });
                let confirm = $ionicPopup.confirm({ title: 'Site Pro', template: 'You are about to end the shift for this person, are you sure?', cancelText: 'No', okText: 'Yes' });
                confirm.then(function (response) {
                    if (response) {
                        $scope.isProcessingShift = true;
                        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>' }).then(function () {
                            newTimeLog(logType, null, false).then(function (response) {
                                JobsSvc.hideTeamMember($stateParams.id, $scope.teamMemberLogs.id).then(function () {
                                    $scope.isProcessingShift = false;
                                    $ionicLoading.hide();
                                    $scope.done();
                                });
                                /*
                                getTeamMemberLogs().then(function(){                                        
                                }, function(error){
                                    alert('An error occured.');
                                }).finally(function(){                        
                                    $ionicLoading.hide();
                                    $scope.isProcessingShift = false;
                                });
                                */
                            });
                        });
                    }
                });
            }
            else {
                newTimeLog(logType, null, false).then(function (response) {
                    getTeamMemberLogs().then(function () {
                        $ionicLoading.hide();
                        $scope.isProcessingShift = false;
                    }, function (error) {
                        alert('An error occured.');
                    }).finally(function () {
                        $ionicLoading.hide();
                        $scope.isProcessingShift = false;
                    });
                });
            }

        }
    };

    $scope.toggleWorkTimeORIG = function(){
        console.log('[109] toggleWorkTime()');
        console.log('$scope.isProcessingShift: ' + $scope.isProcessingShift);
        if(!$scope.isProcessingShift){            
            $scope.isProcessingShift = true;
            $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>' }).then(function(){
                let newLog = newTimeLog();
                let logType = -1;
                if($scope.teamMemberLogs.timeLogs.length == 0){
                    newLog.timeLogType = TimeLogTypeEnum.STARTTIME;
                    newLog.time = new Date();
                }
                else{
                    let lastLog = $scope.teamMemberLogs.timeLogs[$scope.teamMemberLogs.timeLogs.length - 1];
                    if(lastLog.timeLogType < TimeLogTypeEnum.ENDTIME)
                    logType = TimeLogTypeEnum.ENDTIME;
                    else
                    logType = TimeLogTypeEnum.STARTTIME;
                    
                    newLog.timeLogType = logType;
                    newLog.time = new Date();
                }
                $scope.teamMemberLogs.timeLogs.push(newLog);
                JobsSvc.addTimeLog(newLog).then(function(response){
                    getTimeLogsSummary();
                    getTimeLogsList();
                    setButtonStates();
                    $scope.isProcessingShift = false;
                    $ionicLoading.hide();
                });
            });
        }
    };
    /*
    $scope.$on('timelogsChanged', function(){
        console.log('timelogsChanged triggered');
        getTeamMemberLogs();        
    });
    */
    function newTimeLog(logType, duration, isPaid){
        let defer = $q.defer();        
        let newLog = angular.copy(TimeLogVM);        
        newLog.jobId = $scope.teamMemberLogs.jobId;
        newLog.rJobId = $scope.teamMemberLogs.rJobId;
        newLog.teamMemberId = $scope.teamMemberLogs.id;
        newLog.rTeamMemberId = $scope.teamMemberLogs.rId;
        newLog.time = new Date();
        newLog.timeLogType = logType;
        if(logType === TimeLogTypeEnum.BREAKTIMESTART){
            newLog.duration = duration;
            if(isPaid)
                newLog.isPaid = true;
        }
        JobsSvc.addTimeLog(newLog).then(function(response){
                defer.resolve(newLog.time);
        });

        return defer.promise;
    }

    $scope.pauseTime = function(){
        BreakTimeOptions.getOptions($scope.teamMemberLogs.fullName, $scope).then(function(option){
            console.log(option);
            if(option){
                newTimeLog(TimeLogTypeEnum.BREAKTIMESTART, option.duration, option.isPaid).then(function(breakTimeStart){
                    if(option.selected !== 'ongoing'){
                        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>' });
                        JobsSvc.stopBreakTimeAt($stateParams.id, $scope.teamMemberLogs.id, option.duration).then(function(){
                            $ionicLoading.hide();
                            getTeamMemberLogs();
                        });
                    }
                });
            }
        });
    };

    $scope.resumeTime = function(){
        newTimeLog(TimeLogTypeEnum.BREAKTIMEEND).then(function(){
            getTeamMemberLogs();
        });
    };

    $scope.toggleBreakTimeORIG = function(){
        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>' });
        let newLog = newTimeLog();        
        let lastLog = $scope.teamMemberLogs.timeLogs[$scope.teamMemberLogs.timeLogs.length - 1];
        let logType = -1;
        if(lastLog.timeLogType == TimeLogTypeEnum.STARTTIME || lastLog.timeLogType == TimeLogTypeEnum.BREAKTIMEEND)
            logType = TimeLogTypeEnum.BREAKTIMESTART;
        else
            logType = TimeLogTypeEnum.BREAKTIMEEND;

        newLog.timeLogType = logType;
        newLog.time = new Date();
        $scope.teamMemberLogs.timeLogs.push(newLog);
        JobsSvc.addTimeLog(newLog).then(function(response){
            $ionicLoading.hide();
            getTimeLogsSummary();    
            getTimeLogsList();
            setButtonStates();
        });        
    };
    
    $scope.remove = function(){
        $ionicPopup.confirm({template: 'Are you sure you want to remove this team member?'}).then(function(proceed){
            if(proceed){
                console.log($stateParams);                
                $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>' });
                JobsSvc.removeTeamMember($stateParams.id, $stateParams.teamMemberId).then(function(response){            
                    $ionicLoading.hide();
                    $scope.done();                    
                });
            }
        });
    };
}]);