﻿mainApp.controller('OnSiteRecordItemCtrlr', ['$scope', '$state', 'PhotoSvc', 'TempSvc', '$ionicHistory', '$stateParams', 'JobsSvc', '$ionicLoading', function ($scope, $state, PhotoSvc, TempSvc, $ionicHistory, $stateParams, JobsSvc, $ionicLoading) {
    $scope.$on('$ionicView.enter', function () {
        console.log('[3]:');
        console.log(TempSvc.item);
        $scope.isComplete = $stateParams.isComplete;
        $scope.isJobArchived = $stateParams.isJobArchived;
        $scope.item = TempSvc.item;
        $scope.headers = {jobName: $stateParams.jobName};
        // if($stateParams.runGetPhoto)
        //     $scope.getPhotoFromCamera();
    });    
    $scope.done = function () {
        $ionicLoading.show({ template: '<ion-spinner></ion-spinner>' });
        JobsSvc.updateSiteCheckItem($stateParams.jobid, $stateParams.id, $stateParams.itemIndex, $scope.item).then(function(response){
            $ionicLoading.hide();
            $ionicHistory.goBack();
        });        
    };
    $scope.getPhotoFromCamera = function(){
        PhotoSvc.getPhotoFromCamera().then(function(photo){
            $scope.item.photos.push(photo);
        }, function(error){
            alert('Something went wrong while getting a photo.');
        });
    };
    $scope.getPhoto = function () {
        $scope.photoSource = null;
        PhotoSvc.getPhoto().then(function (photo) {
            //console.log(photo);
            $scope.item.photos.push(photo);
        }, function(error){
            alert('Something went wrong while getting a photo.');
        });
    };
    $scope.setCheckedWithIssues = function () {
        $scope.item.checkedWithIssues = true;
        $scope.item.checked = false;        
    };
    $scope.setChecked = function () {
        $scope.item.checked = true;
        $scope.item.checkedWithIssues = false;        
    };
    $scope.removePhoto = function(index){
        $scope.item.photos.splice(index, 1);
    };
}]);