mainApp.controller('ChooseActivityCtrlr', ['$scope', '$q', 'parameters', '$linq', function($scope, $q, parameters, $linq){
    $scope.$on('modal.shown', function(){
        console.log(parameters);
        //show list of activities
        $scope.activities = angular.copy(JobActivities);
        $scope.activities.unshift({Id: -1, Activity: 'Manage Team'});
        //console.log($scope.activities);
        //color code activities
        //green = done
        //blue = On-Site Record
        //refer to last completed activity
        $scope.activities.forEach(activity => {
            if($linq.Enumerable().From(parameters.activityStates).Contains(activity.Id)){
                activity.statusStyle = 'activity-done';
                if(activity.Id === ActivityTypeEnum.OnSiteRecord)
                    activity.statusStyle = 'activity-done-repeat';
                else if(activity.Id === ActivityTypeEnum.StartOfShift)
                    activity.disabled = true;
            }
            
            if(activity.Id != ActivityTypeEnum.ManageTeam && !parameters.teamStarted)
                activity.disabled = true;
            else
                activity.disabled = false;
        });
    });

    $scope.back = function(){
        $scope.closeModal(null);
    };

    $scope.chooseActivity = function(activity){
        //console.log(activity);
        $scope.closeModal(activity.Id);
        return;        
    };
}]);