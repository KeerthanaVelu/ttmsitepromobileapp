﻿mainApp.controller('TeamCtrlr', ['$scope', '$state', '$stateParams', '$linq', 'JobsSvc', 'StaffSvc', 'AccountSvc', '$q', '$ionicLoading', 'ModalFactory', '$ionicPopup', 'BreakTimeOptions', 'AgenciesSvc',
 function ($scope, $state, $stateParams, $linq, JobsSvc, StaffSvc, AccountSvc, $q, $ionicLoading, ModalFactory, $ionicPopup, BreakTimeOptions, AgenciesSvc) {
    $scope.teamMembersList = [];
    $scope.address = "";

    function getTeamMembers(){
        let defer = $q.defer();
        JobsSvc.getTeam($stateParams.id).then(function (teamMembers) {
            console.log('[8]:');
            console.log(teamMembers);
            showTeamMemberList(teamMembers).then(function(response){
                defer.resolve(true);
            });
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    }

    function getAllTeamMembers(){
        let defer = $q.defer();

        JobsSvc.getAllTeam($stateParams.id).then(function (teamMembers) {
            showTeamMemberList(teamMembers).then(function(response){
                defer.resolve(true);
            });
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    }

    function showTeamMemberList(teamMembers){
        function getCurrentTimelogs(listItem, member){
            let lastLog = member.timeLogs ? member.timeLogs[member.timeLogs.length-1] : null;
            let lastStartTime, lastEndTime = null;

            listItem.lastLogType = lastLog ? lastLog.timeLogType : null;
            listItem.lastLogTime = lastLog ? lastLog.time : null;
            listItem.isShiftStarted = lastLog != null && lastLog.timeLogType == 0;

            if(lastLog && lastLog.timeLogType == TimeLogTypeEnum.STARTTIME)
                lastStartTime = { time: lastLog.time };
            else{
                lastStartTime = $linq.Enumerable().From(member.timeLogs).LastOrDefault(null, 'f => f.timeLogType == ' + TimeLogTypeEnum.STARTTIME);
                lastEndTime = $linq.Enumerable().From(member.timeLogs).LastOrDefault(null, 'f => f.timeLogType == ' + TimeLogTypeEnum.ENDTIME);
            }
            if(lastStartTime)
                listItem.lastStartTime = lastStartTime.time;
            if(lastEndTime)
                listItem.lastEndTime = lastEndTime.time;
        }

        function setDefaultTotalBreakTimeMinutes(teamMember){
            if(teamMember){
                if(teamMember.hasOwnProperty('totalBreakTimeMinutes')){
                    if(teamMember.totalBreakTimeMinutes == undefined || teamMember.totalBreakTimeMinutes == null){
                        teamMember.totalBreakTimeMinutes = 30;
                        teamMember.isBreakTimeChanged = true;
                    }
                }
                else{
                    teamMember.totalBreakTimeMinutes = 30;
                    teamMember.isBreakTimeChanged = true;
                }
            }
        }

        let defer = $q.defer();
        $scope.teamMembersList = [];
        async.eachSeries(teamMembers, function(member, callback){
            if(member.staffId == AccountSvc.getUserIdV2()){
                let listItem = {id: member.id ,staffId: member.staffId, fullName: AccountSvc.getUserFullName(), staffType: member.staffType, totalBreakTimeMinutes: member.totalBreakTimeMinutes};
                setDefaultTotalBreakTimeMinutes(listItem);
                console.log('[73]:');
                console.log(listItem);
                getCurrentTimelogs(listItem, member);
                $scope.teamMembersList.push(listItem);
                callback();
            }
            else{
                StaffSvc.get(member.staffId).then(function(staff){
                    if(staff){
                        if(staff.agencyId){
                            AgenciesSvc.getAgency(staff.agencyId).then(function(agency){
                                console.log(agency);
                                let listItem = {id: member.id ,staffId: staff.id, fullName: staff.fullName, staffType: staff.staffType, totalBreakTimeMinutes: member.totalBreakTimeMinutes};
                                setDefaultTotalBreakTimeMinutes(listItem);
                                console.log('[87]:');
                                console.log(listItem);
                                if(!listItem.totalBreakTimeMinutes){

                                }
                                if(agency)
                                    listItem.agencyAcroNym = agency.agencyAcroNym;
                                getCurrentTimelogs(listItem, member);
                                $scope.teamMembersList.push(listItem);
                            });
                        }
                        else{
                            let listItem = {id: member.id ,staffId: staff.id, fullName: staff.fullName, staffType: staff.staffType, totalBreakTimeMinutes: member.totalBreakTimeMinutes};
                            setDefaultTotalBreakTimeMinutes(listItem);
                            console.log('[101]:');
                            console.log(listItem);
                            getCurrentTimelogs(listItem, member);
                            $scope.teamMembersList.push(listItem);
                        }
                    }
                    callback();
                }, function(error){
                    callback();
                });
            }
        }).then(function(error){
            //console.log('teamMembersList');
            //console.log($scope.teamMembersList);
            defer.resolve(true);
        });

        return defer.promise;
    }

    function addTeamMembersORIG(teamMembers){
        let defer = $q.defer();
        let teamMembersQry = $linq.Enumerable().From($scope.teamMembers);
        let newTeamMembers = [];
        teamMembers.forEach(staffId => {
            let exist = teamMembersQry.FirstOrDefault(null, 'f => f == "' + staffId + '"');
            if(!exist){
                let teamMember = angular.copy(TeamMemberVM);
                teamMember.id = (new ObjectId()).toString();
                teamMember._id = objectTypes.teammember + teamMember.id;
                teamMember.staffId = staffId;
                teamMember.jobId = $stateParams.id;
                teamMember.tenantId = AccountSvc.getTenantIdV2();
                newTeamMembers.push(teamMember);
            }
        });

        JobsSvc.addTeamMembers($stateParams.id, newTeamMembers).then(function(response){
            defer.resolve(true);
        });

        return defer.promise;
    }

    function addTeamMembers(teamMembers){
        let defer = $q.defer();
        let teamMembersQry = $linq.Enumerable().From($scope.teamMembers);
        let newTeamMembers = [];
        teamMembers.forEach(staff => {
            let exist = teamMembersQry.FirstOrDefault(null, 'f => f == "' + staff.staffId + '"');
            if(!exist){
                let teamMember = angular.copy(TeamMemberVM);
                teamMember.id = (new ObjectId()).toString();
                teamMember._id = objectTypes.teammember + teamMember.id;
                teamMember.staffId = staff.staffId;
                teamMember.jobId = $stateParams.id;
                teamMember.tenantId = AccountSvc.getTenantIdV2();
                if(staff.agencyId)
                    teamMember.agencyId = staff.agencyId;
                newTeamMembers.push(teamMember);
            }
        });

        JobsSvc.addTeamMembers($stateParams.id, newTeamMembers).then(function(response){
            defer.resolve(true);
        });

        return defer.promise;
    }

    function addDefaultStms(teamMembers){
        let defer = $q.defer();

        let stmsExist = $linq.Enumerable().From(teamMembers).FirstOrDefault(null, 'f => f.staffId == "' + AccountSvc.getUserIdV2() + '"');
        if(teamMembers.length == 0 || !stmsExist){
            addTeamMembers([{ staffId: AccountSvc.getUserIdV2() }]).then(function(response){
                defer.resolve(true);
            });
        }
        else{
            defer.resolve(true);
        }
        return defer.promise;
    }

    function addDefaultStmsORIG(teamMembers){
        let defer = $q.defer();

        let stmsExist = $linq.Enumerable().From(teamMembers).FirstOrDefault(null, 'f => f.staffId == "' + AccountSvc.getUserIdV2() + '"');
        if(teamMembers.length == 0 || !stmsExist){
            addTeamMembers([{ staffId: AccountSvc.getUserIdV2() }]).then(function(response){
                defer.resolve(true);
            });
        }
        else{
            defer.resolve(true);
        }
        return defer.promise;
    }

    $scope.$on('$ionicView.enter', function () {
        //load team members
        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>' });
        $scope.isJobArchived = $stateParams.isJobArchived;
        if($scope.isJobArchived){
            getAllTeamMembers().then(function(){
                $ionicLoading.hide();
            });
        }
        else{
            JobsSvc.getTeam($stateParams.id).then(function (teamMembers) {
                JobsSvc.addDefaultStms($stateParams.id).then(function(response){
                    getTeamMembers().then(function(){
                        $ionicLoading.hide();
                    });
                });
                /*
                addDefaultStms(teamMembers).then(function(response){
                    getTeamMembers().then(function(){
                        $ionicLoading.hide();
                    });
                });
                */
            }, function (error) {
                console.log(error);
                $ionicLoading.hide();
            });
        }

        $scope.getAddress();
    });

    $scope.$on('timelogsChanged', function(){
        console.log('timelogsChanged triggered');
        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>' });
        getTeamMembers().then(function(){
            $ionicLoading.hide();
        }, function(error){
            console.log(error);
            $ionicLoading.hide();
        });
    });


/*
public class TimeLogUpdate
{
    public string JobId { get; set; }
    public string TeamMemberId { get; set; }
    public int TotalBreakTimeMinutes { get; set; }
    public bool IsBreakTimeUpdate { get; set; }
}
*/

    $scope.back = function(){
        let btChanges = $linq.Enumerable().From($scope.teamMembersList).Where('w => w.isBreakTimeChanged').Select('s => { id: s.id, totalBreakTimeMinutes: s.totalBreakTimeMinutes}').ToArray();
        JobsSvc.updateBreakTimes($stateParams.id, btChanges).then(function(){
            $state.go('app.jobdetail', {id: $stateParams.id, showAddActivity: false, isJobArchived: $scope.isJobArchived});
        });
    }

    $scope.remove = function(memberId){
        $ionicPopup.confirm({template: 'Are you sure you want to remove this team member?'}).then(function(proceed){
            if(proceed){
                $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>' });
                JobsSvc.removeTeamMember($stateParams.id, memberId).then(function(response){
                    getTeamMembers().then(function(response){
                        $ionicLoading.hide();
                    });
                });

            }
        });
    };

    $scope.addMember = function () {
        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>' });
        JobsSvc.getTeam($stateParams.id).then(function (teamMembers) {
            $ionicLoading.hide();
            ModalFactory.showChooseTeamMembers(teamMembers).then(function(selectedMembers){
                console.log(selectedMembers);
                if(selectedMembers.length > 0){
                    let tmsToAdd = [];
                    selectedMembers.forEach(member => {
                        tmsToAdd.push({staffId: member});
                    });

                    $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>' });
                    addTeamMembers(tmsToAdd).then(function(response){
                        getTeamMembers().then(function(response){
                            $ionicLoading.hide();
                        });
                    });
                    /*
                    let tmsToAdd = [];
                    JobsSvc.unhideTeamMembers($stateParams.id, selectedMembers).then(function(reactivatedTeamMembers){
                        //let newTms = selectedMembers.remove(reactivatedTeamMembers);
                        //proceed with adding new team members
                        console.log(reactivatedTeamMembers);
                        let newTms = $linq.Enumerable().From(selectedMembers).Except(reactivatedTeamMembers).ToArray();
                        console.log('[286]');
                        console.log(newTms);
                        newTms.forEach(member => {
                            tmsToAdd.push({staffId: member});
                        });
                        addTeamMembers(tmsToAdd).then(function(response){
                            getTeamMembers().then(function(){
                            }).finally(function(){
                                $ionicLoading.hide();
                            });
                        }).finally(function(){
                            $ionicLoading.hide();
                        });
                    });
                    */
                }
            });
        });
    };

    $scope.showTimeLogs = function(teammemberid){
        //$ionicPopup.alert({template: 'Logs!'});
        //return;
        $state.go('app.teammemberlog', {id: $stateParams.id, teamMemberId: teammemberid, isJobArchived: $scope.isJobArchived});
    };

    $scope.startAll = function(){

    };

    $scope.toggleAll = function(toggleLogType){
        function newTimeLog(teamMember){
            let breakTimeUpdate = angular.copy(TimeLogVM);
            breakTimeUpdate.jobId = teamMember.jobId;
            breakTimeUpdate.rJobId = teamMember.rJobId;
            breakTimeUpdate.teamMemberId = teamMember.id;
            breakTimeUpdate.rTeamMemberId = teamMember.rId;
            return breakTimeUpdate;
        }
        let timeToLog = new Date();
        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>' }).then(function(){
            JobsSvc.getTeam($stateParams.id).then(function (teamMembers) {
                async.eachSeries(teamMembers, function(teamMember, callback){
                    JobsSvc.getTeamMemberLogs($stateParams.id, teamMember.id).then(function(teamMemberLogs){
                        //if toggle start
                        /// only if
                        //// timelogs empty or lastlog is end
                        //if toggle end
                        /// only if
                        //// lastlog is start or lastlog is breaktimeend
                        let breakTimeUpdate = null;// newTimeLog(teamMember);
                        if(toggleLogType == TimeLogTypeEnum.STARTTIME){
                            if(teamMemberLogs.timeLogs.length == 0){
                                breakTimeUpdate = newTimeLog(teamMemberLogs);
                                breakTimeUpdate.timeLogType = TimeLogTypeEnum.STARTTIME;
                                breakTimeUpdate.time = timeToLog;//new Date();
                            }
                            else{
                                let lastLog = teamMemberLogs.timeLogs[teamMemberLogs.timeLogs.length - 1];
                                if(lastLog.timeLogType == TimeLogTypeEnum.ENDTIME){
                                    breakTimeUpdate = newTimeLog(teamMember);
                                    breakTimeUpdate.timeLogType = TimeLogTypeEnum.STARTTIME;
                                    breakTimeUpdate.time = timeToLog;//new Date();
                                }
                            }
                        }
                        else if(toggleLogType == TimeLogTypeEnum.ENDTIME){
                            let lastLog = teamMemberLogs.timeLogs[teamMemberLogs.timeLogs.length - 1];
                            if(lastLog.timeLogType == TimeLogTypeEnum.STARTTIME || lastLog.timeLogType == TimeLogTypeEnum.BREAKTIMEEND){
                                breakTimeUpdate = newTimeLog(teamMember);
                                breakTimeUpdate.timeLogType = TimeLogTypeEnum.ENDTIME;
                                breakTimeUpdate.time = timeToLog;//new Date();
                            }
                        }

                        if(breakTimeUpdate){
                            JobsSvc.addTimeLog(breakTimeUpdate).then(function(response){
                                callback();
                            });
                        }
                        else callback();
                    });
                }).then(function(response){
                    //start shift of trucks also
                    if(toggleLogType == TimeLogTypeEnum.STARTTIME){
                        JobsSvc.startTrucksShift($stateParams.id, timeToLog).then(function(){
                            getTeamMembers().then(function(){
                                $ionicLoading.hide();
                            });
                        });
                    }
                    else{
                        getTeamMembers().then(function(){
                            $ionicLoading.hide();
                        });
                    }
                }, function(error){
                    $ionicLoading.hide();
                });
            });
        });
    };

    $scope.newMember = function(){
        ModalFactory.newTeamMember().then(function(response){
            console.log(response);
            if(response){
                addTeamMembers([response]).then(function(){
                    getTeamMembers().then(function(){
                        $ionicLoading.hide();
                    });
                });
            }
        });
    };

    function newTimeLog(jobId, teamMember, logType, duration, isPaid){
        let defer = $q.defer();
        //$ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>' });
        JobsSvc.getTeamMemberLogs(jobId, teamMember.id).then(function(teamMemberLogs){
            console.log(teamMemberLogs);
            let breakTimeUpdate = angular.copy(TimeLogVM);
            breakTimeUpdate.jobId = teamMemberLogs.jobId;
            breakTimeUpdate.rJobId = teamMemberLogs.rJobId;
            breakTimeUpdate.teamMemberId = teamMemberLogs.id;
            breakTimeUpdate.rTeamMemberId = teamMemberLogs.rId;
            breakTimeUpdate.time = new Date();
            breakTimeUpdate.timeLogType = logType;
            breakTimeUpdate.location = $scope.address;
            breakTimeUpdate.IsCreatedFromApp = true;
            if(logType === TimeLogTypeEnum.BREAKTIMESTART){
                breakTimeUpdate.duration = duration;
                if(isPaid)
                    breakTimeUpdate.isPaid = true;
            }
            teamMemberLogs.timeLogs.push(breakTimeUpdate);
            JobsSvc.addTimeLog(breakTimeUpdate).then(function(response){
                getTeamMembers().then(function(){
                    //$ionicLoading.hide();
                    defer.resolve(breakTimeUpdate.time);
                });
            });
        });

        return defer.promise;
    }

    $scope.startTime = function(teamMember){
        if(!teamMember.isProcessingShift){
            teamMember.isProcessingShift = true;
            $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>' }).then(function(){
                newTimeLog($stateParams.id, teamMember, TimeLogTypeEnum.STARTTIME).then(function(result){
                    teamMember.isProcessingShift = false;
                    JobsSvc.startTrucksShift($stateParams.id, result).then(function(){
                        $ionicLoading.hide();
                    });
                    //$ionicLoading.hide();
                });
            });
        }
    };
    $scope.stopTime = function(teamMember){
        if(!teamMember.isProcessingShift){
            //let confirm = ons.notification.confirm({title: 'Site Pro', message: 'You are about to end the shift for this person, are you sure?', cancelable: true, buttonLabels:['Yes', 'No'] });
            let confirm = $ionicPopup.confirm({ title: 'Site Pro', template: 'You are about to end the shift for this person, are you sure?', cancelText: 'No', okText: 'Yes' });
            confirm.then(function(response){
                if(response){
                    teamMember.isProcessingShift = true;
                    $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>' }).then(function(){
                        newTimeLog($stateParams.id, teamMember, TimeLogTypeEnum.ENDTIME).then(function(result){
                            teamMember.isProcessingShift = false;
                            getTeamMembers().then(function(){
                            }).finally(function(){
                                $ionicLoading.hide();
                            });
                            //hide team member on the Add TS or Add Agency list
                            /*
                            JobsSvc.hideTeamMember($stateParams.id, teamMember.id).then(function(){
                                getTeamMembers().then(function(){
                                }).finally(function(){
                                    $ionicLoading.hide();
                                });
                            });
                            */
                        });
                    });
                }
            });
        }
    };
    $scope.pauseTime = function(teamMember){
        BreakTimeOptions.getOptions(teamMember.fullName, $scope).then(function(option){
            console.log(option);
            if(option){
                newTimeLog($stateParams.id, teamMember, TimeLogTypeEnum.BREAKTIMESTART, option.duration, option.isPaid).then(function(breakTimeStart){
                    if(option.selected !== 'ongoing'){
                        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>' });
                        JobsSvc.stopBreakTimeAt($stateParams.id, teamMember.id, option.duration).then(function(){
                            $ionicLoading.hide();
                        });
                    }
                });
            }
        });
    };
    $scope.resumeTime = function(teamMember){
        newTimeLog($stateParams.id, teamMember, TimeLogTypeEnum.BREAKTIMEEND);
    };

    $scope.selectAgencyStaff = function(){
        console.log($scope.teamMembersList);
        //return;
        let currentTms = $linq.Enumerable().From($scope.teamMembersList).Select('s => s.staffId').ToArray();
        ModalFactory.selectAgencyStaff(currentTms).then(function(selectedAgencyStaff){
            console.log('selectAgencyStaff');
            console.log(selectedAgencyStaff);
            if(selectedAgencyStaff.length > 0){
                let tmsToAdd = [];
                let selectedAgencyStaffIds = $linq.Enumerable().From(selectedAgencyStaff).Select('s => s.staffId').ToArray();
                $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>' });
                JobsSvc.unhideTeamMembers($stateParams.id, selectedAgencyStaffIds).then(function(reactivatedTeamMembers){
                    console.log(reactivatedTeamMembers);
                    let newTms = $linq.Enumerable().From(selectedAgencyStaff).Where(f => {
                        return reactivatedTeamMembers.indexOf(f.staffId) < 0;
                    }).ToArray();

                    console.log('[492]');
                    console.log(newTms);
                    /*
                    newTms.forEach(member => {
                        tmsToAdd.push({staffId: member});
                    });
                    */
                    addTeamMembers(newTms).then(function(response){
                        getTeamMembers().then(function(){
                        }).finally(function(){
                            $ionicLoading.hide();
                        });
                    }).finally(function(){
                        $ionicLoading.hide();
                    });
                });
                return;
                $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>' });
                addTeamMembers(selectedAgencyStaff).then(function(response){
                    getTeamMembers().then(function(response){
                        $ionicLoading.hide();
                    });
                });
            }

        });
    };

    $scope.breakTimeChanged = function(teamMember){
        teamMember.isBreakTimeChanged = true;
        console.log(teamMember);
    };

    $scope.selectValue = function(index){
        $('#breakTime_' + index).select();
    };

     $scope.getAddress = function () {
         $ionicLoading.show({ template: "Determining location..." });
         if (navigator.geolocation) {
             navigator.geolocation.getCurrentPosition(function (position) {
                 var location = { lat: position.coords.latitude, lng: position.coords.longitude };
                 GetAddress(location, function (results) {
                     if (results) {
                         console.log(results)

                         $scope.address = results.length > 0 ? results[0].formatted_address : '';
                     }
                     else
                         $ionicPopup.alert({ title: 'Site Pro', template: 'Unable to determine location' });
                     $ionicLoading.hide();
                 });
             }, function (error) {
                 $ionicLoading.hide();
                 console.log(error);
             }, { timeout: 10000, enableHighAccuracy: false });
         }
     };
}]);
