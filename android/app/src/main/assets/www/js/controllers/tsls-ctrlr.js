﻿mainApp.controller('TSLSCtrlr', ['$scope', '$ionicModal', function ($scope, $ionicModal) {
    $ionicModal.fromTemplateUrl('/views/addtsl.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        $scope.modal = modal;
    });

    $scope.AddTSL = function () {
        $scope.modal.show();
    }
}]);

mainApp.controller('AddTSLCtrlr', ['$scope', '$ionicPopup', 'JobsSvc', '$q', '$ionicLoading', '$cordovaDialogs', 'parameters', function ($scope, $ionicPopup, JobsSvc, $q, $ionicLoading, $cordovaDialogs, parameters) {
    //console.log('AddTSLCtrlr');
    $scope.$on('modal.shown', function () {
        $scope.TSL.Limit = JobsSvc.LastTSLSpeedLimitSelected;
        $scope.TSLTo.Limit = JobsSvc.LastTSLSpeedLimitToSelected;

        $scope.TSL.PermanentLimit = JobsSvc.LastTSLPermanentSpeedLimitSelected;
        $scope.TSLTo.PermanentLimit = JobsSvc.LastTSLPermanentSpeedLimitToSelected;
        if(!parameters.tsl){
            $scope.GetCoordinates();
        }
        else{
            $scope.TSL = parameters.tsl[0];
            $scope.TSLTo = parameters.tsl[1];
        }
        $scope.TSL.Timestamp = new Date();
        $scope.TSLTo.Timestamp = new Date();
    });
    $scope.tslsChanged = false;
    function getCoordinates() {
        var defer = $q.defer();
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                defer.resolve(position);
            }, function (error) {
                console.log(error);
                defer.reject(error);
            }, { timeout: 10000, enableHighAccuracy: true });
        }
        else {
            defer.reject(null);
        }
        return defer.promise;
    }
    $scope.isFirstTsl = parameters.isFirstTsl;
    $scope.TSL = { Limit: null, PermanentLimit: null };
    $scope.TSLTo = { Limit: null, PermanentLimit: null };
    $scope.PermanentLimits = [{ Value: 10, Text: '10' }, { Value: 20, Text: '20' }, { Value: 30, Text: '30' }, { Value: 40, Text: '40' }, { Value: 50, Text: '50' }, { Value: 60, Text: '60' }, { Value: 70, Text: '70' }, { Value: 80, Text: '80' }, { Value: 90, Text: '90' }, { Value: 100, Text: '100' }, { Value: 110, Text: '110' }, { Value: 200, Text: 'Open Rd' }];
    $scope.Limits = [{ Value: 10, Text: '10' }, { Value: 20, Text: '20' }, { Value: 30, Text: '30' }, { Value: 40, Text: '40' }, { Value: 50, Text: '50' }, { Value: 60, Text: '60' }, { Value: 70, Text: '70' }, { Value: 80, Text: '80' }, { Value: 90, Text: '90' }, { Value: 100, Text: '100' }];
    $scope.LastTSLSpeedLimitSelected = null;
    $scope.LastTSLSpeedLimitToSelected = null;
    $scope.Done = function () {
        if ($scope.TSL.Limit == null) {
            $ionicPopup.alert({ title: 'Site Pro', template: 'Please choose a From Temporary Limit.' });
            return;
        }
        if ($scope.TSL.PermanentLimit == null) {
            $ionicPopup.alert({ title: 'Site Pro', template: 'Please choose a From Permanent Limit.' });
            return;
        }

        if ($scope.TSLTo.Limit == null) {
            $ionicPopup.alert({ title: 'Site Pro', template: 'Please choose a To Temporary Limit.' });
            return;
        }
        if ($scope.TSLTo.PermanentLimit == null) {
            $ionicPopup.alert({ title: 'Site Pro', template: 'Please choose a To Permanent Limit.' });
            return;
        }

        JobsSvc.LastTSLSpeedLimitSelected = $scope.TSL.Limit;
        JobsSvc.LastTSLSpeedLimitToSelected = $scope.TSLTo.Limit;

        JobsSvc.LastTSLPermanentSpeedLimitSelected = $scope.TSL.PermanentLimit;
        JobsSvc.LastTSLPermanentSpeedLimitToSelected = $scope.TSLTo.PermanentLimit;

        $scope.Tsls = [];
        $scope.Tsls.push($scope.TSL);
        $scope.Tsls.push($scope.TSLTo);
        $scope.closeModal($scope.Tsls);
    };
    $scope.Cancel = function () {
        $scope.closeModal(null);
    };

    $scope.GetCoordinates = function () {
        $ionicLoading.show({ template: '<ion-spinner></ion-spinner>' });
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                $scope.TSL.Coordinates = { latitude: position.coords.latitude, longitude: position.coords.longitude };
                $scope.TSLTo.Coordinates = { latitude: position.coords.latitude, longitude: position.coords.longitude };
                if(!parameters.tsl)
                    $scope.TSL.Timestamp = new Date();
                $scope.TSL.remainsInPlace = new Date($scope.TSL.Timestamp.getFullYear(), $scope.TSL.Timestamp.getMonth(), $scope.TSL.Timestamp.getDate(), 0, 0, 0, 0);
                $scope.TSLTo.remainsInPlace = new Date($scope.TSLTo.Timestamp.getFullYear(), $scope.TSLTo.Timestamp.getMonth(), $scope.TSLTo.Timestamp.getDate(), 0, 0, 0, 0);

                $ionicLoading.hide();
            }, function (error) {
                console.log(error);
                $ionicLoading.hide();
                $cordovaDialogs.alert('Unable to determine location. Tap Refresh to try again.', 'Site Pro');
            }, { timeout: 10000, enableHighAccuracy: true });
        }
    };

    $scope.getAddress = function(){
        $ionicLoading.show({ template: "Determining location..." });
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var location = { lat: position.coords.latitude, lng: position.coords.longitude };
                $scope.TSL.Coordinates = { latitude: position.coords.latitude, longitude: position.coords.longitude };
                GetAddress(location, function (results) {
                    if (results) {
                        $scope.TSL.address = results.length > 0 ? results[0].formatted_address : '';
                        $scope.TSL.Timestamp = new Date();
                    }
                    else
                        $ionicPopup.alert({title: 'Site Pro', template: 'Unable to determine location'});
                    $ionicLoading.hide();
                });
            }, function (error) {
                $ionicLoading.hide();
                $ionicPopup.alert({title: 'Site Pro', template: message});
                console.log(error);
            }, { timeout: 10000, enableHighAccuracy: true });
        }
    };

    $scope.getToAddress = function () {
        $ionicLoading.show({ template: "Determining location..." });
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var location = { lat: position.coords.latitude, lng: position.coords.longitude };
                $scope.TSLTo.Coordinates = { latitude: position.coords.latitude, longitude: position.coords.longitude };
                GetAddress(location, function (results) {
                    if (results) {
                        $scope.TSLTo.address = results.length > 0 ? results[0].formatted_address : '';
                        $scope.TSLTo.Timestamp = new Date();
                    }
                    else
                        $ionicPopup.alert({ title: 'Site Pro', template: 'Unable to determine location' });
                    $ionicLoading.hide();
                });
            }, function (error) {
                $ionicLoading.hide();
                $ionicPopup.alert({ title: 'Site Pro', template: message });
                console.log(error);
            }, { timeout: 10000, enableHighAccuracy: true });
        }
    };

    $scope.getDate = function(){
        datePicker.show({date: $scope.TSL.remainsInPlace, mode: 'date'}, function(result){
            console.log(result);
            $scope.TSL.remainsInPlace = result;
        }, function(error){
            console.log(error);
        });
    };

    $scope.getDateTo = function () {
        datePicker.show({ date: $scope.TSLTo.remainsInPlace, mode: 'date' }, function (result) {
            console.log(result);
            $scope.TSLTo.remainsInPlace = result;
        }, function (error) {
            console.log(error);
        });
    };
}]);