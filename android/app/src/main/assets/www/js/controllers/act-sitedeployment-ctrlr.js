﻿mainApp.controller('ActSiteDeploymentCtrlr', ['$scope', '$state', '$window', 'TSLSvc', 'JobsSvc', 'ClosureTypeSvc', 'LocationSvc', '$ionicLoading', 'ModalFactory', 'WebApiQSvc', 'SiteCheckAlarmSvc', 'VoiceToTextSvc', '$ionicPopup', function ($scope, $state, $window, TSLSvc, JobsSvc, ClosureTypeSvc, LocationSvc, $ionicLoading, ModalFactory, WebApiQSvc, SiteCheckAlarmSvc, VoiceToTextSvc, $ionicPopup) {
    $scope.$on('$ionicView.enter', function () {
        $scope.fields = JobsSvc.job.CurrentActivity;
        $scope.tsls = $scope.fields.tsls || [];
        $scope.addedClosureTypes = $scope.fields.addedClosureTypes || [];
        $scope.jobName = JobsSvc.job.jobName;
        if (!$scope.fields.complete && $scope.fields.id == null) {
            ModalFactory.showClosureTypes($scope.addedClosureTypes).then(function (result) {
                if(result){
                    $scope.addedClosureTypes = result;
                    $scope.Start();
                }
                else
                    $state.go('jobdetails');
            });
        }
        else if ($scope.fields.complete) {
            if(typeof $scope.fields.endTime === "string")
                $scope.fields.endTime = new Date($scope.fields.endTime);
            if(typeof $scope.fields.startTime === "string")
                $scope.fields.startTime = new Date($scope.fields.startTime);
            var diff = $scope.fields.endTime.valueOf() - $scope.fields.startTime.valueOf();
            $scope.TotalDuration = LeadZero(moment.duration(diff).hours()) + ":" + LeadZero(moment.duration(diff).minutes()) + ":" + LeadZero(moment.duration(diff).seconds());
        }
        else
            $scope.$broadcast('timer-start');        
    });

    $scope.tsls = [];
    $scope.addedClosureTypes = [];
    $scope.Start = function () {
        $ionicLoading.show({ template: '<ion-spinner></ion-spinner>' });
        var posOptions = { timeout: 5000, enableHighAccuracy: true };
        var coordinates = null;
        
        LocationSvc.getCurrentPosition(posOptions).then(function (position) {
            console.log(position);
            coordinates = position;
        }, function (error) {
            console.log(error);
            coordinates = error;
        }).finally(function () {
            $ionicLoading.hide();
            $scope.fields.startTime = new Date();
            $scope.$broadcast('timer-start');
            var activity = { id: (new $window.ObjectId()).toString(), jobId: JobsSvc.job.id, activityId: 2, startTime: $scope.fields.startTime, endTime: $scope.fields.endTime, tsls: $scope.tsls, addedClosureTypes: $scope.addedClosureTypes, complete: false, startCoordinates: {}, endCoordinates: {}, mode: 0, notes: $scope.fields.note, closureTypesOption: $scope.addedClosureTypes };
            
            if(coordinates.coords){
                activity.startCoordinates = { latitude: coordinates.coords.latitude, longitude: coordinates.coords.longitude };
            }
            $scope.fields = JobsSvc.job.CurrentActivity = activity;
            JobsSvc.AddSendActivity(JobsSvc.job.CurrentActivity);
        });
    };
    $scope.Stop = function () {
        $ionicLoading.show({ template: '<ion-spinner></ion-spinner>' });
        var posOptions = { timeout: 5000, enableHighAccuracy: true };
        var coordinates = null;
        
        LocationSvc.getCurrentPosition(posOptions).then(function (position) {
            coordinates = position;
        }, function (error) {
            coordinates = error;
        }).finally(function () {
            $ionicLoading.hide();
            JobsSvc.job.CurrentActivity.endTime = new Date();
            JobsSvc.job.CurrentActivity.complete = true;

            if (coordinates.coords)
                JobsSvc.job.CurrentActivity.endCoordinates = { latitude: coordinates.coords.latitude, longitude: coordinates.coords.longitude };
            JobsSvc.job.CurrentActivity.mode = 1;
            $scope.fields = JobsSvc.job.CurrentActivity;
            JobsSvc.UpdateActivity(JobsSvc.job.CurrentActivity);
            JobsSvc.showAddActivity = true;
            //set site check alarm
            SiteCheckAlarmSvc.start();
            $scope.Done();
        });
    };
    $scope.AddTSL = function () {
        ModalFactory.showTSL().then(function (result) {
            console.log('[82]');
            console.log(result);
            if(result)
                JobsSvc.job.CurrentActivity.tsls.push(result);
        });
    };
    $scope.CancelTSL = function () {
        TSLSvc.Cancel();
    };
    $scope.back = function () {
        $state.go('jobdetails');
    };
    $scope.Done = function () {        
        $state.go('jobdetails');
    };
    $scope.ChooseClosureTypes = function () {        
        ClosureTypeSvc.AddClosureTypes($scope);
    };
    $scope.CloseClosureTypes = function () {
        ClosureTypeSvc.Close();
        $scope.Start();
    };

    $scope.EditClosureTypes = function () {
        ModalFactory.showClosureTypes($scope.addedClosureTypes).then(function (result) {
            if(result){
                $scope.addedClosureTypes = result;
                $scope.fields.addedClosureTypes = result;
                $scope.fields.mode = 1;
                JobsSvc.UpdateActivity(JobsSvc.job.CurrentActivity);
            }
        });
    };

    $scope.Cancel = function () {
        var confirmPopup = $ionicPopup.confirm({
            title: 'Site Pro',
            template: 'Are you sure you want to cancel this activity?',
            okText: 'Yes',
            cancelText: 'No'
        });

        confirmPopup.then(function (res) {
            if (res) {
                JobsSvc.job.CurrentActivity.tsls = [];
                JobsSvc.job.CurrentActivity.cancelled = true;
                WebApiQSvc.Send({ cancelActivity: true, jobId: JobsSvc.job.id, activityId: JobsSvc.job.CurrentActivity.id });
                $state.go('jobdetails');
            }
        });        
    };
    $scope.voiceToText = function () {
        VoiceToTextSvc.Capture().then(function (result) {
            console.log(result);
            if ($scope.fields.notes == null)
                $scope.fields.notes = '';
            if ($scope.fields.notes.length > 0)
                $scope.fields.notes += ' ' + result[0];
            else
                $scope.fields.notes += result[0];
        }, function (error) {
            console.log(error);
        });        
    };
}]);