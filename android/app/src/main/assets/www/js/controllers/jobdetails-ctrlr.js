﻿mainApp.controller('JobDetailsCtrlr', ['$scope', '$state', 'JobsSvc', '$ionicPlatform', '$ionicModal', '$ionicLoading', '$linq', 'SiteCheckAlarmSvc', 'ModalFactory', 'TrucksSvc', 'TruckAuditSvc', '$ionicPopup', '$window', '$cordovaFileTransfer', '$q', 'SPMTruckAuditSvc', '$ionicPopover', 'UtilitySvc', function ($scope, $state, JobsSvc, $ionicPlatform, $ionicModal, $ionicLoading, $linq, SiteCheckAlarmSvc, ModalFactory, TrucksSvc, TruckAuditSvc, $ionicPopup, $window, $cordovaFileTransfer, $q, SPMTruckAuditSvc, $ionicPopover, UtilitySvc) {
    $ionicPopover.fromTemplateUrl('views/jobmenuoptions.html', {
        scope: $scope
    }).then(function (popover) {
        $scope.jobMenuOptions = popover;
    });

    function getTruckNames() {
        var truckNames = [];
        if ($scope.job.trucks != null && $scope.job.trucks.length > 0) {
            for (var i = 0; i < $scope.job.trucks.length; i++) {
            //for (var i = 0; i < truckIds.length; i++) {
                var truck = TrucksSvc.getById($scope.job.trucks[i].truckId);
                if (truck)
                    truckNames.push(truck.truckNumber + ' - ' + truck.truckRego);
            }
        }

        return truckNames;
    }

    function sendTruckAudit(truckAudit) {
        truckAudit.isTruckAudit = true;
        truckAudit.activityId = 10;
        truckAudit.startTime = new Date();
        var photos = $linq.Enumerable().From(truckAudit.inspections).SelectMany('w => w.photos').ToArray();
        var audioNotes = $linq.Enumerable().From(truckAudit.inspections).SelectMany('w => w.audioNotes').ToArray();

        if (truckAudit.oilLevelDetails.photos && Array.isArray(truckAudit.oilLevelDetails.photos)) {
            photos = photos.concat(truckAudit.oilLevelDetails.photos);
        }
        if (truckAudit.oilLevelDetails.audioNotes && Array.isArray(truckAudit.oilLevelDetails.audioNotes)) {
            audioNotes = audioNotes.concat(truckAudit.oilLevelDetails.audioNotes);
        }
        if (truckAudit.waterLevelDetails.photos && Array.isArray(truckAudit.waterLevelDetails.photos)) {
            photos = photos.concat(truckAudit.waterLevelDetails.photos);
        }
        if (truckAudit.waterLevelDetails.audioNotes && Array.isArray(truckAudit.waterLevelDetails.audioNotes)) {
            audioNotes = audioNotes.concat(truckAudit.waterLevelDetails.audioNotes);
        }
        if (truckAudit.dieselLevelDetails.photos && Array.isArray(truckAudit.dieselLevelDetails.photos)) {
            photos = photos.concat(truckAudit.dieselLevelDetails.photos);
        }
        if (truckAudit.dieselLevelDetails.audioNotes && Array.isArray(truckAudit.dieselLevelDetails.audioNotes)) {
            audioNotes = audioNotes.concat(truckAudit.dieselLevelDetails.audioNotes);
        }
        var fileUploadList = photos.concat(audioNotes);
        var fileUpload = [];        
        for (var i = 0; i < fileUploadList.length; i++) {
            var path = fileUploadList[i];
            fileUpload.push(JobsSvc.addToQ({ id: (new $window.ObjectId()).toString(), isTruckAuditFileUpload: true, path: path }));
            //fileUpload.push(upload);
        }        
        $q.all(fileUpload).then(function (results) {
            JobsSvc.AddSendActivity(truckAudit);
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });
        
        //JobsSvc.AddSendActivity(truckAudit);
    }
    
    $ionicModal.fromTemplateUrl('views/addactivityselection.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        $scope.modal = modal;
    });
    $scope.$on('$destroy', function () {
        $scope.modal.remove();
    });
    $scope.$on('$ionicView.beforeEnter', function () {
        //disabled the items(1/8/9) in the Activities selection modal
        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>' });
        //$scope.Activities = angular.copy(JobActivities);
        $scope.Activity = null;
        $scope.job = JobsSvc.job;
        $scope.activities = $linq.Enumerable().From($scope.job.activities).Where('w => w.cancelled != true').ToArray();        
        $scope.truckNames = getTruckNames();//($scope.job.trucks);
    });
    $scope.$on('$ionicView.loaded', function () {
        //$state.go('acttmponsiteassessment');
        //trackJs.track('Job details page loaded.');
        console.log(JobActivities);
        $scope.Activities = angular.copy(JobActivities);
        activities = $linq.Enumerable().From(JobsSvc.job.activities).Where('w => w.cancelled != true').ToArray();
        //get last activity
        //if it is sitedeployment the start the alarm
        if(JobsSvc.job.activities != null && JobsSvc.job.activities.length > 0){
            var lastAct = JobsSvc.job.activities[0];
            var siteDeployment = $linq.Enumerable().From(activities).FirstOrDefault(null, 'f => f.activityId == 2');
            var siteCheck = $linq.Enumerable().From(activities).LastOrDefault(null, 'f => f.activityId == 6');
            if (lastAct != null && lastAct.activityId == 2) {
                if (lastAct.endTime != null) {
                    var diff = moment().diff(moment(lastAct.endTime), 'minutes');
                    if (diff >= 110) {
                        SiteCheckAlarmSvc.alarmNow();
                        SiteCheckAlarmSvc.start();
                    }
                }
            }
        }
    });
    $scope.$on('$ionicView.enter', function () {
        //trackJs.track('Job details page enter.');
        if(JobsSvc.showAddActivity)
            $scope.AddActivitySelection();

        $ionicLoading.hide();
    });
    $scope.$on('unathorizedaccess', function (event, data) {
        //trackJs.track('unauthorizedaccess triggered');
        $scope.CloseActivitySelection();
    });

    $scope.Activity = null;
    $scope.AddActivity = function (activity) {
    console.log("1");
        //trackJs.track('Add activity option tapped. Activity: ' + activity.Id);
        if(activity.locked)
            return;
        //activity.start();
        $scope.CloseActivitySelection();
        switch (activity.Id) {
            case 1:
                JobsSvc.job.activityId = 1;
                JobsSvc.job.CurrentActivity = new JobActivity(null);
                JobsSvc.job.CurrentActivity.activityId = 1;
                //JobsSvc.job.CurrentActivity = { id: null, activityId: 1, startTime: null, endTime: null, complete: false, truckNumber: null, cancelled: false, notes: null };
                $state.go('acttraveltosite');
                break;
            case 2:
                JobsSvc.job.activityId = 2;
                JobsSvc.job.CurrentActivity = { id: null, activityId: 2, startTime: null, endTime: null, addedClosureTypes: [], tsls: [], complete: false, notes: null };
                $state.go('actsitedeployment');
                break;
            case 3:
                JobsSvc.job.activityId = 3;
                JobsSvc.job.CurrentActivity = { id: null, activityId: 3, description: null, startTime: null, endTime: null, addedClosureTypes: [], tsls: [], complete: false, notes: null };
                $state.go('actsitehazardassessment');
                break;            
            case 4:
                JobsSvc.job.activityId = 4;
                JobsSvc.job.CurrentActivity = { id: null, activityId: 4, startTime: null, endTime: null, complete: false, notes: null };
                $state.go('actwaiting');
                break;
            case 5:
                JobsSvc.job.activityId = 5;
                JobsSvc.job.CurrentActivity = { id: null, activityId: 5, startTime: null, endTime: null, complete: false, notes: null };
                $state.go('actworkspaceactive');
                break;
            case 6:
                JobsSvc.job.activityId = 6;
                JobsSvc.job.CurrentActivity = { id: null, activityId: 6, complete: false, siteCheckItems: [], notes: null };
                angular.copy(siteCheckItems, JobsSvc.job.CurrentActivity.siteCheckItems);
                $state.go('actsitecheck');
                break;
            case 7:
                JobsSvc.job.activityId = 7;
                JobsSvc.job.CurrentActivity = { id: null, activityId: 7, startTime: null, endTime: null, addedClosureTypes: [], tsls: [], complete: false, notes: null };
                $state.go('actsiteamendment');
                break;
            case 8:
                JobsSvc.job.activityId = 8;
                JobsSvc.job.CurrentActivity = { id: null, activityId: 7, startTime: null, endTime: null, addedClosureTypes: [], tsls: [], complete: false, notes: null };
                $state.go('actsiteretrieval');
                break;            
            case 9:
                JobsSvc.job.activityId = 9;
                JobsSvc.job.CurrentActivity = { id: null, activityId: 1, startTime: null, endTime: null, complete: false, notes: null };
                $state.go('acttraveltodepot');
                break;
            case 10:
                JobsSvc.job.activityId = 10;
                TruckAuditSvc.start(JobsSvc.job.jobName).then(function (result) {
                });
                TruckAuditSvc.complete().then(function (truckAudit) {
                    if (truckAudit) {
                        truckAudit.dateOfAudit = new Date();
                        SPMTruckAuditSvc.save(truckAudit, JobsSvc.job).then(function () {
                            $scope.activities = $linq.Enumerable().From(JobsSvc.job.activities).Where('w => w.cancelled != true').ToArray();
                        });
                    }
                });
                break;
            case 11:
                JobsSvc.job.CurrentActivity = { id: null, activityId: activity.Id, startTime: null, endTime: null, complete: false, notes: null };
                $state.go('actprestartprep');
                break;
            case 12:                
                JobsSvc.job.CurrentActivity = { id: null, activityId: activity.Id, startTime: null, endTime: null, complete: false, notes: null };
                $state.go('acttraveltonextjob');
                break;
            case 13:
                $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>' });
                var activity = { id: (new $window.ObjectId()).toString(), jobId: $scope.job.id, activityId: activity.Id, startTime: new Date(), endTime: null, complete: true, notes: null, mode: 0 };
                JobsSvc.endOfShift(activity).then(function () {
                    //add EoS timestamp to each team member
                    for (var i = 0; i < JobsSvc.job.teamMembers.length; i++) {
                        var member = JobsSvc.job.teamMembers[i];
                        member.endOfShift = activity.startTime;
                    }
                    JobsSvc.save().then(function () {
                        $scope.activities = $linq.Enumerable().From($scope.job.activities).Where('w => w.cancelled != true').ToArray();
                        SiteCheckAlarmSvc.stop();
                        $ionicLoading.hide();
                        $scope.modal.hide();
                    }, function (error) {
                        console.log(error);
                        $scope.modal.hide();
                        $ionicLoading.hide();
                        alert('an error occured while saving the data');
                    });
                }, function (error) {
                    console.log(error);
                    $scope.modal.hide();
                    $ionicLoading.hide();
                    alert('Something went wrong.');
                });
                break;
        }
    };
    function teamMembersHasStarted() {        
        if ($scope.job.teamMembers.length == 0)
            return false;

        var openTeamMembers = $linq.Enumerable().From($scope.job.teamMembers).Where('w => w.endOfShift == null || w.endOfShift.length == 0').ToArray();
        if (openTeamMembers.length == 0)
            return false;
        var hasStartTime = false;
        for (var i = 0; i < openTeamMembers.length; i++) {
            var timelogs = openTeamMembers[i].timeLogs;
            var lastLog = $linq.Enumerable().From(timelogs).LastOrDefault();//(null, 'a => a.timeLogType == 0');
            //if (lastLog != null && lastLog.timeLogType == 0) {
            if (lastLog != null && lastLog.timeLogType <= 2) {
                hasStartTime = true;
                break;
            }
        }

        return hasStartTime;
    }
    function hideEndOfShiftOption() {
        $scope.Activities[$scope.Activities.length - 1].hide = false;
        if ($scope.activities == null || $scope.activities.length == 0)
            $scope.Activities[$scope.Activities.length -1].hide = true;
        else {
            if ($scope.activities.length > 0) {
                if ($scope.activities[0].activityId == 13)
                    $scope.Activities[$scope.Activities.length - 1].hide = true;
            }
        }

    }
    $scope.AddActivitySelection = function () {
        if ($scope.job.trucks == null || $scope.job.trucks.length == 0) {
            $ionicPopup.alert({ title: 'Site Pro', template: 'Please choose trucks before starting an activity.' });
            return;
        }
        //prompt user if team time hasn't started
        if (!teamMembersHasStarted()) {
            //$ionicPopup.alert({ title: 'Site Pro', template: "You haven't started your team's time yet." });
            var teamlogOption = $ionicPopup.show({
                title: 'Site Pro', template: "You haven't started your team's time yet.",
                buttons: [
                    { text: 'Go Back', type: 'button-positive', onTap: function (e) { return true; } },
                    { text: 'Continue', type: 'button-stable', onTap: function (e) { return false; }}
                ]
            });
            teamlogOption.then(function (goBack) {                
                if (goBack) {
                    $scope.Team();
                    $scope.modal.hide();
                }
                else {
                    hideEndOfShiftOption();
                    $scope.modal.show();
                }
            });
            return;
        }
        hideEndOfShiftOption();
        $scope.modal.show();
    };
    $scope.CloseActivitySelection = function () {
        $scope.modal.hide();
    };

    $scope.Parties = function () {
        //trackJs.track('Parties button tapped.');
        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>' });
        $state.go('parties');
    };
    $scope.Team = function () {
        //trackJs.track('Team button tapped.');
        JobsSvc.job.teamChanged = false;
        $state.go('team');
    };
    $scope.GeneralInfo = function () {
        //trackJs.track('General Info button tapped.');
        $state.go('generalinfo');
    };
    $scope.done = function () {
        //trackJs.track('Done button tapped.');
        //do housekeeping
        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>' });
        JobsSvc.save().then(function (result) {
            $ionicLoading.hide();
            JobsSvc.job = null;
            SiteCheckAlarmSvc.stop();
            $state.go('app.jobs');
        }, function (error) {
            console.log(error);
            $ionicLoading.hide();
            alert('an error occured while saving the data');
        });
    };

    $scope.ActivityDetails = function (activity) {
        switch (activity.activityId) {
            case 1:
                JobsSvc.job.CurrentActivity = activity;
                $state.go('acttraveltosite');
                break;
            case 2:
                JobsSvc.job.activityId = 2;
                JobsSvc.job.CurrentActivity = activity;
                $state.go('actsitedeployment');
                break;
            case 3:
                JobsSvc.job.activityId = 3;
                JobsSvc.job.CurrentActivity = activity;
                $state.go('actsitehazardassessment');
                break;
            case 4:
                JobsSvc.job.activityId = 4;
                JobsSvc.job.CurrentActivity = activity;
                $state.go('actwaiting');
                break;
            case 5:
                JobsSvc.job.CurrentActivity = activity;
                $state.go('actworkspaceactive');
                break;
            case 6:
                JobsSvc.job.CurrentActivity = activity;
                $state.go('actsitecheck');
                break;
            case 7:
                //JobsSvc.job.activityId = 7;
                JobsSvc.job.CurrentActivity = activity;
                $state.go('actsiteamendment');
                break;                
            case 8:
                JobsSvc.job.activityId = 8;
                JobsSvc.job.CurrentActivity = activity;
                $state.go('actsiteretrieval');
                break;                
            case 9:
                JobsSvc.job.CurrentActivity = activity;
                $state.go('acttraveltodepot');
                break;
            case 11:
                JobsSvc.job.CurrentActivity = activity;
                $state.go('actprestartprep');
                break;
            case 12:
                JobsSvc.job.CurrentActivity = activity;
                $state.go('acttraveltonextjob');
                break;
            case 10:
                //get the audit
                //load page with the audit
                JobsSvc.getTruckAudit(activity.truckAuditId).then(function (audit) {
                    //console.log(audit);
                    TruckAuditSvc.start(JobsSvc.job.jobName, audit).then(function (result) { });
                });
                break;
        }
    };
    $scope.showDetails = false;
    $scope.toggleDetails = function () {
        $scope.showDetails = !$scope.showDetails;
    };
    $scope.generalNotes = function () {
        //trackJs.track('General notes button tapped.');
        $state.go('generalnotes');
    };
    $scope.chooseTrucks = function () {
        //trackJs.track('Choose trucks button tapped.');
        //alert('choose trucks');
        if ($scope.job.trucks == null)
            $scope.job.trucks = [];

        ModalFactory.showTrucksSelection($scope.job.trucks).then(function (result) {
            console.log(result);
            if (result) {
                //set the trucks
                //migrate to new data structure
                if (result.trucks != null && result.trucks.length > 0 && (typeof result.trucks[0] === 'string')) {
                    console.log('old truck data structure');
                    for (var i = 0; i < result.trucks.length; i++) {
                        var truckId = result.trucks[i];
                        result.trucks[i] = { truckId: truckId, useType: '' };
                    }
                }
                $scope.job.trucks = result.trucks;
                $scope.truckNames = getTruckNames();//($scope.job.trucks);
                //send changes to server
                JobsSvc.save().then(function (result) {
                    JobsSvc.updateTrucks($scope.job);
                });
            }
        }, function (error) {
            console.log(error);
        });
    };
    $scope.showJobMenuOptions = function ($event) {
        //alert('menu!');
        $scope.jobMenuOptions.show($event);
    };
    $scope.doManualUpload = function () {
        //alert('doManualUpload');
        $scope.jobMenuOptions.hide();
        UtilitySvc.uploadJob($scope.job).then(function (result) {
            alert('done!');
        }, function (error) {
            console.log(error);
            alert('error');
        });
    };
}]);