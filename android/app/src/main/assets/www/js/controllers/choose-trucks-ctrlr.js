﻿mainApp.controller('ChooseTrucksCtrlr', ['$scope', 'TrucksSvc', '$linq', '$ionicPopup', '$ionicLoading', 'parameters', function ($scope, TrucksSvc, $linq, $ionicPopup, $ionicLoading, parameters) {
    //$scope.$on("$ionicView.loaded", function () {
    //    $scope.tenantTrucks = $linq.Enumerable().From($scope.tenantTrucks).OrderBy('o => o.truckNumber').ToArray();
    //});
    //console.log('ChooseTrucksCtrlr');

    $scope.$on('modal.shown', function(event){
        console.log('modal.shown');
        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>'});
        TrucksSvc.getAll().then(function(trucks){
            $scope.tenantTrucks = trucks;
            setSelectedTrucks();
            $ionicLoading.hide();
        }, function(error){
            console.log(error);
            $ionicLoading.hide();
        });
    });
    function setSelectedTrucks() {
        /*
        var numTruckNum = [], stringTruckNum = [];
        for (var i = 0; i < $scope.tenantTrucks.length; i++) {
            var truck = $scope.tenantTrucks[i];
            var parsedTruckNum = parseInt(truck.truckNumber);
            if (!isNaN(parsedTruckNum)) {
                truck.truckNumber = parsedTruckNum;
                numTruckNum.push(truck);
            }
            else
                stringTruckNum.push(truck);
        }
        numTruckNum = $linq.Enumerable().From(numTruckNum).OrderBy('o => o.truckNumber').ToArray();
        stringTruckNum = $linq.Enumerable().From(stringTruckNum).OrderBy('o => o.truckNumber').ToArray();        
        $scope.tenantTrucks = numTruckNum.concat(stringTruckNum);
        */
        for (var i = 0; i < $scope.tenantTrucks.length; i++) {
            var truck = $scope.tenantTrucks[i];
            var truckSelected = $linq.Enumerable().From($scope.selectedTrucks).FirstOrDefault(null, 'f => f == "' + truck.id + '"');
            if (truckSelected != null)
                truck.checked = true;
            //if ($scope.selectedTrucks.indexOf(truck.id) >= 0)
            //    truck.checked = true;
        }

        $scope.selectedCount = $scope.selectedTrucks.length;
    }
    $scope.tenantTrucks = angular.copy(TrucksSvc.getAll());
    $scope.selectedTrucks = parameters.selectedTrucks || [];
    $scope.selectedCount = 0;
    $scope.cancel = function () {
        $scope.closeModal(null);
    };
    $scope.done = function () {        
        var selectedTrucks = $linq.Enumerable().From($scope.tenantTrucks).Where('w => w.checked == true').ToArray();
        var truckIds = $linq.Enumerable().From(selectedTrucks).Select('s => s.id').ToArray();
        var truckNames = $linq.Enumerable().From(selectedTrucks).Select('s => (s.truckNumber + " - " + s.truckRego)').ToArray();
        $scope.closeModal({ trucks: truckIds, truckNames: truckNames });
    };
    $scope.addTruck = function (truck) {
        //console.log(truck);        
        if (truck.checked)
            $scope.selectedCount++;
        else
            $scope.selectedCount--;
            
        if ($scope.selectedCount > 6) {
            truck.checked = false;
            $scope.selectedCount--;
        }
    };
    $scope.refreshTrucks = function () {
        return;
        $ionicLoading.show({ template: '<ion-spinner></ion-spinner>', hideOnStateChange: true });
        TrucksSvc.getFromServer().then(function (result) {
            $scope.tenantTrucks = angular.copy(TrucksSvc.getAll());
            setSelectedTrucks();
        }, function (error) {
            $ionicPopup.alert({ title: 'Site Pro', template: 'Unable to load new data.' });
        }).finally(function () {
            $ionicLoading.hide();
        });
    };
}]);