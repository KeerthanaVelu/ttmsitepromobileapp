﻿mainApp.service('TransferJobSvc', ['ModalFactory', '$q', '$resource', 'ApiSvc', 'AccountSvc', 'JobsSvc', function (ModalFactory, $q, $resource, ApiSvc, AccountSvc, JobsSvc) {
    var service = {};

    service.transferForm = function (job, branches) {
        var defer = $q.defer();
        ModalFactory.showTransferJobView(job, branches).then(function (result) {
            defer.resolve(result);
        }, function (error) {
            defer.reject(error);
        });

        return defer.promise;
    };

    service.transfer = function (data) {
        console.log('transfer');
        var authHeader = { 'Authorization': 'Bearer ' + AccountSvc.getAccessToken() };
        var resource = $resource(ApiSvc.mobileJobTransfer, null, {
            'mobileJobTransfer': { method: 'POST', url: ApiSvc.mobileJobTransfer, headers: authHeader }
        });

        return resource.mobileJobTransfer(data).$promise;
    };

    return service;
}]);
mainApp.controller('TransferJobCtrlr', ['$scope', '$resource', 'parameters', 'AccountSvc', '$ionicLoading', 'TenantSvc', function ($scope, $resource, parameters, AccountSvc, $ionicLoading, TenantSvc) {
    console.log(parameters);
    $scope.model = {};
    $scope.job = parameters.job;
    $scope.branches = parameters.branches;    
    $scope.cancelTransfer = function () {
        $scope.closeModal(null);
    };
    $scope.transferJob = function () {
        $scope.model.jobId = parameters.job.id;
        $scope.closeModal($scope.model);
    };
    $scope.getSTMSs = function (branchId) {
        if (branchId == null || branchId.length == 0)
            return;

        $ionicLoading.show({ template: '<ion-spinner></ion-spinner>' });
        TenantSvc.getStmssByBranch(AccountSvc.getTenantId(), branchId).then(function (results) {
            //var excludeStms = $linq.Enumerable().From(results).FirstOrDefault(null, 'f => f.fullName == "' + $scope.currentStms + '"');
            //var index = results.indexOf(excludeStms);
            //if (index >= 0)
            //    results.splice(index, 1);
            console.log(results);
            $scope.stmss = results;
            $ionicLoading.hide();
        }, function (error) {
            console.log(error);
            $ionicLoading.hide();
            alert('An error occured while processing the request.');
        });
    };
}]);