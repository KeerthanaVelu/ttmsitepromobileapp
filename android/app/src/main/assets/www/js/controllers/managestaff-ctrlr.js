﻿mainApp.controller('ManageStaffCtrlr', ['$scope', '$state', '$linq', 'StaffSvc', 'AccountSvc', '$ionicLoading', function ($scope, $state, $linq, StaffSvc, AccountSvc, $ionicLoading) {
    $scope.$on('$ionicView.enter', function () {
        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>'});
        AccountSvc.getBranchesV2().then(function(branches){
            $scope.branches = branches;
            StaffSvc.getAll().then(function(staffMembers){                
                console.log(staffMembers);        
                $scope.staff = staffMembers;
                $ionicLoading.hide();
            });
        }, function(error){
            console.log(error);
        });
    });
    function loadStaff() {
        $scope.staff = StaffSvc.get();
    }
    $scope.search = '';
    $scope.branchId = '';
    //$scope.branches = AccountSvc.getBranchesV2();
    $scope.filters = { search: '', branchId: '' };
    $scope.getBranchName = function (member) {
        var branch = $linq.Enumerable().From($scope.branches).FirstOrDefault(null, 'f => f.id == "' + member.branchId + '"');
        return branch != null ? branch.branch : '';
    };
    $scope.edit = function (member) {
        StaffSvc.staffMember = member;
        $state.go('app.staffform', { mode: 'Edit' });
    };
    $scope.new = function () {
        //StaffSvc.staffMember = new Staff();
        StaffSvc.staffMember = angular.copy(Staff);
        $state.go('app.staffform', { mode: 'New' });
    };
    $scope.close = function () {
        $state.go('app.jobs');
    };
}]);