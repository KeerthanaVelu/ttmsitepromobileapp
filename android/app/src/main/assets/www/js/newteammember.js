﻿mainApp.controller('NewTeamMemberCtrlr', ['$scope', '$state', '$ionicPlatform', '$ionicPopup', 'JobsSvc', 'StaffSvc', '$cordovaCamera', 'AccountSvc', 'PhotoSvc', '$cordovaFile', function ($scope, $state, $ionicPlatform, $ionicPopup, JobsSvc, StaffSvc, $cordovaCamera, AccountSvc, PhotoSvc, $cordovaFile) {
    $scope.Branches = JobsSvc.Branches;
    $scope.member = { id: (new ObjectId()).toString(), branchId: null, fullName: null, phone: null, qualification: null, notes: null, photo: '', timeLogs: [] };
   
    $scope.cancel = function () {
        $state.go('team');
    };

    function saveAndExit() {
        //var newStaff = { id: $scope.member.id, branchId: $scope.member.branchId, fullName: $scope.member.fullName, phone: $scope.member.phone, qualification: $scope.member.qualification, notes: $scope.member.notes, photo: $scope.member.photo, tenantId: AccountSvc.getTenantId(), archived: false };
        var newStaff = new Staff();
        newStaff.id = $scope.member.id;
        newStaff.branchId = $scope.member.branchId;
        newStaff.fullName = $scope.member.fullName;
        newStaff.phone = $scope.member.phone;
        newStaff.qualification = $scope.member.qualification;
        newStaff.notes = $scope.member.notes;
        newStaff.photo = $scope.member.photo;
        newStaff.tenantId = AccountSvc.getTenantId();

        if (!JobsSvc.job.teamMembers)
            JobsSvc.job.teamMembers = [];        
        StaffSvc.addStaff(newStaff).then(function () {
            JobsSvc.job.teamChanged = true;
            JobsSvc.job.teamMembers.push({ staffId: $scope.member.id, timeLogs: $scope.member.timeLogs });
            //JobsSvc.job.teamMembers.push({ staffMemberId: $scope.member.id, timeLogs: $scope.member.timeLogs });
            $state.go('team');
        });

        //JobsSvc.job.teamMembers.push({ staffMemberId: $scope.member.id, timeLogs: $scope.member.timeLogs });               
        //StaffSvc.addStaff(newStaff).then(function () {
        //    JobsSvc.job.teamChanged = true;
        //    $state.go('team');
        //});
    }

    function valid() {
        var errors = [];
        if ($scope.member.fullName == null || $scope.member.fullName.length == 0)
            errors.push("Please enter a Full Name.");
        if ($scope.member.branchId == null || $scope.member.branchId.length == 0)
            errors.push("Please choose a Branch.");

        if (errors.length > 0) {
            var errorMessage = '';
            for (var i = 0; i < errors.length; i++) {
                errorMessage += errors[i];
                if (i < errors.length)
                    errorMessage += "<br/>";
            }

            $ionicPopup.alert({ title: 'Site Pro', template: errorMessage });
        }

        return errors.length <= 0;
    }
    $scope.branches = AccountSvc.getBranches();
    $scope.done = function () {
        if (!valid()) {
            return;
        }
        return;
        if(JobsSvc.job.activities && JobsSvc.job.activities.length > 0)
            $scope.member.timeLogs.push({ timeLogType: 0, time: new Date() });

        saveAndExit();        
    };
    $scope.addPhoto = function () {
        PhotoSvc.getPhoto().then(function (photo) {
            console.log(photo);
            $cordovaFile.readAsDataURL(getDirectory(photo.path), getFileName(photo.path)).then(function (result) {
                //console.log(result);
                $scope.member.photo = result.replace('data:image/jpeg;base64,', '');
            });
            //window.resolveLocalFileSystemURL(photo.path, function (resolvedFile) {
            //    resolvedFile.file(function (file) {
            //        var reader = new FileReader();
            //        reader.onloadend = function (result) {
            //            $scope.member.photo = result;
            //        };
            //        reader.readAsDataURL(file);
            //    });                
            //});            
            //    getDirectory(photo.path), getFileName(photo.path)).then(function (result) {
            //    console.log(result);
            //    $scope.member.photo = result;
            //});

            //$scope.member.photo = photo;

        }, function(error){
            alert('Something went wrong while getting a photo.');
        });
        /*
        $ionicPlatform.ready(function () {
            var options = {
                quality: 75,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.CAMERA,
                allowEdit: false,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 256,
                targetHeight: 256,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: true,
                correctOrientation: true
            };

            $cordovaCamera.getPicture(options).then(function (result) {                
                var photo = result;
                $scope.member.photo = photo;
                //$scope.$apply();
            }, function (error) {
                // error
                alert(error);
            });
        });
        */
    };
    $scope.Start = function () {

    };
}]);