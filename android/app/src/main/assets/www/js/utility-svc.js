﻿mainApp.factory('UtilitySvc', ['$resource', '$q', 'ApiSvc', 'AccountSvc', function ($resource, $q, ApiSvc, AccountSvc) {
    var service = {};
    service.uploadJob = function (job) {
        var defer = $q.defer();
        var token = AccountSvc.getAccessToken();
        var resource = $resource('/', null, {
            'uploadJob': { url: ApiSvc.manualJobUpload, method: 'POST', headers: { 'Authorization': 'Bearer ' + token }, isArray: false }
        });
        resource.uploadJob({}, job, function (result) {
            defer.resolve(result);
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    };
    
    return service;
}]);