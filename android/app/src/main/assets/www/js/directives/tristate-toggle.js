app.directive('triStateToggle', function() {
	return {
    restrict: 'E',
    replace: true,    
    scope: { ngModel: '=', hasComment: '=' },
    link: function(scope, element, attributes){
        scope.setVal = function(){
            scope.ngModel++;
            if(scope.ngModel > 3)
                scope.ngModel = 1;
        }
        scope.$watch('hasComment', function(newVal, oldVal){
            if(scope.ngModel == 3 && newVal)
                scope.ngModel = 4;
        });
        //init
        if(!scope.ngModel){
            scope.ngModel = 1;
        }
    },
    template: '<button class="button button-icon icon tri-state-toggle"' +
        ' ng-class="{\'ion-ios-circle-outline\': ngModel == 1, \'ion-ios-checkmark green-icon\': ngModel == 2, \'ion-ios-close red-icon\': ngModel == 3,' +
        ' \'ion-ios-close blue-icon\': ngModel == 4}"' +
        ' ng-click="setVal()"></button>'      
 }
});