mainApp.directive('crossCheckToggle', function(){
    return {
        restrict: 'E',
        templateUrl: 'views/crossCheckToggle.html',
        scope: { checked: '=ngModel' },
        controller: 'CrossCheckToggleCtrlr'
    };
});