mainApp.directive('spJobActivity', ['$filter', function ($filter) {    
    return {
        restrict: 'E',
        transclude: true,
        link: function (scope, element, attrs, controller) {
            scope.$watch(scope, function () {
                var dummy = 1;
            });

            switch (scope.activity.ActivityId) {
                case 1:
                    var template = '<strong>Travel to Site</strong><div>Departure Time <span class="pull-right">' + $filter('date')(scope.activity.startTime, 'hh:mm a dd-MM-yyyy') + '</span></div>';
                    template += '<div>Arrival Time <span class="pull-right">' + $filter('date')(scope.activity.endTime, 'hh:mm a dd-MM-yyyy') + '</span></div>';
                    element.append(template);
                    break;
                case 2:
                    var template = '<strong>Site Deployment</strong><div>Start Time <span class="pull-right">' + $filter('date')(scope.activity.startTime, 'hh:mm a dd-MM-yyyy') + '</span></div>';
                    template += '<div>End Time <span class="pull-right">' + $filter('date')(scope.activity.endTime, 'hh:mm a dd-MM-yyyy') + '</span></div>';
                    element.append(template);
                    break;
                case 3:
                    var template = '<strong>Site Hazard Assessment</strong><div class="wrap-ellipse">' + scope.activity.Description + '</span></div>';
                    template += '<div>Start Time <span class="pull-right">' + $filter('date')(scope.activity.startTime, 'hh:mm a dd-MM-yyyy') + '</span></div>';
                    template += '<div>End Time <span class="pull-right">' + $filter('date')(scope.activity.endTime, 'hh:mm a dd-MM-yyyy') + '</span></div>';
                    element.append(template);
                    break;
                case 4:
                    var template = '<strong>Waiting</strong><div>Start Time <span class="pull-right">' + $filter('date')(scope.activity.startTime, 'hh:mm a dd-MM-yyyy') + '</span></div>';
                    template += '<div>End Time <span class="pull-right">' + $filter('date')(scope.activity.endTime, 'hh:mm a dd-MM-yyyy') + '</span></div>';
                    element.append(template);
                    break;
                case 5:
                    var template = '<strong>Work Space Active</strong><div>Start Time <span class="pull-right">' + (scope.activity.startTime != null ? $filter('date')(scope.activity.startTime, 'hh:mm a dd-MM-yyyy') : '') + '</span></div>';
                    template += '<div>End Time <span class="pull-right">' + (scope.activity.endTime != null ? $filter('date')(scope.activity.endTime, 'hh:mm a dd-MM-yyyy') : '') + '</span></div>';
                    element.append(template);
                    break;
                case 6:
                    var template = '<strong>Site Check</strong><div>Time <span class="pull-right">' + $filter('date')(scope.activity.SiteCheckTime, 'hh:mm a dd-MM-yyyy') + '</span></div>';
                    element.append(template);
                    break;
                case 7:
                    var template = '<strong>Site Deployment</strong><div>Start Time <span class="pull-right">' + $filter('date')(scope.activity.startTime, 'hh:mm a dd-MM-yyyy') + '</span></div>';
                    template += '<div>End Time <span class="pull-right">' + $filter('date')(scope.activity.endTime, 'hh:mm a dd-MM-yyyy') + '</span></div>';
                    element.append(template);
                    break;
                case 8:
                    var template = '<strong>Site Retrieval</strong><div>Start Time <span class="pull-right">' + $filter('date')(scope.activity.startTime, 'hh:mm a dd-MM-yyyy') + '</span></div>';
                    template += '<div>End Time <span class="pull-right">' + $filter('date')(scope.activity.endTime, 'hh:mm a dd-MM-yyyy') + '</span></div>';
                    element.append(template);
                    break;
                case 9:
                    var template = '<strong>Travel to Depot</strong><div>Departure Time <span class="pull-right">' + $filter('date')(scope.activity.startTime, 'hh:mm a dd-MM-yyyy') + '</span></div>';
                    template += '<div>Arrival Time <span class="pull-right">' + $filter('date')(scope.activity.endTime, 'hh:mm a dd-MM-yyyy') + '</span></div>';
                    element.append(template);
                    break;
            }
        }
    };
}]);