﻿mainApp.controller('PartiesCtrlr', ['$scope', '$state', 'JobsSvc', 'RCASvc', 'ContractorSvc', 'AssetOwnerSvc', '$ionicLoading', '$linq', '$q', function ($scope, $state, JobsSvc, RCASvc, ContractorSvc, AssetOwnerSvc, $ionicLoading, $linq, $q) {    
    $scope.$on('$ionicView.enter', function () {
        initParties();
        $ionicLoading.hide();
    });

    $scope.parties = { rca: null, contractor: null, contractorBranch: null, projectManager: null, assetOwner: null, onsiteContact: null, onsiteContactNum: null };
    //$scope.parties = { rca: null, contractor: null, contractorBranch: null, projectManager: null, assetOwner: null };

    function initParties() {
        var jobParties = angular.copy(JobsSvc.job.parties);
        if (jobParties != null) {
            if (jobParties.rcaId != null) {
                $scope.parties.rca = $linq.Enumerable().From($scope.RCAs).FirstOrDefault(null, 'f => f.id == "' + jobParties.rcaId + '"');
            }
            if ($scope.parties.rca && jobParties.contractorId != null) {
                getContractorsByRCA($scope.parties.rca).then(function (result) {
                    $scope.contractors = result;
                    $scope.parties.contractor = $linq.Enumerable().From($scope.contractors).FirstOrDefault(null, 'f => f.id == "' + jobParties.contractorId + '"');
                    if ($scope.parties.contractor && jobParties.contractorBranchId != null) {
                        //if($scope.parties.contractor.branches != null)
                          $scope.parties.contractorBranch = $linq.Enumerable().From($scope.parties.contractor.branches).FirstOrDefault(null, 'f => f.id =="' + jobParties.contractorBranchId + '"');
                    }
                    if ($scope.parties.contractorBranch && jobParties.projectManagerId != null) {
                        $scope.parties.projectManager = $linq.Enumerable().From($scope.parties.contractorBranch.projectManagers).FirstOrDefault(null, 'f => f.id =="' + jobParties.projectManagerId + '"');                        
                    }
                });
            }
            
            if ($scope.parties.rca && jobParties.assetOwnerId != null) {
                getAssetOwnersByRCA($scope.parties.rca).then(function (result) {
                    $scope.assetOwners = result;
                    $scope.parties.assetOwner = $linq.Enumerable().From($scope.assetOwners).FirstOrDefault(null, 'f => f.id =="' + jobParties.assetOwnerId + '"');
                });
            }            
        }
            
    }

    $scope.RCAs = RCASvc.GetAll();
    $scope.contractors = [];
    $scope.assetOwners = [];

    $scope.cancel = function () {
        $state.go('jobdetails');
    }
    $scope.done = function () {
        if(JobsSvc.job.parties == null)
            JobsSvc.job.parties={};

        JobsSvc.job.parties.rcaId = $scope.parties.rca ? $scope.parties.rca.id : null;
        JobsSvc.job.parties.contractorId = $scope.parties.contractor ? $scope.parties.contractor.id : null;
        JobsSvc.job.parties.contractorBranchId = $scope.parties.contractorBranch ? $scope.parties.contractorBranch.id : null;
        JobsSvc.job.parties.projectManagerId = $scope.parties.ProjectManager ? $scope.parties.projectManager.id : null;
        JobsSvc.job.parties.assetOwnerId = $scope.parties.assetOwner ? $scope.parties.assetOwner.id : null;

        $state.go('jobdetails');
    };

    function getOtherParties(rca){
        var defer = $q.defer();        
        $q.all({contractors: getContractorsByRCA(rca), assetOwners: getAssetOwnersByRCA(rca)}).then(function (result) {
            defer.resolve(result);
        });
        
        return defer.promise;
    }

    $scope.GetOtherParties = function (rca) {        
        if (rca == null) {
            $scope.contractors = null;
            return;
        }

        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>' });
        getOtherParties(rca).then(function (result) {
            $scope.contractors = result.contractors;
            $scope.assetOwners = result.assetOwners;
            $ionicLoading.hide();
        });        
    };

    function getContractorsByRCA(rca) {
        var defer = $q.defer();
        var matchingContractors = ContractorSvc.GetAllByRCA(rca.id);        
        defer.resolve(matchingContractors);

        return defer.promise;
    }

    function getAssetOwnersByRCA(rca) {
        var defer = $q.defer();
        var matchingAssetOwners = AssetOwnerSvc.GetAllByRCA(rca.id);        
        defer.resolve(matchingAssetOwners);

        return defer.promise;
    }

    $scope.GetAssetOwners = function (rcaId) {
        if (rcaId == null)
            $scope.assetOwners = null;
        else {
            $scope.assetOwners = AssetOwnerSvc.GetAllByRCA(rcaId);
        }
    };  
    
    $scope.SetParties = function () {
        $scope.GetOtherParties($scope.parties.rcaId);
        $scope.contractors = ContractorSvc.GetAllByRCA($scope.parties.rcaId);
        angular.forEach($scope.Contractors, function (value, key) {
            if(value.Id == $scope.parties.contractorId)
                $scope.contractor = value;
        });        
        $scope.contractorBranches = $scope.contractor.branches;
        angular.forEach($scope.contractorBranches, function (value, key) {
            if(value.Id == $scope.parties.contractorBranchId)
                $scope.contractorBranch = value;
        });        
        $scope.GetProjectManagers($scope.contractorBranch);        
    };
}]);