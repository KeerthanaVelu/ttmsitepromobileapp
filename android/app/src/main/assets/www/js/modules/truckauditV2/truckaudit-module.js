﻿let TruckAuditVw = {
    id: null, truckId: null, tenantId: null, truckRego: null, truckNumber: null, cofExpiry: null, regoExpiry: null,
    rucMaxReading: null, currentHuboReading: null, currentOdometerReading: null, flashingBeaconsCount: null, flashingBeaconsTotal: null,
    nextServiceDue: null, dateOfAudit: null, lights: [], inspections: []
};
let TruckAuditInspectionItemInputType = { Toggle: 0, Text: 1, Star: 2, Fluid: 3 };
let DefaultUnixTime = "1970-01-01";//new Date(1970,0,1,0,0,0);
let FluidLevelVw = { index: -1, name: 'Oil', inputType: 3, photos: [], audioNotes: [], description: null, rating: 0, value: null, defaultRating: 0 };


var truckAuditMod = angular.module('TruckAudit', ['ionic', 'angucomplete-alt', 'mainApp']);
truckAuditMod.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
    $stateProvider.state('app.truckaudit', {
        url: '/',
        templateUrl: 'js/modules/truckauditV2/truckaudit.html',
        controller: 'TruckAuditCtrlr'
    }).state('app.inspectiondetails', {
        url: '/inspectiondetails',
        templateUrl: 'js/modules/truckauditV2/inspectiondetails.html',
        controller: 'InspectionDetailsCtrlr',
        params:{index: null, inputtype: null, isReadonly: false}
    }).state('app.truckreport', {
        url: '/truckreport',
        templateUrl: 'js/modules/pretruckreport/truckreport.html',
        controller: 'TruckReportCtrlr'     
    });
}]);
truckAuditMod.directive('myStarRating', function () {
    return {
        restrict: 'E',
        replace: true,
        template: '<span class="star-rating">' +
            '<span ng-repeat="star in stars" ng-click="setRating(star)" ng-class="{\'star-on\': star.highlight}" class="icon ion-ios-star star"></span>' +
            '</span>',
        scope: {
            defaultRating: '=',
            maxStars: '=',
            index: '=',
            ratingCallback: '=',
            isDisabled: '='
        },
        link: function (scope, element, attributes) {
            scope.maxStars = scope.maxStars || 5;
            scope.prevRating = -1;
            scope.stars = [];
            for (var i = 0; i < scope.maxStars; i++) {
                scope.stars.push({ value: i + 1, highlight: (scope.defaultRating >= (i + 1)) });
            }
            scope.setRating = function (star) {
                if(scope.isDisabled)
                    return;
                if (scope.defaultRating == star.value)
                    highlightStars(star, true);
                else
                    scope.defaultRating = star.value;
            };
            scope.starHighlighted = false;
            function highlightStars(star, uiEvent) {
                scope.starHighlighted = false;
                //if same prevvalue then toggle
                for (var i = 0; i < scope.stars.length; i++) {
                    if (star.value == scope.prevRating) {
                        if ((i + 1) <= star.value)
                            scope.stars[i].highlight = !scope.stars[i].highlight;
                        else
                            scope.stars[i].highlight = false;
                    }
                    else
                        scope.stars[i].highlight = (i + 1) <= star.value;

                    if (scope.stars[i].highlight)
                        scope.starHighlighted = true;
                }
                returnVal = scope.starHighlighted ? star.value : 0;
                scope.prevRating = star.value;
                scope.ratingCallback(scope.index, returnVal);
            };

            scope.$watch('defaultRating', function (newVal, oldVal) {
                //console.log('rating changed');                
                highlightStars({ value: newVal });
            });
        }
    }
});
truckAuditMod.directive('fluidLevel', function () {
    return {
        restrict: 'E',
        replace: true,
        template:
        '<span class="fluid-level">' +
        '<span class="fluid-level-item" ng-class="{\'fluid-level-item-selected\': ngModel >= 1 && ngModel <= 7 }" ng-click="setValue(1)">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span > ' +
        '<span class="fluid-level-item" ng-class="{\'fluid-level-item-selected\': ngModel >= 2 && ngModel <= 7 }" ng-click="setValue(2)">1/4</span>' +
        '<span class="fluid-level-item" ng-class="{\'fluid-level-item-selected\': ngModel >= 3 && ngModel <= 7 }" ng-click="setValue(3)">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>' +
        '<span class="fluid-level-item" ng-class="{\'fluid-level-item-selected\': ngModel >= 4 && ngModel <= 7 }" ng-click="setValue(4)">1/2</span>' +
        '<span class="fluid-level-item" ng-class="{\'fluid-level-item-selected\': ngModel >= 5 && ngModel <= 7 }" ng-click="setValue(5)">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>' +
        '<span class="fluid-level-item" ng-class="{\'fluid-level-item-selected\': ngModel >= 6 && ngModel <= 7 }" ng-click="setValue(6)">3/4</span>' +
        '<span class="fluid-level-item" ng-class="{\'fluid-level-item-selected\': ngModel >= 7 && ngModel <= 7 }" ng-click="setValue(7)">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>' +
        '</span>',
        scope: {
            fluidValue: '=?',
            ngModel: '=',
            isDisabled: '='
        },
        link: function (scope, element, attributes) {
            scope.prevValue = '';
            scope.setValue = function (value) {
                if(scope.isDisabled)
                    return;
                if (value != scope.prevValue)
                    scope.prevValue = scope.ngModel = value;
                else
                    scope.prevValue = scope.ngModel = '';
            };
        }

    }
});
truckAuditMod.factory('TruckAuditSvc', ['$state', '$rootScope', '$q', 'TrucksSvc', function ($state, $rootScope, $q, TrucksSvc) {
    let service = {};
    let trucks = null;
    
    service.start = function (trucks, jobName, audit, isReadonly = false) {
        let defer = $q.defer();
        service.jobName = jobName;
        service.trucks = trucks;
        service.audit = audit;
        service.isReadonly = isReadonly;
        $rootScope.$on('auditdone', function (event, audit) {
            defer.resolve(audit);
        });
        $state.go('app.truckaudit');
        return defer.promise;
    };

    service.getLastAudit = function(id){
        return TrucksSvc.getLastAudit(id);
    };

    return service;
}]);
truckAuditMod.filter('relativeDataDirectoryPath', function () {
    return function (filename) {
        return cordova.file.dataDirectory + '/' + filename;
    };
});
truckAuditMod.controller('TruckAuditCtrlr', ['$scope', '$ionicHistory', '$rootScope', 'DBSvc', '$state', '$ionicPopup', '$ionicActionSheet', 'TruckAuditSvc', '$ionicLoading', '$linq', 'TrucksSvc', 'ModalFactory', '$q', function ($scope, $ionicHistory, $rootScope, DBSvc, $state, $ionicPopup, $ionicActionSheet, TruckAuditSvc, $ionicLoading, $linq, TrucksSvc, ModalFactory, $q) {
    $scope.$on('$ionicView.beforeEnter', function(){
        console.log('$ionicView.beforeEnter');
        //refreshTruckData();
    });

    $scope.$on('$ionicView.enter', function(){
        console.log('$ionicView.enter');
        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>'});
        refreshTruckData().then(function(){            
        }).catch(function(){
            console.log('catch error');
            $ionicPopup.alert({title: 'Site Pro', template: 'Unable to refresh truck data.'});
        }).finally(function(){
            console.log('finally');
            $scope.trucks = TruckAuditSvc.trucks;
            $scope.audit = TruckAuditSvc.audit;
            $scope.isReadonly = TruckAuditSvc.isReadonly;
            if($scope.audit){
                $scope.fields = $scope.audit;
                var auditDate = moment($scope.fields.lastAudit);
                $scope.fields.daysSinceLastAudit = moment().diff(auditDate, 'days');
                $scope.fields.regoExpiry = $scope.fields.regoExpiry ? new Date($scope.fields.regoExpiry) : null;
                $scope.fields.cofExpiry = $scope.fields.cofExpiry ? new Date($scope.fields.cofExpiry) : null;
                $scope.getDaysUntilExp($scope.fields.regoExpiry, 'daysUntilRegoExp');
                $scope.getDaysUntilExp($scope.fields.cofExpiry, 'daysUntilCOFExp');
                $scope.getKmsUntilServiceDue();
                $scope.getRucsRemaining();
                let truck = $linq.Enumerable().From($scope.trucks).FirstOrDefault(null, 'f => f.id == "' + $scope.audit.truckId + '"');
                $scope.items.selectedTruck = truck;
                //$scope.$broadcast('angucomplete-alt:changeInput', 'regoNoAngucomplete', $scope.fields['truckRego']);            
            }
            else{
                $scope.chooseTruck();
            }
            $ionicLoading.hide();
        });

        /*
        $scope.trucks = TruckAuditSvc.trucks;
        $scope.audit = TruckAuditSvc.audit;
        $scope.isReadonly = TruckAuditSvc.isReadonly;
        if($scope.audit){
            $scope.fields = $scope.audit;
            var auditDate = moment($scope.fields.lastAudit);
            $scope.fields.daysSinceLastAudit = moment().diff(auditDate, 'days');
            $scope.fields.regoExpiry = $scope.fields.regoExpiry ? new Date($scope.fields.regoExpiry) : null;
            $scope.fields.cofExpiry = $scope.fields.cofExpiry ? new Date($scope.fields.cofExpiry) : null;
            $scope.getDaysUntilExp($scope.fields.regoExpiry, 'daysUntilRegoExp');
            $scope.getDaysUntilExp($scope.fields.cofExpiry, 'daysUntilCOFExp');
            $scope.getKmsUntilServiceDue();
            $scope.getRucsRemaining();
            let truck = $linq.Enumerable().From($scope.trucks).FirstOrDefault(null, 'f => f.id == "' + $scope.audit.truckId + '"');
            $scope.items.selectedTruck = truck;
            //$scope.$broadcast('angucomplete-alt:changeInput', 'regoNoAngucomplete', $scope.fields['truckRego']);            
        }
        else{
            $scope.chooseTruck();
        }
        */
    });
    $scope.truck = null;
    function newAudit(truck) {
        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>'});
        $scope.fields = angular.copy(TruckAuditVw);
        //$scope.fields.id = truck.id;
        $scope.fields.truckId = truck.id;
        $scope.fields.tenantId = truck.tenantId;
        $scope.fields.truckRego = truck.truckRego;
        $scope.fields.truckNumber = truck.truckNumber;
        $scope.fields.dateOfAudit = new Date();
        $scope.fields.cofExpiry = truck.cofExpiry ? new Date(truck.cofExpiry) : null;
        $scope.fields.regoExpiry = truck.regoExpiry ? new Date(truck.regoExpiry) : null;
        $scope.fields.rucMaxReading = truck.rucMaxReading;        
        $scope.fields.nextServiceDue = truck.nextServiceDue;
        
        TruckAuditSvc.getLastAudit(truck.id).then(function (audit) {
            if (audit) {            
                $scope.fields.lastDateOfAudit = audit.dateOfAudit;
            }
            
            //$scope.$broadcast('angucomplete-alt:changeInput', params.target, $scope.fields[params.fieldValue]);
            var lastAuditDate = moment($scope.fields.lastDateOfAudit);
            $scope.fields.daysSinceLastAudit = moment().diff(lastAuditDate, 'days');
            if (!$scope.fields.inspections) {
                $scope.fields.inspections = [];
            }
            var inspectionNames = ['Fire Ext', 'First Aid', 'Harness', 'Lanyard', 'Interior Clean', 'Exterior Clean', 'Tyres - Front Left', 'Tyres - Front Right', 'Tyres - Rear Left', 'Tyres - Rear Right', 'Tyres - Inside L', 'Tyres - Outside L', 'New Damage'];
            var lights = ['Lights-Low Beam', 'Lights-High Beam', 'Lights-Tail Lights', 'Lights-Brake Lights', 'Lights-Indicators Forward', 'Lights-Indicators Rear', 'Lights-Reverse Buzzer'];
            /*
            var lights = ['Lights-Left Head Light LOW BEAM','Lights-Right Head Light LOW BEAM','Lights-Left Head Light HIGH BEAM','Lights-Right Head Light HIGH BEAM',
                'Lights-Left Tail Light','Lights-Right Tail Light','Lights-Left Brake Light','Lights-Right Brake Light','Lights-Left Indicator Forward','Lights-Right Indicator Forward',
                'Lights-Left Indicator Rear','Lights-Right Indicator Rear','Lights-Reverse Buzzer'];
            */
            $scope.fields.lights = lights.map(item => { return { name: item, value: null }; });

            for (var i = 0; i <= 12; i++) {
                $scope.fields.inspections[i] = { index: i, name: inspectionNames[i], photos: [], audioNotes: [], description: null, rating: 0, value: null, defaultRating: 0 };
                $scope.fields.inspections[i].rateSettings = $scope.ratingsSettings[i];
                if (i < 4)
                    $scope.fields.inspections[i].inputType = TruckAuditInspectionItemInputType.Text;// 1;
                else if (i < 12)
                    $scope.fields.inspections[i].inputType = TruckAuditInspectionItemInputType.Star;// 2;
            }
            //inspection for new damage
            $scope.fields.inspections[12] = { index: 12, name: inspectionNames[12], inputType: 0, photos: [], audioNotes: [], description: null, rating: 0, value: null, defaultRating: 0 };
            
            $scope.getDaysUntilExp($scope.fields.regoExpiry, 'daysUntilRegoExp');
            $scope.getDaysUntilExp($scope.fields.cofExpiry, 'daysUntilCOFExp');
            $scope.getKmsUntilServiceDue();
            $scope.getRucsRemaining();


            if (!$scope.fields.inspections)
                $scope.fields.inspections = [];
            TruckAuditSvc.currentTruck = $scope.fields;

            $scope.fields.oilLevelDefaultRating = $scope.fields.oilLevel;
            $scope.fields.waterLevelDefaultRating = $scope.fields.waterLevel;
            $scope.fields.dieselLevelDefaultRating = $scope.fields.dieselLevel;
            $scope.fields.oilLevelDetails = angular.copy(FluidLevelVw);            
            $scope.fields.oilLevelDetails.index = -1;
            $scope.fields.oilLevelDetails.name = 'Oil';
            $scope.fields.waterLevelDetails = angular.copy(FluidLevelVw);
            $scope.fields.waterLevelDetails.index = -2;
            $scope.fields.waterLevelDetails.name = 'Water';
            $scope.fields.dieselLevelDetails = angular.copy(FluidLevelVw);
            $scope.fields.dieselLevelDetails.index = -3;
            $scope.fields.dieselLevelDetails.name = 'Diesel';

            //reset if no inspections
            if ($scope.fields.inspections.length == 0) {
                for (var i = 0; i < $scope.ratingsSettings.length; i++) {
                    $scope.ratingsSettings[i].rating = 0;
                }
            }

            $scope.items.selectedTruck = truck;
            TruckAuditSvc.audit = $scope.fields;
            $ionicLoading.hide();
        }, function (error) {
            console.log(error);
            $ionicLoading.hide();
            console.log('[246]');
            alert('error');
        });
    }
    function newAuditOLD(truck, params) {
        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>'});
        $scope.fields = angular.copy(TruckAuditVw);
        //$scope.fields.id = truck.id;
        $scope.fields.truckId = truck.id;
        $scope.fields.tenantId = truck.tenantId;
        $scope.fields.truckRego = truck.truckRego;
        $scope.fields.truckNumber = truck.truckNumber;
        $scope.fields.dateOfAudit = new Date();
        $scope.fields.cofExpiry = truck.cofExpiry ? new Date(truck.cofExpiry) : null;
        $scope.fields.regoExpiry = truck.regoExpiry ? new Date(truck.regoExpiry) : null;
        $scope.fields.rucMaxReading = truck.rucMaxReading;        
        $scope.fields.nextServiceDue = truck.nextServiceDue;
        
        TruckAuditSvc.getLastAudit(truck.id).then(function (audit) {
            if (audit) {            
                $scope.fields.lastDateOfAudit = audit.dateOfAudit;
            }
            
            $scope.$broadcast('angucomplete-alt:changeInput', params.target, $scope.fields[params.fieldValue]);
            var lastAuditDate = moment($scope.fields.lastDateOfAudit);
            $scope.fields.daysSinceLastAudit = moment().diff(lastAuditDate, 'days');
            if (!$scope.fields.inspections) {
                $scope.fields.inspections = [];
            }
            var inspectionNames = ['Fire Ext', 'First Aid', 'Harness', 'Lanyard', 'Interior Clean', 'Exterior Clean', 'Tyres - Front Left', 'Tyres - Front Right', 'Tyres - Rear Left', 'Tyres - Rear Right', 'Tyres - Inside L', 'Tyres - Outside L', 'New Damage'];
            var lights = ['Lights-Left Head Light LOW BEAM','Lights-Right Head Light LOW BEAM','Lights-Left Head Light HIGH BEAM','Lights-Right Head Light HIGH BEAM',
                'Lights-Left Tail Light','Lights-Right Tail Light','Lights-Left Brake Light','Lights-Right Brake Light','Lights-Left Indicator Forward','Lights-Right Indicator Forward',
                'Lights-Left Indicator Rear','Lights-Right Indicator Rear','Lights-Reverse Buzzer'];
            $scope.fields.lights = lights.map(item => { return { name: item, value: null }; });

            for (var i = 0; i <= 12; i++) {
                $scope.fields.inspections[i] = { index: i, name: inspectionNames[i], photos: [], audioNotes: [], description: null, rating: 0, value: null, defaultRating: 0 };
                $scope.fields.inspections[i].rateSettings = $scope.ratingsSettings[i];
                if (i < 4)
                    $scope.fields.inspections[i].inputType = TruckAuditInspectionItemInputType.Text;// 1;
                else if (i < 12)
                    $scope.fields.inspections[i].inputType = TruckAuditInspectionItemInputType.Star;// 2;
            }
            //inspection for new damage
            $scope.fields.inspections[12] = { index: 12, name: inspectionNames[12], inputType: 0, photos: [], audioNotes: [], description: null, rating: 0, value: null, defaultRating: 0 };
            
            $scope.getDaysUntilExp($scope.fields.regoExpiry, 'daysUntilRegoExp');
            $scope.getDaysUntilExp($scope.fields.cofExpiry, 'daysUntilCOFExp');
            $scope.getKmsUntilServiceDue();
            $scope.getRucsRemaining();


            if (!$scope.fields.inspections)
                $scope.fields.inspections = [];
            TruckAuditSvc.currentTruck = $scope.fields;

            $scope.fields.oilLevelDefaultRating = $scope.fields.oilLevel;
            $scope.fields.waterLevelDefaultRating = $scope.fields.waterLevel;
            $scope.fields.dieselLevelDefaultRating = $scope.fields.dieselLevel;
            $scope.fields.oilLevelDetails = angular.copy(FluidLevelVw);            
            $scope.fields.oilLevelDetails.index = -1;
            $scope.fields.oilLevelDetails.name = 'Oil';
            $scope.fields.waterLevelDetails = angular.copy(FluidLevelVw);
            $scope.fields.waterLevelDetails.index = -2;
            $scope.fields.waterLevelDetails.name = 'Water';
            $scope.fields.dieselLevelDetails = angular.copy(FluidLevelVw);
            $scope.fields.dieselLevelDetails.index = -3;
            $scope.fields.dieselLevelDetails.name = 'Diesel';

            //reset if no inspections
            if ($scope.fields.inspections.length == 0) {
                for (var i = 0; i < $scope.ratingsSettings.length; i++) {
                    $scope.ratingsSettings[i].rating = 0;
                }
            }

            $scope.items.selectedTruck = truck;
            TruckAuditSvc.audit = $scope.fields;
            $ionicLoading.hide();
        }, function (error) {
            console.log(error);
            $ionicLoading.hide();
            console.log('[246]');
            alert('error');
        });
    }

    function getLastAudit(truck) {
        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>'});
        TruckAuditSvc.getLastAudit(truck.id).then(function (audit) {
            if (audit) {
                $scope.fields = audit;                
                //$scope.$broadcast('angucomplete-alt:changeInput', params.target, $scope.fields[params.fieldValue]);
                var auditDate = moment($scope.fields.lastAudit);
                $scope.fields.truckRego = truck.truckRego;
                $scope.fields.truckNumber = truck.truckNumber;
                $scope.fields.daysSinceLastAudit = moment().diff(auditDate, 'days');
                $scope.fields.regoExpiry = $scope.fields.regoExpiry ? new Date($scope.fields.regoExpiry) : null;
                $scope.fields.cofExpiry = $scope.fields.cofExpiry ? new Date($scope.fields.cofExpiry) : null;
                $scope.getDaysUntilExp($scope.fields.regoExpiry, 'daysUntilRegoExp');
                $scope.getDaysUntilExp($scope.fields.cofExpiry, 'daysUntilCOFExp');
                $scope.getKmsUntilServiceDue();
                $scope.getRucsRemaining();
                $scope.auditIsReadonly = true;
                $scope.isReadonly = true;
                $scope.fields.oilLevelDetails.index = -1;
                $scope.fields.waterLevelDetails.index = -2;
                $scope.fields.dieselLevelDetails.index = -3;
                angular.forEach($scope.fields.lights, function(elem, index){
                    if(elem.value != null)
                        elem.value = elem.value.toString();
                });

                $scope.fields.inspections.forEach((item, index) => {
                    item.index = index;
                    item.defaultRating = item.rating;
                });
            }
            else {
                $scope.items.selectedTruck = null;
                //ons.notification.confirm({title: 'Site Pro', message: 'Last audit not found.'});
                $ionicPopup.alert({title: 'Site Pro', template: 'Last audit not found.'})
            }
            $ionicLoading.hide();
        }, function (error) {
            console.log(error);
            $scope.items.selectedTruck = null;
            $ionicLoading.hide();            
            //ons.notification.alert({title: 'Site Pro', message: 'Unable to access server.'});
            $ionicPopup.alert({title: 'Site Pro', template: 'Unable to access server.'});
        });
    }

    function getLastAuditOLD(truck, params) {
        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>'});
        TruckAuditSvc.getLastAudit(truck.id).then(function (audit) {
            if (audit) {
                $scope.fields = audit;                
                $scope.$broadcast('angucomplete-alt:changeInput', params.target, $scope.fields[params.fieldValue]);
                var auditDate = moment($scope.fields.lastAudit);
                $scope.fields.daysSinceLastAudit = moment().diff(auditDate, 'days');
                $scope.fields.regoExpiry = $scope.fields.regoExpiry ? new Date($scope.fields.regoExpiry) : null;
                $scope.fields.cofExpiry = $scope.fields.cofExpiry ? new Date($scope.fields.cofExpiry) : null;
                $scope.getDaysUntilExp($scope.fields.regoExpiry, 'daysUntilRegoExp');
                $scope.getDaysUntilExp($scope.fields.cofExpiry, 'daysUntilCOFExp');
                $scope.getKmsUntilServiceDue();
                $scope.getRucsRemaining();
                $scope.auditIsReadonly = true;
                $scope.isReadonly = true;
                $scope.fields.oilLevelDetails.index = -1;
                $scope.fields.waterLevelDetails.index = -2;
                $scope.fields.dieselLevelDetails.index = -3;
                angular.forEach($scope.fields.lights, function(elem, index){
                    if(elem.value != null)
                        elem.value = elem.value.toString();
                });

                $scope.fields.inspections.forEach((item, index) => {
                    item.index = index;
                    item.defaultRating = item.rating;
                });
            }
            else {
                $scope.items.selectedTruck = null;
                //ons.notification.confirm({title: 'Site Pro', message: 'Last audit not found.'});
                $ionicPopup.alert({title: 'Site Pro', template: 'Last audit not found.'})
            }
            $ionicLoading.hide();
        }, function (error) {
            console.log(error);
            $scope.items.selectedTruck = null;
            $ionicLoading.hide();            
            //ons.notification.alert({title: 'Site Pro', message: 'Unable to access server.'});
            $ionicPopup.alert({title: 'Site Pro', template: 'Unable to access server.'});
        });
    }    
    $scope.auditIsReadonly = false;
    $scope.closeAudit = function () {
        if($scope.audit){
            $scope.$emit('auditdone', null);
        }
        else{
            $scope.fields = null;
            $scope.items.selectedTruck = null;
            $scope.$broadcast('angucomplete-alt:clearInput', 'regoNoAngucomplete');
            $scope.$broadcast('angucomplete-alt:clearInput', 'truckNoAngucomplete');
            $scope.auditIsReadonly = false;
            $scope.isReadonly = false;       
        }
    };
    $scope.starRatingCallback = function (index, rating) {
        $scope.fields.inspections[index].rating = rating;
    };
    $scope.fluidLevelRatingCallback = function (index, rating) {
        switch (index) {
            case 10:
                $scope.fields.oilLevel = rating;
                break;
            case 11:
                $scope.fields.waterLevel = rating;
                break;
            case 12:
                $scope.fields.dieselLevel = rating;
                break;
        }
    };    
    $scope.fields = {};
    $scope.inspectionDetails = function (inspection, index) {
        if (!$scope.fields.truckId) {
            alert('Please select a truck.');
            return;
        }

        TruckAuditSvc.inspectionDetails = inspection;
        $state.go('app.inspectiondetails', { index: index, inputtype: inspection.inputType, isReadonly: $scope.isReadonly });
    };    
    $scope.done = function () {
        if (!$scope.fields.truckId) {
            alert('Please select a truck.');
            return;
        }
        //TruckAuditSvc.audit = null;
        $scope.$emit('auditdone', $scope.fields);
        //$scope.audit = TruckAuditSvc.audit = null;
        $scope.fields = null;
    };
    $scope.ratingsSettings = [];
    $scope.items = {};
    $scope.truckModal = null;
    $scope.truckSelected = function (value, params) {
        if (value && value.originalObject) {
            var hideSheet = $ionicActionSheet.show({
                buttons: [{ text: 'New Audit' },{ text: 'View Last Audit' }],
                titleText: 'Options',
                cancelText: 'Cancel',
                cancel: function() {
                    $scope.fields = null;
                    $scope.items.selectedTruck = null;
                    var regoNoInput = document.getElementById('regoNoAngucomplete');
                    var elem = angular.element(regoNoInput);
                    angular.element(elem[0]).find('input')[0].value = '';
                    hideSheet();
                },
                buttonClicked: function(index) {
                    var truck = value.originalObject;
                    if (index === 0) {
                        newAudit(truck, params);
                    }
                    else if (index == 1) {
                        getLastAudit(truck, params);
                    }

                    return true;
                }
            });
        }        
    };
    $scope.getDaysUntilExp = function (exp, displayField) {        
        var expMoment = moment(exp);
        var diff = expMoment.diff(moment(), 'days');
        $scope.fields[displayField] = diff < 0 ? 0 : diff;
    };
    $scope.getKmsUntilServiceDue = function () {
        $scope.fields.kmsUntilServiceDue = $scope.fields.nextServiceDue - $scope.fields.currentOdometerReading;
    };
    $scope.getRucsRemaining = function () {
        $scope.fields.rucsRemaining = $scope.fields.rucMaxReading - $scope.fields.currentHuboReading;
    };
    $scope.cancel = function () {
        $scope.fields = null;
        $scope.items.selectedTruck = null;
        $scope.$emit('auditdone', null);
    };
    $scope.oilLevelDetails = function () {
        TruckAuditSvc.audit = $scope.fields;        
        TruckAuditSvc.inspectionDetails = $scope.fields.oilLevelDetails;
        TruckAuditSvc.inspectionDetails.value = $scope.fields.oilLevel;
        $state.go('app.inspectiondetails', { index: $scope.fields.oilLevelDetails.index, inputtype: $scope.fields.oilLevelDetails.inputType, isReadonly: $scope.isReadonly });
    };
    $scope.waterLevelDetails = function () {
        TruckAuditSvc.audit = $scope.fields;
        TruckAuditSvc.inspectionDetails = $scope.fields.waterLevelDetails;
        TruckAuditSvc.inspectionDetails.value = $scope.fields.waterLevel;
        $state.go('app.inspectiondetails', { index: $scope.fields.waterLevelDetails.index, inputtype: $scope.fields.waterLevelDetails.inputType, isReadonly: $scope.isReadonly });
    };
    $scope.dieselLevelDetails = function () {
        TruckAuditSvc.audit = $scope.fields;
        TruckAuditSvc.inspectionDetails = $scope.fields.dieselLevelDetails;
        TruckAuditSvc.inspectionDetails.value = $scope.fields.dieselLevel;
        $state.go('app.inspectiondetails', { index: $scope.fields.dieselLevelDetails.index, inputtype: $scope.fields.dieselLevelDetails.inputType, isReadonly: $scope.isReadonly });
    };

    function refreshTruckData(){
        let defer = $q.defer();
        //$ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>'});
        TrucksSvc.refreshData().then(function(trucks){
            $scope.trucks = trucks;
            //update truck data if there is ongoing audit
            if($scope.fields && $scope.fields.truckId){
                let currentTruck = $linq.Enumerable().From($scope.trucks).FirstOrDefault(null, 'f => f.id == "' + $scope.fields.truckId + '"');
                console.log('[412]');
                console.log(currentTruck);
                if(currentTruck != null){
                    $scope.fields.cofExpiry = currentTruck.cofExpiry ? new Date(currentTruck.cofExpiry) : null;
                    $scope.fields.regoExpiry = currentTruck.regoExpiry ? new Date(currentTruck.regoExpiry) : null;
                    $scope.fields.rucMaxReading = currentTruck.rucMaxReading;        
                    $scope.fields.nextServiceDue = currentTruck.nextServiceDue;
                    $scope.getDaysUntilExp($scope.fields.regoExpiry, 'daysUntilRegoExp');
                    $scope.getDaysUntilExp($scope.fields.cofExpiry, 'daysUntilCOFExp');
                    $scope.getKmsUntilServiceDue();
                    $scope.getRucsRemaining();
                }
            }
            //$ionicLoading.hide();
            defer.resolve(true);
        }, function(){
            defer.reject()
            //$ionicLoading.hide();
            //$ionicPopup.alert({title: 'Site Pro', template: 'Unable to refresh truck data.'});
        });

        return defer.promise;
    }

    $scope.showOptions = function(){
        let hideSheet = $ionicActionSheet.show({
            buttons: [{ text: 'Refresh Trucks Data' }],
            titleText: 'Options',
            cancelText: 'Cancel',
            cancel: function() {                
                hideSheet();
            },
            buttonClicked: function(index) {
                if (index === 0) {
                    hideSheet();
                    $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>'});
                    refreshTruckData().then(function(){}).catch(function(){
                        $ionicPopup.alert({title: 'Site Pro', template: 'Unable to refresh trucks data.'});
                    }).finally(function(){
                        $ionicLoading.hide();
                    });
                }

                return true;
            }
        });
    };

    $scope.chooseTruck = function(){
        ModalFactory.chooseTruck().then(function(truckSelected){
            console.log('[460]');
            console.log(truckSelected);
            if(truckSelected){
                var hideSheet = $ionicActionSheet.show({
                    buttons: [{ text: 'New Audit' },{ text: 'View Last Audit' }],
                    titleText: 'Options',
                    cancelText: 'Cancel',
                    cancel: function() {
                        $scope.fields = null;
                        $scope.items.selectedTruck = null;
                        /*
                        var regoNoInput = document.getElementById('regoNoAngucomplete');
                        var elem = angular.element(regoNoInput);
                        angular.element(elem[0]).find('input')[0].value = '';
                        */
                        hideSheet();
                    },
                    buttonClicked: function(index) {
                        let truck = truckSelected;//value.originalObject;
                        if (index === 0) {
                            newAudit(truck);
                        }
                        else if (index == 1) {
                            getLastAudit(truck);
                        }
    
                        return true;
                    }
                });                
            }
        });
    };
}]);
truckAuditMod.controller('InspectionDetailsCtrlr', ['$scope', '$state', '$ionicPlatform', '$cordovaCamera', '$cordovaCapture', '$cordovaMedia', '$cordovaFile', 'TruckAuditSvc', '$ionicPopup', '$ionicLoading', '$q', 'VoiceToTextSvc', '$stateParams', 'PhotoSvc', function ($scope, $state, $ionicPlatform, $cordovaCamera, $cordovaCapture, $cordovaMedia, $cordovaFile, TruckAuditSvc, $ionicPopup, $ionicLoading, $q, VoiceToTextSvc, $stateParams, PhotoSvc) {
    $scope.$on('$ionicView.enter', function(){
        $scope.fields = TruckAuditSvc.inspectionDetails;
        $scope.isReadonly = $stateParams.isReadonly;
    });
    $scope.getItemName = function(name){
        if(name && name.startsWith('Lights'))
            return name.substr(7);

        return name;
    };
    function getItemNameX(id) {
        switch (id) {
            case '0':
                return 'Fire Ext';
            case '1':
                return 'First Aid';
            case '2':
                return 'Harness';
            case '3':
                return 'Lanyard';
            case '4':
                return 'Tyres - Front Left';
            case '5':
                return 'Tyres - Front Right';
            case '6':
                return 'Tyres - Rear Left';
            case '7':
                return 'Tyres - Rear Right';
            case '8':
                return 'Tyres - Inside L';
            case '9':
                return 'Tyres - Outside L';
            case '10':
                return 'New Damage?';
            default:
        }
    }
        
    $scope.fields = {};
    $scope.starRatingCallback = function (index, rating) {
        if($scope.fields && $scope.fields.rating)
            $scope.fields.rating = rating;
    };
    $scope.back = function () {
        if(TruckAuditSvc.inspectionDetails.name === 'Oil')
            TruckAuditSvc.audit.oilLevel = TruckAuditSvc.inspectionDetails.value;
        if(TruckAuditSvc.inspectionDetails.name === 'Water')
            TruckAuditSvc.audit.waterLevel = TruckAuditSvc.inspectionDetails.value;
        if(TruckAuditSvc.inspectionDetails.name === 'Diesel')
            TruckAuditSvc.audit.dieselLevel = TruckAuditSvc.inspectionDetails.value;
        TruckAuditSvc.inspectionDetails = null;
        TruckAuditSvc.isReadonly = $scope.isReadonly;
        $scope.fields = null;
        
        $state.go('app.truckaudit');
    };
    $scope.rowPhotos = [];
    $scope.photos = [];
    $scope.audioNotes = [];
    $scope.getPhoto = function () {
        PhotoSvc.getPhoto().then(function (photo) {
            console.log(photo);
            $scope.fields.photos.push(photo.path);
        }, function(error){
            alert('Something went wrong while getting a photo.');
        });        
    };
    $scope.removePhoto = function (index) {
        $scope.fields.photos.splice(index, 1);
    };
    $scope.getAudio = function () {
        var options = { limit: 1 };
        navigator.device.audiorecorder.recordAudio(function (audio) {
            //create new file name
            //move file to app directory with new name
            //save only name
            let audioData = JSON.parse(audio);
            let fullPath = 'file://' + audioData.full_path;
            console.log(fullPath);
            window.resolveLocalFileSystemURL(fullPath, function (resolvedUrl) {
                console.log(resolvedUrl);
                var newFilename = (new ObjectId()).toString() + '.' + fullPath.split('.').pop();
                $cordovaFile.moveFile(getDirectory(fullPath), audioData.file_name, cordova.file.dataDirectory, newFilename).then(function (newAudio) {
                    console.log(newAudio);
                    $scope.fields.audioNotes.push(newAudio.nativeURL);
                }, function (error) {
                    if (error.code == FileError.ENCODING_ERR)
                        alert('ENCODING_ERR');
                    console.log(error);
                });
            }, function(error){
                console.log(error);
            });                    
        }, function (error) {
            console.log(error);
            alert(getCaptureError(error));
        });
    };
    var currentAudio = null;
    $scope.playAudio = function (audio) {
        var audioPath = audio.replace('file://', '');
        currentAudio = $cordovaMedia.newMedia(audioPath);
        currentAudio.play();
    };
    $scope.stopAudio = function (audio) {
        currentAudio.stop();
    };
    $scope.removeAudio = function (index) {
        $scope.fields.audioNotes.splice(index, 1);
    };    

    $scope.startVoiceToText = function () {        
        VoiceToTextSvc.Capture().then(function (audit) {
            if ($scope.fields.description == null)
                $scope.fields.description = '';
            if ($scope.fields.description.length > 0)
                $scope.fields.description += ' ' + audit[0];
            else
                $scope.fields.description += audit[0];
        }, function (error) {
            console.log(error);
        });        
    };
}]);



truckAuditMod.factory('TruckReportSvc', ['$state', '$rootScope', '$q', 'TrucksSvc', function ($state, $rootScope, $q, TrucksSvc) {
    let service = {};
    let trucks = null;

    service.start = function (trucks, jobName, jobid,  audit, isReadonly = false) {
        let defer = $q.defer();
        service.jobid = jobid;
        service.jobName = jobName;
        service.trucks = trucks;
        service.audit = audit;
        //service.isReadonly = isReadonly;
        $rootScope.$on('TruckCheckdone', function (event, audit) {
            defer.resolve(audit);
        });
        $state.go('app.truckreport');
        return defer.promise;
    };
    return service;
}]);
truckAuditMod.controller('TruckReportCtrlr', ['$scope', '$ionicHistory', '$rootScope', 'DBSvc', '$state', '$ionicPopup', '$ionicActionSheet', 'TruckReportSvc', '$ionicLoading', '$linq', 'TrucksSvc', 'ModalFactory', '$q', 'JobsSvc', function ($scope, $ionicHistory, $rootScope, DBSvc, $state, $ionicPopup, $ionicActionSheet, TruckReportSvc, $ionicLoading, $linq, TrucksSvc, ModalFactory, $q, JobsSvc) {

    $scope.$on('$ionicView.beforeEnter', function () {
        console.log('$ionicView.beforeEnter');
        //refreshTruckData();
    });
    $scope.fields = {};
    $scope.$on('$ionicView.enter', function () {
        console.log('$ionicView.enter');

        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>' });

        refreshTruckData().then(function () {
        }).catch(function () {
            console.log('catch error');
            $ionicPopup.alert({ title: 'Site Pro', template: 'Unable to refresh truck data.' });
        }).finally(function () {
            console.log('finally');
            $scope.audit = TruckReportSvc.audit;
            $scope.trucks = TruckReportSvc.trucks;
            $scope.jobid = TruckReportSvc.jobid;
            
            //$scope.audit = TruckReportSvc.truckreports;
            if ($scope.audit) {
                $scope.fields = $scope.audit;
                $scope.fields.regoExpiry = $scope.fields.regoExpiry ? new Date($scope.fields.regoExpiry) : null;
                $scope.fields.cofExpiry = $scope.fields.cofExpiry ? new Date($scope.fields.cofExpiry) : null;
                $scope.fields.hubExpiry = $scope.fields.hubExpiry ? new Date($scope.fields.hubExpiry) : null;
                $scope.getDaysUntilExp($scope.fields.regoExpiry, 'daysUntilRegoExp');
                $scope.getDaysUntilExp($scope.fields.cofExpiry, 'daysUntilCOFExp');
                $scope.getDaysUntilExp($scope.fields.hubExpiry, 'daysUntilCOFExp');
                let truck = $linq.Enumerable().From($scope.trucks).FirstOrDefault(null, 'f => f.id == "' + $scope.audit.truckId + '"');
                $scope.fields.truckId = truck.id;
                $scope.fields.truckRego = truck.truckRego;
                $scope.fields.truckNumber = truck.truckNumber;
                $scope.isReadonly = true;
                //$scope.items.selectedTruck = truck;
    
                //$scope.$broadcast('angucomplete-alt:changeInput', 'regoNoAngucomplete', $scope.fields['truckRego']);            
            }
            else {
                $scope.chooseTruck();
            }
            $ionicLoading.hide();
        });

        
    });

    $scope.getDaysUntilExp = function (exp, displayField) {
        var expMoment = moment(exp);
        var diff = expMoment.diff(moment(), 'days');
        $scope.fields[displayField] = diff < 0 ? 0 : diff;
    };
    $scope.cancel = function () {
        $scope.fields = null;
        //$scope.items.selectedTruck = null;
        $scope.$emit('TruckCheckdone', null);
        $state.go('app.jobdetail', { id: $scope.jobid });
    };
    $scope.closeAudit = function () {
        if ($scope.audit) {
            $scope.$emit('TruckCheckdone', null);
            $state.go('app.jobdetail', { id: $scope.audit.jobId });
        }
        else {
            $scope.fields = null;
            $scope.items.selectedTruck = null;
            $scope.isReadonly = false;
        }
    };

    $scope.done = function () {
        if (!$scope.fields.truckId) {
            alert('Please select a truck.');
            return;
        }
        $scope.$emit('TruckCheckdone', $scope.fields);
       // $scope.audit = TruckReportSvc.audit = null;
        $scope.fields = null;
    };
    function refreshTruckData() {
        let defer = $q.defer();
        //$ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>'});
        TrucksSvc.refreshData().then(function (trucks) {
            $scope.trucks = trucks;
            if ($scope.fields && $scope.fields.truckId) {
                let currentTruck = $linq.Enumerable().From($scope.trucks).FirstOrDefault(null, 'f => f.id == "' + $scope.fields.truckId + '"');
                console.log(currentTruck);
                if (currentTruck != null) {
                    $scope.fields.cofExpiry = currentTruck.cofExpiry ? new Date(currentTruck.cofExpiry) : null;
                    $scope.fields.regoExpiry = currentTruck.regoExpiry ? new Date(currentTruck.regoExpiry) : null;
                    $scope.fields.rucMaxReading = currentTruck.rucMaxReading;
                    $scope.fields.nextServiceDue = currentTruck.nextServiceDue;
                    $scope.getDaysUntilExp($scope.fields.regoExpiry, 'daysUntilRegoExp');
                    $scope.getDaysUntilExp($scope.fields.cofExpiry, 'daysUntilCOFExp');
                    $scope.getKmsUntilServiceDue();
                    $scope.getRucsRemaining();
                }
            }
            //$ionicLoading.hide();
            defer.resolve(true);
        }, function () {
            defer.reject()
            //$ionicLoading.hide();
            //$ionicPopup.alert({title: 'Site Pro', template: 'Unable to refresh truck data.'});
        });

        return defer.promise;
    }
    $scope.chooseTruck = function () {
        $scope.fields = {
            isCamera: false,
            isReverseCamera: false,
            HubExpiry: null,
            HuboReading: 0,
            CofExpiry: null,
            RegoExpiry: null,
            isAlarms: false,
            isAssistDevices: false,
            isEngineoilwater: false,
            isTyres: false,
            isFireExtinguisher: false,
            isFirstAidKit: false,
            isYardbroom: false,
            isPailofsoapywater_brush: false,
            isAmber: false,
            isAlllights: false,
            isTruckMirrors: false,
            isWheel: false,
            isWaterCoolerFull: false,
            isNotes: null,
};
        ModalFactory.chooseTruck().then(function (truckSelected) {
            console.log(truckSelected);
            if (truckSelected) {
                $scope.fields.truckId = truckSelected.id;
                $scope.fields.truckRego = truckSelected.truckRego;
                $scope.fields.truckNumber = truckSelected.truckNumber;
                $scope.fields.cofExpiry = truckSelected.cofExpiry ? new Date(truckSelected.cofExpiry) : null;
                $scope.fields.regoExpiry = truckSelected.regoExpiry ? new Date(truckSelected.regoExpiry) : null;
                $scope.fields.rucMaxReading = truckSelected.rucMaxReading;
               // $scope.fields.rucMaxReading = truckSelected.rucMaxReading;
                $scope.fields.currentHuboReading = truckSelected.currentHuboReading;
                $scope.getDaysUntilExp($scope.fields.regoExpiry, 'daysUntilRegoExp');
                $scope.getDaysUntilExp($scope.fields.cofExpiry, 'daysUntilCOFExp');             
            }
        });
    };
}]);