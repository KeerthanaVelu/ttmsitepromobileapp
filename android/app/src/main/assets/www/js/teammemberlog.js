﻿mainApp.controller('TeamMemberLogCtrlr', ['$scope', '$state', '$linq', 'JobsSvc', '$q', '$ionicLoading', 'WebApiQSvc', function ($scope, $state, $linq, JobsSvc, $q, $ionicLoading, WebApiQSvc) {
    $scope.$on('$ionicView.enter', function () {
        setLogStates().then(function () {
            if ($scope.HasStarted()) {
                $scope.Started = true;
                $scope.startStopColor = { color: "#ef473a" };
            }
            else
                $scope.startStopColor = { color: "#33cd5f" };

            if ($scope.HasPaused()) {
                $scope.Paused = true;
                $scope.pauseResumeColor = {color: "#ef473a"};
            }

            $ionicLoading.hide();
        }, function (error) {
            alert(error);
        });
    });
    $scope.Started = false;
    $scope.Paused = false;
    $scope.Member = JobsSvc.job.teamMember;
    $scope.Done = function () {
        $state.go('team');
    };
    $scope.Cancel = function () {
        $state.go('team');
    };
    $scope.GetTimeLogType = function (type) {
        return TimeLogType[type];
    };
    $scope.Start = function () {
        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>' });
        var timelog = { isTeamMemberTimelog: true, jobId: JobsSvc.job.id, staffMemberId: $scope.Member.id, timeLogType: 0, time: new Date() };
        $scope.Member.timeLogs.push(timelog);
        WebApiQSvc.Send(timelog);
        $scope.Started = true;
        $scope.startStopColor = { color: "#ef473a" };
        setLogStates().then(function () {
            $ionicLoading.hide();
        }, function (error) {
            $ionicLoading.hide();
            alert(error);            
        });
    };
    $scope.Stop = function () {
        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>' });
        var timelog = { isTeamMemberTimelog: true, jobId: JobsSvc.job.id, staffMemberId: $scope.Member.id, timeLogType: 3, time: new Date() };    
        $scope.Member.timeLogs.push(timelog);
        WebApiQSvc.Send(timelog);
        $scope.Started = false;
        $scope.startStopColor = { color: "#33cd5f" };
        setLogStates().then(function () {
            $ionicLoading.hide();
        }, function (error) {
            $ionicLoading.hide();
            alert(error);
        });
    };
    $scope.Pause = function () {
        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>' });
        var timelog = { isTeamMemberTimelog: true, jobId: JobsSvc.job.id, staffMemberId: $scope.Member.id, timeLogType: 1, time: new Date() };
        $scope.Member.timeLogs.push(timelog);
        WebApiQSvc.Send(timelog);
        $scope.Paused = true;
        $scope.pauseResumeColor = { color: "#ef473a" };
        $ionicLoading.show({ template: '<ion-spinner></ion-spinner>' });
        setLogStates().then(function () {
            $ionicLoading.hide();
        }, function (error) {
            $ionicLoading.hide();
            alert(error);
        });
    };
    $scope.Resume = function () {
        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>' });
        var timelog = { isTeamMemberTimelog: true, jobId: JobsSvc.job.id, staffMemberId: $scope.Member.id, timeLogType: 2, time: new Date() };
        //JobsSvc.job.teamMember.timeLogs.push(timelog);//push({ timeLogType: 2, time: new Date() });
        $scope.Member.timeLogs.push(timelog);
        WebApiQSvc.Send(timelog);
        $scope.Paused = false;
        $scope.pauseResumeColor = {};
        setLogStates().then(function () {
            $ionicLoading.hide();
        }, function (error) {
            $ionicLoading.hide();
            alert(error);
        });
    };
    $scope.HasPaused = function () {
        var lastTimeLog = $linq.Enumerable().From($scope.Member.timeLogs).LastOrDefault(-1);
        return lastTimeLog != -1 ? lastTimeLog.timeLogType == 1 : false;
    };
    $scope.HasStarted = function () {
        var lastTimeLog = $linq.Enumerable().From($scope.Member.timeLogs).LastOrDefault(-1);
        return lastTimeLog != -1 ? lastTimeLog.timeLogType != 3 : false;
    };

    function setLogStates() {
        var defer = $q.defer();

        try {
            var workTimeCount = 0, breakTimeCount = 0;
            var workTimeStart, workTimeEnd, breakTimeStart, breakTimeEnd;
            if ($scope.Member.timeLogs != null) {
                for (var i = 0; i < $scope.Member.timeLogs.length; i++) {
                    var timeLog = $scope.Member.timeLogs[i];
                    switch (timeLog.timeLogType) {
                        case 0:
                            workTimeStart = timeLog.time;
                            break;
                        case 1:
                            breakTimeStart = timeLog.time;
                            break;
                        case 2:
                            breakTimeEnd = timeLog.time;
                            break;
                        case 3:
                            workTimeEnd = timeLog.time;
                            break;
                    }

                    if (workTimeStart != null && workTimeEnd != null) {
                        var mmtWorkTimeStart = moment(workTimeStart);
                        var mmtWorkTimeEnd = moment(workTimeEnd);
                        var timeDiff = mmtWorkTimeEnd.diff(mmtWorkTimeStart, 'minutes');
                        workTimeCount += timeDiff;
                        workTimeStart = null;
                        workTimeEnd = null;
                    }

                    if (breakTimeStart != null && breakTimeEnd != null) {
                        var mmtBreakTimeStart = moment(breakTimeStart);
                        var mmtBreakTimeEnd = moment(breakTimeEnd);
                        var timeDiff = mmtBreakTimeEnd.diff(mmtBreakTimeStart, 'minutes');
                        breakTimeCount += timeDiff;
                        breakTimeStart = null;
                        breakTimeEnd = null;
                    }

                    if (i == $scope.Member.timeLogs.length - 1 && workTimeStart != null) {
                        var mmtWorkTimeStart = moment(workTimeStart);
                        var timeDiff = moment().diff(mmtWorkTimeStart, 'minutes');
                        workTimeCount += timeDiff;
                        workTimeStart = null;
                        $scope.Started = true;
                    }

                    if (i == $scope.Member.timeLogs.length - 1 && timeLog.TimeLogType == 1) {
                        var mmtBreakTimeStart = moment(breakTimeStart);
                        var timeDiff = moment().diff(mmtBreakTimeStart, 'minutes');
                        breakTimeCount += timeDiff;

                        var mmtLastWorkTimeStart = moment($scope.Member.timeLogs[i - 1].Time);
                        var lastWorkTimeDiff = moment().diff(mmtLastWorkTimeStart, 'minutes');
                        workTimeCount += lastWorkTimeDiff;
                        breakTimeStart = null;
                        $scope.Paused = true;
                    }
                }
            }

            var totalTimeCount = workTimeCount;
            workTimeCount -= breakTimeCount;
            $scope.WorkHours = workTimeCount == 0 ? 0 : moment.duration(workTimeCount, 'minutes').hours();
            $scope.WorkMins = workTimeCount == 0 ? 0 : moment.duration(workTimeCount, 'minutes').minutes();
            $scope.BreakHours = breakTimeCount == 0 ? 0 : moment.duration(breakTimeCount, 'minutes').hours();
            $scope.BreakMins = breakTimeCount == 0 ? 0 : moment.duration(breakTimeCount, 'minutes').minutes();
            $scope.TotalHours = totalTimeCount == 0 ? 0 : moment.duration(totalTimeCount, 'minutes').hours();
            $scope.TotalMins = totalTimeCount == 0 ? 0 : moment.duration(totalTimeCount, 'minutes').minutes();
            JobsSvc.save();

            defer.resolve(true);
        }
        catch (error) {
            defer.reject(error);
        }

        return defer.promise;
    };
}]);