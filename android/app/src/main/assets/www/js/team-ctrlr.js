﻿mainApp.controller('TeamCtrlr', ['$scope', '$state', '$linq', '$filter', 'JobsSvc', 'StaffSvc', 'AccountSvc', '$ionicModal', '$q', '$ionicLoading', '$cordovaDialogs', 'WebApiQSvc', function ($scope, $state, $linq, $filter, JobsSvc, StaffSvc, AccountSvc, $ionicModal, $q, $ionicLoading, $cordovaDialogs, WebApiQSvc) {
    //EVENTS
    $scope.$on('$ionicView.enter', function () {
        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>' });
        $q.when(loadTeam()).then(function (result) {
            $ionicLoading.hide();
        }, function (error) {
            console.log(error);
            $ionicLoading.hide();
            alert('Something went wrong.');
        });        
    });

    $ionicModal.fromTemplateUrl('views/findteammember.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        $scope.modal = modal;
    });
    
    function loadTeam() {
        //console.log(JobsSvc.job.teamMembers);
        $scope.members = [];
        var currentUserId = AccountSvc.getUserId();
        var userInfo = AccountSvc.getUserInfo();
        var openMembers = $linq.Enumerable().From(JobsSvc.job.teamMembers).Where('f => f.endOfShift == null').ToArray();
        var currentUserExist = $linq.Enumerable().From(openMembers).FirstOrDefault(null, 'f => f.staffId == "' + currentUserId + '"');
        if (currentUserExist == null) {
            JobsSvc.job.teamMembers.push({ staffId: currentUserId, timeLogs: [] });
            var teamMembers = { isTeamMembers: true, jobId: JobsSvc.job.id, team: JobsSvc.job.teamMembers, isCurrentUser: true };
            WebApiQSvc.Send(teamMembers);
        }
        var staff = StaffSvc.get();
        openMembers = $linq.Enumerable().From(JobsSvc.job.teamMembers).Where('f => f.endOfShift == null').ToArray();
        var memberIds = $linq.Enumerable().From(openMembers).Select('val, i => val.staffId').ToArray();
        var staffMembers = StaffSvc.getByIds(memberIds);
        for (var i = 0; i < openMembers.length; i++) {
            var teamMember = openMembers[i];
            var staffMember = $linq.Enumerable().From(staffMembers).FirstOrDefault(null, 'f => f.id == "' + teamMember.staffId + '"');
            var branch;
            if (staffMember) {
                branch = $linq.Enumerable().From(AccountSvc.getBranches()).FirstOrDefault(null, 'f => f.id == "' + staffMember.branchId + '"');
                var lastEndTime = $linq.Enumerable().From(teamMember.timeLogs).LastOrDefault(null, 'f => f.timeLogType == 3');
                $scope.members.push({ isCurrentUser: (staffMember.id == currentUserId ? true : false), id: staffMember.id, fullName: staffMember.fullName, branchName: (branch != null ? branch.branch : ''), photo: staffMember.photo, timeLogs: teamMember.timeLogs, started: lastEndTime == null });
                //$scope.members.push({ isCurrentUser: false, id: staffMember.id, fullName: staffMember.fullName, branchName: (branch != null ? branch.branch : ''), photo: staffMember.photo, timeLogs: teamMember.timeLogs, started: lastEndTime == null });
            }
            else {
                if (teamMember.staffId == currentUserId) {
                    $scope.members.push({ isCurrentUser: false, id: currentUserId, fullName: userInfo.fullName, timeLogs: teamMember.timeLogs });
                }
                //$scope.members.push({ isCurrentUser: false, id: currentUserId, fullName: userInfo.fullName, timeLogs: teamMember.timeLogs });
            }
        }

        return true;
    }
    
    function updateTimeLog() {
        var defer = $q.defer();
        if (JobsSvc.job.teamChanged) {
            var emptyStaff = $linq.Enumerable().From(JobsSvc.job.teamMembers).Where('w => w.staffId == null').ToArray();
            for (var i = 0; i < emptyStaff.length; i++) {
                var index = JobsSvc.job.teamMembers.indexOf(emptyStaff[i]);
                JobsSvc.job.teamMembers.splice(index, 1);
            }
            var teamMembers = { isTeamMembers: true, jobId: JobsSvc.job.id, team: JobsSvc.job.teamMembers };
            JobsSvc.save().then(function () {
                WebApiQSvc.Send(teamMembers);
                defer.resolve(true);
            }, function (error) {
                console.log(error);
                defer.reject(error);
            });
        }
        else
            defer.resolve(true);

        return defer.promise;
    }
    
    function startAllTime() {
        var defer = $q.defer();

        var noStartLogs = $linq.Enumerable().From(JobsSvc.job.teamMembers).Where(function (member) {
            var hasStarted = $linq.Enumerable().From(member.timeLogs).LastOrDefault(null);
            if (hasStarted == null || (hasStarted != null && hasStarted.timeLogType != TimeLogTypeConst.StartTime.value))
                return true;
            return false;
        }).ToArray();
        angular.forEach(noStartLogs, function (member, index) {            
            var timelog = { isTeamMemberTimelog: true, jobId: JobsSvc.job.id, staffMemberId: member.staffId, timeLogType: 0, time: new Date() };
            member.timeLogs.push(timelog);
            WebApiQSvc.Send(timelog);
        });
        defer.resolve(true);

        return defer.promise;
    }
    function endAllTime() {
        var defer = $q.defer();

        var noEndLogs = $linq.Enumerable().From(JobsSvc.job.teamMembers).Where(function (member) {
            var hasStarted = $linq.Enumerable().From(member.timeLogs).FirstOrDefault(null, 'f => f.timeLogType == ' + TimeLogTypeConst.EndTime.value);
            //var hasStarted = $linq.Enumerable().From(member.timeLogs).FirstOrDefault(null, 'f => f.timeLogType == 3');
            if (hasStarted == null || (hasStarted != null && hasStarted.timeLogType != TimeLogTypeConst.EndTime.value))
                return true;
            return false;
        }).ToArray();
        angular.forEach(noEndLogs, function (member, index) {            
            var timelog = { isTeamMemberTimelog: true, jobId: JobsSvc.job.id, staffMemberId: member.staffId, timeLogType: 3, time: new Date() };
            member.timeLogs.push(timelog);
            WebApiQSvc.Send(timelog);
        });
        defer.resolve(true);

        return defer.promise;
    }

    $scope.loadTeam = function () {
        $q.when(loadTeam()).then(function () {
            $ionicLoading.hide();
        }, function (error) {
            console.log(error);
            $ionicLoading.hide();
            alert(error);
        });
    };
    $scope.done = function () {
        updateTimeLog().then(function () {
            $state.go('jobdetails');
        }, function (error) {
            console.log(error);
            alert('Something went wrong.');
            $state.go('jobdetails');
        });
    }
    $scope.editTeam = function () {
        $state.go('teammember');
    };
    $scope.teamMemberLog = function (member) {
        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>' });
        JobsSvc.job.teamMember = member;
        $state.go('teammemberlog');
    };
    $scope.getStartTime = function (log) {
        var startTime = $linq.Enumerable().From(log).LastOrDefault(null, 'f => f.timeLogType == 0');
        return startTime != null ? $filter('date')(startTime.time, 'hh:mm a') : '';
    };
    $scope.getEndTime = function (log) {
        var endTime = $linq.Enumerable().From(log).LastOrDefault(null);
        if(endTime && endTime.timeLogType != 3)
            endTime = null;
        
        return endTime != null ? $filter('date')(endTime.time, 'hh:mm a') : '';
    };
    $scope.find = function () {
        $scope.modal.show();
    };
    $scope.new = function () {
        $state.go('newteammember');
    };
    $scope.hide = function () {
        $scope.modal.hide();
    };
    $scope.remove = function (index) {        
        $cordovaDialogs.confirm('Do you want to remove this team member?', 'Site Pro', ['No', 'Yes'])
        .then(function (buttonIndex) {
            // no button = 0, 'OK' = 1, 'Cancel' = 2
            console.log(buttonIndex);
            if (buttonIndex == 2) {                
                var member = $scope.members[index];
                $scope.members.splice(index, 1);                
                var teamMember = $linq.Enumerable().From(JobsSvc.job.teamMembers).FirstOrDefault(null, 'f => f.staffId == "' + member.id + '"');
                var teamIndex = JobsSvc.job.teamMembers.indexOf(teamMember);
                JobsSvc.job.teamMembers.splice(teamIndex, 1);
                JobsSvc.job.teamChanged = true;                
            }
        });

        return false;
    };
    
    $scope.startAll = function () {
        $ionicLoading.show();
        startAllTime().then(function () {
            JobsSvc.save().then(function () {
                $q.when(loadTeam()).then(function () {
                    $ionicLoading.hide();
                }, function (error) {
                    console.log(error);
                    $ionicLoading.hide();
                    alert('Something went wrong.');
                });                
            }, function (error) {
                console.log(error);
                $ionicLoading.hide();
                alert('Something went wrong.');
            });            
        }, function (error) {
            $ionicLoading.hide();
            alert('Something went wrong.');
        });
    };
    $scope.endAll = function () {
        $ionicLoading.show();
        endAllTime().then(function () {
            JobsSvc.save().then(function () {
                $q.when(loadTeam()).then(function () {
                    $ionicLoading.hide();
                }, function (error) {
                    console.log(error);
                    $ionicLoading.hide();
                    alert('Something went wrong.');
                });
            }, function (error) {
                console.log(error);
                $ionicLoading.hide();
                alert('Something went wrong.');
            });
        }, function (error) {
            $ionicLoading.hide();
            alert('Something went wrong.');
        });
    };
}]);