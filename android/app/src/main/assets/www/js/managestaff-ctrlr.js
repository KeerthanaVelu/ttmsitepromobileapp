﻿mainApp.controller('ManageStaffCtrlr', ['$scope', '$state', '$linq', 'StaffSvc', 'AccountSvc', function ($scope, $state, $linq, StaffSvc, AccountSvc) {
    $scope.$on('$ionicView.enter', function () {
        loadStaff();
    });
    function loadStaff() {
        $scope.staff = StaffSvc.get();
    }
    $scope.search = '';
    $scope.branchId = '';
    $scope.branches = AccountSvc.getBranches();
    $scope.filters = { search: '', branchId: '' };
    $scope.getBranchName = function (member) {
        var branch = $linq.Enumerable().From($scope.branches).FirstOrDefault(null, 'f => f.id == "' + member.branchId + '"');
        return branch != null ? branch.branch : '';
    };
    $scope.edit = function (member) {
        StaffSvc.staffMember = member;
        $state.go('staffform', { mode: 'Edit' });
    };
    $scope.new = function () {
        StaffSvc.staffMember = new Staff();
        $state.go('staffform', { mode: 'New' });
    };
    $scope.close = function () {
        $state.go('app.jobs');
    };
}]);