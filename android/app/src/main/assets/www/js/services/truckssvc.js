mainApp.factory('TrucksSvc', ['$linq', '$resource', 'AccountSvc', 'ApiSvc', '$q', 'DBSvc', function ($linq, $resource, AccountSvc, ApiSvc, $q, DBSvc) {
    var service = {};
    var trucks = [];
    service.init = function (trucks) {
        this.trucks = trucks;
    };
    service.getAll = function(){
        let defer = $q.defer();
        DBSvc.getAllTrucks().then(function(trucks){
            trucks.forEach(truck => {
                let truckNum = parseInt(truck.truckNumber);
                if(truckNum)
                    truck.truckNumberSort = truckNum;
                else
                    truck.truckNumberSort = Number.MAX_VALUE;
            });
            trucks = $linq.Enumerable().From(trucks).OrderBy('o => o.truckNumberSort').ToArray();
            defer.resolve(trucks);
        }, function(error){
            defer.reject(error);
        });

        return defer.promise;
    };
    //#region
    /*
    service.getAll = function () {
        return this.trucks;
    };
    */
    //#endregion

    service.getById = function (id) {
        return $linq.Enumerable().From(this.trucks).FirstOrDefault(null, 'f => f.id == "' + id + '"');
    };
    service.getFromServer = function () {
        var defer = $q.defer();
        
        var authHeader = { 'Authorization': 'Bearer ' + AccountSvc.getAccessToken() };
        var resource = $resource(ApiSvc.trucks, null, {
            'getTrucks': { isArray: true, params: { tenantId: '@tenantId' }, headers: authHeader }
        });
        resource.getTrucks({ tenantId: AccountSvc.getTenantId() }, function (result) {
            console.log(result);
            DBSvc.saveTrucks(result).then(function (saveResult) {
                console.log(saveResult);
                service.init(result);
                //this.trucks = result;
                defer.resolve(true);
            }, function (error) {
                console.log(error);
                defer.reject(error);
            });
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });
        
        return defer.promise;
    };

    service.get = function(id){
        let defer = $q.defer();
        id = objectTypes.truck + id;
        DBSvc.get(id).then(function(truck){
            defer.resolve(truck);
        }, function(error){
            console.log(error);
            if(error.status === 404){
                defer.resolve(null);
                return;
            }

            defer.reject(error);
        });

        return defer.promise;
    };

    return service;
}]);