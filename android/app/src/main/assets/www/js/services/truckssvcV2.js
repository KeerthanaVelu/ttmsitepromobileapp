mainApp.factory('TrucksSvc', ['$linq', '$resource', 'AccountSvc', 'ApiSvc', '$q', 'DBSvc', 'RemoteSyncSvc', 'RemoteUrls', function ($linq, $resource, AccountSvc, ApiSvc, $q, DBSvc, RemoteSyncSvc, RemoteUrls) {
    var service = {};
    var trucks = [];
    service.init = function (trucks) {
        this.trucks = trucks;
    };
    service.getAll = function(){
        let defer = $q.defer();
        DBSvc.getAllTrucks().then(function(trucks){
            trucks.forEach(truck => {
                let truckNum = parseInt(truck.truckNumber);
                if(truckNum)
                    truck.truckNumberSort = truckNum;
                else
                    truck.truckNumberSort = Number.MAX_VALUE;
            });
            trucks = $linq.Enumerable().From(trucks).OrderBy('o => o.truckNumberSort').ToArray();
            console.log('[18]');
            console.log(trucks);
            defer.resolve(trucks);
        }, function(error){
            defer.reject(error);
        });

        return defer.promise;
    };

    //#region
    /*
    service.getAll = function () {
        return this.trucks;
    };
    */
    //#endregion

    service.getById = function (id) {
        return $linq.Enumerable().From(this.trucks).FirstOrDefault(null, 'f => f.id == "' + id + '"');
    };
    service.getFromServer = function () {
        var defer = $q.defer();
        
        var authHeader = { 'Authorization': 'Bearer ' + AccountSvc.getAccessToken() };
        var resource = $resource(ApiSvc.trucks, null, {
            'getTrucks': { isArray: true, params: { tenantId: '@tenantId' }, headers: authHeader }
        });
        resource.getTrucks({ tenantId: AccountSvc.getTenantId() }, function (result) {
            console.log(result);
            DBSvc.saveTrucks(result).then(function (saveResult) {
                console.log(saveResult);
                service.init(result);
                //this.trucks = result;
                defer.resolve(true);
            }, function (error) {
                console.log(error);
                defer.reject(error);
            });
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });
        
        return defer.promise;
    };

    service.get = function(id){
        let defer = $q.defer();
        id = objectTypes.truck + id;
        DBSvc.get(id).then(function(truck){
            defer.resolve(truck);
        }, function(error){
            console.log(error);
            if(error.status === 404){
                defer.resolve(null);
                return;
            }

            defer.reject(error);
        });

        return defer.promise;
    };
    
    service.getLastAudit = function(truckId){
        let defer = $q.defer();
        let authHeader = { 'Authorization': 'Bearer ' + AccountSvc.getAccessTokenV2() };
        let resource = $resource('', null, {'getLastAudit': { url: RemoteUrls.lastTruckAudit, method: 'GET', headers: authHeader, params: { tenantId: '@tenantId', truckId: '@truckId' } }});
        resource.getLastAudit({ tenantId: AccountSvc.getTenantIdV2(), truckId: truckId }).$promise.then(function (result) {
            console.log('[92]');
            console.log(result);
            if(result.hasOwnProperty('id'))
                defer.resolve(result);
            else
                defer.resolve(null);
        }, function (error) {
            console.log('[95]');
            console.log(error);
            defer.resolve(null);
        });
        
        return defer.promise;        
    };
    /*
    service.getLastAudit = function(id){
        return DBSvc.getLastAudit(id);
    };
    */

    service.getAudit = function(id){
        return DBSvc.getTruckAuditV2(id);
    };

    service.saveAudit = function(audit){
        let defer = $q.defer();
        audit.id = (new ObjectId()).toString();
        audit._id = objectTypes.truckaudit + audit.id;
        let createdOn = new Date();
        audit.createdOn = new Date(createdOn.getFullYear(), createdOn.getMonth(), createdOn.getDate(), 0, 0, 0);
        DBSvc.save([audit]).then(function(response){
            RemoteSyncSvc.add(audit).then(function(response){
                defer.resolve(audit.id);
            }, function(error){
                console.log(error);
                defer.reject(error);
            });            
        });

        return defer.promise;
    };
    service.truckreportSave = function (jobId, activity) {
        alert("S");
        console.log(activity, "activity");
        let defer = $q.defer();
        let objJobId = objectTypes.job + jobId;
        DBSvc.get(objJobId).then(function (job) {
            if (job) {
                let token = AccountSvc.getAccessTokenV2();
                let activityData = $linq.Enumerable().From(job.activities).FirstOrDefault(null, 'f => f.id == "' + activity.id + '"');
                console.log(activityData, "activityData");
                activity.activityType = 17;
                //activityData = activity;
                resource = $resource('', null, { 'updateActivity': { url: RemoteUrls.updateActivityUrl, method: 'PUT', headers: { 'Authorization': 'Bearer ' + token }, params: { id: '@id', jobid: '@jobid' } } });
                resource.updateActivity({ jobid: job.rId, id: activityData.rId }, activity).$promise.then(function (response) {
                    defer.resolve(true);
                }, function (error) {
                    console.log(error);
                    defer.reject(error);
                });
                }           
        });

        return defer.promise;
    };
    service.getTruckReportId = function (jobId,Id) {
        let defer = $q.defer();
        let objJobId = objectTypes.job + jobId;
        DBSvc.get(objJobId).then(function (job) {
            if (job) {
                let token = AccountSvc.getAccessTokenV2();
                let activityData = $linq.Enumerable().From(job.activities).FirstOrDefault(null, 'f => f.id == "' + Id + '"');
                console.log(activityData,"activityData")
                resource = $resource('', null, { 'GetJob': { url: RemoteUrls.getJob, method: 'Get', headers: { 'Authorization': 'Bearer ' + token }, params: { id: '@id' } } });
                resource.GetJob({ id: job.rId }).$promise.then(function (response) {
                    console.log(response, "response");
                    let truckReport = $linq.Enumerable().From(response.activities).FirstOrDefault(null, 'f => f.id == "' + activityData.rId + '"')
                        defer.resolve(truckReport);
                }, function (error) {
                    console.log(error);
                    defer.reject(error);
                });
            }
        });

        return defer.promise;
    };
    service.refreshData = function(){
        console.log('[131]');
        let defer = $q.defer();
        let resource = $resource('', {}, {
            'getTruckData': { url: RemoteUrls.trucksDataSeed, method: 'GET', isArray: true, headers: { 'Authorization': 'Bearer ' + AccountSvc.getAccessTokenV2() }, params: { tenantId: '@tenantId' } }
        });

        resource.getTruckData({tenantId: AccountSvc.getTenantIdV2()}).$promise.then(function(trucks){
            console.log('trucks loaded from server');
            console.log(trucks);
            trucks.forEach(truck => {
                truck._id = objectTypes.truck + truck.id;
            });
            //remove all trucks
            DBSvc.clearTrucksData().then(function(cleared){
                console.log('trucks cleared');
                if(cleared){   
                    //save new trucks
                    DBSvc.save(trucks).then(function(saveResponse){
                        console.log('trucks saved');
                        defer.resolve(trucks);
                    });
                }
            }, function(error){
                console.log('[153]');
                console.log(error);
                defer.reject(error);
            });
        }, function(error){
            console.log('[157]');
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    };
    
    return service;
}]);