mainApp.factory('AccountSvc', ['$q', 'ApiSvc', '$resource', '$http', 'DBSvc', 'RemoteUrls',  function ($q, ApiSvc, $resource, $http, DBSvc, RemoteUrls) {
    var resource = $resource(RemoteUrls.loginUrl, {}, {
        'getToken': { method: 'POST', isArray: false, headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }        
    });
    var accountToken = {};
    var service = {};
    var parties = {};
    var siteprodb = null;
    var branches =[];
    /*
    function getParties(tenantId) {
        console.log('getParties(): 625');
        var defer = $q.defer();
        var partiesResource = $resource(ApiSvc.partiesUrl, {}, {
            'getParties': { method: 'GET', isArray: false, headers: { 'Authorization': 'Bearer ' + accountToken.access_token }, url: ApiSvc.partiesUrl, params: { tenantId: '@tenantId' } }
        });        

        partiesResource.getParties({ tenantId: accountToken.tenantId }, null, function (result) {
            RCASvc.set(result.rcas);
            ContractorSvc.set(result.contractors);
            AssetOwnerSvc.set(result.assetOwners);
            StaffSvc.setStaff(result.staff);
            branches = result.branches;
            trucks = result.trucks;
            console.log('638: partiesResource.getParties() success callback');
            defer.resolve(result);
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    }
    */
    /*
    function relogin(email, password) {
        var credentials = "grant_type=password&username=" + email + "&password=" + password;
        var defer = $q.defer();

        $http.post(ApiSvc.loginUrl, credentials, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' }, timeout: 60000 }).then(function (response) {
            //after successful login save token data to db
            console.log('AccountSvc.login');
            //console.log(response);
            accountToken = response.data;
            DBSvc.saveToken(accountToken).then(function (result) {
                defer.resolve(result);
            }, function (error) {
                defer.reject(error);
            });
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    }
    */
    function login(email, password) {
        var credentials = "grant_type=password&username=" + email + "&password=" + password;
        var defer = $q.defer();

        $http.post(RemoteUrls.loginUrl, credentials, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' }, timeout: 60000 }).then(function (response) {
            defer.resolve(response.data);            
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    }

    function updateSignature(userId, signatireData, signatureName) {
        var defer = $q.defer();
        $http({
            method: 'POST',
            url: RemoteUrls.updateSignature,
            headers: { 'Content-Type': undefined },
            data: {
                id: userId,
                signatire: signatireData,
                fileName: signatureName,
            },
            transformRequest: function (data, headersGetter) {
                var formData = new FormData();
                angular.forEach(data, function (value, key) {
                    formData.append(key, value);
                });

                var headers = headersGetter();
                delete headers['Content-Type'];

                return formData;
            }
        }).success(function (data) {

            console.log(data);

        }).error(function (data, status) {
            console.log(data);
            console.log(status);
        });

        return defer.promise;
    }

    service.init = function (accountDetails) {
        /*
        RCASvc.set(accountDetails.parties.rcas);
        ContractorSvc.set(accountDetails.parties.contractors);
        AssetOwnerSvc.set(accountDetails.parties.assetOwners);
        StaffSvc.setStaff(accountDetails.staff);
        accountToken = accountDetails.token;
        branches = accountDetails.parties.branches;
        */        
    };

    service.login = function (email, password) {
        var defer = $q.defer();
        login(email, password).then(function (result) {
            defer.resolve(result);
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    };
    /*
    service.relogin = function (email, password) {
        var defer = $q.defer();
        relogin(email, password).then(function (result) {
            defer.resolve(result);
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    };
    */
    service.getAccessToken = function () {        
        return accountToken.access_token;
    };

    service.getTenantId = function () {
        return accountToken.tenantId;
    };

    service.getBranchId = function () {
        return accountToken.branchId;
    };

    service.getBranches = function () {
        return branches;
    };

    service.getUserId = function () {
        return accountToken.id;
    };

    service.getUserInfo = function () {
        return { fullName: account.fullName, email: account.userName };
    };

    service.forgotPassword = function (email) {
        var resource = $resource('/token', {}, {
            'forgotPassword': { method: 'POST', url: RemoteUrls.forgotPassword + '?email=' + email }
        });
        return resource.forgotPassword().$promise;
    };

    service.hasSeedData = function(){
      return DBSvc.hasSeedData();  
    };

    service.save = function(account){
        return DBSvc.saveAccountDetails(account);
    };

    service.setHasSeedData = function(hasSeedData){
        return DBSvc.setHasSeedData(hasSeedData);
    };

    let account;
    service.getAccessTokenV2 = function(){
        return account.access_token;
    };
    service.getTenantIdV2 = function(){
        return account.tenantId;
    };
    service.getBranchIdV2 = function(){
        return account.branchId;
    };
    service.getUserIdV2 = function(){
        return account.id;
    };
    service.getUserFullName = function(){
        return account.fullName;
    };   

    service.initV2 = function(){
        //console.log('accountsvc.initV2')
        let defer = $q.defer();
        DBSvc.getAccountDetails().then(function(response){
            //console.log(response);
            account = response;
            defer.resolve(true);
        }, function(error){
            console.log(error);
            defer.reject(error);
        });
        return defer.promise;
    };

    service.getBranchesV2 = function(){
        return DBSvc.getTenantBranches();
    };

    service.get = function(){
        return account;
    };

    service.update = function(account){
        return DBSvc.save([account]);
    };

    service.updateSignature = function (userId, signatireData, signatureName) {
        var defer = $q.defer();
        updateSignature(userId, signatireData, signatureName).then(function (result) {
            defer.resolve(result);
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    };

    return service;
}]);