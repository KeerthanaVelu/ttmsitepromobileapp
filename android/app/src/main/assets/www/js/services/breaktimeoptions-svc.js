mainApp.factory('BreakTimeOptions', ['$q', '$ionicPopup', function($q, $ionicPopup){
    let service = {};

    service.getOptions = function(staffName, scope){
        let defer = $q.defer();
        scope.breakTimeOptionMember = staffName;
        scope.breakTimeOption = '30minutes';
        let breakTimeOptions = $ionicPopup.show({
            templateUrl: 'views/breaktimeoptions.html',
            title: 'Break Time',
            scope: scope,
            buttons: [
                { text: 'Cancel' },
                {
                    text: '<b>OK</b>',  
                    type: 'button-positive',
                    onTap: function (e) {
                        if(this.scope.breakTimeOption === "duration" && !this.scope.breakTimeDuration){
                            $ionicPopup.alert({template: 'Please specify duration.'});
                            e.preventDefault();
                        }
                        else
                            return { breakTimeOption: this.scope.breakTimeOption, breakTimeDurationMins: this.scope.breakTimeDuration };
                    }
                }
            ]
        });

        breakTimeOptions.then(function(option){
            let duration = 0;
            let minToMilsecs = 60000;
            let isPaid = false;
            if(option){
                switch(option.breakTimeOption){
                    case '30minutes':
                        duration = 30 * minToMilsecs;
                        break;                    
                    case 'ongoing':
                        duration = 0;
                        break;
                    case '10minutespaid':
                        duration = 10 * minToMilsecs;
                        isPaid = true;
                        break;
                    case 'duration':
                        duration = option.breakTimeDurationMins * minToMilsecs;
                        break;
                }
                defer.resolve({duration: duration, isPaid: isPaid, selected: option.breakTimeOption});                
            }
            else
                defer.resolve(null);
        });

        return defer.promise;
    };
    
    return service;
}]);