mainApp.factory('RemoteSyncHandler', ['$resource', '$q', 'RemoteUrls', 'DBSvc', 'AccountSvc', '$linq', '$cordovaFileTransfer', '$timeout', function ($resource, $q, RemoteUrls, DBSvc, AccountSvc, $linq, $cordovaFileTransfer, $timeout) {
    let service = {};

    function sendJob(job) {
        console.log('remotesynchandler -> sendjob()');
        let token = AccountSvc.getAccessTokenV2();
        let defer = $q.defer();
        let resource = $resource('', null, { 'create': { url: RemoteUrls.jobsUrl, method: 'POST', headers: { 'Authorization': 'Bearer ' + token } } });
        resource.create(job).$promise.then(function (createRes) {
            console.log('[10]');
            console.log(createRes);
            DBSvc.updateJobRemoteId(job.id, createRes.id).then(function (response) {
                //update teammember ids if any
                jobId = objectTypes.job + job.id;
                DBSvc.get(jobId).then(function (jobData) {
                    console.log('[16]');
                    console.log(jobData);
                    for (let i = 0; i < jobData.teamMembers.length; i++) {
                        const tm = jobData.teamMembers[i];
                        var tmDto = createRes.teamMembers.find(f => { return f.staffMemberId === tm.staffId });
                        if (tmDto) {
                            tm.rId = tmDto.id;
                            tm.jobId = job.id;
                            tm.rJobId = createRes.id;
                        }
                    }
                    DBSvc.save([jobData]).then(function () {
                        defer.resolve(true);
                    })
                });
            });
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    }

    function UploadFile(path) {
        var defer = $q.defer();
        let token = AccountSvc.getAccessTokenV2();
        var tokenConfig = { headers: { 'Authorization': 'Bearer ' + token } };
        let tryCnt = 1;
        if (typeof path !== "string")
            defer.reject("path must be string");

        function upload(path) {
            var ft = new FileTransfer();
            var ftOptions = {};
            ftOptions.fileName = path.substr(path.lastIndexOf('/') + 1);
            ftOptions.headers = tokenConfig.headers;
            async.retry({ times: 3, interval: 5000 }, function (retryCallback) {
                //console.log('upload try: ' + tryCnt);
                ft.upload(path, RemoteUrls.mediaFilesUrl, function (result) {
                    //defer.resolve(result);
                    tryCnt++;
                    retryCallback(null, null);
                }, function (error) {
                    //defer.reject(error);
                    tryCnt++;
                    retryCallback(error, null);
                }, ftOptions);
            }).catch(function (error) {
                console.log(error);
            }).finally(function () {
                defer.resolve(true);
            });

            // ft.upload(path, RemoteUrls.mediaFilesUrl, function (result) {
            //     defer.resolve(result);
            // }, function (error) {
            //     defer.reject(error);                
            // }, ftOptions);
        }
        $timeout(function () {
            upload(path);
        }, 10000, false);
        return defer.promise;
    }

    function updateJob(job) {
        console.log('remotesynchandler -> sendjob()');
        let token = AccountSvc.getAccessTokenV2();
        let defer = $q.defer();
        let resource = $resource('', null, { 'update': { url: RemoteUrls.updateJobUrl, method: 'PUT', headers: { 'Authorization': 'Bearer ' + token }, params: { id: '@id' } } });
        job.id = job.rId;

        resource.update({ id: job.rId }, job).$promise.then(function (response) {
            console.log(response);
            //if general notes photos/audio exist then upload
            let genNotesMedia = [];
            if (job.generalNotes && job.generalNotes.photos && job.generalNotes.photos.length > 0)
                genNotesMedia = genNotesMedia.concat(job.generalNotes.photos);
            if (job.generalNotes && job.generalNotes.audioNotes && job.generalNotes.audioNotes.length > 0)
                genNotesMedia = genNotesMedia.concat(job.generalNotes.audioNotes);
            async.eachSeries(genNotesMedia, function (media, callback) {
                $timeout(function () {
                    console.log('media upload: ' + new Date());
                    let token = AccountSvc.getAccessTokenV2();
                    UploadFile(media).then(function (result) {
                        console.log('media upload done: ' + new Date());
                        callback(null, true);
                    }, function (error) {
                        callback(error, false);
                    });
                }, 5000);
            }).then(function (error, response) {
                if (error)
                    defer.reject(error);
                else
                    defer.resolve(true);
            });

            //defer.resolve(true);            
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    }

    function completeJob(jobId) {
        let defer = $q.defer();
        let token = AccountSvc.getAccessTokenV2();
        let resource = $resource('', null, { 'completeJob': { url: RemoteUrls.completeJobUrl, method: 'PUT', headers: { 'Authorization': 'Bearer ' + token }, params: { id: '@id' } } });
        resource.completeJob({ id: jobId }).$promise.then(function (response) {
            defer.resolve(true);
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    }

    function addTeamMember(teamMember) {
        //console.log('addTeamMember');
        let defer = $q.defer();
        let token = AccountSvc.getAccessTokenV2();
        let resource = $resource('', null, { 'add': { url: RemoteUrls.addTeamMemberUrl, method: 'POST', headers: { 'Authorization': 'Bearer ' + token }, params: { id: '@id' } } });
        let data = angular.copy(TeamMemberDto);
        //get remote staffid first
        let staffId = objectTypes.staff + teamMember.staffId;
        DBSvc.get(staffId).then(function (staff) {
            data.staffId = staff ? (staff.rId ? staff.rId : staff.id) : teamMember.staffId;
            data.tenantId = teamMember.tenantId;
            data.isSTMS = teamMember.isSTMS;
            if (teamMember.agencyId)
                data.agencyId = teamMember.agencyId;
            DBSvc.getRemoteJobId(teamMember.jobId).then(function (rJobId) {
                if (rJobId) {
                    data.jobId = rJobId;
                    resource.add({ id: rJobId }, data).$promise.then(function (response) {
                        //console.log(response);
                        if (response.id !== "" && response.id !== null && response.id !== undefined) {
                            DBSvc.updateTeamMemberRemId(teamMember.jobId, teamMember.id, response.id).then(function (response) {
                                defer.resolve(true);
                            });
                        }
                        defer.resolve(true);
                    }, function (error) {
                        console.log(error);
                        defer.reject(error);
                    });
                }
            });
        });
        return defer.promise;
    }

    function removeTeamMember(teamMember) {
        console.log('removeTeamMember');
        console.log(teamMember.rId);
        let defer = $q.defer();
        let token = AccountSvc.getAccessTokenV2();
        let resource = $resource('', null, { 'remove': { url: RemoteUrls.removeTeamMemberUrl, method: 'DELETE', headers: { 'Authorization': 'Bearer ' + token }, params: { id: '@id', teammemberid: '@teammemberid' } } });
        //get teammember and check if rId exist
        //then remove teammember based on rId
        DBSvc.getTeamMemberRemId(teamMember.jobId, teamMember.id).then(function (rId) {
            console.log(rId);
            if (rId) {
                resource.remove({ id: teamMember.rJobId, teammemberid: rId }).$promise.then(function (response) {
                    defer.resolve(true);
                }, function (error) {
                    console.log(error);
                    defer.reject(error);
                });
            }
            else defer.resolve(true);
        });
        return defer.promise;
    }

    function addTimeLog(timelog) {
        let defer = $q.defer();
        let token = AccountSvc.getAccessTokenV2();
        let resource = $resource('', null, { 'add': { url: RemoteUrls.addTimeLogUrl, method: 'POST', headers: { 'Authorization': 'Bearer ' + token }, params: { id: '@id', teammemberid: '@teammemberid' } } });
        //get rTeamMemberId to make sure
        let result = {};
        let tasks = async.seq(function (callback) {
            let objId = objectTypes.job + timelog.jobId;
            DBSvc.get(objId).then(function (job) {
                if (job) {
                    result.jobId = job.id;
                    result.rJobId = job.rId;
                }
                callback(null, job);
            });
        }, function (job, callback) {
            if (job) {
                let teamMember = $linq.Enumerable().From(job.teamMembers).FirstOrDefault(null, 'f => f.id == "' + timelog.teamMemberId + '"');
                return callback(null, teamMember);
            }
            callback(null, null);
        }, function (teamMember, callback) {
            if (teamMember) {
                result.id = teamMember.id;
                result.rId = teamMember.rId;
                callback(null, true);
            }
            else callback(null, null);
        }
        );
        tasks(function (error, response) {
            if (result.rId !== null && result.rId !== "" && result.rId !== undefined) {
                resource.add({ id: result.rJobId, teammemberid: result.rId }, timelog).$promise.then(function (response) {
                    //console.log(response);
                    defer.resolve(true);
                }, function (error) {
                    console.log(error);
                    defer.reject(error);
                });
            } else {
                defer.resolve(true);
            }
        });

        return defer.promise;
    }

    function addStaff(staff) {
        let defer = $q.defer();
        let token = AccountSvc.getAccessTokenV2();
        let resource = $resource('', null, { 'add': { url: RemoteUrls.addStaffUrl, method: 'POST', headers: { 'Authorization': 'Bearer ' + token } } });
        resource.add(staff).$promise.then(function (response) {
            console.log(response);
            DBSvc.updateStaffRemId(staff.id, response.id).then(function (response) {
                defer.resolve(true);
            });
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    }

    function uploadSiteCheckItemPhotos(activity) {
        let defer = $q.defer();
        if (activity.activityType == ActivityTypeEnum.OnSiteRecord) {
            //send each photo
            //if ok update item on server with the new filename
            async.eachSeries(activity.siteCheckItems, function (item, itemCallback) {
                async.eachSeries(item.photos, function (photo, callback) {
                    $timeout(function () {
                        console.log('photo upload: ' + new Date());
                        UploadFile(photo.path).then(function (result) {
                            //console.log('media upload done: ' + new Date());
                            callback(null, true);
                        }, function (error) {
                            callback(error, false);
                        });
                    }, 5000);
                }).then(function (error, response) {
                    if (error) {
                        console.log(error);
                        itemCallback(error, false);
                    }
                    else
                        itemCallback(null, true);
                });
            }).then(function (error, response) {
                if (error) {
                    console.log(error);
                    defer.reject(error);
                }
                else
                    defer.resolve(true);
            });
        }

        return defer.promise;
    }

    function addActivity(activity) {
        //console.log('remotesynchandler: addActivity()');
       // alert(activity);
        console.log(activity,"remotesync");
        let defer = $q.defer();
        let result = {};
        let token = AccountSvc.getAccessTokenV2();
        let resource = null;
        activity.userId = AccountSvc.getUserIdV2();
        let tasks = async.seq(function (callback) {
            let objId = objectTypes.job + activity.jobId;
            DBSvc.get(objId).then(function (job) {
                if (job) {
                    result.jobId = job.id;
                    result.rJobId = job.rId;
                    let actDbData = $linq.Enumerable().From(job.activities).FirstOrDefault(null, 'f => f.id == "' + activity.id + '"');
                    if (actDbData && actDbData.rId) {
                        activity.rId = actDbData.rId;
                    }
                }
                callback(null, job);
            });
        }
        );
        tasks(function (error, response) {
            if (activity.action == ActivityActionEnum.UPDATE) {//update an activity
                let activityData = $linq.Enumerable().From(response.activities).FirstOrDefault(null, 'f => f.id == "' + activity.id + '"');
                resource = $resource('', null, { 'updateActivity': { url: RemoteUrls.updateActivityUrl, method: 'PUT', headers: { 'Authorization': 'Bearer ' + token }, params: { id: '@id', jobid: '@jobid' } } });
                resource.updateActivity({ jobid: result.rJobId, id: activityData.rId }, activity).$promise.then(function (response) {
                    defer.resolve(true);
                }, function (error) {
                    console.log(error);
                    defer.reject(error);
                });
            }
            else if (activity.isCancelled) {//cancelled an activity
                resource = $resource('', null, { 'cancelActivity': { url: RemoteUrls.cancelActivityUrl, method: 'POST', headers: { 'Authorization': 'Bearer ' + token }, params: { id: '@id', jobid: '@jobid' } } });
                resource.cancelActivity({ jobid: result.rJobId, id: activity.rId }).$promise.then(function (response) {
                    defer.resolve(true);
                }, function (error) {
                    console.log(error);
                    defer.reject(error);
                });
            }
            else if (activity.startTime && !activity.endTime || activity.activityType === ActivityTypeEnum.EndOfShift) {//add an activity
                resource = $resource('', null, { 'add': { url: RemoteUrls.activitiesUrl, method: 'POST', headers: { 'Authorization': 'Bearer ' + token }, params: { id: '@id' } } });
                resource.add({ id: result.rJobId }, activity).$promise.then(function (response) {
                    DBSvc.updateActivityRemId(activity.jobId, activity.id, response.id).then(function (response) {
                        defer.resolve(true);
                    }, function (error) {
                        console.log(error);
                        defer.reject(error);
                    });
                }, function (error) {
                    console.log(error);
                    defer.reject(error);
                });
            }
            else {//stop activity
                console.log('stop activity: ' + ActivityTypeEnum.props[activity.activityType].name);
                if (activity.activityType == ActivityTypeEnum.PreStartTruckCheck) {
                    DBSvc.getTruckAuditV2(activity.truckAuditId).then(function (audit) {
                        if (audit)
                            activity.truckAuditId = audit.rId;
                        resource = $resource('', null, { 'stop': { url: RemoteUrls.stopActivityUrl, method: 'POST', headers: { 'Authorization': 'Bearer ' + token }, params: { id: '@id', jobid: '@jobid' } } });
                        resource.stop({ jobid: result.rJobId, id: activity.rId }, activity).$promise.then(function (response) {
                            defer.resolve(response);
                        }, function (error) {
                            console.log(error);
                            defer.reject(error);
                        });
                    });
                }
                else {
                    resource = $resource('', null, { 'stop': { url: RemoteUrls.stopActivityUrl, method: 'POST', headers: { 'Authorization': 'Bearer ' + token }, params: { id: '@id', jobid: '@jobid' } } });
                    resource.stop({ jobid: result.rJobId, id: activity.rId }, activity).$promise.then(function (response) {
                        if (activity.activityType == ActivityTypeEnum.OnSiteRecord) {
                            //upload photos
                            uploadSiteCheckItemPhotos(activity).then(function (response) {
                                //console.log('photo uploads complete');
                                defer.resolve(response);
                            }).catch(function (error) {
                                console.log(error);
                            }).finally(function () {
                                defer.resolve(true);
                            });
                        }
                        else defer.resolve(response);
                    }, function (error) {
                        console.log(error);
                        defer.reject(error);
                    });
                }
            }
        });

        return defer.promise;
    }

    function addOnSiteRecord(onsiterecord) {
        console.log(onsiterecord);
        let defer = $q.defer();
        //send data first then send photos for each item


        return defer.promise;
    }

    function teamMemberEndShift(teamMember) {
        let defer = $q.defer();
        let token = AccountSvc.getAccessTokenV2();
        let resource = $resource('', null, { 'add': { url: RemoteUrls.teamMemberEndShiftUrl, method: 'POST', headers: { 'Authorization': 'Bearer ' + token }, params: { id: '@id', teammemberid: '@teammemberid' } } });
        //get rTeamMemberId to make sure
        let result = {};
        let tasks = async.seq(function (callback) {
            let objId = objectTypes.job + teamMember.jobId;
            DBSvc.get(objId).then(function (job) {
                if (job) {
                    result.jobId = job.id;
                    result.rJobId = job.rId;
                }
                callback(null, job);
            });
        }, function (job, callback) {
            if (job) {
                let teamMemberData = $linq.Enumerable().From(job.teamMembers).FirstOrDefault(null, 'f => f.id == "' + teamMember.id + '"');
                return callback(null, teamMemberData);
            }
            callback(null, null);
        }, function (teamMember, callback) {
            if (teamMember) {
                result.id = teamMember.id;
                result.rId = teamMember.rId;
                callback(null, true);
            }
            else callback(null, null);
        }
        );
        tasks(function (error, response) {
            resource.add({ id: result.rJobId, teammemberid: result.rId }, { endOfShiftTime: teamMember.endOfShiftTime }).$promise.then(function (response) {
                //console.log(response);
                defer.resolve(true);
            }, function (error) {
                console.log(error);
                defer.reject(error);
            });
        });

        return defer.promise;
    }

    function addTruckAudit(audit) {
        console.log('add truck audit');
        let defer = $q.defer();
        let token = AccountSvc.getAccessTokenV2();
        let resource = $resource('', null, { 'add': { url: RemoteUrls.truckAuditUrl, method: 'POST', headers: { 'Authorization': 'Bearer ' + token }, params: { id: '@id' } } });
        resource.add({ id: audit.truckId }, audit).$promise.then(function (response) {
            //console.log(response);
            DBSvc.updateTruckAuditRemId(audit.id, response.id).then(function (updateRes) {
                let photos = $linq.Enumerable().From(audit.inspections).SelectMany('w => w.photos').ToArray();
                let audioNotes = $linq.Enumerable().From(audit.inspections).SelectMany('w => w.audioNotes').ToArray();
                photos = photos.concat(audit.oilLevelDetails.photos);
                photos = photos.concat(audit.dieselLevelDetails.photos);
                photos = photos.concat(audit.waterLevelDetails.photos);
                audioNotes = audioNotes.concat(audit.oilLevelDetails.audioNotes);
                audioNotes = audioNotes.concat(audit.waterLevelDetails.audioNotes);
                audioNotes = audioNotes.concat(audit.dieselLevelDetails.audioNotes);

                let fileUploadList = photos.concat(audioNotes);

                async.eachSeries(fileUploadList, function (file, callback) {
                    $timeout(function () {
                        console.log('photo upload: ' + new Date());
                        let token = AccountSvc.getAccessTokenV2();
                        var fileUploadApiPath = RemoteUrls.truckauditfileupload + '&userid=' + AccountSvc.getUserIdV2();
                        $cordovaFileTransfer.upload(fileUploadApiPath, file, { fileName: getFileName(file), headers: { 'Authorization': 'Bearer ' + token } }).then(function (result) {
                            console.log('photo upload done: ' + new Date());
                            callback(null, true);
                        }, function (error) {
                            callback(error, false);
                        });
                    }, 5000);
                }, function (error, response) {
                    console.log(error);
                    if (error)
                        defer.reject(error);
                    else
                        defer.resolve(true);
                });
            }, function (error) {
                console.log(error);
                defer.reject(error);
            });
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    }

    function updateBreakTimes(updateData) {
        console.log('[473]:');
        console.log(updateData);
        let defer = $q.defer();
        let token = AccountSvc.getAccessTokenV2();
        let resource = $resource('', null, { 'updateBreakTimes': { url: RemoteUrls.updateShifts, method: 'POST', headers: { 'Authorization': 'Bearer ' + token }, params: { jobid: '@jobid' } } });
        let objJobId = objectTypes.job + updateData.jobId;
        DBSvc.get(objJobId).then(function (job) {
            if (job) {
                updateData.jobId = job.rId;
                let teamMembersQry = $linq.Enumerable().From(job.teamMembers);
                updateData.updates.forEach(update => {
                    let teamMember = teamMembersQry.FirstOrDefault(null, 'f => f.id == "' + update.teamMemberId + '"');
                    if (teamMember)
                        update.teamMemberId = teamMember.rId;
                });
                console.log('[488]:');
                console.log(updateData);
                resource.updateBreakTimes({ jobid: updateData.jobId }, updateData.updates).$promise.then(function (result) {
                    console.log(result);
                    defer.resolve(true);
                }, function (error) {
                    console.log(error);
                    defer.reject(error);
                });
            }
            else {
                defer.resolve(true);
            }
        })

        return defer.promise;
    }

    function startTrucksShift(data) {
        console.log('[525]');
        console.log(data);
        let defer = $q.defer();
        let token = AccountSvc.getAccessTokenV2();
        let apiResource = $resource(RemoteUrls.rootPath, null, { 'startShift': { url: RemoteUrls.startTrucksShift, method: 'POST', isArray: false, headers: { 'Authorization': 'Bearer ' + token }, params: { jobId: '@jobId' } } });
        let jobId = objectTypes.job + data.jobId;
        DBSvc.get(jobId).then(function (job) {
            apiResource.startShift({ jobId: job.rId }, { time: data.startTime }).$promise.then(function (result) {
                console.log(result);
                defer.resolve(true);
            }, function (error) {
                console.log('[536]');
                console.log(error);
                defer.reject(error);
            });
        });

        return defer.promise;
    }

    function siteRecordpdflog(data) {
        console.log(data);
        let defer = $q.defer();
        let token = AccountSvc.getAccessTokenV2();
        let apiResource = $resource(RemoteUrls.rootPath, null, { 'siteRecordpdflog': { url: RemoteUrls.siteRecordpdflog, method: 'POST', isArray: false, headers: { 'Authorization': 'Bearer ' + token }, params: { jobId: '@jobId' } } });

        apiResource.siteRecordpdflog({ jobId: data.id }, { createdOn: data.siteRecordPDFLog.createdOn, pdfPath: data.siteRecordPDFLog.pdfPath }).$promise.then(function (result) {
            console.log(result);
            defer.resolve(true);
        }, function (error) {
            console.log(error);
            defer.resolve(true);
        });

        return defer.promise;
    }

    function updateJobStatus(data) {
        console.log(data);
        let defer = $q.defer();
        let token = AccountSvc.getAccessTokenV2();
        let apiResource = $resource(RemoteUrls.rootPath, null, { 'updateJobStatus': { url: RemoteUrls.updateJobStatus, method: 'DELETE', isArray: false, headers: { 'Authorization': 'Bearer ' + token }, params: { jobId: '@jobId' } } });

        apiResource.updateJobStatus({ jobId: data.id }).$promise.then(function (result) {
            console.log(result);
            defer.resolve(true);
        }, function (error) {
            console.log(error);
            defer.resolve(true);
        });

        return defer.promise;
    }

    service.execute = function (qData) {
        //alert("s1");
        //temp
        //return $q.resolve(true);

        //console.log('execute ' + qData._id);
        console.log(objectTypes,"objectTypes");
        try {
            if (qData._id.search(objectTypes.job) >= 0) {
                if (qData.rId == null)
                    return sendJob(qData);
                else if (qData.complete)
                    return completeJob(qData.rId);
                else
                    return updateJob(qData);
            }
            else if (qData._id.search(objectTypes.teammember) >= 0) {
                if (qData.dataMode === DataActionEnum.DELETE) {
                    return removeTeamMember(qData);
                }
                else if (qData.endOfShiftTime) {
                    return teamMemberEndShift(qData);
                }
                else return addTeamMember(qData);
            }
            else if (qData._id.search(objectTypes.timelog) >= 0) {
                return addTimeLog(qData);
            }
            else if (qData._id.search(objectTypes.staff) >= 0) {
                return addStaff(qData);
            }
            else if (qData._id.search(objectTypes.jobactivity) >= 0 || qData._id.search(objectTypes.siteactivity) >= 0 || qData._id.search(objectTypes.onsiterecord) >= 0 || qData._id.search(objectTypes.geargeftonsiterecord) >= 0 || qData._id.search(objectTypes.truckPrecheckRecord) >= 0) {
                //alert("s");
                return addActivity(qData);
            }
            else if (qData._id.search(objectTypes.truckaudit) >= 0) {
                return addTruckAudit(qData);
            }
            else if (qData._id.search(objectTypes.breaktimeupdate) >= 0) {
                return updateBreakTimes(qData);
            }
            else if (qData._id.search(objectTypes.starttrucksshift) >= 0) {
                return startTrucksShift(qData);
            }
            else if (qData._id.search(objectTypes.siteRecordpdflog) >= 0) {
                updateJob(qData);
                return siteRecordpdflog(qData);
            }
            else if (qData._id.search(objectTypes.updateJobStatus) >= 0) {
                return updateJobStatus(qData);
            }
            else
                return $q.resolve(true);
        }
        catch (error) {
            console.log(error);
            return $q.resolve(true);
        }
    };

    return service;
}]);