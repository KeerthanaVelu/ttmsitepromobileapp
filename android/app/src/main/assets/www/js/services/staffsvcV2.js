mainApp.factory('StaffSvc', ['$linq', 'DBSvc', '$q', 'AccountSvc', 'RemoteSyncSvc', '$resource', 'RemoteUrls', function ($linq, DBSvc, $q, AccountSvc, RemoteSyncSvc, $resource, RemoteUrls) {
  let service = {};

  service.getAll = function () {
    return DBSvc.getAllStaff();
  };

  service.get = function (id) {
    //console.log('StaffSvc.get: ' + id);
    let defer = $q.defer();
    let staffId = objectTypes.staff + id;
    DBSvc.get(staffId).then(function (staff) {
      if (staff)
        defer.resolve(staff);
      else {
        DBSvc.get(objectTypes.agencystaff + id).then(function (agencyStaff) {
          defer.resolve(agencyStaff ? agencyStaff : null);
        });
      }
    }, function (error) {
      defer.reject(error);
    });

    return defer.promise;
  };

  service.getORIG = function (id) {
    //console.log('StaffSvc.get: ' + id);
    let defer = $q.defer();
    let staffId = objectTypes.staff + id;
    DBSvc.get(staffId).then(function (staff) {
      if (staff)
        defer.resolve(staff);
      else {
        DBSvc.getStaffByUser(id).then(function (response) {
          //console.log(response);
          if (response)
            defer.resolve(response);
          else {
            DBSvc.get(objectTypes.agencystaff + id).then(function (agencyStaff) {
              if (agencyStaff)
                defer.resolve(agencyStaff);
            });
          }
        }, function (error) {
          defer.reject(error);
        });
      }
    }, function (error) {
      defer.reject(error);
    });

    return defer.promise;
  };

  service.add = function (staff) {
    let defer = $q.defer();
    staff.id = (new ObjectId()).toString();
    staff._id = objectTypes.staff + staff.id;
    staff.tenantId = AccountSvc.getTenantIdV2();
    //add to db
    /// add to sync
    DBSvc.addStaff(staff).then(function (response) {
      RemoteSyncSvc.add(staff).then(function (response) {
        defer.resolve(staff.id);
      });
    });

    return defer.promise;
  };

  service.updateStaff = function (staffMember) {
    var defer = $q.defer();
    DBSvc.saveStaff(this.staff).then(function (result) {
      staffMember.isStaffUpdate = true;
      WebApiQSvc.Send(staffMember);
      defer.resolve(result);
    }, function (error) {
      defer.reject(error);
    });

    return defer.promise;
  };

  service.refreshData = function () {
    let defer = $q.defer();
    let resource = $resource('', {}, {
      'refreshData': { url: RemoteUrls.staffDataSeedV2, method: 'GET', isArray: true, headers: { 'Authorization': 'Bearer ' + AccountSvc.getAccessTokenV2() }, params: { tenantId: '@tenantId' } }
    });

    resource.refreshData({ tenantId: AccountSvc.getTenantIdV2() }).$promise.then(function (staff) {
      console.log('staff loaded from server');
      console.log(staff);
      staff.forEach(member => {
        member._id = objectTypes.staff + member.id;
      });
      //remove all staff
      DBSvc.clearStaffData().then(function (cleared) {
        console.log('staff cleared');
        if (cleared) {
          //save new staff
          DBSvc.save(staff).then(function (saveResponse) {
            console.log('trucks saved');
            defer.resolve(staff);
          });
        }
      }, function (error) {
        console.log('[108]');
        console.log(error);
        defer.reject(error);
      });
    }, function (error) {
      console.log('[113]');
      console.log(error);
      defer.reject(error);
    });

    return defer.promise;
  };

  return service;
}]);