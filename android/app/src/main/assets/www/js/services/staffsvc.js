mainApp.factory('StaffSvc', ['$linq', 'DBSvc', '$q', 'WebApiQSvc', function ($linq, DBSvc, $q, WebApiQSvc) {
    var service = {};
    var staff = [];

    service.getById = function (id) {
        return $linq.Enumerable().From(this.staff).FirstOrDefault(null, 'f => f.id == "' + id + '"');
    };
    service.getByIds = function (ids) {
        var result = [];

        $linq.Enumerable().From(ids).ForEach(function (id) {
            var member = service.getById(id);
            if (member != null && member.archived == false)
                result.push(member);
        });

        return result;
    };
    service.staffMember = null;
    service.get = function () {
      var active = $linq.Enumerable().From(this.staff).Where('w => w.archived == false').ToArray();
      return active;
        //return this.staff;
    };
    service.getAllInactive = function () {
        return $linq.Enumerable().From(this.staff).Where('w => w.active == false').ToArray();
    };
    service.setStaff = function (staff) {
        this.staff = staff;
    };
    
    service.addStaff = function (staffMember) {
        var defer = $q.defer();
        this.staff.push(staffMember);
        DBSvc.saveStaff(this.staff).then(function (result) {
            staffMember.isStaff = true;
            WebApiQSvc.Send(staffMember);
            defer.resolve(result);
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    };

    service.updateStaff = function (staffMember) {
      var defer = $q.defer();

      DBSvc.saveStaff(this.staff).then(function (result) {
        staffMember.isStaffUpdate = true;
        WebApiQSvc.Send(staffMember);
        defer.resolve(result);
      }, function (error) {
        defer.reject(error);
      });

      return defer.promise;
    };

    service.changeTempIdToServerId = function (tempId, serverId) {
      var defer = $q.defer();

      var member = $linq.Enumerable().From(this.staff).FirstOrDefault(null, 'f => f.id == "' + tempId + '"');
      if (member != null)
        member.id = serverId;

      DBSvc.saveStaff(this.staff).then(function (result) {        
        defer.resolve(result);
      }, function (error) {
        defer.reject(error);
      });

      return defer.promise;
    };

    service.archiveStaff = function (staffId, archived) {
      var defer = $q.defer();

      var member = $linq.Enumerable().From(this.staff).FirstOrDefault(null, 'f => f.id == "' + staffId + '"');
      if (member != null)
        member.archived = archived;

      DBSvc.saveStaff(this.staff).then(function (result) {
        defer.resolve(result);
      }, function (error) {
        defer.reject(error);
      });

      return defer.promise;
    };

    service.addStaffMemberLocal = function (staffMember) {
        var defer = $q.defer();
        this.staff.push(staffMember);
        DBSvc.saveStaff(this.staff).then(function (result) {
            defer.resolve(result);
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    };

    return service;
}]);