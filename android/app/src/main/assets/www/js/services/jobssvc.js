﻿mainApp.factory('JobsSvc', ['$linq', 'WebApiQSvc', 'DBSvc', '$q', 'ApiSvc', '$rootScope', 'StaffSvc', '$timeout', '$cordovaLocalNotification', '$ionicPopup', 'SyncService', 'RemoteSyncSvc', 'AccountSvc', function ($linq, WebApiQSvc, DBSvc, $q, ApiSvc, $rootScope, StaffSvc, $timeout, $cordovaLocalNotification, $ionicPopup, SyncService, RemoteSyncSvc, AccountSvc) {
    var jobs = [];
    var service = {};
    var accessToken = {};    
    $rootScope.$on('uploadSiteCheckPhotosDone', function (event, activity) {
        console.log(activity);
        WebApiQSvc.Send(activity);
    });
    $rootScope.$on('newStaffMemberAdded', function (event, params) {
        console.log(event);
        //update the staff
        StaffSvc.changeTempIdToServerId(params.tempId, params.newId).then(function (result) {            
            if (service.job) {
                var member = $linq.Enumerable().From(service.job.teamMembers).FirstOrDefault(null, 'f => f.staffId == "' + params.tempId + '"');
                if (member != null) {
                    //member.staffMemberId = params.newId;
                    member.staffId = params.newId;
                    DBSvc.saveJobs(service.jobs).then(function () {
                        //$rootScope.$broadcast('reloadTeamMembers');
                    }, function (error) {
                        console.log(error);
                    });
                }
            }
        }, function (error) {
            console.log(error);
        });    
    });
    
    function initSignalr() {
        var connection = $.hubConnection();
        connection.qs = { "Authorization": "Bearer " + AccountSvc.getAccessTokenV2() };
        console.log(connection.qs);
        //connection.qs = { "Authorization": "Bearer " + WebApiQSvc.getAccessToken() };
        var hubProxy = connection.createHubProxy('jobsHub');
        var staffHubProxy = connection.createHubProxy('staffHub');
        hubProxy.on('receiveJob', function (job) {
            console.log('job received');
            console.log(job);
            var jobId = job.id;
            hubProxy.invoke('jobReceived', jobId).done(function () {
                console.log('notify admin of jobReceived successfully');
            }).fail(function (error) {
                console.log('Invocation of jobReceived failed. Error: ' + error);
            });

            var staff = StaffSvc.get();
            var teamMemberIds = $linq.Enumerable().From(job.team).Select('s => s.id').ToArray();
            var staffIds = $linq.Enumerable().From(staff).Select('s => s.id').ToArray();
            var notExist = $linq.Enumerable().From(teamMemberIds).Except(staffIds).ToArray();
            if (notExist.length > 0) {
                angular.forEach(notExist, function (value, key) {
                    if (job.team) {
                        var teamMember = $linq.Enumerable().From(job.team).FirstOrDefault(null, 'f => f.id == "' + value + '"');
                        if (teamMember != null) {
                            var newStaff = new Staff();
                            newStaff.id = teamMember.id;
                            newStaff.branchId = teamMember.branchId;
                            newStaff.fullName = teamMember.fullName;
                            newStaff.phone = teamMember.phone;
                            newStaff.qualification = teamMember.qualification;
                            newStaff.notes = teamMember.notes;
                            newStaff.photo = teamMember.photo;
                            StaffSvc.addStaffMemberLocal(newStaff);
                        }
                    }
                });
            }
            var jobExist = $linq.Enumerable().From(service.jobs).Count('c => c.id == "' + job.id + '"');
            if (jobExist == 0) {
                service.jobs.push(job);
                service.save().then(function () {
                    $cordovaLocalNotification.schedule({
                        id: 101,
                        title: 'Site Pro - New Job',
                        text: job.jobName
                    });
                    $rootScope.$broadcast('jobReceived');
                });
            }
        });
        hubProxy.on('transferJob', function (job) {
            console.log('transferJob');
            //console.log(job);
            $cordovaLocalNotification.schedule({
                id: 102,
                title: 'Site Pro - Transfer Job',
                text: job.job.jobName + ' (From: ' + job.sourceStms + ')'
            });
            //alert('Transfer!');
            var confirmTransfer = $ionicPopup.confirm({
                title: 'Transfer Job',
                template: "Job Name: <b>" + job.job.jobName + "</b><br/>Do you want to accept the job? "
            });
            confirmTransfer.then(function (result) {
                if (result) {
                    console.log('pull job');
                    var staff = StaffSvc.get();
                    var teamMemberIds = $linq.Enumerable().From(job.job.team).Select('s => s.id').ToArray();
                    var staffIds = $linq.Enumerable().From(staff).Select('s => s.id').ToArray();
                    var notExist = $linq.Enumerable().From(teamMemberIds).Except(staffIds).ToArray();
                    if (notExist.length > 0) {
                        angular.forEach(notExist, function (value, key) {
                            if (job.team) {
                                var teamMember = $linq.Enumerable().From(job.job.team).FirstOrDefault(null, 'f => f.id == "' + value + '"');
                                if (teamMember != null) {
                                    var newStaff = new Staff();
                                    newStaff.id = teamMember.id;
                                    newStaff.branchId = teamMember.branchId;
                                    newStaff.fullName = teamMember.fullName;
                                    newStaff.phone = teamMember.phone;
                                    newStaff.qualification = teamMember.qualification;
                                    newStaff.notes = teamMember.notes;
                                    newStaff.photo = teamMember.photo;
                                    StaffSvc.addStaffMemberLocal(newStaff);
                                }
                            }
                        });
                    }
                    service.jobs.push(job.job);
                    service.save().then(function () {
                        if (job.isMobileTransfer) {
                            hubProxy.invoke('mobileJobTransferReceived', job.job.id).done(function () {
                                console.log('notify server of mobile job transfer received successfully');
                            }).fail(function (error) {
                                console.log('Invocation of mobileJobTransferReceived failed. Error: ' + error);
                            });
                        }
                        else {
                            hubProxy.invoke('jobReceived', job.job.id).done(function () {
                                console.log('notify admin of jobReceived successfully');
                            }).fail(function (error) {
                                console.log('Invocation of jobReceived failed. Error: ' + error);
                            });
                        }
                    });
                }
            });
        });
        hubProxy.on('mobileJobTransferReceived', function (jobId) {
            $rootScope.$broadcast('mobileJobTransferReceived', { jobId: jobId });
        });
        hubProxy.on('updateTeamMembers', function (teamMembers) {
            //update team members if any
            //foreach teammember
            //if new then add
            service.job.teamMembers = teamMembers;            
            service.save();
        });
        staffHubProxy.on('newStaff', function (newStaff) {
            //console.log(newStaff);
            StaffSvc.addStaff(newStaff, function () {

            });
        });
        staffHubProxy.on('staffArchived', function (result) {
            console.log(result);
            StaffSvc.archiveStaff(result.staffId, result.archived).then(function () {
            });
        });
        connection.url = ApiSvc.signalrHubs;
        //connection.logging = true;        
        connection.error(function (error) {
            console.log('SignalR error');
            console.log(error);
        });
        connection.disconnected(function () {
            //connection.start();
            //console.log('Restart disconnected at: ' + (new Date()).toLocaleTimeString());
            $timeout(function () {
                //console.log('Restart at: ' + (new Date()).toLocaleTimeString());
                connection.start();
            }, 30000);
        });

        connection.start().done(function () {
            console.log('connection established');
        }).fail(function () {
            console.log('unable to connect');
        });
    }

    service.job = null;
    service.branches = [];
    service.teamMember = null;
    service.add = function (job) {
        if (this.jobs == null)
            this.jobs = [];
        this.jobs.push(job);
        service.save();
        WebApiQSvc.Send(job);
    };    

    service.AddSendActivity = function (activity) {
        if (service.job.activities == null) {
            service.job.activities = [];
        }

        service.job.activities.unshift(activity);
        service.save();
        SyncService.addToQ(activity);
        //WebApiQSvc.Send(activity);
    };
    service.UpdateActivity = function (activity) {
        service.save();
        WebApiQSvc.Send(activity);
    };
    service.AddActivity = function (activity) {

        if (service.job.activities == null)
            service.job.activities = [];
        service.job.activities.unshift(activity);
        service.save();
    };
    service.getAll = function () {
        return this.jobs;
    };
    service.setBranches = function (branches) {
        this.branches = branches;
    };
    service.save = function () {
        var defer = $q.defer();

        DBSvc.saveJobs(this.jobs).then(function (result) {
            defer.resolve(result);
        }, function (error) {
            defer.reject(error);
        });

        return defer.promise;
    };
    service.addLocal = function (job) {
        this.jobs.push(job);
    };
    service.init = function (jobs) {
        this.jobs = jobs;
        initSignalr();
    };
    service.initSignalr = function(){
        initSignalr();
    };
    service.uploadPhoto = function (photoData) {
        WebApiQSvc.Send(photoData);
    };
    service.addToQ = function (data) {
        var defer = $q.defer();

        DBSvc.saveQData(data).then(function (result) {
            console.log('service.addToQ: result');
            defer.resolve(result);
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    };
    service.completeJob = function (job) {
        WebApiQSvc.Send({ jobId: job.id, isJob: true, complete: true });
    };
    service.updateGeneralNotes = function (generalNotes) {
        service.save().then(function () {
            WebApiQSvc.Send(generalNotes);
        });
    };
    service.deleteJob = function (job) {
        var defer = $q.defer();

        var index = this.jobs.indexOf(job);
        if (index >= 0) {
            this.jobs.splice(index, 1);
            service.save().then(function () {
                defer.resolve(true);
            }, function (error) {
                console.log(error);
                defer.reject(error);
            });
        }
        defer.resolve(true);

        return defer.promise;
    };
    service.updateTrucks = function (job) {
        var defer = $q.defer();

        service.save().then(function (result) {
            var truckIds = null;
            if (job.trucks != null && job.trucks.length > 0 && (typeof job.trucks[0] === 'object'))
                truckIds = $linq.Enumerable().From(job.trucks).Select('s => s.truckId').ToArray();
            WebApiQSvc.Send({ isTrucksUpdate: true, jobId: job.id, truckIds: (truckIds || job.trucks) });
            //WebApiQSvc.Send({ isTrucksUpdate: true, jobId: job.id, truckIds: job.trucks });
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    };
    service.updateGeneralInfo = function (data) {
        var defer = $q.defer();

        service.save().then(function (result) {
            WebApiQSvc.Send(data);
            defer.resolve(true);
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    };
    service.endOfShift = function (activity) {
        var defer = $q.defer();
        if (service.job != null && service.job.activities != null) {
            service.job.activities.unshift(activity);
        }
        //stop time of team
        for (var i = 0; i < service.job.teamMembers.length; i++) {
            var member = service.job.teamMembers[i];
            var timelog = { isTeamMemberTimelog: true, jobId: service.job.id, staffMemberId: member.id, timeLogType: 3, time: new Date() };
            member.timeLogs.push(timelog);
        }

        service.save().then(function () {
            WebApiQSvc.Send(activity);
            defer.resolve(true);
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    };

    service.getTruckAudit = function (truckAuditId) {
        var defer = $q.defer();

        DBSvc.getTruckAudit(truckAuditId).then(function (result) {
            defer.resolve(result);
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    };
    
    service.updateJobId = function(newJobId, tempJobId){
        var defer = $q.defer();
        defer.resolve(true);

        return defer.promise;
    };

    service.get = function(id){
        var defer = $q.defer();
        id = objectTypes.job + id;
        DBSvc.get(id).then(function(job){
            defer.resolve(job);
        }, function(error){
            console.log(error);
            if(error.status === 404)
                defer.resolve(null);
            else
                defer.reject(error);
        });
        
        return defer.promise;
    };

    service.getAllV2 = function(){
        let defer = $q.defer();
        DBSvc.getAllJobs().then(function(response){
            defer.resolve(response);
        }, function(error){
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    }

    service.updateJobRemoteId = function(id, rId){
        DBSvc.updateJobRemoteId(id, rId).then(function(){
            $rootScope.$broadcast('jobCreated', { rId: rId });            
        });
    };

    service.addV2 = function(job){
        var defer = $q.defer();
        job.id = (new ObjectId()).toString();
        job._id = objectTypes.job + job.id;
        job.tenantId = AccountSvc.getTenantIdV2();
        job.branchId = AccountSvc.getBranchIdV2();
        job.assignedUserId = AccountSvc.getUserIdV2();
        job.createdOn = moment().utc();// new Date();
        DBSvc.addJob(job).then(function(response){
            //send to SyncService
            RemoteSyncSvc.add(job).then(function(response){
                console.log(response);
                defer.resolve(job.id);
            }, function(error){
                console.log(error);
                defer.reject(error);
            });
        }, function(error){
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    };

    service.getGeneralInfo = function(id){
        return DBSvc.getGeneralInfo(id);
    };

    service.updateGeneralInfoV2 = function(generalInfo){
        let defer = $q.defer();
        DBSvc.get(objectTypes.job + generalInfo.id).then(function(job){
            if(job != null){
                job.jobName = generalInfo.jobName;
                job.reference = generalInfo.reference;
                job.description = generalInfo.description;
                job.poNumber = generalInfo.poNumber;
                job.address = generalInfo.address;
                job.roadLevels = generalInfo.roadLevels;
                DBSvc.save([job]).then(function(response){
                    RemoteSyncSvc.add(job).then(function(response){
                        defer.resolve(true);
                    });                                                            
                }, function(error){
                    console.log(error);
                    defer.reject(error);
                });
            }
        });

        return defer.promise;
    };

    service.getGeneralNotes = function(id){
        return DBSvc.getGeneralNotes(id);
    }

    service.updateGeneralNotes = function(jobid, generalNotes){
        let defer = $q.defer();
        DBSvc.get(objectTypes.job + jobid).then(function(job){
            if(job != null){
                delete generalNotes.id;
                delete generalNotes._id;        
                job.generalNotes = generalNotes;
                DBSvc.save([job]).then(function(response){
                    RemoteSyncSvc.add(job).then(function(response){
                        defer.resolve(true);
                    });                                                            
                }, function(error){
                    console.log(error);
                    defer.reject(error);
                });
            }
        });

        return defer.promise;
    };

    service.updateTrucksV2 = function(jobId, trucks){
        let defer = $q.defer();
        DBSvc.get(objectTypes.job + jobId).then(function(job){
            if(job != null){
                job.trucks = trucks;
                DBSvc.save([job]).then(function(response){
                    RemoteSyncSvc.add(job).then(function(response){
                        defer.resolve(true);
                    });                                                            
                }, function(error){
                    console.log(error);
                    defer.reject(error);
                })
            }
        });

        return defer.promise;
    };
    
    service.getParties = function(id){
        return DBSvc.getParties(id);
    };

    service.updateParties = function(parties){
        let defer = $q.defer();

        DBSvc.get(objectTypes.job + parties.id).then(function(job){
            if(job){
                job.parties = parties;
                DBSvc.save([job]).then(function(response){
                    RemoteSyncSvc.add(job).then(function(response){
                        defer.resolve(true);
                    });                                                            
                }, function(error){
                    console.log(error);
                    defer.reject(error);
                });
            }
        }, function(error){
            console.log(error);
            alert('Something went wrong.');
        });

        return defer.promise;
    };    

    service.getTeam = function(id){
        return DBSvc.getTeam(id);        
    };

    
    service.updateTeam = function(id, team){
        let defer = $q.defer();
        let jobid = objectTypes.job + id;
        DBSvc.get(jobid).then(function(job){
            if(job){
                job.teamMembers = team;
                DBSvc.save([job]).then(function(response){
                    RemoteSyncSvc.add(job).then(function(response){
                        defer.resolve(true);
                    });                    
                });
            }
        });

        return defer.promise;
    };

    service.getTeamMemberLogs = function(jobId, teamMemberId){
        let defer = $q.defer();
        let objId = objectTypes.job + jobId;
        let result = {};
        let seqResults = {};
        let tasks = async.seq(function(callback){
            DBSvc.get(objId).then(function(job){
                    if(job){     
                        result.jobId = job.id;
                        result.rJobId = job.rId;
                    }
                    callback(null, job);
                });
            }, function(job, callback){
                if(job){
                    let teamMember = $linq.Enumerable().From(job.teamMembers).FirstOrDefault(null, 'f => f.id == "' + teamMemberId + '"');                
                    return callback(null, teamMember);
                }
                callback(null, null);
            }, function(teamMember, callback){
                if(teamMember){                    
                    result.id = teamMember.id;
                    result.rId = teamMember.rId;
                    result.timeLogs = teamMember.timeLogs;
                    let staffId = objectTypes.staff + teamMember.staffId;
                    DBSvc.get(staffId).then(function(staff){
                        if(staff){
                            result.fullName = staff.fullName;
                            callback(null, staff.branchId);
                        }
                        else callback(null, null);
                    });
                }
                else callback(null, null);
            }, function(branchId, callback){
                let objBranchId = objectTypes.tenantbranch + branchId;
                DBSvc.get(objBranchId).then(function(branch){
                    if(branch){
                        result.branchName = branch.branch;
                        callback(null, true);
                    }
                    else callback(null, true);
                });
            }
        );
        tasks(function(error, response){
            defer.resolve(result);
        });
        
        return defer.promise;
    };


    service.addTimeLogTemp = function(){
        let defer = $q.defer();
        setTimeout(function(){
            console.log('timelog added');
            defer.resolve(true);
        }, 5000);

        return defer.promise;
    };
    service.addTimeLog = function(timelog){
        let defer = $q.defer();
        let objJobId = objectTypes.job + timelog.jobId;
        timelog.id = (new ObjectId()).toString();
        timelog._id = objectTypes.timelog + timelog.id;
        DBSvc.get(objJobId).then(function(job){
            if(!job)
                return defer.resolve(true);

            let teamMember = $linq.Enumerable().From(job.teamMembers).FirstOrDefault(null, 'f => f.id == "' + timelog.teamMemberId +'"');
            if(teamMember){
                teamMember.timeLogs.push(timelog);
            }
            DBSvc.save([job]).then(function(response){
                RemoteSyncSvc.add(timelog).then(function(response){                    
                    defer.resolve(true);
                });
            });
        });

        return defer.promise;
    };

    service.addTeamMembers = function(jobId, teamMembers){
        let defer = $q.defer();
        let objJobId = objectTypes.job + jobId;
        DBSvc.get(objJobId).then(function(job){
            if(job){
                teamMembers.forEach(item => {
                    item.rJobId = job.rId;
                });
                job.teamMembers = job.teamMembers.concat(teamMembers);
                DBSvc.save([job]).then(function(response){
                    async.eachSeries(teamMembers, function(item, callback){
                        RemoteSyncSvc.add(item).then(function(response){
                            callback();
                        });
                    }).then(function(response){
                        defer.resolve(true);
                    });                    
                });
            }
        });

        return defer.promise;
    };

    service.removeTeamMember = function(jobId, teamMemberId){
        let defer = $q.defer();
        let objJobId = objectTypes.job + jobId;
        DBSvc.get(objJobId).then(function(job){
            if(job){
                let teamMember = $linq.Enumerable().From(job.teamMembers).FirstOrDefault(null, 'f => f.id == "' + teamMemberId + '"');
                if(teamMember){
                    let ndx = job.teamMembers.indexOf(teamMember);
                    job.teamMembers.splice(ndx, 1);
                    DBSvc.save([job]).then(function(response){
                        teamMember.dataMode = DataActionEnum.DELETE;
                        RemoteSyncSvc.add(teamMember).then(function(response){
                            defer.resolve(true);
                        });                    
                    });
                }
                else defer.resolve(true);          
            }
        });

        return defer.promise;
    };

    function getObjectType(activityType){
        switch(activityType){
            case ActivityTypeEnum.TravelToSite:
            case ActivityTypeEnum.TravelToDepot:
            case ActivityTypeEnum.TravelToNextJob:
            case ActivityTypeEnum.SiteHazardAssessment:
            case ActivityTypeEnum.WorkSpaceActive:
            case ActivityTypeEnum.EndOfShift:
            case ActivityTypeEnum.PreStartTruckCheck:
                return objectTypes.jobactivity;
            case ActivityTypeEnum.SiteDeployment:
            case ActivityTypeEnum.SiteAmendmentRecord:
            case ActivityTypeEnum.SiteRetrievalRecord:
                return objectTypes.siteactivity;
            case ActivityTypeEnum.OnSiteRecord:
                return objectTypes.onsiterecord;
            case ActivityTypeEnum.GearLeftOnSiteRecord:
                return objectTypes.geargeftonsiterecord;
        }
    }
    
    service.addActivityV2 = function(jobId, activity){
        let defer = $q.defer();
        let objJobId = objectTypes.job + jobId;
        DBSvc.get(objJobId).then(function(job){
            if(job){
                activity.id = (new ObjectId()).toString();
                activity._id = getObjectType(activity.activityType) + activity.id;//objectTypes.jobactivity + activity.id;
                if(!job.activities)
                    job.activities = [];
                job.activities.push(activity);
                DBSvc.save([job]).then(function(response){
                    RemoteSyncSvc.add(activity).then(function(response){
                        defer.resolve(activity.id);
                    });                    
                });
            }
        });

        return defer.promise;
    };

    service.stopActivity = function(jobId, activity){
        let defer = $q.defer();
        let objJobId = objectTypes.job + jobId;
        DBSvc.get(objJobId).then(function(job){
            if(job){
                //if(activity.activityType != ActivityTypeEnum.OnSiteRecord){
                let activityData = $linq.Enumerable().From(job.activities).FirstOrDefault(null, 'f => f.id == "' + activity.id + '"');
                console.log(activityData,"---addactivityData---")
                    if(activityData){
                        activityData.endTime = activity.endTime;
                        activityData.endCoordinates = activity.endCoordinates;
                        activityData.notes = activity.notes;
                        activityData.description = activity.description;
                        activityData.truckAuditId = activity.truckAuditId;
                        if(activityData.activityType == ActivityTypeEnum.OnSiteRecord)
                            activityData.siteCheckItems = activity.siteCheckItems;

                        DBSvc.save([job]).then(function(response){
                            RemoteSyncSvc.add(activityData).then(function(response){
                                defer.resolve(activityData.id);
                            });
                        });
                    }                
            }
        });

        return defer.promise;
    };

    service.getActivity = function(jobId, id){
        let defer = $q.defer();
        let objId = objectTypes.job + jobId;
        let result = null;
        DBSvc.get(objId).then(function(job){
            if(job){     
                result = $linq.Enumerable().From(job.activities).FirstOrDefault(null, 'f => f.id == "' + id + '"');
                if(result){
                    defer.resolve(result);
                }
            }
            defer.resolve(result);
        }, function(error){
            console.log(error);
            defer.reject(null);
        });
        
        return defer.promise;
    };

    service.cancelActivity = function(jobId, activityId){   
        let defer = $q.defer();
        let objJobId = objectTypes.job + jobId;
        DBSvc.get(objJobId).then(function(job){
            if(job){
                let activityData = $linq.Enumerable().From(job.activities).FirstOrDefault(null, 'f => f.id == "' + activityId + '"');
                if(activityData){
                    activityData.isCancelled = true;
                    DBSvc.save([job]).then(function(response){
                        //add to sync
                        RemoteSyncSvc.add(activityData).then(function(response){
                            defer.resolve(activityData.id);
                        });
                    });
                }                                
            }
        });

        return defer.promise;
    };

    service.updateActivityV2 = function(jobId, activity){
        let defer = $q.defer();
        let objJobId = objectTypes.job + jobId;
        DBSvc.get(objJobId).then(function(job){
            if(job){
                let activityData = $linq.Enumerable().From(job.activities).FirstOrDefault(null, 'f => f.id == "' + activity.id + '"');
                if(activityData){                                        
                    if(activityData.activityType == ActivityTypeEnum.SiteDeployment){
                        activityData.tsls = activity.tsls;
                        activityData.addedClosureTypes = activity.addedClosureTypes;
                    }
                    activityData.notes = activity.notes;
                    activity.action = ActivityActionEnum.UPDATE;
                    DBSvc.save([job]).then(function(response){
                        RemoteSyncSvc.add(activity).then(function(response){
                            defer.resolve(true);
                        });
                    });
                }
                //#region 
                // let activityData = $linq.Enumerable().From(job.activities).FirstOrDefault(null, 'f => f.id == "' + activity.id + '"');
                // let activityDataNdx = $linq.Enumerable().From(job.activities).IndexOf(activityData);
                // if(activityData){                    
                //     activityData = job.activities[activityDataNdx];
                //     if(activityData.activityType == ActivityTypeEnum.SiteDeployment){
                //         activityData.tsls = activity.tsls;
                //         activityData.addedClosureTypes = activity.addedClosureTypes;
                //     }
                //     activityData.notes = activity.notes;
                //     activity.action = ActivityActionEnum.UPDATE;
                //     DBSvc.save([job]).then(function(response){
                //         RemoteSyncSvc.add(activity).then(function(response){
                //             defer.resolve(true);
                //         });
                //     });
                // }
                //#endregion
            }
        });

        return defer.promise;        
    };

    service.updateSiteCheckItem = function(jobId, activityId, itemIndex, item){
        let defer = $q.defer();
        let objJobId = objectTypes.job + jobId;
        DBSvc.get(objJobId).then(function(job){
            if(job){
                let activityData = $linq.Enumerable().From(job.activities).FirstOrDefault(null, 'f => f.id == "' + activityId + '"');                
                if(activityData){
                    activityData.siteCheckItems[itemIndex] = item;
                    DBSvc.save([job]).then(function(response){
                        defer.resolve(true);
                    });
                }
            }
        });

        return defer.promise;        
    };

    service.completeJobV2 = function(job){
        let defer = $q.defer();
        DBSvc.get(objectTypes.job + job.id).then(function(jobData){
            if(job != null){                
                jobData.complete = true;
                DBSvc.save([jobData]).then(function(response){
                    RemoteSyncSvc.add(jobData).then(function(response){
                        defer.resolve(true);
                    });
                }, function(error){
                    console.log(error);
                    defer.reject(error);
                });
            }
        });

        return defer.promise;
    };
    
    service.endShiftTeamMember = function(member){
        let defer = $q.defer();
        //params: jobid, teammemberid
        let objJobId = objectTypes.job + member.jobId;
        DBSvc.get(objJobId).then(function(job){
            if(!job)
                return defer.resolve(true);

            let teamMember = $linq.Enumerable().From(job.teamMembers).FirstOrDefault(null, 'f => f.id == "' + member.id +'"');
            if(teamMember){
                teamMember.endOfShiftTime = member.endOfShiftTime;
                DBSvc.save([job]).then(function(response){
                    RemoteSyncSvc.add(teamMember).then(function(response){
                        defer.resolve(true);
                    });
                });
            }            
        });
        // RemoteSyncSvc.add(activity).then(function(response){
        //     defer.resolve(true);
        // });
        

        // setTimeout(function(){
        //     console.log('endshift ok');
        //     console.log(member);
        //     defer.resolve(true);
        // }, 5000);

        return defer.promise;
    };
    
    return service;
}]);