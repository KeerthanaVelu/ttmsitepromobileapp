﻿mainApp.factory('TenantSvc', ['ApiSvc', '$resource', 'AccountSvc', '$q', function (ApiSvc, $resource, AccountSvc, $q) {
    var service = {};
    var authHeader = { 'Authorization': 'Bearer ' + AccountSvc.getAccessToken() };
    service.getInfo = function (tenantId) {
        console.log('getInfo');
        
        var defer = $q.defer();
        var resource = $resource(ApiSvc.tenantInfo, null, {
            'getInfo': { method: 'GET', url: ApiSvc.tenantInfo, isArray: false, params: { tenantId: '@tenantId' }, headers: authHeader }
        });
        resource.getInfo({ tenantId: tenantId }, null, function (result) {
            defer.resolve(result);
        }, function (error) {
            defer.reject(error);
        });

        return defer.promise;
    };

    service.getStmssByBranch = function (tenantid, branchid) {
        var resource = $resource('/api/tenants', {}, {
            'getStmssByBranch': { method: 'GET', url: ApiSvc.tenantStmss, isArray: true, headers: authHeader, params: { tenantid: '@tenantid', branchid: '@branchid' } }
        });
        return resource.getStmssByBranch({ tenantid: tenantid, branchid: branchid }).$promise;
    };

    return service;
}]);