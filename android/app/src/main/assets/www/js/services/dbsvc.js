﻿mainApp.factory('DBSvc', ['$cordovaSQLite', '$q', '$window', '$linq', function ($cordovaSQLite, $q, $window, $linq) {
    console.log('DBSvc startup');
    var service = {};
    var siteprodb = null;
    var mypouchdb;
    var token, tenantId;    
    
    service.init = function () {
        var defer = $q.defer();
        siteprodb = $cordovaSQLite.openDB({ name: 'sitepro.db', iosDatabaseLocation: 'default' });
        var createJobsTblNew = "create table if not exists jobs(id string, tempId string, jobName string, document text)";
        var createAccountsTbl = 'create table if not exists account(document text)';
        var createStaffTbl = 'create table if not exists staff(document text)';
        var createQDataTbl = 'create table if not exists qdata(id string, data string, createdOn datetime)';
        var createTrucksTbl = 'create table if not exists trucks(document text)';
        var createTruckAuditsTbl = 'create table if not exists truckaudits(id string, createdOn datetime, truckId string, document text)';
        var createRcasTbl = 'create table if not exists rcas(id string, organisationName string, regions, string, tenantId string)';
        var createContractorsTbl = 'create table if not exists contractors()';
        var tasks = [$cordovaSQLite.execute(siteprodb, createJobsTblNew), $cordovaSQLite.execute(siteprodb, createAccountsTbl), $cordovaSQLite.execute(siteprodb, createStaffTbl),
            $cordovaSQLite.execute(siteprodb, createQDataTbl), $cordovaSQLite.execute(siteprodb, createTrucksTbl), $cordovaSQLite.execute(siteprodb, createTruckAuditsTbl)];

        var tasksQ = $q.when();
        tasks.forEach((task) => {
            tasksQ = tasksQ.then(task);
        });
        tasksQ.then(function(result){
            console.log('all tables created');
            defer.resolve(true);
        }, function(error){
            console.log(error);
            defer.reject(error);
        });        
        return defer.promise;

    };

    service.saveTokenParties = function (token, parties, staff) {
        var defer = $q.defer();
        var tokenParties = { token: token, parties: parties };//, staff: staff };
        var saveTokenDml = 'insert into account(document) values(?)';
        var saveStaffDml = 'insert into staff(document) values(?)';
        var saveTrucksDml = 'insert into trucks(document) values(?)';

        $q.all($cordovaSQLite.execute(siteprodb, saveTokenDml, [JSON.stringify(tokenParties)]), $cordovaSQLite.execute(siteprodb, saveStaffDml, [JSON.stringify(staff)]),
            $cordovaSQLite.execute(siteprodb, saveTrucksDml, [JSON.stringify(parties.trucks)])).then(function () {
                defer.resolve(true);
            }, function (error) {
                defer.reject(error);
            });

        return defer.promise;
    };

    service.saveAccountDetails = function(account){
        var defer = $q.defer();
        account._id = objectTypes.account + account.id;
        mypouchdb.get(account._id, function(error, doc){
            if(doc)
                account._rev = doc._rev;
            mypouchdb.put(account, {force: true}, function(error, response){
                if(error) defer.reject(error)
                else{
                    token = account.access_token;
                    tenantId = account.tenantId;
                    defer.resolve(true);
                }
            });
        });

        return defer.promise;
    }
    
    service.getAccessToken = function(){
        return token;
    }
    
    service.getTenantId = function(){
        return tenantId;
    }
    
    service.saveToken = function (token) {
        var defer = $q.defer();
        var tokenParties = { token: token };
        var qryAccount = 'select * from account';
        var updateAccount = 'update account set document=?';

        $cordovaSQLite.execute(siteprodb, qryAccount).then(function (result) {
            //console.log(result);
            var parsed = JSON.parse(result.rows.item(0).document);
            parsed.token = token;
            $cordovaSQLite.execute(siteprodb, updateAccount, [JSON.stringify(parsed)]).then(function (result) {
                defer.resolve({ token: parsed.token });
            }, function (error) {
                defer.reject(error);
            });
        });

        return defer.promise;
    };

    service.getAccount = function () {
        var defer = $q.defer();
        var accountQry = 'select * from account';
        var staffQry = 'select * from staff';
        var jobsQry = 'select * from jobs';
        var trucksQry = 'select * from trucks'

        var queries = {
            account: $cordovaSQLite.execute(siteprodb, accountQry), staff: $cordovaSQLite.execute(siteprodb, staffQry), jobs: $cordovaSQLite.execute(siteprodb, jobsQry),
            trucks: $cordovaSQLite.execute(siteprodb, trucksQry)
        };

        $q.all(queries).then(function (qryResults) {
            var result = null;

            if (qryResults.account.rows.length > 0 || qryResults.staff.rows.length > 0 || qryResults.jobs.rows.length > 0 || qryResults.trucks.rows.length > 0) {
                result = {};
                result = qryResults.account.rows.length > 0 ? JSON.parse(qryResults.account.rows.item(0).document) : {};
                result.staff = qryResults.staff.rows.length > 0 ? JSON.parse(qryResults.staff.rows.item(0).document) : [];
                result.jobs = qryResults.jobs.rows.length > 0 ? JSON.parse(qryResults.jobs.rows.item(0).document) : [];
                result.trucks = qryResults.trucks.rows.length > 0 ? JSON.parse(qryResults.trucks.rows.item(0).document) : [];
            }

            //console.log('result');
            //console.log(result);
            defer.resolve(result);
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    };

    service.saveStaff = function (staff) {
        var defer = $q.defer();
        var updateDml = 'update staff set id =?';

        if (siteprodb === null)
            siteprodb = $cordovaSQLite.openDB({ name: 'sitepro.db', iosDatabaseLocation: 'default' });

        $cordovaSQLite.execute(siteprodb, updateDml, [JSON.stringify(staff)]).then(function (result) {
            defer.resolve(result);
        }, function (error) {
            defer.reject(error);
        });

        return defer.promise;
    };

    service.updateStaff = function (staff) {
        var defer = $q.defer();
        var updateDml = 'update staff set id =?';

        if (siteprodb === null)
            siteprodb = $cordovaSQLite.openDB({ name: 'sitepro.db', iosDatabaseLocation: 'default' });

        $cordovaSQLite.execute(siteprodb, updateDml, [JSON.stringify(staff)]).then(function (result) {
            defer.resolve(result);
        }, function (error) {
            defer.reject(error);
        });

        return defer.promise;
    };

    function ifJobsEmpty() {

        var defer = $q.defer();

        var qry = 'select * from jobs';
        $cordovaSQLite.execute(siteprodb, qry).then(function (result) {
            if (result.rows.length == 0)
                defer.resolve(true);
            else
                defer.resolve(false);
        }, function (error) {
            defer.reject(error);
        });

        return defer.promise;
    }

    service.saveJobs = function (jobs) {
        var defer = $q.defer();
        var updateDml = 'update jobs set document=?';
        var insertDml = 'insert into jobs(document) values(?)';

        ifJobsEmpty().then(function (empty) {
            if (empty) {
                $cordovaSQLite.execute(siteprodb, insertDml, [JSON.stringify(jobs)]).then(function (result) {
                    console.log('insert jobs');
                    defer.resolve(true);
                }, function (error) {
                    console.log(error);
                    defer.reject(error);
                });
            }
            else {
                $cordovaSQLite.execute(siteprodb, updateDml, [JSON.stringify(jobs)]).then(function (result) {
                    //console.log('updates jobs');
                    defer.resolve(true);
                }, function (error) {
                    console.log(error);
                    defer.reject(error);
                });
            }
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    };

    service.getQData = function (dataId) {
        var defer = $q.defer();

        var qry = 'select * from qdata where id = ?';
        return $cordovaSQLite.execute(siteprodb, qry, [dataId]).then(function (result) {
            if (result.rows.length == 0)
                defer.resolve(null);
            else
                defer.resolve(JSON.parse(result.rows.item(0).data));
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    };

    service.getNextQData = function () {
        var defer = $q.defer();
        var qry = 'select * from qdata order by createdOn asc limit 1';
        $cordovaSQLite.execute(siteprodb, qry).then(function (result) {
            if (result.rows.length > 0) {
                var dataResult = JSON.parse(result.rows.item(0).data);
                dataResult.qid = result.rows.item(0).id;
                if (dataResult.startTime && dataResult.startTime.length > 0)
                    dataResult.startTime = new Date(dataResult.startTime);
                if (dataResult.endTime && dataResult.endTime.length > 0)
                    dataResult.endTime = new Date(dataResult.endTime);
                if (dataResult.createdOn && dataResult.createdOn.length > 0)
                    dataResult.createdOn = new Date(dataResult.createdOn);
                if (dataResult.modifiedOn && dataResult.modifiedOn.length > 0)
                    dataResult.modifiedOn = new Date(dataResult.modifiedOn);

                defer.resolve(dataResult);
            }
            else
                defer.resolve(null);

            $cordovaSQLite.execute(siteprodb, 'select * from qdata order by createdOn asc').then(function (result) {
            });
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });
        
        return defer.promise;
    };

    service.getAllQData = function () {
        var defer = $q.defer();
        var qry = 'select * from qdata';

        $cordovaSQLite.execute(siteprodb, qry).then(function (result) {
            //console.log(result);
            if (result.rows.length > 0) {
                var results = [];

                for (var i = 0; i < result.rows.length; i++) {
                    console.log(result.rows.item(i));
                    results.push(JSON.parse(result.rows.item(i).data));
                }

                defer.resolve(results);
            }
            else
                defer.resolve(null);
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    };

    service.saveQData = function (data) {
        var defer = $q.defer();

        var insertDml = 'insert into qdata(id, data, createdOn) values(?,?,?)';
        var qid = (new $window.ObjectId()).toString();
        $cordovaSQLite.execute(siteprodb, insertDml, [qid, JSON.stringify(data), new Date()]).then(function (result) {
            //console.log(result);
            defer.resolve(result.rowsAffected);
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    };

    service.saveQDataWId = function (qid, data) {
        var defer = $q.defer();

        var insertDml = 'insert into qdata(id, data, createdOn) values(?,?,?)';
        //var qid = qid || (new $window.ObjectId()).toString();
        $cordovaSQLite.execute(siteprodb, insertDml, [qid, JSON.stringify(data), new Date()]).then(function (result) {
            //console.log(result);
            defer.resolve(result.rowsAffected);
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    };

    service.getQDataCount = function () {
        var defer = $q.defer();
        var qry = 'select count(*) from qdata';

        return defer.promise;
    };

    service.deleteQData = function (id) {
        var defer = $q.defer();
        //var qry = 'delete from qdata where createdOn = (select createdOn from qdata where id = ? order by createdOn asc limit 1)';
        var qry = 'delete from qdata where id = ?';

        $cordovaSQLite.execute(siteprodb, qry, [id]).then(function (result) {
            defer.resolve(result.rowsAffected);
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    };

    service.getLastAuditOfTruck = function (truckId) {
        var defer = $q.defer();
        var qry = 'select * from truckaudits where truckId = ? order by createdOn desc limit 1';
        $cordovaSQLite.execute(siteprodb, qry, [truckId]).then(function (result) {
            //console.log(result);
            if (result.rows.length == 0) {
                defer.resolve(null);
                return;
            }
            var result = JSON.parse(result.rows.item(0).document);
            if (result.regoExpiry)
                result.regoExpiry = new Date(result.regoExpiry);
            if (result.cofExpiry)
                result.cofExpiry = new Date(result.cofExpiry);
            defer.resolve(result);
        }, function (error) {
            defer.reject(error);
        });

        return defer.promise;
    };

    service.saveTruckAudit = function (audit) {
        var defer = $q.defer();

        var updateDml = 'insert into truckaudits(id, createdOn, truckId, document) values(?,?,?,?)';        
        $cordovaSQLite.execute(siteprodb, updateDml, [audit.id, new Date(), audit.truckId, JSON.stringify(audit)]).then(function (result) {           
            defer.resolve(true);
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    };

    service.getTruckAudit = function (id) {
        var defer = $q.defer();
        var qry = 'select * from truckaudits where id = ?';
        $cordovaSQLite.execute(siteprodb, qry, [id]).then(function (result) {
            //console.log(result);
            if (result.rows.length == 0) {
                defer.resolve(null);
                return;
            }
            var result = JSON.parse(result.rows.item(0).document);
            if (result.regoExpiry)
                result.regoExpiry = new Date(result.regoExpiry);
            if (result.cofExpiry)
                result.cofExpiry = new Date(result.cofExpiry);
            defer.resolve(result);
            //defer.resolve(JSON.parse(result.rows.item(0).document));
        }, function (error) {
            defer.reject(error);
        });


        return defer.promise;
    };

    service.updateTruckAuditId = function (tempId, serverId) {
        var defer = $q.defer();
        var updateDml = 'update truckaudits set id=? where id=?';
        $cordovaSQLite.execute(siteprodb, updateDml, [serverId, tempId]).then(function (result) {
            //console.log(result);
            defer.resolve(true);
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    };

    service.saveTrucks = function (trucks) {
        var defer = $q.defer();
        var saveTrucksDml = 'insert into trucks(document) values(?)';
        $cordovaSQLite.execute(siteprodb, saveTrucksDml, [JSON.stringify(trucks)]).then(function () {
            defer.resolve(true);
        }, function (error) {
            defer.reject(error);
        });

        return defer.promise;
    };

    service.getJobById = function (id) {
        console.log('DBSvc.getJobById() start');
        var defer = $q.defer();
        var qry = 'select * from jobs';
        $cordovaSQLite.execute(siteprodb, qry).then(function (qryresult) {
            console.log(qryresult);
            if (qryresult.rows.length == 0) {
                defer.resolve(null);
                return;
            }
            var result = JSON.parse(qryresult.rows.item(0).document);
            var job = $linq.Enumerable().From(result).FirstOrDefault(null, 'f => f.id == "' + id + '"');
            defer.resolve(job);
        }, function (error) {
            defer.reject(error);
        });

        return defer.promise;
    };

    service.saveQToFirst = function (data) {
        var defer = $q.defer();
        var qry = 'select createdOn from qdata order by createdOn asc limit 1';        
        $cordovaSQLite.execute(siteprodb, 'select createdOn from qdata order by createdOn asc limit 1').then(function (result) {
            console.log(result.rows.length);
            console.log('Q length: ' + result.rows.length);
            console.log(result.rows.item(0));
            var newCreatedOn = moment(moment(result.rows.item(0).createdOn)).subtract(1, 's');
            var qid = (new $window.ObjectId()).toString();
            $cordovaSQLite.execute(siteprodb, 'insert into qdata(id, data, createdOn) values(?,?,?)', [qid, JSON.stringify(data), newCreatedOn]).then(function (result) {
                defer.resolve(result.rowsAffected);
            }, function (error) {
                console.log(error);
                defer.reject(error);
            });
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    };

    //#region POUCHDB IMPLEMENTATION
    service.initPouchDB = function(){
        let defer = $q.defer();
        console.log('initPouchDB');
        //PouchDB.debug.enable('*');
        mypouchdb = new PouchDB('mypouchdb.db', {adapter: 'cordova-sqlite', iosDatabaseLocation: 'default'});
        mypouchdb.createIndex({index: {fields: ['createdOn']}}).then(function(){
            defer.resolve(true);
        });        

        return defer.promise;
    };

    service.hasSeedData = function(){
        var defer = $q.defer();
        mypouchdb.allDocs({include_docs: true, startkey: 'account_', endkey: 'account_\ufff0', limit: 1}, function(error, response){
            if(error) defer.reject(error);
            else{
                console.log(response);
                if(response.rows.length == 0)
                    defer.resolve(false);
                else{
                    defer.resolve(!!response.rows[0].doc.hasSeedData);                    
                }                    
            }
        });
        return defer.promise;
    };

    service.save = function(data){
        var defer = $q.defer();
        mypouchdb.bulkDocs(data, function(error, response){
            console.log(response);
            if(error) defer.reject(error);
            else defer.resolve(true);
        });

        return defer.promise;
    };

    service.setHasSeedData = function(hasSeedData){
        var defer = $q.defer();        
        mypouchdb.allDocs({include_docs: true, startkey: objectTypes.account, endkey: objectTypes.account + '\ufff0', limit: 1}, function(error, response){
            if(error){
                defer.reject(error); return;
            }
            var account = response.rows[0].doc;
            account.hasSeedData = hasSeedData;
            mypouchdb.put(account, function(error, response){
                if(error)
                    defer.reject(error);
                else
                    defer.resolve(true);
            })
        });
        return defer.promise;
    };

    service.getAllRcas = function(){
        var defer = $q.defer();
        mypouchdb.allDocs({include_docs: true, startkey: objectTypes.rca, endkey: objectTypes.rca + '\ufff0'}, function(error, response){
            if(error)
                defer.reject(error);                
            else{
                if(response.rows.length === 0)
                    defer.resolve([]);
                else{
                    let docs = $linq.Enumerable().From(response.rows).Select('s => s.doc').OrderBy('o => o.organisationName').ToArray();
                    defer.resolve(docs);
                }
            }
        });

        return defer.promise;
    };

    service.getRcaById = function(id){
        var defer = $q.defer();
        mypouchdb.get(objectTypes.rca + id, function(error, doc){
            console.log(doc);
            if(error)
                defer.resolve(null);
            else
                defer.resolve(doc);
        });
        return defer.promise;
    };

    service.getAllContractors = function(){
        var defer = $q.defer();
        mypouchdb.allDocs({include_docs: true, startkey: objectTypes.contractor, endkey: objectTypes.contractor + '\ufff0'}, function(error, response){
            if(error)
                defer.reject(error);
            else{
                if(response.rows.length == 0)
                    defer.resolve([]);
                else{
                    var docs = $linq.Enumerable().From(response.rows).Select('s => s.doc').OrderBy('o => o.contractorName').ToArray();                    
                    defer.resolve(docs);
                }
            }
        });

        return defer.promise;
    };

    service.getAllAssetOwners = function(){
        let defer = $q.defer();
        mypouchdb.allDocs({include_docs: true, startkey: objectTypes.assetowner, endkey: objectTypes.assetowner + '\ufff0'}, function(error, response){
            if(error)
                return defer.reject(error);
            
            if(response.rows.length == 0)
                defer.resolve([]);
            else{
                let docs = $linq.Enumerable().From(response.rows).Select('s => s.doc').OrderBy('o => o.organisationName').ToArray();                    
                defer.resolve(docs);
            }            
        });

        return defer.promise;
    };

    service.getAllTrucks = function(){
        let defer = $q.defer();
        mypouchdb.allDocs({include_docs: true, startkey: objectTypes.truck, endkey: objectTypes.truck + '\ufff0'}, function(error, response){
            if(error)
                return defer.reject(error);
                
            if(response.rows.length == 0)
                return defer.resolve([]);
            
            let docs = $linq.Enumerable().From(response.rows).Select('s => s.doc').OrderBy('o => o.truckNumber').ToArray();
            defer.resolve(docs);
        });

        return defer.promise;
    };

    service.addJob = function(job){
        let defer = $q.defer();
        mypouchdb.put(job).then(function(response){
            console.log(response);
            defer.resolve(true);
        }).catch(function(error){
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    };

    service.get = function(id){
        let defer = $q.defer();
        mypouchdb.get(id).then(function(response){
            defer.resolve(response);
        }, function(error){
            console.log('[611]:');
            console.log(error);
            if(error.status === 404)
                return defer.resolve(null);
            defer.reject(error);
        });

        return defer.promise;
    };

    service.getAllJobs = function(){
        let defer = $q.defer();
        mypouchdb.find({
            selector: { createdOn: {$gte: null}, _id: {$regex: '^' + objectTypes.job}, archived: { $exists: false } },
            fields: ['id', 'jobName', 'createdOn', 'archived', 'expectedStartDate'],
            sort: [{createdOn: 'desc'}]
        }).then(function(response){
            defer.resolve(response.docs);
        }, function(error){
            console.log('[629]: getAllJobs ERROR!');
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    };

    service.getArchivedJobs = function(){
        let defer = $q.defer();
        mypouchdb.find({
            selector: { createdOn: {$gte: null}, _id: {$regex: '^' + objectTypes.job}, archived: true},
            fields: ['_id', 'id', 'jobName', 'createdOn', 'archived', 'archivedOn', 'expectedStartDate', '_rev'],
            sort: [{createdOn: 'desc'}]
        }).then(function(response){
            defer.resolve(response.docs);
        }, function(error){
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    };

    service.addToSyncQ = function(data){
        //console.log('service.addToSyncQ');
        let defer = $q.defer();
        delete data._rev;
        let objId = (new ObjectId()).toString();
        data._id = objectTypes.qdata + data._id + objId;
        data.createdOn = new Date();
        //console.log(data._id);
        mypouchdb.put(data).then(function(response){
            //console.log(response);
            defer.resolve(true);
        }, function(error){
            console.log(error);
            if(error.status == 409)
                defer.resolve(true);
            else defer.reject(error);
        });

        return defer.promise;
    };

    service.getNextQData = function(){
        let defer = $q.defer();
        mypouchdb.find({
            selector: { createdOn: {$gte: null}, _id: {$regex: '^' + objectTypes.qdata}},
            sort: [{createdOn: 'asc'}],
            limit: 1
        }).then(function(response){
            //console.log(response);
            if(response.docs.length == 0)
                defer.resolve(null);
            else
                defer.resolve(response.docs[0]);
        }, function(error){
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    };

    service.removeQData = function(data){
        return mypouchdb.remove(data);
    };

    service.getAccountDetails = function(){
        let defer = $q.defer();
        mypouchdb.find({
            selector: { _id: {$regex: '^' + objectTypes.account}}
        }).then(function(response){
            console.log(response);
            if(response.docs.length == 0)
                defer.resolve(null);
            else
                defer.resolve(response.docs[0]);
        }, function(error){
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
        //return mypouchdb.get(objectTypes.account);
    };

    service.updateJobRemoteId = function(id, rId){
        let defer = $q.defer();
        mypouchdb.get(objectTypes.job + id).then(function(doc){
            doc.rId = rId;
            mypouchdb.put(doc).then(function(response){
                defer.resolve(true);
            });
        }, function(error){
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;        
    };

    service.getGeneralInfo = function(id){
        console.log('service.getGeneralInfo');
        let defer = $q.defer();
        mypouchdb.find({selector: {_id: objectTypes.job + id}, 
            fields:['_id', 'id', 'jobName', 'reference', 'description', 'poNumber', 'address', 'roadLevels', 'archived'], limit: 1}).then(function(response){
                console.log(response);
                if(response.docs.length === 1)
                    defer.resolve(response.docs[0]);
                else
                    defer.resolve(null);
            }, function(error){
                console.log(error);
                defer.reject(error);
            });
        return defer.promise;
    };

    service.getGeneralNotes = function(id){
        console.log('service.getGeneralNotes');
        let defer = $q.defer();
        mypouchdb.find({selector: {_id: objectTypes.job + id}, 
            fields:['_id', 'id', 'generalNotes', 'archived'], limit: 1}).then(function(response){
                if(response.docs.length === 1){
                    let doc = response.docs[0];
                    let generalNotes  = doc.generalNotes;
                    generalNotes.id = doc.id;
                    generalNotes.archived = doc.archived;
                    defer.resolve(generalNotes);
                }
                else
                    defer.resolve(null);
            }, function(error){
                console.log(error);
                defer.resolve(null);
                //defer.reject(error);
            });
        return defer.promise;
    };

    service.getParties = function(id){
        console.log('service.getParties');
        let defer = $q.defer();
        mypouchdb.find({selector: {_id: objectTypes.job + id}, 
            fields:['_id', 'id', 'parties'], limit: 1}).then(function(response){
                console.log(response);
                if(response.docs.length === 1){
                    let doc = response.docs[0];
                    let parties = doc.parties;
                    parties.id = doc.id;
                    defer.resolve(parties);
                }
                else
                    defer.resolve(null);
            }, function(error){
                console.log(error);
                defer.reject(error);
            });
        return defer.promise;
    };

    service.getTeam = function(id){
        //console.log('service.getTeam');
        //return only active
        let defer = $q.defer();
        mypouchdb.find({selector: {_id: objectTypes.job + id}, 
            fields:['_id', 'id', 'teamMembers'], limit: 1}).then(function(response){
                //console.log(response);
                if(response.docs.length === 1){
                    let doc = response.docs[0];
                    let active = $linq.Enumerable().From(doc.teamMembers).Where('w => w.endOfShiftTime == null && w.dataMode != ' + DataActionEnum.DELETE).ToArray();
                    defer.resolve(active);
                }
                else
                    defer.resolve([]);
            }, function(error){
                console.log(error);
                defer.reject(error);
            });
        return defer.promise;
    };

    service.getAllTeam = function(id){
        let defer = $q.defer();
        mypouchdb.find({selector: {_id: objectTypes.job + id}, 
            fields:['_id', 'id', 'teamMembers'], limit: 1}).then(function(response){
                //console.log(response);
                if(response.docs.length === 1){
                    let doc = response.docs[0];
                    //let active = $linq.Enumerable().From(doc.teamMembers).Where('w => w.endOfShiftTime == null && w.dataMode != ' + DataActionEnum.DELETE).ToArray();
                    defer.resolve(doc.teamMembers);
                }
                else
                    defer.resolve([]);
            }, function(error){
                console.log(error);
                defer.reject(error);
            });
        return defer.promise;
    };

    service.getStaffByUser = function(userId){
        let defer = $q.defer();
        mypouchdb.find({selector: {userId: userId}, limit: 1}).then(function(response){
            if(response.docs.length === 1)
                defer.resolve(response.docs[0]);
            else
                defer.resolve(null);
        }, function(error){
            defer.resolve(null);
        });

        return defer.promise;
    };

    service.getAllStaff = function(){
        let defer = $q.defer();
        mypouchdb.allDocs({include_docs: true, startkey: objectTypes.staff, endkey: objectTypes.staff + '\ufff0'}, function(error, response){
            console.log(response);
            if(error)
                defer.resolve([]);
            else{
                if(response.rows.length === 0)
                    defer.resolve([]);
                else{
                    let staff = $linq.Enumerable().From(response.rows).Select('s => s.doc').OrderBy('o => o.fullName').ToArray();
                    defer.resolve(staff);
                }
            }
        });

        return defer.promise;
    };

    service.getTenantBranches = function(){
        let defer = $q.defer();
        mypouchdb.allDocs({include_docs: true, startkey: objectTypes.tenantbranch, endkey: objectTypes.tenantbranch + 'u\fff0'}, function(error, response){
            if((error && error.status === 404) || response.rows.length === 0)
                return defer.resolve([]);
            let results = $linq.Enumerable().From(response.rows).Select('s => s.doc').OrderBy('o => o.branch').ToArray();
            defer.resolve(results);
        });

        return defer.promise;
    };

    service.updateTeamMemberRemId = function(jobId, teamMemberId, rId){
        console.log('updateTeamMemberRemId');
        let defer = $q.defer();
        let objJobId = objectTypes.job + jobId;
        mypouchdb.get(objJobId).then(function(job){
            let teamMember = $linq.Enumerable().From(job.teamMembers).FirstOrDefault(null, 'f => f.id == "' + teamMemberId + '"');
            if(teamMember){
                teamMember.rId = rId;
                mypouchdb.put(job).then(function(response){
                    defer.resolve(true);
                });
            }
            else defer.resolve(true);         
        }, function(error){
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    };

    service.getTeamMemberRemId = function(jobId, teamMemberId){
        let defer = $q.defer();
        let objJobId = objectTypes.job + jobId;
        mypouchdb.get(objJobId).then(function(job){
            let teamMember = $linq.Enumerable().From(job.teamMembers).FirstOrDefault(null, 'f => f.id == "' + teamMemberId + '"');
            if(teamMember) defer.resolve(teamMember.rId);
            else defer.resolve(null);         
        }, function(error){
            console.log(error);
            defer.resolve(null);
        });

        return defer.promise;
    };

    service.addStaff = function(staff){
        let defer = $q.defer();
        mypouchdb.put(staff).then(function(response){
            defer.resolve(true);
        }).catch(function(error){
            defer.reject(error);
        });

        return defer.promise;
    };

    service.updateStaffRemId = function(staffId, rId){
        let defer = $q.defer();
        let objStaffId = objectTypes.staff + staffId;
        mypouchdb.get(objStaffId).then(function(staff){            
            staff.rId = rId;
            mypouchdb.put(staff).then(function(response){
                defer.resolve(true);
            }, function(error){
                console.log(error);
                defer.reject(error);
            });            
        }, function(error){
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;        
    };


    service.updateStaffData = function (staffData) {
        let defer = $q.defer();
        let objStaffId = objectTypes.staff + staffData.id;
        mypouchdb.get(objStaffId).then(function (staff) {
            staff.stmsCertificationNo = staffData.stmsCertificationNo;
            staff.expiryDate = staffData.expiryDate;
            staff.signiture = staffData.signiture;
            mypouchdb.put(staff).then(function (response) {
                defer.resolve(true);
            }, function (error) {
                console.log(error);
                defer.reject(error);
            });
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    };

    service.updatesigniture = function (id, signiture) {
        let defer = $q.defer();
        let objStaffId = objectTypes.staff + id;
        mypouchdb.get(objStaffId).then(function (staff) {
            staff.signiture = signiture;
            mypouchdb.put(staff).then(function (response) {
                defer.resolve(true);
            }, function (error) {
                console.log(error);
                defer.reject(error);
            });
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    };

    service.updateActivityRemId = function(jobId, activityId, rId){
        let defer = $q.defer();
        let objJobId = objectTypes.job + jobId;
        mypouchdb.get(objJobId).then(function(job){
            let activity = $linq.Enumerable().From(job.activities).FirstOrDefault(null, 'f => f.id == "' + activityId + '"');
            if(activity){
                activity.rId = rId;
                mypouchdb.put(job).then(function(response){
                    defer.resolve(true);
                });
            }
            else defer.resolve(true);         
        }, function(error){
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;        
    };

    service.getLastAudit = function(truckId){
        let defer = $q.defer();
        mypouchdb.find({            
            selector: { createdOn: {$gte: null}, truckId: truckId},
            sort: [{createdOn: 'desc'}],
            limit: 1
        }).then(function(response){
            //console.log(response);
            if(response.docs.length == 0)
                defer.resolve(null);
            else
                defer.resolve(response.docs[0]);
        }, function(error){
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    };

    service.updateTruckAuditRemId = function(id, rId){
        let defer = $q.defer();
        let objId = objectTypes.truckaudit + id;
        mypouchdb.get(objId).then(function(truckAudit){
            if(!truckAudit)
                return defer.resolve(true);

            truckAudit.rId = rId;
            mypouchdb.put(truckAudit).then(function(response){
                defer.resolve(true);
            }, function(error){
                console.log(error);
                defer.resolve(true);
            });
        }, function(error){
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    };

    service.getTruckAuditV2 = function(id){
        let defer = $q.defer();
        let objId = objectTypes.truckaudit + id;
        mypouchdb.get(objId).then(function(truckAudit){
            defer.resolve(truckAudit);
        }, function(error){
            defer.resolve(null);
        });

        return defer.promise;
    }

    service.deleteJob = function(id){
        let defer = $q.defer();
        let jobId = objectTypes.job + id;
        service.get(jobId).then(function(doc){
            mypouchdb.remove(doc).then(function(response){
                console.log(response);
                defer.resolve(true);
            }, function(error){
                console.log(error);
                defer.resolve(true);
            });
        }, function(error){
            console.log(error);
            defer.resolve(true);
        });

        return defer.promise;
    };

    service.getJobInfo = function(id){
        let defer = $q.defer();
        mypouchdb.find({selector: {_id: objectTypes.job + id}, 
            fields: ['_id', 'id', 'jobName', 'reference', 'description', 'poNumber', 'address', 'roadLevels', 'parties', 'expectedStartDate', 'expectedDaysDuration', 'expectedClosureTypes', 'notes', 'archived', 'tmpName', 'rpFrom', 'rpTo', 'emergencyCallout', 'fences', 'fencesPrice', 'fencesQuantity'], limit: 1}).then(function(response){
                console.log(response);
                if(response.docs.length === 1){
                    let doc = response.docs[0];                    
                    defer.resolve(doc);
                }
                else
                    defer.resolve(null);
            }, function(error){
                console.log(error);
                defer.reject(error);
            });
        return defer.promise;
    };

    service.getRemoteJobId = function(jobId){
        let defer = $q.defer();
        mypouchdb.find({selector: {_id: objectTypes.job + jobId}, 
            fields:['_id', 'id', 'rId'], limit: 1}).then(function(response){
                console.log(response);
                if(response.docs.length === 1){
                    let doc = response.docs[0];                    
                    defer.resolve(doc.rId);
                }
                else
                    defer.resolve(null);
            }, function(error){
                console.log(error);
                defer.resolve(null);
            });

        return defer.promise;
    };

    service.deleteJobs = function(jobs){
        let defer = $q.defer();

        mypouchdb.bulkDocs(jobs, function(error, response){
            //console.log(response);
            if(error) defer.reject(error);
            else defer.resolve(true);
        });

        return defer.promise;
    };

    service.deleteStopBreakTimeTimer = function(id){
        let defer = $q.defer();
        let _id = objectTypes.stopbreaktimetimer + id;
        console.log('[1088]: ' + _id);
        service.get(_id).then(function(doc){
            console.log('[1089]:');
            console.log(doc);
            if(doc){
                mypouchdb.remove(doc).then(function(response){
                    console.log('[1090]: deleteStopBreakTimeTimerId DONE');
                    defer.resolve(true);
                });
            }
        });
        return defer.promise;
    };

    service.deleteStopBreakTimeTimerByTeamMember = function(jobId, teamMemberId){
        let defer = $q.defer();
        
        mypouchdb.find({selector: {_id: {$regex: '^' + objectTypes.stopbreaktimetimer}, jobId: jobId, teamMemberId: teamMemberId}}).then(function(response){
            console.log('[1100] deleteStopBreakTimeTimerByTeamMember DONE');
            console.log(response);
            let docsToDelete = [];
            response.docs.forEach(doc => {
                //doc._deleted = true;
                docsToDelete.push({_id: doc._id, _rev: doc._rev, _deleted: true});
            });
            
            console.log('[1107]');
            console.log(docsToDelete);
            mypouchdb.bulkDocs(docsToDelete).then(function(actionResponse){
                console.log('[1110]');
                console.log(actionResponse);
                defer.resolve(true);
            }, function(error){
                console.log('[1113]');
                console.log('error while deleting stops');
                console.log(error);
                defer.reject(error);
            });

            //defer.resolve(true);
        });
        
        return defer.promise;
    };

    service.getPendingBreakTimeStops = function(){
        console.log('DBSvc.getPendingBreakTimeStops()');
        let defer = $q.defer();
        mypouchdb.allDocs({include_docs: true, startkey: objectTypes.stopbreaktimetimer, endkey: objectTypes.stopbreaktimetimer + '\ufff0'}, function(error, response){
            console.log('DBSvc.getPendingBreakTimeStops() -> query results');
            console.log(error);
            console.log(response);
            if(error)
                defer.reject(error);                
            else{
                if(response.rows.length === 0)
                    defer.resolve([]);
                else{
                    let docs = $linq.Enumerable().From(response.rows).Select('s => s.doc').ToArray();
                    defer.resolve(docs);
                }
            }
        });        

        return defer.promise;
    };

    service.getAgencies = function(){
        let defer = $q.defer();
        mypouchdb.allDocs({include_docs: true, startkey: objectTypes.agency, endkey: objectTypes.agency + '\ufff0'}, function(error, response){
            if(error)
                defer.reject(error);
            else{
                if(response.rows.length === 0)
                    defer.resolve([]);
                else{
                    let docs = $linq.Enumerable().From(response.rows).Select('s => s.doc').ToArray();
                    defer.resolve(docs);   
                }
            }
        });

        return defer.promise;
    };

    service.getAgenciesStaff = function(){
        let defer = $q.defer();
        mypouchdb.allDocs({include_docs: true, startkey: objectTypes.agencystaff, endkey: objectTypes.agencystaff + '\ufff0'}, function(error, response){
            if(error)
                defer.reject(error);
            else{
                if(response.rows.length === 0)
                    defer.resolve([]);
                else{
                    let docs = $linq.Enumerable().From(response.rows).Select('s => s.doc').ToArray();
                    defer.resolve(docs);   
                }
            }
        });

        return defer.promise;
    };

    service.clearTrucksData = function(){
        let defer = $q.defer();
        service.getAllTrucks().then(function(trucks){
            trucks.forEach(truck => {
                truck._deleted = true;
            });
            mypouchdb.bulkDocs(trucks, function(error, response){
                if(error) defer.reject(error);
                else defer.resolve(true);
            });
        });

        return defer.promise;
    };

    service.getAllStaff = function(){
        let defer = $q.defer();
        mypouchdb.allDocs({include_docs: true, startkey: objectTypes.staff, endkey: objectTypes.staff + '\ufff0'}, function(error, response){
            if(error)
                return defer.reject(error);
                
            if(response.rows.length == 0)
                return defer.resolve([]);
            
            let docs = $linq.Enumerable().From(response.rows).Select('s => s.doc').OrderBy('o => o.fullName').ToArray();
            defer.resolve(docs);
        });

        return defer.promise;
    };

    service.clearStaffData = function(){
        let defer = $q.defer();
        service.getAllStaff().then(function(staff){
            staff.forEach(member => {
                member._deleted = true;
            });
            mypouchdb.bulkDocs(staff, function(error, response){
                if(error) defer.reject(error);
                else defer.resolve(true);
            });
        });

        return defer.promise;
    };

    service.clearAgenciesData = function(){
        let defer = $q.defer();

        async.series({
            agencies: function(callback){
                service.getAgencies().then(function(agencies){
                    callback(null, agencies);
                }, function(error){
                    callback(error,'agencies error');
                });
            },
            agenciesStaff: function(callback){
                service.getAgenciesStaff().then(function(agencyStaff){
                    callback(null, agencyStaff);
                }, function(error){
                    callback(error,'agency staff error');
                })
            }
        }).then(function(results){
            results.agencies.forEach(agency => {
                agency._deleted = true;
            });
            results.agenciesStaff.forEach(agencyStaff => {
                agencyStaff._deleted = true;
            });
            let dataToDelete = results.agencies.concat(results.agenciesStaff);
            mypouchdb.bulkDocs(dataToDelete, function(error, response){
                if(error) defer.reject(error);
                else defer.resolve(true);
            });
        });

        return defer.promise;
    };

    service.getSiteRecordPDFLog = function (id) {
        console.log('service.getSiteRecordPDFLog');
        let defer = $q.defer();
        mypouchdb.find({
            selector: { _id: objectTypes.job + id },
            fields: ['_id', 'id', 'siteRecordPDFLog'], limit: 1
        }).then(function (response) {
            if (response.docs.length === 1) {
                let doc = response.docs[0];
                if (doc.siteRecordPDFLog === null || doc.siteRecordPDFLog === undefined)
                    doc.siteRecordPDFLog = [];
                let siteRecordPDFLog = doc.siteRecordPDFLog;
                siteRecordPDFLog.id = doc.id;
                defer.resolve(siteRecordPDFLog);
            }
            else
                defer.resolve(null);
        }, function (error) {
            console.log(error);
            defer.resolve(null);
            //defer.reject(error);
        });
        return defer.promise;
    };

    //#endregion

    return service;
}]);