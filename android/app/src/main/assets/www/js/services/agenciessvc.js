mainApp.factory('AgenciesSvc', ['$q', 'DBSvc', 'RemoteUrls', 'AccountSvc', '$resource', function($q, DBSvc, RemoteUrls, AccountSvc, $resource){
    let service = {};

    service.getAgencies = function(){
        return DBSvc.getAgencies();
    };

    service.getAgenciesStaff = function(){
        return DBSvc.getAgenciesStaff();
    };

    service.getAgency = function(id){
        id = objectTypes.agency + id;
        return DBSvc.get(id);
    };

    service.refreshData = function () {
        let defer = $q.defer();
        let resource = $resource('', {}, {
          'getAgenciesData': { url: RemoteUrls.agenciesDataSeed, method: 'GET', isArray: true, headers: { 'Authorization': 'Bearer ' + AccountSvc.getAccessTokenV2() }, params: { tenantId: '@tenantId' } },
          'getAgencyStaffData': { url: RemoteUrls.agenciesStaffListDataSeed, method: 'GET', isArray: true, headers: { 'Authorization': 'Bearer ' + AccountSvc.getAccessTokenV2() }, params: { tenantId: '@tenantId' } }
        });
    
        async.series({
            agencies: function(callback){
                resource.getAgenciesData({ tenantId: AccountSvc.getTenantIdV2() }).$promise.then(function(agencies){                    
                    callback(null, agencies);
                }, function(error){
                    callback(error, 'agencies error');
                });
            },
            agencyStaffList: function(callback){
                resource.getAgencyStaffData({ tenantId: AccountSvc.getTenantIdV2() }).$promise.then(function(agencyStaff){
                    callback(null, agencyStaff);
                }, function(error){
                    callback(error, 'agencystaff error');
                });
            }
        }, function(error, results){
            if(error)
                defer.reject(error);
            else{
                DBSvc.clearAgenciesData().then(function(cleared){                    
                    results.agencies.forEach(agency => {
                        agency._id = objectTypes.agency + agency.id;
                    });
                    results.agencyStaffList.forEach(agencyStaff => {
                        agencyStaff._id = objectTypes.agencystaff + agencyStaff.id;
                    });
                    let dataToSave = results.agencies.concat(results.agencyStaffList);
                    DBSvc.save(dataToSave).then(function (saveResponse) {
                        console.log('new data saved');
                        defer.resolve(true);
                    });
                });
            }
        });
        
        return defer.promise;
    };

    return service;
}]);