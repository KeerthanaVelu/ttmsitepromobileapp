/*
sync to server
preferred state: new job succeeded
state: new job data succeeded with a new id while keeping the tempid
actions:
- if current job active?
-- set current job id to the one from server
-- save current active job to db

state(w/ preferred state): activity
actions: before sending
only update activity.jobid if job.id is present
- if(activejob.id is present and activejob.tempid == activity.jobid)//the job has been succeeded
-- activity.jobId = job.id
-- update activity in Q
--- dbsvc.updateQ(activity.qid, activity)
-- send to server
- else
-- job = dbsvc.findbytempid(activity.jobid)
-- if(job.id)
--- activity.jobid = job.id
--- dbsvc.updateQ(activity)


sendactivity lifecycle
- beforesend
- aftersend
*/



mainApp.factory('SyncSendHandler', ['$resource', 'ApiSvc', 'AccountSvc', '$q', '$rootScope', 'DBSvc', function($resource, ApiSvc, AccountSvc, $q, DBSvc){
    var handlers = {};

    handlers.sendJobActivity = function(data){
        var defer = $q.defer();
        var resource = $resource(ApiSvc.apiRootUrl, null, {
            'addActivity': { method: 'POST', url: ApiSvc.addActivityUrl, headers: { 'Authorization': 'Bearer ' + AccountSvc.getAccessToken() }, params: { JobId: '@jobId' } },
            'updateActivity': { method: 'PUT', url: ApiSvc.updateActivityUrl, headers: { 'Authorization': 'Bearer ' + AccountSvc.getAccessToken() }, params: { JobId: '@jobId' } }            
        });
        if(data.mode == activityMode.start){
            resource.addActivity({ jobId: data.jobId }, data, function (response, headers) {
                defer.resolve(true);
            }, function (error) {
                console.log(error);
                defer.reject(error);
            });
        }
        else if(data.mode == activityMode.stop){
            resource.updateActivity({ jobId: data.jobId }, data, function (response, headers) {
                defer.resolve(true);
            }, function (error) {
                console.log(error);
                defer.reject(error);
            });
        }

        return defer.promise;
    };

    handlers.sendSiteCheckActivity = function(data){

    };

    handlers.sendJob = function(job){
        var defer = $q.defer();
        var resource = $resource(ApiSvc.apiRootUrl, null, {
            'addJob': { method: 'POST', url: ApiSvc.jobsUrl, headers: { 'Authorization': 'Bearer ' + AccountSvc.getAccessToken() } }
        });

        resource.addJob(data, function (response, headers) {
            console.log(response);
            DBSvc.updateJobId(response.id, job.id).then(function(){
                defer.resolve(true);
            });            
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    };

    return handlers;
}]);

mainApp.factory('SyncService', ['DBSvc', '$window', '$timeout', '$q', 'SyncSendHandler', function(DBSvc, $window, $timeout, $q, SyncSendHandler){
    var service = {};
    var isServiceRunning = false;
    var sendExecutionTime = 5000;
    function getActivity(type){
        var jobActivityIds = [1,4,9,11,12,13];
        var siteActivityIds = [2,5,7,8];
        if(jobActivityIds.indexOf(type) >= 0)
            return new JobActivity(SyncSendHandler.sendJobActivity);
        else if(siteActivityIds.indexOf(type) >= 0)
            return new SiteActivity(SyncSendHandler.sendJobActivity);
        else if(type === 3)
            return new SiteHazardAssessment(SyncSendHandler.sendJobActivity);
        else if(type === 6)
            return new SiteCheck(SyncSendHandler.sendSiteCheckActivity);
    }
    function sendHandler(){
        console.log('sendHandler()');
        if(isServiceRunning){
            console.log('isServviceRunning: ' + isServiceRunning);
            DBSvc.getNextQData().then(function(qData){
                console.log('QData:');
                console.log(qData);
                if(qData != null && qData.hasOwnProperty('activityId')){
                    var activity = getActivity(qData.activityId);
                    angular.extend(activity, qData);
                    activity.send().then(function(result){
                        DBSvc.deleteQData(qData.qid).then(function(res){
                            $timeout(sendHandler, sendExecutionTime);
                        });
                    }, function(error){
                        console.log('error: retry');
                        $timeout(sendHandler, sendExecutionTime);
                    });
                }
                else
                    service.stop();
            });
        }
    }

    service.start = function(){
        console.log('service.start()');
        if(!isServiceRunning){
            isServiceRunning = true;
            sendHandler();
        }
    }

    service.stop = function(){
        console.log('service.stop()');
        if(isServiceRunning)
            isServiceRunning = false;
    }

    service.addToQ = function(data){
        console.log('service.addToQ()');
        var defer = $q.defer();

        var qid = (new $window.ObjectId()).toString();
        DBSvc.saveQDataWId(qid, data).then(function(result){
            console.log('addToQ -> saveQDataWId success');
            service.start();
            defer.resolve(true);
        }, function(error){
            console.log(error);
            defer.reject(error);
        });

        return  defer.promise;
    }    

    return service;
}]);