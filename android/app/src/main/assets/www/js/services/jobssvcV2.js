﻿mainApp.factory('JobsSvc', ['$linq', 'WebApiQSvc', 'DBSvc', '$q', 'ApiSvc', '$rootScope', 'StaffSvc', '$timeout', '$cordovaLocalNotification', '$ionicPopup', 'SyncService', 'RemoteSyncSvc', 'AccountSvc', 'RemoteUrls', function ($linq, WebApiQSvc, DBSvc, $q, ApiSvc, $rootScope, StaffSvc, $timeout, $cordovaLocalNotification, $ionicPopup, SyncService, RemoteSyncSvc, AccountSvc, RemoteUrls) {
    var jobs = [];
    var service = {};
    var accessToken = {};
    let signalrDelay = 30000;
    $rootScope.$on('uploadSiteCheckPhotosDone', function (event, activity) {
        //console.log(activity);
        WebApiQSvc.Send(activity);
    });
    $rootScope.$on('newStaffMemberAdded', function (event, params) {
        //console.log(event);
        //update the staff
        StaffSvc.changeTempIdToServerId(params.tempId, params.newId).then(function (result) {            
            if (service.job) {
                var member = $linq.Enumerable().From(service.job.teamMembers).FirstOrDefault(null, 'f => f.staffId == "' + params.tempId + '"');
                if (member != null) {
                    //member.staffMemberId = params.newId;
                    member.staffId = params.newId;
                    DBSvc.saveJobs(service.jobs).then(function () {
                        //$rootScope.$broadcast('reloadTeamMembers');
                    }, function (error) {
                        console.log(error);
                    });
                }
            }
        }, function (error) {
            console.log(error);
        });    
    });

    function updteStaffData(staffData) {
        let defer = $q.defer();

        //StaffSvc.refreshData().then(function (staff) {
        //    console.log(staff);
        //}, function (error) {
        //    $ionicLoading.hide();
        //    ons.notification.alert({ title: 'Site Pro', message: 'Unable to refresh data.' });
        //});

        DBSvc.updateStaffData(staffData).then(function (result) {
           
        }, function (error) {
            defer.reject(error);
        });

        return defer.promise;
    }
    
    function syncStaffData(job){
        let defer = $q.defer();
        if(job.teamMembers.length === 0)
            return $q.resolve(true);

        let staffDataToSave = [];
        job.teamMembers.forEach(tm => {
           if(tm.createdInJobForm && tm.dataJson){
               let staffData = JSON.parse(tm.dataJson);
               if(!staffData.isAgency){
                staffData._id = objectTypes.staff + staffData.id;
               }
               else if(staffData.agencyId){
                staffData._id = objectTypes.agencystaff + staffData.id;
               }
               staffDataToSave.push(staffData);
            }
        });

        if(staffDataToSave.length > 0){
            DBSvc.save(staffDataToSave).then(function(){
                defer.resolve(true);                    
            }, function(error){
                console.log(error);
                defer.resolve(true);                
            });
        }
        else defer.resolve(true);

        return defer.promise;
    }

    function initSignalr() {
        console.log('initSignalr');
        var connection = $.hubConnection();
        connection.qs = { "Authorization": "Bearer " + AccountSvc.getAccessTokenV2() };
        //console.log(connection.qs);
        var jobsHubProxy = connection.createHubProxy('jobsHub');
        var staffHubProxy = connection.createHubProxy('staffHub');
        jobsHubProxy.on('receiveJob', function (job) {
            console.log('job received');
            console.log(job);
            angular.forEach(job.teamMembers, function (tm, key) {
                console.log(tm);
                tm._id = objectTypes.teammember + tm.id;
                tm.rId = tm.id;
                tm.jobId = job.id;
                tm.rJobId = job.id;
            });
            service.get(job.id).then(function (jobExist) {
               
                if (!jobExist) {
                    syncStaffData(job).then(function(syncRes){
                        job.rId = job.id;
                        job._id = objectTypes.job + job.id;
                        job.createdOn = (job.createdOn === null || job.createdOn === undefined || job.createdOn === "") ? new Date() : job.createdOn;
                        DBSvc.addJob(job).then(function () {
                            jobsHubProxy.invoke('received', job.id).done(function () {
                                //console.log('notify admin of jobReceived successfully');
                                $cordovaLocalNotification.schedule({
                                    id: 101,
                                    title: 'Site Pro - New Job',
                                    text: job.jobName
                                });
                                $rootScope.$broadcast('jobReceived');
                            }).fail(function (error) {
                                console.log('Invocation of jobReceived failed. Error: ' + error);
                            });
                        });
                    });
                }
                else {
                    jobsHubProxy.invoke('received', job.id).done(function () {
                        //console.log('notify admin of jobReceived successfully');                        
                    }).fail(function (error) {
                        //console.log('Invocation of jobReceived failed. Error: ' + error);
                    });
                }

                if (job.staff) {
                    updteStaffData(job.staff);
                }
            });
        });
        jobsHubProxy.on('transferJob', function (job) {
            $cordovaLocalNotification.schedule({
                id: 102,
                title: 'Site Pro - Transfer Job',
                text: job.job.jobName + ' (From: ' + job.sourceStms + ')'
            });
            //alert('Transfer!');
            var confirmTransfer = $ionicPopup.confirm({
                title: 'Transfer Job',
                template: "Job Name: <b>" + job.job.jobName + "</b><br/>Do you want to accept the job? "
            });
            confirmTransfer.then(function (result) {
                if (result) {
                    //console.log('pull job');
                    var staff = StaffSvc.get();
                    var teamMemberIds = $linq.Enumerable().From(job.job.team).Select('s => s.id').ToArray();
                    var staffIds = $linq.Enumerable().From(staff).Select('s => s.id').ToArray();
                    var notExist = $linq.Enumerable().From(teamMemberIds).Except(staffIds).ToArray();
                    if (notExist.length > 0) {
                        angular.forEach(notExist, function (value, key) {
                            if (job.team) {
                                var teamMember = $linq.Enumerable().From(job.job.team).FirstOrDefault(null, 'f => f.id == "' + value + '"');
                                if (teamMember != null) {
                                    var newStaff = new Staff();
                                    newStaff.id = teamMember.id;
                                    newStaff.branchId = teamMember.branchId;
                                    newStaff.fullName = teamMember.fullName;
                                    newStaff.phone = teamMember.phone;
                                    newStaff.qualification = teamMember.qualification;
                                    newStaff.notes = teamMember.notes;
                                    newStaff.photo = teamMember.photo;
                                    StaffSvc.addStaffMemberLocal(newStaff);
                                }
                            }
                        });
                    }
                    service.jobs.push(job.job);
                    service.save().then(function () {
                        if (job.isMobileTransfer) {
                            jobsHubProxy.invoke('mobileJobTransferReceived', job.job.id).done(function () {
                                //console.log('notify server of mobile job transfer received successfully');
                            }).fail(function (error) {
                                //console.log('Invocation of mobileJobTransferReceived failed. Error: ' + error);
                            });
                        }
                        else {
                            jobsHubProxy.invoke('jobReceived', job.job.id).done(function () {
                                //console.log('notify admin of jobReceived successfully');
                            }).fail(function (error) {
                                //console.log('Invocation of jobReceived failed. Error: ' + error);
                            });
                        }
                    });
                }
            });
        });
        jobsHubProxy.on('mobileJobTransferReceived', function (jobId) {
            $rootScope.$broadcast('mobileJobTransferReceived', { jobId: jobId });
        });
        jobsHubProxy.on('updateTeamMembers', function (teamMembers) {
            //update team members if any
            //foreach teammember
            //if new then add
            service.job.teamMembers = teamMembers;
            service.save();
        });
        staffHubProxy.on('newStaff', function (newStaff) {
            ////console.log(newStaff);
            StaffSvc.addStaff(newStaff, function () {

            });
        });
        staffHubProxy.on('staffArchived', function (result) {
            //console.log(result);
            StaffSvc.archiveStaff(result.staffId, result.archived).then(function () {
            });
        });
        connection.url = RemoteUrls.signalrHubs;
        connection.error(function (error) {
            //console.log('SignalR error');
            //console.log(error);
        });
        connection.disconnected(function () {
            //connection.start();
            //console.log('disconnected at: ' + (new Date()).toLocaleTimeString());
            setTimeout(function () {
                //console.log('reconnecting....');    
                connection.start();
            }, 10000);
        });
        connection.reconnecting(function () {
            //console.log('reconnecting');
        });
        connection.reconnected(function () {
            //console.log('reconnected');
        });
        connection.start().done(function () {
            //console.log('connection established');
        }).fail(function () {
            //console.log('unable to connect');
        });
    }

    service.job = null;
    service.branches = [];
    service.teamMember = null;
    service.add = function (job) {
        if (this.jobs == null)
            this.jobs = [];
        this.jobs.push(job);
        service.save();
        WebApiQSvc.Send(job);
    };    
    
    service.UpdateActivity = function (activity) {
        service.save();
        WebApiQSvc.Send(activity);
    };
    
    service.getAll = function () {
        return this.jobs;
    };
    service.setBranches = function (branches) {
        this.branches = branches;
    };
    service.save = function () {
        var defer = $q.defer();

        DBSvc.saveJobs(this.jobs).then(function (result) {
            defer.resolve(result);
        }, function (error) {
            defer.reject(error);
        });

        return defer.promise;
    };
    service.addLocal = function (job) {
        this.jobs.push(job);
    };
    service.init = function (jobs) {
        this.jobs = jobs;
        initSignalr();
    };
    service.initSignalr = function(){
        initSignalr();
    };
    service.uploadPhoto = function (photoData) {
        WebApiQSvc.Send(photoData);
    };
    service.addToQ = function (data) {
        var defer = $q.defer();

        DBSvc.saveQData(data).then(function (result) {
            //console.log('service.addToQ: result');
            defer.resolve(result);
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    };
    service.completeJob = function (job) {
        WebApiQSvc.Send({ jobId: job.id, isJob: true, complete: true });
    };
    service.updateGeneralNotes = function (generalNotes) {
        service.save().then(function () {
            WebApiQSvc.Send(generalNotes);
        });
    };
    service.deleteJob = function (job) {
        var defer = $q.defer();

        var index = this.jobs.indexOf(job);
        if (index >= 0) {
            this.jobs.splice(index, 1);
            service.save().then(function () {
                defer.resolve(true);
            }, function (error) {
                console.log(error);
                defer.reject(error);
            });
        }
        defer.resolve(true);

        return defer.promise;
    };
    service.updateTrucks = function (job) {
        var defer = $q.defer();

        service.save().then(function (result) {
            var truckIds = null;
            if (job.trucks != null && job.trucks.length > 0 && (typeof job.trucks[0] === 'object'))
                truckIds = $linq.Enumerable().From(job.trucks).Select('s => s.truckId').ToArray();
            WebApiQSvc.Send({ isTrucksUpdate: true, jobId: job.id, truckIds: (truckIds || job.trucks) });
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    };
    service.updateGeneralInfo = function (data) {
        var defer = $q.defer();

        service.save().then(function (result) {
            WebApiQSvc.Send(data);
            defer.resolve(true);
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    };
    service.endOfShift = function (activity) {
        var defer = $q.defer();
        if (service.job != null && service.job.activities != null) {
            service.job.activities.unshift(activity);
        }
        //stop time of team
        for (var i = 0; i < service.job.teamMembers.length; i++) {
            var member = service.job.teamMembers[i];
            var timelog = { isTeamMemberTimelog: true, jobId: service.job.id, staffMemberId: member.id, timeLogType: 3, time: new Date() };
            member.timeLogs.push(timelog);
        }

        service.save().then(function () {
            WebApiQSvc.Send(activity);
            defer.resolve(true);
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    };

    service.getTruckAudit = function (truckAuditId) {
        var defer = $q.defer();

        DBSvc.getTruckAudit(truckAuditId).then(function (result) {
            defer.resolve(result);
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    };
    
    service.updateJobId = function(newJobId, tempJobId){
        var defer = $q.defer();
        defer.resolve(true);

        return defer.promise;
    };

    service.get = function(id){
        var defer = $q.defer();
        id = objectTypes.job + id;
        DBSvc.get(id).then(function(job){
            defer.resolve(job);
        }, function(error){
            console.log(error);
            if(error.status === 404)
                defer.resolve(null);
            else
                defer.reject(error);
        });
        
        return defer.promise;
    };

    service.getAllV2 = function(){
        let defer = $q.defer();
        DBSvc.getAllJobs().then(function(response){
            defer.resolve(response);
        }, function(error){
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    }

    service.getAllByExpectedStartDate = function(){
        let defer = $q.defer();

        service.getAllV2().then(function(jobs){
            jobs.forEach(job => {
                job.expectedStartDateMoment = moment(job.expectedStartDate);
                if(job.expectedStartDateMoment.isValid()){
                    job.expectedStartDay = job.expectedStartDateMoment.format('DD-MM-YYYY');
                }
            });
            let groupedByDay = $linq.Enumerable().From(jobs).GroupBy('g => g.expectedStartDay').Select('s => {shiftDate: s.Key(), jobs: s.source}').ToArray();
            defer.resolve(groupedByDay);
        }, function(error){
            defer.reject(error);
        });

        return defer.promise;
    };

    service.getArchivedJobs = function(){
        function deleteArchivedJobs(jobsToDelete){
            let defer = $q.defer();
            if(jobsToDelete.length == 0)
                defer.resolve(true);
            else{
                DBSvc.deleteJobs(jobsToDelete).then(function(){
                    defer.resolve(true);
                }, function(error){
                    defer.reject(error);
                });
            }
            return defer.promise;
        };
        let defer = $q.defer();        
        //remove 2 week old archived jobs
        DBSvc.getArchivedJobs().then(function(response){
            let currentDateTime = moment();            
            response.forEach(job => {
                if(job.archivedOn){
                    let archivedOn = moment(job.archivedOn);
                    let daysSinceArchived = currentDateTime.diff(archivedOn, 'days');
                    console.log(daysSinceArchived);
                    if(daysSinceArchived >= 14){
                        job._deleted = true;
                    }
                }  
            });
            let archivedJobsForDeletion = $linq.Enumerable().From(response).Where('w => w._deleted == true').ToArray();
            deleteArchivedJobs(archivedJobsForDeletion).then(function(){
                DBSvc.getArchivedJobs().then(function(archivedJobs){
                    console.log(archivedJobs);
                    defer.resolve(archivedJobs);
                });
            });
        }, function(error){
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    }

    service.getArchivedJobsByDate = function(){
        let defer = $q.defer();
        service.getArchivedJobs().then(function(jobs){
            jobs.forEach(job => {
                job.expectedStartDateMoment = moment(job.expectedStartDate);
                if(job.expectedStartDateMoment.isValid()){
                    job.expectedStartDay = job.expectedStartDateMoment.format('DD-MM-YYYY');
                }
            });
            let groupedByDay = $linq.Enumerable().From(jobs).GroupBy('g => g.expectedStartDay').Select('s => {shiftDate: s.Key(), jobs: s.source}').ToArray();            
            defer.resolve(groupedByDay);
        }, function(error){
            defer.reject(error);
        });

        return defer.promise;
    };

    service.updateJobRemoteId = function(id, rId){
        DBSvc.updateJobRemoteId(id, rId).then(function(){
            $rootScope.$broadcast('jobCreated', { rId: rId });            
        });
    };

    service.addV2 = function(job){
        var defer = $q.defer();
        job.id = (new ObjectId()).toString();
        job._id = objectTypes.job + job.id;
        job.tenantId = AccountSvc.getTenantIdV2();
        job.branchId = AccountSvc.getBranchIdV2();
        job.assignedUserId = AccountSvc.getUserIdV2();
        job.createdOn = moment().utc();// new Date();
        job.expectedDaysDuration = job.expectedDaysDuration;
        job.expectedClosureTypes = job.expectedClosureTypes;
        DBSvc.addJob(job).then(function(response){
            //send to SyncService
            RemoteSyncSvc.add(job).then(function(response){
                console.log(response);
                defer.resolve(job.id);
            }, function(error){
                console.log(error);
                defer.reject(error);
            });
        }, function(error){
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    };

    service.getGeneralInfo = function(id){
        return DBSvc.getGeneralInfo(id);
    };

    service.updateGeneralInfoV2 = function(generalInfo){
        let defer = $q.defer();
        DBSvc.get(objectTypes.job + generalInfo.id).then(function(job){
            if(job != null){
                job.jobName = generalInfo.jobName;
                job.reference = generalInfo.reference;
                job.description = generalInfo.description;
                job.poNumber = generalInfo.poNumber;
                job.address = generalInfo.address;
                job.roadLevels = generalInfo.roadLevels;

                DBSvc.save([job]).then(function(response){
                    RemoteSyncSvc.add(job).then(function(response){
                        defer.resolve(true);
                    });                                                            
                }, function(error){
                    console.log(error);
                    defer.reject(error);
                });
            }
        });

        return defer.promise;
    };

    service.getGeneralNotes = function(id){
        return DBSvc.getGeneralNotes(id);
    }

    service.updateGeneralNotes = function(jobid, generalNotes){
        let defer = $q.defer();
        DBSvc.get(objectTypes.job + jobid).then(function(job){
            if(job != null){
                delete generalNotes.id;
                delete generalNotes._id;        
                job.generalNotes = generalNotes;
                DBSvc.save([job]).then(function(response){
                    RemoteSyncSvc.add(job).then(function(response){
                        defer.resolve(true);
                    });                                                            
                }, function(error){
                    console.log(error);
                    defer.reject(error);
                });
            }
        });

        return defer.promise;
    };

    service.updateTrucksV2 = function(jobId, trucks){
        let defer = $q.defer();
        DBSvc.get(objectTypes.job + jobId).then(function(job){
            if(job != null){
                job.trucks = trucks;
                DBSvc.save([job]).then(function(response){
                    RemoteSyncSvc.add(job).then(function(response){
                        defer.resolve(true);
                    });                                                            
                }, function(error){
                    console.log(error);
                    defer.reject(error);
                })
            }
        });

        return defer.promise;
    };
    
    service.getParties = function(id){
        return DBSvc.getParties(id);
    };

    service.updateParties = function(parties){
        let defer = $q.defer();

        DBSvc.get(objectTypes.job + parties.id).then(function(job){
            if(job){
                job.parties = parties;
                DBSvc.save([job]).then(function(response){
                    RemoteSyncSvc.add(job).then(function(response){
                        defer.resolve(true);
                    });                                                            
                }, function(error){
                    console.log(error);
                    defer.reject(error);
                });
            }
        }, function(error){
            console.log(error);
            alert('Something went wrong.');
        });

        return defer.promise;
    };    

    service.getTeam = function(id){
        //show only team members that were not hidden
        let defer = $q.defer();
        DBSvc.getTeam(id).then(function(team){
            let activeTeam = $linq.Enumerable().From(team).Where(member => {
                if(member.timeLogs == undefined || member.timeLogs == null || member.timeLogs.length == 0)
                    return true;
                let lastLog = member.timeLogs[member.timeLogs.length -1];
                if(lastLog.timeLogType !== TimeLogTypeEnum.ENDTIME)
                    return true;
            }).ToArray();
            defer.resolve(activeTeam);
        });

        return defer.promise;
    };

    service.getTeamORIG = function(id){
        //show only team members that were not hidden
        let defer = $q.defer();
        DBSvc.getTeam(id).then(function(team){
            let activeTeam = $linq.Enumerable().From(team).Where('w => !(w.hidden == true)').ToArray();
            defer.resolve(activeTeam);
        });

        return defer.promise;
    };

    service.getAllTeam = function(id){
        return DBSvc.getAllTeam(id);
    };
    
    service.updateTeam = function(id, team){
        let defer = $q.defer();
        let jobid = objectTypes.job + id;
        DBSvc.get(jobid).then(function(job){
            if(job){
                job.teamMembers = team;
                DBSvc.save([job]).then(function(response){
                    RemoteSyncSvc.add(job).then(function(response){
                        defer.resolve(true);
                    });                    
                });
            }
        });

        return defer.promise;
    };

    service.getTeamMemberLogs = function(jobId, teamMemberId){
        let defer = $q.defer();
        let objId = objectTypes.job + jobId;
        let result = {};
        let seqResults = {};
        let tasks = async.seq(function(callback){
            DBSvc.get(objId).then(function(job){
                    if(job){     
                        result.jobId = job.id;
                        result.rJobId = job.rId;
                    }
                    callback(null, job);
                });
            }, function(job, callback){
                if(job){
                    let teamMember = $linq.Enumerable().From(job.teamMembers).FirstOrDefault(null, 'f => f.id == "' + teamMemberId + '"');                
                    return callback(null, teamMember);
                }
                callback(null, null);
            }, function(teamMember, callback){
                if(teamMember){
                    if(!teamMember.timeLogs)
                        teamMember.timeLogs = [];
                    result.id = teamMember.id;
                    result.rId = teamMember.rId;
                    result.timeLogs = teamMember.timeLogs;
                    result.totalBreakTimeMinutes = teamMember.totalBreakTimeMinutes;
                    let staffId = objectTypes.staff + teamMember.staffId;
                    DBSvc.get(staffId).then(function(staff){
                        if(staff){
                            result.fullName = staff.fullName;
                            result.staffType = staff.staffType;
                            callback(null, staff.branchId);
                        }
                        else callback(null, null);
                    });
                }
                else callback(null, null);
            }, function(branchId, callback){
                let objBranchId = objectTypes.tenantbranch + branchId;
                DBSvc.get(objBranchId).then(function(branch){
                    if(branch){
                        result.branchName = branch.branch;
                        callback(null, true);
                    }
                    else callback(null, true);
                });
            }
        );
        tasks(function(error, response){
            defer.resolve(result);
        });
        
        return defer.promise;
    };


    service.addTimeLogTemp = function(){
        let defer = $q.defer();
        setTimeout(function(){
            console.log('timelog added');
            defer.resolve(true);
        }, 5000);

        return defer.promise;
    };
    service.addTimeLog = function(timelog){
        let defer = $q.defer();
        let objJobId = objectTypes.job + timelog.jobId;
        timelog.id = (new ObjectId()).toString();
        timelog._id = objectTypes.timelog + timelog.id;
        DBSvc.get(objJobId).then(function(job){
            if(!job)
                return defer.resolve(true);

            let teamMember = $linq.Enumerable().From(job.teamMembers).FirstOrDefault(null, 'f => f.id == "' + timelog.teamMemberId +'"');
            if(teamMember){
                if(!teamMember.timeLogs)
                    teamMember.timeLogs = [];
                teamMember.timeLogs.push(timelog);
            }
            DBSvc.save([job]).then(function(response){
                RemoteSyncSvc.add(timelog).then(function(response){                    
                    defer.resolve(true);
                });
            });
        });

        return defer.promise;
    };

    service.addTeamMembers = function(jobId, teamMembers){
        let defer = $q.defer();
        let objJobId = objectTypes.job + jobId;
        DBSvc.get(objJobId).then(function(job){
            if(job){
                teamMembers.forEach(item => {
                    item.rJobId = job.rId;
                });
                job.teamMembers = job.teamMembers.concat(teamMembers);
                DBSvc.save([job]).then(function(response){
                    async.eachSeries(teamMembers, function(item, callback){
                        RemoteSyncSvc.add(item).then(function(response){
                            callback();
                        });
                    }).then(function(response){
                        defer.resolve(true);
                    });                    
                });
            }
        });

        return defer.promise;
    };

    service.removeTeamMember = function(jobId, teamMemberId){
        let defer = $q.defer();
        let objJobId = objectTypes.job + jobId;
        DBSvc.get(objJobId).then(function(job){
            if(job){
                let teamMember = $linq.Enumerable().From(job.teamMembers).FirstOrDefault(null, 'f => f.id == "' + teamMemberId + '"');
                if(teamMember){
                    teamMember.dataMode = DataActionEnum.DELETE;
                    DBSvc.save([job]).then(function(response){                        
                        RemoteSyncSvc.add(teamMember).then(function(response){
                            defer.resolve(true);
                        });                    
                    });
                }
                else defer.resolve(true);          
            }
        });

        return defer.promise;
    };

    function getObjectType(activityType){
        switch(activityType){
            case ActivityTypeEnum.TravelToSite:
            case ActivityTypeEnum.TravelToDepot:
            case ActivityTypeEnum.TravelToNextJob:
            case ActivityTypeEnum.SiteHazardAssessment:
            case ActivityTypeEnum.WorkSpaceActive:
            case ActivityTypeEnum.EndOfShift:
            case ActivityTypeEnum.PreStartTruckCheck:
            case ActivityTypeEnum.TruckPrecheckRecord:
            case ActivityTypeEnum.TMPOnSiteAssessment:
                return objectTypes.jobactivity;
            case ActivityTypeEnum.SiteDeployment:
            case ActivityTypeEnum.SiteAmendmentRecord:
            case ActivityTypeEnum.SiteRetrievalRecord:
                return objectTypes.siteactivity;
            case ActivityTypeEnum.OnSiteRecord:
                return objectTypes.onsiterecord;
            case ActivityTypeEnum.GearLeftOnSiteRecord:
                return objectTypes.geargeftonsiterecord
            //case ActivityTypeEnum.TruckPrecheckRecord:
            //    return objectTypes.truckPrecheckRecord;         

        }
    }
    
    service.addActivity = function(jobId, activity){
        console.log("4");
       // alert("1");
        let defer = $q.defer();
        let objJobId = objectTypes.job + jobId;
        DBSvc.get(objJobId).then(function(job){
            if(job){
                activity.id = (new ObjectId()).toString();
                activity._id = getObjectType(activity.activityType) + activity.id;
                if(!job.activities)
                    job.activities = [];
                job.activities.push(activity);
                DBSvc.save([job]).then(function(response){
                    defer.resolve(activity.id);                  
                });
            }
        });

        return defer.promise;
    };

    service.addSendActivity = function (jobId, activity) {
        //alert("2");
        let defer = $q.defer();
        let objJobId = objectTypes.job + jobId;
        DBSvc.get(objJobId).then(function(job){
            if(job){
                activity.id = (new ObjectId()).toString();
                activity._id = getObjectType(activity.activityType) + activity.id;
                if(!job.activities)
                    job.activities = [];
                job.activities.push(activity);
                DBSvc.save([job]).then(function(response){
                    RemoteSyncSvc.add(activity).then(function(response){
                        defer.resolve(activity.id);
                    });                    
                });
            }
        });

        return defer.promise;
    };

    service.stopActivity = function (jobId, activity) {
        // alert("3");
        let defer = $q.defer();
        let objJobId = objectTypes.job + jobId;
        DBSvc.get(objJobId).then(function(job){
            if(job){
                let activityData = $linq.Enumerable().From(job.activities).FirstOrDefault(null, 'f => f.id == "' + activity.id + '"');
                console.log(activityData, "---addactivityData---")
                if(activityData){
                    activityData.endTime = activity.endTime;
                    activityData.endCoordinates = activity.endCoordinates;
                    activityData.notes = activity.notes;
                    activityData.description = activity.description;
                    activityData.truckAuditId = activity.truckAuditId;
                    if(activityData.activityType == ActivityTypeEnum.OnSiteRecord)
                        activityData.siteCheckItems = activity.siteCheckItems;

                    if (activityData.activityType == ActivityTypeEnum.GearLeftOnSiteRecord) {
                        activityData.level1Items = activity.level1Items;
                        activityData.level2Items = activity.level2Items;
                        activityData.accessoriesItems = activity.accessoriesItems;
                    }

                    if (activityData.activityType == ActivityTypeEnum.TruckPrecheckRecord) {
                        activityData.truckId = activity.truckId;
                        activityData.truckNumber = activity.truckNumber;
                        activityData.truckRego = activity.truckRego;
                        activityData.isCamera = activity.isCamera;
                        activityData.isReverseCamera = activity.isReverseCamera;
                        activityData.hubExpiry = activity.hubExpiry;
                        activityData.huboReading = activity.huboReading;
                        activityData.cofExpiry = activity.cofExpiry;
                        activityData.regoExpiry = activity.regoExpiry;
                        activityData.isAlarms = activity.isAlarms;
                        activityData.isAssistDevices = activity.isAssistDevices;
                        activityData.isEngineoilwater = activity.isEngineoilwater;
                        activityData.isTyres = activity.isTyres;
                        activityData.isFireExtinguisher = activity.isFireExtinguisher;
                        activityData.isFirstAidKit = activity.isFirstAidKit;
                        activityData.isYardbroom = activity.isYardbroom;
                        activityData.isPailofsoapywater_brush = activity.isPailofsoapywater_brush;
                        activityData.isAmber = activity.isAmber;
                        activityData.isAlllights = activity.isAlllights;
                        activityData.isTruckMirrors = activity.isTruckMirrors;
                        activityData.isWheel = activity.isWheel;
                        activityData.isNotes = activity.isNotes
                    }

                    DBSvc.save([job]).then(function(response){
                        RemoteSyncSvc.add(activityData).then(function(response){
                            defer.resolve(activityData.id);
                        });
                    });
                }                
            }
        });

        return defer.promise;
    };

    service.getActivity = function (jobId, id) {
       // alert("4");
        let defer = $q.defer();
        let objId = objectTypes.job + jobId;
        let result = null;
        DBSvc.get(objId).then(function(job){
            if(job){     
                result = $linq.Enumerable().From(job.activities).FirstOrDefault(null, 'f => f.id == "' + id + '"');
                if(result){
                    defer.resolve(result);
                }
            }
            defer.resolve(result);
        }, function(error){
            console.log(error);
            defer.reject(null);
        });
        
        return defer.promise;
    };

    service.cancelActivity = function (jobId, activityId) {   
        //alert("5");
        let defer = $q.defer();
        let objJobId = objectTypes.job + jobId;
        DBSvc.get(objJobId).then(function(job){
            if(job){
                let activityData = $linq.Enumerable().From(job.activities).FirstOrDefault(null, 'f => f.id == "' + activityId + '"');
                if(activityData){
                    activityData.isCancelled = true;
                    DBSvc.save([job]).then(function(response){
                        //add to sync
                        RemoteSyncSvc.add(activityData).then(function(response){
                            defer.resolve(activityData.id);
                        });
                    });
                }                                
            }
        });

        return defer.promise;
    };

    service.updateActivityV2 = function (jobId, activity) {
        //alert("6");
        let defer = $q.defer();
        let objJobId = objectTypes.job + jobId;
        DBSvc.get(objJobId).then(function(job){
            if(job){
                let activityData = $linq.Enumerable().From(job.activities).FirstOrDefault(null, 'f => f.id == "' + activity.id + '"');
                if(activityData){                                        
                    if(activityData.activityType == ActivityTypeEnum.SiteDeployment || activityData.activityType == ActivityTypeEnum.SiteAmendmentRecord || activityData.activityType == ActivityTypeEnum.SiteRetrievalRecord){
                        activityData.tsls = activity.tsls;
                        activityData.addedClosureTypes = activity.addedClosureTypes;
                    }
                    activityData.notes = activity.notes;
                    activity.action = ActivityActionEnum.UPDATE;
                    DBSvc.save([job]).then(function(response){
                        RemoteSyncSvc.add(activity).then(function(response){
                            defer.resolve(true);
                        });
                    });
                }                
            }
        });

        return defer.promise;        
    };

    service.updateSiteCheckItem = function (jobId, activityId, itemIndex, item) {
        //alert("7");
        let defer = $q.defer();
        let objJobId = objectTypes.job + jobId;
        DBSvc.get(objJobId).then(function(job){
            if(job){
                let activityData = $linq.Enumerable().From(job.activities).FirstOrDefault(null, 'f => f.id == "' + activityId + '"');                
                if(activityData){
                    activityData.siteCheckItems[itemIndex] = item;
                    DBSvc.save([job]).then(function(response){
                        defer.resolve(true);
                    });
                }
            }
        });

        return defer.promise;        
    };

    service.completeJobV2 = function(job){
        let defer = $q.defer();
        DBSvc.get(objectTypes.job + job.id).then(function(jobData){
            if(job != null){                
                jobData.complete = true;
                DBSvc.save([jobData]).then(function(response){
                    RemoteSyncSvc.add(jobData).then(function(response){
                        defer.resolve(true);
                    });
                }, function(error){
                    console.log(error);
                    defer.reject(error);
                });
            }
        });

        return defer.promise;
    };
    
    service.endShiftTeamMember = function(member){
        let defer = $q.defer();
        let objJobId = objectTypes.job + member.jobId;
        DBSvc.get(objJobId).then(function(job){
            if(!job)
                return defer.resolve(true);

            let teamMember = $linq.Enumerable().From(job.teamMembers).FirstOrDefault(null, 'f => f.id == "' + member.id +'"');
            if(teamMember){
                teamMember.endOfShiftTime = member.endOfShiftTime;
                DBSvc.save([job]).then(function(response){
                    RemoteSyncSvc.add(teamMember).then(function(response){
                        defer.resolve(true);
                    });
                });
            }            
        });
        
        return defer.promise;
    };

    service.delete = function(jobId){
        return DBSvc.deleteJob(jobId);
    };

    service.getJobInfo = function(id){
        let defer = $q.defer();
        DBSvc.getJobInfo(id).then(function(jobInfo){
            if(jobInfo.expectedStartDate){
                jobInfo.expectedStartDate = moment(jobInfo.expectedStartDate, 'YYYY-MM-DDThh:mmZ').toDate();                
            }
            defer.resolve(jobInfo);
        }, function(error){
            defer.reject(error);
        });
        
        return defer.promise;
    };

    service.updateJobInfo = function(jobInfo){
        let defer = $q.defer();
        DBSvc.get(objectTypes.job + jobInfo.id).then(function(job){
            if(job != null){
                job.jobName = jobInfo.jobName;
                job.reference = jobInfo.reference;
                job.description = jobInfo.description;
                job.poNumber = jobInfo.poNumber;
                job.address = jobInfo.address;
                job.roadLevels = jobInfo.roadLevels;
                job.parties = jobInfo.parties;
                job.expectedDaysDuration = jobInfo.expectedDaysDuration;
                job.expectedClosureTypes = jobInfo.expectedClosureTypes;
                job.notes = jobInfo.notes;
                job.tmpName = jobInfo.tmpName;
                job.rpFrom = jobInfo.rpFrom;
                job.rpTo = jobInfo.rpTo;
                job.emergencyCallout = jobInfo.emergencyCallout;
                job.fences = jobInfo.fences;
                job.fencesQuantity = jobInfo.fencesQuantity;
                job.fencesPrice = jobInfo.fencesPrice;
                DBSvc.save([job]).then(function(response){
                    RemoteSyncSvc.add(job).then(function(response){
                        defer.resolve(true);
                    });                                                            
                }, function(error){
                    console.log(error);
                    defer.reject(error);
                });
            }
        });

        return defer.promise;
    }

    service.clearActivities = function(jobId){
        let defer = $q.defer();
        let jobObjId = objectTypes.job + jobId;
        DBSvc.get(jobObjId).then(function(job){
            if(job){
                job.activities = [];
                DBSvc.save([job]).then(function(response){                    
                    defer.resolve(true);             
                });
            }
        });

        return defer.promise;
    }

    service.archiveJob = function(jobId){
        let defer = $q.defer();
        let jobObjId = objectTypes.job + jobId;
        DBSvc.get(jobObjId).then(function(job){
            if(job){
                job.archived = true;
                job.archivedOn = new Date();
                DBSvc.save([job]).then(function(response){                    
                    defer.resolve(true);             
                });
            }
        });

        return defer.promise;
    }

    function deleteStopBreakTimeTimerId(timerId){
        let defer = $q.defer();
        DBSvc.deleteStopBreakTimeTimer(timerId).then(function(res){
            console.log('JobSvc.deleteStopBreakTimeTimerId DONE');
            defer.resolve(true);
        });

        return defer.promise;
    }
    function saveStopBreakTimeTimerId(timerId, jobId, teamMemberId){
        let defer = $q.defer();
        let _id = objectTypes.stopbreaktimetimer + timerId;
        DBSvc.get(_id).then(function(data){
            console.log('[1004]: data');
            console.log(data);
            if(!data){
                data = {_id: _id, timerId: null, jobId: jobId, teamMemberId: teamMemberId};
            }
            data.timerId = timerId;        
            DBSvc.save([data]).then(function(res){
                console.log('saveStopBreakTimeTimerId DONE');
                defer.resolve(true);
            });
        });
            
        return defer.promise;
    }


    function resumePendingBreakTimeStops(){
        console.log('JobsSvc -> resumePendingBreakTimeStops');
        let defer = $q.defer();
        DBSvc.getPendingBreakTimeStops().then(function(stops){
            console.log('stops:');
            console.log(stops);
            async.eachSeries(stops, function(stop, subCallback){
                console.log('stop data');
                console.log(stop);
                service.getTeamMemberLogs(stop.jobId, stop.teamMemberId).then(function(tmLogs){
                    let lastLog = $linq.Enumerable().From(tmLogs.timeLogs).LastOrDefault();
                    console.log('lastLog');
                    console.log(lastLog);
                    if(lastLog && lastLog.timeLogType === TimeLogTypeEnum.BREAKTIMESTART){
                        let breakTimeStart = new Date(lastLog.time);
                        console.log('breakTimeStart: ' + breakTimeStart.toISOString());
                        let expectedBreakTimeStop = new Date(breakTimeStart.valueOf() + lastLog.duration);
                        console.log('expectedBreakTimeStop: ' + expectedBreakTimeStop.toISOString());
                        let currentTime = new Date();
                        if(currentTime.valueOf() > expectedBreakTimeStop.valueOf()){
                            console.log('expected break time stop has passed: add stop log');
                            let newLog = angular.copy(TimeLogVM);
                            newLog.jobId = tmLogs.jobId;
                            newLog.rJobId = tmLogs.rJobId;
                            newLog.teamMemberId = tmLogs.id;
                            newLog.rTeamMemberId = tmLogs.rId;
                            newLog.time = expectedBreakTimeStop;
                            newLog.timeLogType = TimeLogTypeEnum.BREAKTIMEEND;
                            tmLogs.timeLogs.push(newLog);
                            deleteStopBreakTimeTimerId(stop.timerId).then(function(){
                                service.addTimeLog(newLog).then(function(response){
                                    console.log('[1050]: expired break time stop added');
                                    subCallback();
                                });
                            });                            
                        }
                        else{
                            let remainingDuration = expectedBreakTimeStop.valueOf() - currentTime.valueOf();
                            console.log('remainingDuration');
                            console.log(remainingDuration);
                            DBSvc.deleteStopBreakTimeTimerByTeamMember(stop.jobId, stop.teamMemberId).then(function(){
                                service.stopBreakTimeAt(stop.jobId, stop.teamMemberId, remainingDuration).then(function(){
                                    console.log('[1060]: new stop break time at');
                                    subCallback();
                                });
                            });
                        }
                    }
                    else
                        subCallback();
                });
            }).then(function(){
                console.log('resumePendingBreakTimeStops DONE');
                defer.resolve(true);
            });
        });    

        return defer.promise;
    }
    service.initStopBreakTimeTimer = function(){
        console.log('JobsSvc.initStopBreakTimeTimer()');
        return resumePendingBreakTimeStops();
    }
    service.stopBreakTimeAt = function(jobId, teamMemberId, duration){
        let defer = $q.defer();
        let timerId = setTimeout(() => {
            //newbreaktimeend log
            service.getTeamMemberLogs(jobId, teamMemberId).then(function(teamMemberLogs){
                console.log(teamMemberLogs);
                let newLog = angular.copy(TimeLogVM);
                newLog.jobId = teamMemberLogs.jobId;
                newLog.rJobId = teamMemberLogs.rJobId;
                newLog.teamMemberId = teamMemberLogs.id;
                newLog.rTeamMemberId = teamMemberLogs.rId;
                newLog.time = new Date();
                newLog.timeLogType = TimeLogTypeEnum.BREAKTIMEEND;
                teamMemberLogs.timeLogs.push(newLog);
                console.log('[1158]:');
                console.log(newLog);
                service.addTimeLog(newLog).then(function(response){
                    //delete timer data
                    deleteStopBreakTimeTimerId(timerId).then(function(res){
                        console.log('stopBreakTimeAt -> timer executed DONE');
                        $rootScope.$broadcast('timelogsChanged');
                    });
                });
            });
        }, duration);
        let expectedEndTime = new Date((new Date).valueOf() + duration);
        console.log('[1170]:');
        console.log(expectedEndTime);
        saveStopBreakTimeTimerId(timerId, jobId, teamMemberId).then(function(res){
            defer.resolve(true);
        });

        return defer.promise;
    };

    service.startTeamShift = function(jobId){
        function newTimeLog(teamMember){
            let newLog = angular.copy(TimeLogVM);  
            newLog.jobId = teamMember.jobId;
            newLog.rJobId = teamMember.rJobId;
            newLog.teamMemberId = teamMember.id;
            newLog.rTeamMemberId = teamMember.rId;
            return newLog;
        }
        function addDefaultStms(activeTeam){
            let defer = $q.defer();
    
            let stmsExist = $linq.Enumerable().From(activeTeam).FirstOrDefault(null, 'f => f.staffId == "' + AccountSvc.getUserIdV2() + '"');
            if(activeTeam.length == 0 || !stmsExist){
                let teamMember = angular.copy(TeamMemberVM);                
                teamMember.id = (new ObjectId()).toString();
                teamMember._id = objectTypes.teammember + teamMember.id;
                teamMember.staffId = AccountSvc.getUserIdV2();
                teamMember.jobId = jobId;
                teamMember.tenantId = AccountSvc.getTenantIdV2();
                teamMember.isSTMS = true;

                service.addTeamMembers(jobId, [teamMember]).then(function(response){
                    activeTeam.push(teamMember);
                    defer.resolve(true);
                });
            }
            else{
                defer.resolve(true);
            }
            return defer.promise;        
        }        

        let defer = $q.defer();        
        service.getTeam(jobId).then(function (activeTeam) {
            addDefaultStms(activeTeam).then(function(){
                async.eachSeries(activeTeam, function(teamMember, callback){
                    service.getTeamMemberLogs(jobId, teamMember.id).then(function(teamMemberLogs){
                        //if toggle start
                        /// only if
                        //// timelogs empty or lastlog is end
                        //if toggle end
                        /// only if
                        //// lastlog is start or lastlog is breaktimeend
                        let newLog = newTimeLog(teamMemberLogs);
                        newLog.timeLogType = TimeLogTypeEnum.STARTTIME;
                        newLog.time = new Date();                                                
                        service.addTimeLog(newLog).then(function(response){
                            callback();
                        });
                    });
                }).then(function(response){
                    defer.resolve(true);                
                }, function(error){
                    defer.reject(error);
                });
            }, function(error){
                defer.reject(error);
            });
        });

        return defer.promise;
    };
    
    service.updateBreakTimes = function(jobId, breakTimeUpdates){
        let defer = $q.defer();        
        if(breakTimeUpdates.length > 0){
            async.each(breakTimeUpdates, function(btChange, callback){
                console.log('[1178]:');
                console.log(btChange);
                service.getTeamMemberLogs(jobId, btChange.id).then(function(teamMemberLogs){
                    if(teamMemberLogs){
                        btChange.jobId = teamMemberLogs.jobId;
                        btChange.teamMemberId = teamMemberLogs.id;                        
                        btChange.isBreakTimeUpdate = true;
                        console.log('[1185]:');
                        console.log(btChange);
                    }
                    callback(null);
                });
            }).then(function(error){
                console.log(error);
                if(error){
                    return defer.reject(error);
                }

                updateBreakTimes(jobId, breakTimeUpdates).then(function(response){
                    console.log('[1198]:');
                    defer.resolve(true);
                });
            });
        }
        else
            defer.resolve(true);

        return defer.promise;
    };


    function updateBreakTimes(jobId, breakTimeUpdates){
        //get job
        //get team members that need updating
        //update each team member total break time
        //send break time updates to server
        let defer = $q.defer();
        let objJobId = objectTypes.job + jobId;

        DBSvc.get(objJobId).then(function(job){
            if(!job)
                return defer.resolve(true);            
            
            let teamMembersQry = $linq.Enumerable().From(job.teamMembers);
            breakTimeUpdates.forEach(btUpdate =>{
                let teamMember = teamMembersQry.FirstOrDefault(null, 'f => f.id == "' + btUpdate.teamMemberId +'"');
                if(teamMember){
                    teamMember.totalBreakTimeMinutes = btUpdate.totalBreakTimeMinutes;
                }
            });
            console.log(job);
            DBSvc.save([job]).then(function(saveResult){
                let updateData = { _id: objectTypes.breaktimeupdate + (new ObjectId()).toString(), jobId: jobId, updates: breakTimeUpdates };                
                RemoteSyncSvc.add(updateData).then(function(addResult){
                    defer.resolve(true);
                });                
            });
        });

        return defer.promise;
    };

    service.hideTeamMember = function(jobId, teamMemberId){
        let defer = $q.defer();
        let objJobId = objectTypes.job + jobId;
        DBSvc.get(objJobId).then(function(job){
            if(!job)
                return $q.resolve(true);
            let teamMember = $linq.Enumerable().From(job.teamMembers).FirstOrDefault(null, 'f => f.id == "' + teamMemberId +'"');
            if(teamMember){
                teamMember.hidden = true;
                DBSvc.save([job]).then(function(response){
                    defer.resolve(true);                    
                });
            }
        })

        return defer.promise;
    };

    service.unhideTeamMembers = function(jobId, staffIds){
        let defer = $q.defer();
        let objJobId = objectTypes.job + jobId;
        let result = [];
        DBSvc.get(objJobId).then(function(job){
            if(!job)//don't forget to test this section
                return $q.resolve(true);            
            let teamMembersQry = $linq.Enumerable().From(job.teamMembers);                        
            staffIds.forEach(staffId => {
                let teamMember = teamMembersQry.FirstOrDefault(null, 'f => f.staffId == "' + staffId + '" && f.hidden == true');
                console.log(teamMember);
                if(teamMember){
                    teamMember.hidden = false;
                    result.push(staffId);
                }
            });
            DBSvc.save([job]).then(function(response){
                defer.resolve(result);                    
            });
        });

        return defer.promise;
    };

    service.addDefaultStms = function(jobId){
        let defer = $q.defer();
        DBSvc.getTeam(jobId).then(function(team){
            //if owner not exist in team
            //add owner to team            
            let stmsExist = $linq.Enumerable().From(team).FirstOrDefault(null, 'f => f.staffId == "' + AccountSvc.getUserIdV2() + '"');
            if(!stmsExist){
                let teamMember = angular.copy(TeamMemberVM);                
                teamMember.id = (new ObjectId()).toString();
                teamMember._id = objectTypes.teammember + teamMember.id;
                teamMember.staffId = AccountSvc.getUserIdV2();
                teamMember.jobId = jobId;
                teamMember.tenantId = AccountSvc.getTenantIdV2();
                teamMember.isSTMS = true;
                service.addTeamMembers(jobId, [teamMember]).then(function(response){
                    defer.resolve(true);
                });
            }
            else defer.resolve(true);
        });
        return defer.promise;
    };

    service.saveJob = function(job){
        return DBSvc.save([job]);
    };

    service.startTrucksShift = function(jobId, startTime){
        let defer = $q.defer();
        var data = {};
        data.id = (new ObjectId()).toString();
        data._id = objectTypes.starttrucksshift + data.id;
        data.jobId = jobId;
        data.startTime = startTime;
        RemoteSyncSvc.add(data).then(function(){
            defer.resolve(true);
        });

        return defer.promise;
    };

    service.getSiteRecordPDFLog = function (id) {
        return DBSvc.getSiteRecordPDFLog(id);
    }

    service.updateSiteRecordPDFLog = function (jobid, siteRecordPDFLog) {
        let defer = $q.defer();
        DBSvc.get(objectTypes.job + jobid).then(function (job) {
            if (job != null) {
                delete siteRecordPDFLog.id;
                delete siteRecordPDFLog._id;
                job.siteRecordPDFLog = siteRecordPDFLog;
                DBSvc.save([job]).then(function (response) {
                    job._id = objectTypes.siteRecordpdflog + job.id;
                    RemoteSyncSvc.add(job).then(function (response) {
                        defer.resolve(true);
                    });
                }, function (error) {
                    console.log(error);
                    defer.reject(error);
                });
            }
        });

        return defer.promise;
    };

    service.updateJobStatus = function (jobid) {
        let defer = $q.defer();
        DBSvc.get(objectTypes.job + jobid).then(function (job) {
            if (job != null) {
                DBSvc.save([job]).then(function (response) {
                    job._id = objectTypes.updateJobStatus + job.id;
                    RemoteSyncSvc.add(job).then(function (response) {
                        defer.resolve(true);
                    });
                }, function (error) {
                    console.log(error);
                    defer.reject(error);
                });
            }
        });

        return defer.promise;
    };
    
    return service;
}]);