mainApp.factory('SiteCheckAlarmSvc', ['$q', 'JobsSvc', '$linq', '$cordovaLocalNotification', '$rootScope', '$cordovaVibration', '$timeout', function myfunction($q, JobsSvc, $linq, $cordovaLocalNotification, $rootScope, $cordovaVibration, $timeout) {
    //after site deployment start site check alarm timer
    //if times up show site check alarm
    //reset timer
    //after a site check is done reset the timer
    var isRunning = false;
    var service = {};
    var alarmTimeout = null;
    let vibrateDuration = 2000;
    var notifyLapse = 6600000;//110 mins
    //var notifyLapse = 30000;//30 secs for testing only
    var alarmTimeout = null;
    function alarmUser() {
        alarmTimeout = $timeout(function () {
            if(isRunning)
                $cordovaLocalNotification.schedule({
                    id: 1,
                    title: 'Site Pro',
                    text: 'Site Check required.',
                    data: { source: 'sitecheckalarm', immediate: false },
                    vibrate: true
                }).then(function (result) {
                    console.log('site check alarm triggered');
                    navigator.vibrate(vibrateDuration);
                });
            alarmUser();
        }, notifyLapse);
    }

    service.start = function () {
        console.log('service.start');
        if(!isRunning){
            isRunning = true;
            alarmUser();
        }
    };
    service.reset = function () {
        console.log('service.reset');
        var result = $timeout.cancel(alarmTimeout);
        isRunning = true;
        alarmUser();        
    };
    service.alarmNow = function () {
        console.log('service.alarmNow');
        $cordovaLocalNotification.schedule({
            id: 1,
            title: 'Site Pro',
            text: 'Site Check required.',
            data: { source: 'sitecheckalarm', immediate: false },
            vibrate: true
        }).then(function (result) {
            navigator.vibrate(vibrateDuration);
        });
    };
    service.stop = function () {
        console.log('service.stop');
        isRunning = false;
        $timeout.cancel(alarmTimeout);        
    };
    service.pause = function () {
        console.log('service.pause');
        isRunning = false;
    };
    service.resume = function () {
        console.log('service.resume');
        isRunning = true;
    };
    return service;
}]);