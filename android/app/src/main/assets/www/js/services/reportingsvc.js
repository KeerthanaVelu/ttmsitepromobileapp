mainApp.factory('ReportingService', ['$filter', '$q', 'AccountSvc', '$timeout', 'RemoteUrls', 'JobsSvc', function ($filter, $q, AccountSvc, $timeout, RemoteUrls, JobsSvc){
    let service = {};

    let jobId = "";
    function splitDataToPages(data){
        var size = 7;
        let pagedSiteChecks = [];
        for(var x = 0;x < data.length;){
            console.log(x);
            var page;
            var page = data.slice(x,x+size);
            var siteCheck = page[page.length-1];
            if(pagedSiteChecks.length < 1){
                if(siteCheck && siteCheck.isTTMRemoval || !siteCheck.hasOwnProperty('isTTMRemoval')){
                    size = 6;
                }
            }
            else if(!siteCheck.hasOwnProperty('isTTMRemoval')){
                size = 5;
            }
            else{
                size = 6;
            }
            page = data.slice(x, x+size);
            pagedSiteChecks.push({siteChecks: page});
            x += size;
        }
        //get adjustments
        //console.log(pagedSiteChecks);
        pagedSiteChecks.forEach((item) => {
            var adjustments = [];
            item.siteChecks.forEach(scItem => {
                if(scItem.notes){
                    console.log(scItem.startTime);
                    console.log(scItem.notes);
                    adjustments.push({time: scItem.startTime, notes: scItem.notes});
                }
            });
            item.adjustments = adjustments;
        });
        console.log(pagedSiteChecks);
        return pagedSiteChecks;
    }

    function getSCItemResult(item){
        if(item.checked)
            return '<img src="file:///android_asset/www/pdftemplates/check-solid-green.png" style="width: 1em;"/>';
        else if(item.checkedWithIssues)
            return '<img src="file:///android_asset/www/pdftemplates/times-solid-red.png" style="width: 1em;"/>';
        else if(item.notApplicable)
            return 'N/A';
        else
            return '';
    }

    service.generateOnsiteRecord = function (siteChecks, tsls, tmpReference, address, rps, onsitecontact, onsitecontactsignature, stmssignature, id, staff, createdOn, truckReport, gearLeftOnSiteRecord, ClientContract) {

        let jobId = id;
        //read the template file
        //generate report data
        //save to a report file
        //generate pdf
        function readOnsiteRecordTemplate(){
            let defer = $q.defer();

            window.resolveLocalFileSystemURL('file:///android_asset/www/pdftemplates/onsiterecord-template.html', function(fileEntry){
                fileEntry.file(function(file){
                    var reader = new FileReader();
                    reader.onloadend = function(){
                        defer.resolve(this.result);
                    };
                    reader.onerror = function(error){
                        defer.reject(error);
                    };

                    reader.readAsText(file);
                });
            }, function(error){
                alert('Something went wrong.');
            });

            return defer.promise;
        }

        function readMoreTslContainerTemplate(){
            let defer = $q.defer();

            window.resolveLocalFileSystemURL('file:///android_asset/www/pdftemplates/onsiterecord-moretslcontainer-template.html', function(fileEntry){
                fileEntry.file(function(file){
                    var reader = new FileReader();
                    reader.onloadend = function(){
                        defer.resolve(this.result);
                    };
                    reader.onerror = function(error){
                        defer.reject(error);
                    };

                    reader.readAsText(file);
                });
            }, function(error){
                alert('Something went wrong.');
            });

            return defer.promise;
        }

        function readTslTemplate(){
            let defer = $q.defer();

            window.resolveLocalFileSystemURL('file:///android_asset/www/pdftemplates/onsiterecord-tsl-template.html', function(fileEntry){
                fileEntry.file(function(file){
                    var reader = new FileReader();
                    reader.onloadend = function(){
                        defer.resolve(this.result);
                    };
                    reader.onerror = function(error){
                        defer.reject(error);
                    };

                    reader.readAsText(file);
                });
            }, function(error){
                alert('Something went wrong.');
            });

            return defer.promise;
        }

        function readTslTableContainerTemplate(){
            let defer = $q.defer();

            window.resolveLocalFileSystemURL('file:///android_asset/www/pdftemplates/tsl-table-container-template.html', function(fileEntry){
                fileEntry.file(function(file){
                    var reader = new FileReader();
                    reader.onloadend = function(){
                        defer.resolve(this.result);
                    };
                    reader.onerror = function(error){
                        defer.reject(error);
                    };

                    reader.readAsText(file);
                });
            }, function(error){
                alert('Something went wrong.');
            });

            return defer.promise;
        }

        function generateReportData(siteChecks) {
            let pages = splitDataToPages(siteChecks);
            //console.log(pages);
            let pagesContent = '';
            var rowsToShow = JSON.parse(JSON.stringify(siteCheckItems));
            //rowsToShow.push({ id: 13, itemName: '' });
            rowsToShow.push({ id: 14, itemName: 'Time inspection completed:' });
            rowsToShow.push({ id: 15, itemName: 'Signature:' });
            //show per page
            //2 data points - site checks, adjustments
            pages.forEach((page, pageIndex) => {
                let pageContent = '';
                pageContent += '<table class="report-container">';
                pageContent += '    <thead class="report-header">';
                pageContent += '    <tr>';
                pageContent += '        <th class="report-header-cell">';
                pageContent += '            <div class="header-info">';
                pageContent += '                <div class="header">';
                pageContent += '                    <img src="file:///android_asset/www/pdftemplates/wakakotahinztalogo.png" class="nzta-logo" style="float: left;"/>';
                pageContent += '                    <table class="bordered" style="float: right;">';
                pageContent += '                        <colgroup><col width="30%"/><col/></colgroup>';
                pageContent += '                        <tr><td class="header2">TMP or generic plan reference</td><td>{{tmpReference}}</td></tr>';
                pageContent += '                    </table>';
                pageContent += '                </div>';
                pageContent += '            </div>';
                pageContent += '        </th>';
                pageContent += '    </tr>';
                pageContent += '    </thead>';
                pageContent += '    <tbody class="report-content">';
                pageContent += '        <tr>';
                pageContent += '            <td class="report-content-cell">';
                pageContent += '                <div class="main">';
                pageContent = pageContent.replace('{{tmpReference}}', (tmpReference ? tmpReference : ''));
                let tables = '';
                // site checks <--
                var table = '<table class="bordered">';
                table += '<colgroup>';
                table += '<col width="30%"/><col/>';
                table += '<col width="10%"/>';
                table += '<col width="10%"/>';
                table += '<col width="10%"/>';
                table += '<col width="10%"/>';
                table += '<col width="10%"/>';
                table += '<col width="10%"/>';
                table += '<col width="10%"/>';
                table += '</colgroup>';
                table += '<tr class="header1">';
                table += '<td colspan="8">Worksite monitoring</td>';
                table += '</tr>';
                table += '<tr>';
                table += '<td colspan="8">TTM to be monitored and 2 hourly inspections documented below.</td>';
                table += '</tr>';
                table += '<tr class="header2">';
                table += '<td style="text-align: left;">Items to be inspected</td>';
                //table += '<td>TTM<br/>set-up   </td>';
                table += '<td width="10%">2 hourly<br/>check</td>';
                table += '<td>2 hourly<br/>check</td>';
                table += '<td>2 hourly<br/>check</td>';
                table += '<td>2 hourly<br/>check</td>';
                table += '<td>2 hourly<br/>check</td>';
                table += '<td>2 hourly<br/>check</td>';
                table += '<td>TTM<br/>removal</td>';
                table += '</tr>';
                rowsToShow.forEach((item, scItemIndex) => {
                    var rowTag = '<tr><td class="' + (item.id <= 11 ? 'item-name' : (item.id == 14 || item.id == 15 ? 'header4' : '')) + '">' + item.itemName + '</td>';
                    var dataColumns = pageIndex == 0 && page.siteChecks.length <= 7 ? 7 : 6;
                    for (var a = 0; a < dataColumns; a++) {
                        var row = page.siteChecks[a];
                        if (pageIndex > 0 && a == 0)
                            rowTag += '<td></td>';

                        //prepend empty cells when row is ttmremoval
                        if (row && row.isTTMRemoval) {
                            var emptyColsCount = dataColumns - a;
                            for (var b = 0; b < emptyColsCount - 1; b++)
                                rowTag += '<td></td>';
                        }
                        if (row) {
                            var rowData = row.siteCheckItems[scItemIndex];
                            if (item.id <= 13 || item.id == 15) {
                                if (rowData)
                                    rowTag += '<td style="text-align: center;">' + getSCItemResult(rowData) + '</td>';
                                else
                                    rowTag += stmssignature && stmssignature.length > 0 ? '<td><img src="' + stmssignature + '" style="height: 50px;"/></td>' : '<td></td>';//signature
                                //rowTag += '<td>230</td>';//signature
                            }
                            else if (item.id == 14)
                                rowTag += '<td>' + $filter('date')(row.startTime, 'dd-MM-yyyy hh:mm a') + '</td>';//time inspection completed
                        }
                        else {
                            rowTag += '<td></td>';
                        }
                    }
                    rowTag += '</tr>';
                    table += rowTag;
                });
                table += '<td colspan="8" class="header4">Comments:</td>';
                table += '</table>';
                tables += table;
                pageContent += tables;
                // site checks -->

                // adjustments <--
                var adjustmentsTable = '';
                adjustmentsTable += '<table class="bordered" style="border-top: none;">';
                adjustmentsTable += '    <colgroup><col width="20%"/><col/></colgroup>';
                adjustmentsTable += '    <tr class="no-height"><td></td><td></td></tr>';
                adjustmentsTable += '    <tr class="next-sibling"><td class="header3"><b>Time</b></td><td class="header3"><b>Adjustments made and reason for change</b></td></tr>';
                var maxRows = 7;
                var adjustments = page.adjustments;
                adjustments.forEach(adjustment => {
                    var row = '<tr>';
                    var timeCol = '<td>' + $filter('date')(adjustment.time, 'dd-MM-yyyy hh:mm a') + '</td>';
                    var adjustmentCol = '<td>' + adjustment.notes + '</td>';
                    row += timeCol + adjustmentCol;
                    row += '</tr>';
                    adjustmentsTable += row;
                });
                var emptyRowsCount = maxRows - adjustments.length;
                for (var a = 0; a < emptyRowsCount; a++) {
                    adjustmentsTable += '<tr><td></td><td></td></tr>';
                }
                adjustmentsTable += '</table></div>';
                pageContent += adjustmentsTable;
                // adjustments -->
                pageContent += '                    </div>';
                pageContent += '                </td>';
                pageContent += '            </tr>';
                pageContent += '        </tbody>';
                pageContent += '        <tfoot class="report-footer">';
                pageContent += '        <tr>';
                pageContent += '            <td class="report-footer-cell">';
                pageContent += '            <div class="footer-container">';
                pageContent += '                <div class="bottom">';
                pageContent += '                        <table style="width: 100%;font-size:11px;">';
                pageContent += '                            <tr><td><i>Traffic control devices manual</i> part 8 CoPTTM</td><td>Section E, appendix A: Traffic management plans</td><td style="text-align: right;padding-right: 10px;">Edition 4, April 2020</td></tr>';
                pageContent += '                            <tr><td colspan="3" style="text-align: center;">Page ' + (pageIndex + 2) + '</td></tr>';
                pageContent += '                        </table>';
                pageContent += '                </div>';
                pageContent += '            </div>';
                pageContent += '            </td>';
                pageContent += '        </tr>';
                pageContent += '        </tfoot>';
                pageContent += '</table>';
                pagesContent += pageContent;
            });

            return pagesContent;
        }

        function generateTslData(tsls, tslTemplate, tslTableContainerTemplate, moreTslContainerTemplate) {
            let result = { tsls: '', moreTsls: '' };
            if (tsls === undefined || tsls === null || !Array.isArray(tsls))
                return result;
            if (tsls.length <= 1) {
                let tslsData = [];
                for (let i = 1; i <= 5; i++) {
                    let tslRow = tslTemplate;
                    tslRow = tslRow.replace('{{fromLocation}}', '');
                    tslRow = tslRow.replace('{{toLocation}}', '');
                    tslRow = tslRow.replace('{{installDate}}', '');
                    tslRow = tslRow.replace('{{installTime}}', '');
                    tslRow = tslRow.replace('{{removalDate}}', '');
                    tslRow = tslRow.replace('{{removalTime}}', '');
                    tslRow = tslRow.replace('{{remainsInPlace}}', '');
                    tslRow = tslRow.replace('{{tslSpeed}}', '');
                    tslRow = tslRow.replace('{{tslLength}}', '');
                    tslsData += tslRow;
                    result.tsls = tslTableContainerTemplate.replace('{{tsls}}', tslsData);
                }
                return result;
            }

            let tslsData = '';
            let moreTslsData = '';
            let rowCount = 0;
            let moreTslsPageSize = 11;
            let moreTslsLastRowIndexPerPage = 16;
            let moreTslsPageNumber = 2;
            for (let index = 0; index < tsls.length; index = index + 2) {
                let fromTsl = tsls[index];
                let toTsl = tsls[index + 1];
                let tslRow = tslTemplate;
                if (fromTsl && toTsl && (fromTsl.address && fromTsl.address.length > 0) && (toTsl.address && toTsl.address.length > 0)) {
                    rowCount++;
                    tslRow = tslRow.replace('{{fromLocation}}', !isNullOrWhiteSpace(fromTsl.address) ? fromTsl.address.replace(', New Zealand', '') : '');
                    tslRow = tslRow.replace('{{toLocation}}', !isNullOrWhiteSpace(toTsl.address) ? toTsl.address.replace(', New Zealand', '') : '');
                    //tslRow = tslRow.replace('{{toLocation}}', toTsl.address ? toTsl.address : '');
                    tslRow = tslRow.replace('{{installDate}}', toTsl.Timestamp ? $filter('date')(toTsl.Timestamp, 'dd-MM-yyyy') : '');
                    tslRow = tslRow.replace('{{installTime}}', toTsl.Timestamp ? $filter('date')(toTsl.Timestamp, 'hh:mm a') : '');
                    tslRow = tslRow.replace('{{removalDate}}', toTsl.tslRemoved ? $filter('date')(toTsl.tslRemoved, 'dd-MM-yyyy') : '');
                    tslRow = tslRow.replace('{{removalTime}}', toTsl.tslRemoved ? $filter('date')(toTsl.tslRemoved, 'hh:mm a') : '');
                    tslRow = tslRow.replace('{{remainsInPlace}}', toTsl.remainsInPlace ? $filter('date')(toTsl.remainsInPlace, 'dd-MM-yyyy') : '');
                    tslRow = tslRow.replace('{{tslSpeed}}', toTsl.PermanentLimit + '/' + toTsl.Limit);
                    tslRow = tslRow.replace('{{tslLength}}', toTsl.length ? toTsl.length : '');

                    tslRow = tslRow.replace('{{rpFrom}}', fromTsl.rp ? 'RP: ' + fromTsl.rp : '');
                    tslRow = tslRow.replace('{{rpTo}}', toTsl.rp ? 'RP: ' + toTsl.rp : '');

                    if (rowCount <= 5) {//for page 1
                        tslsData += tslRow;
                    }
                    else {
                        moreTslsData += tslRow;
                        //add page number?
                        //row 6 up
                        if (rowCount === moreTslsLastRowIndexPerPage || rowCount === (tsls.length - 1)) {
                            result.moreTsls += moreTslContainerTemplate.replace('{{tsls}}', moreTslsData).replace("{{pageNumber}}", moreTslsPageNumber);
                            moreTslsData = '';
                            moreTslsLastRowIndexPerPage += moreTslsPageSize;
                            //console.log(moreTslsPageNumber);
                            moreTslsPageNumber++;
                        }
                    }
                }
                else {
                    tslRow = tslRow.replace('{{fromLocation}}', '');
                    tslRow = tslRow.replace('{{toLocation}}', '');
                    tslRow = tslRow.replace('{{installDate}}', '');
                    tslRow = tslRow.replace('{{installTime}}', '');
                    tslRow = tslRow.replace('{{removalDate}}', '');
                    tslRow = tslRow.replace('{{removalTime}}', '');
                    tslRow = tslRow.replace('{{remainsInPlace}}', '');
                    tslRow = tslRow.replace('{{tslSpeed}}', '');
                    tslRow = tslRow.replace('{{tslLength}}', '');
                    tslRow = tslRow.replace('{{rpFrom}}', '');
                    tslRow = tslRow.replace('{{rpTo}}', '');
                }
                console.log(rowCount);
            }

            let emptyTslBoxes = '';
            if (rowCount > 0 && rowCount <= 5) {
                //append empty tsl boxes
                if (rowCount < 5) {
                    let emptyTslsCount = 5 - rowCount;
                    for (let i = 1; i <= emptyTslsCount; i++) {
                        let tslRow = tslTemplate;
                        tslRow = tslRow.replace('{{fromLocation}}', '');
                        tslRow = tslRow.replace('{{toLocation}}', '');
                        tslRow = tslRow.replace('{{installDate}}', '');
                        tslRow = tslRow.replace('{{installTime}}', '');
                        tslRow = tslRow.replace('{{removalDate}}', '');
                        tslRow = tslRow.replace('{{removalTime}}', '');
                        tslRow = tslRow.replace('{{remainsInPlace}}', '');
                        tslRow = tslRow.replace('{{tslSpeed}}', '');
                        tslRow = tslRow.replace('{{tslLength}}', '');
                        tslRow = tslRow.replace('{{rpFrom}}', '');
                        tslRow = tslRow.replace('{{rpTo}}', '');
                        emptyTslBoxes += tslRow;
                    }
                }
            }
            tslsData += emptyTslBoxes;
            result.tsls = tslTableContainerTemplate.replace('{{tsls}}', tslsData);

            return result;
        }


        function generateTslTruckData(truckReport, truckcontainer_template, staff, createdOn) {
            console.log(createdOn, "createdOn--");
            let result = { truckcontainer: '' };
            let temptemplate = truckcontainer_template

            for (let i = 0; i < truckReport.length; i++) {
                let temp = temptemplate;
                console.log(staff.isSignApp, "---")

                if (staff !== null && staff !== undefined) {
                    temp = temp.replace('{{STMSName}}', staff.fullName)
                    temp = temp.replace('{{STMSId}}', staff.stmsCertificationNo === null ? "" : staff.stmsCertificationNo)
                    temp = temp.replace('{{signiture}}', staff.signiture === null ? "" : staff.signiture === "" ? "" : (staff.isSignApp == true ? (RemoteUrls.auditproRootPath + "/UserData/images/" + staff.signiture) : (RemoteUrls.tmproRootPath + "/Uploads/User/" + staff.signiture)))
                } else {
                    temp = temp.replace('{{STMSName}}', '')
                    temp = temp.replace('{{STMSId}}', '')
                    temp = temp.replace('{{signiture}}', '')
                }

                temp = temp.replace('{{TruckNo}}', truckReport[i].truckNumber != null ? truckReport[i].truckNumber :'')
                temp = temp.replace('{{createdOn}}', createdOn != null ? $filter('date')(createdOn, 'dd-MM-yyyy') : '')

                temp = temp.replace('{{isCamera}}', truckReport[i].isCamera == true ?'<img src="file:///android_asset/www/pdftemplates/tick-icon.svg" />' :'')
                temp = temp.replace('{{isReverseCamera}}', truckReport[i].isReverseCamera == true ?'<img src="file:///android_asset/www/pdftemplates/tick-icon.svg" />' :'')
                temp = temp.replace('{{hubExpiry}}', truckReport[i].hubExpiry != null ? $filter('date')(truckReport[i].hubExpiry, 'dd-MM-yyyy') :'' )
                temp = temp.replace('{{huboReading}}', truckReport[i].huboReading )
                temp = temp.replace('{{cofExpiry}}', truckReport[i].cofExpiry != null ? $filter('date')(truckReport[i].cofExpiry, 'dd-MM-yyyy') : '')
                temp = temp.replace('{{regoExpiry}}', truckReport[i].regoExpiry != null ? $filter('date')(truckReport[i].regoExpiry, 'dd-MM-yyyy') : '')
                temp = temp.replace('{{isAlarms}}', truckReport[i].isAlarms == true ? '<img src="file:///android_asset/www/pdftemplates/tick-icon.svg" />' : '')
                temp = temp.replace('{{isAssistDevices}}', truckReport[i].isAssistDevices == true ? '<img src="file:///android_asset/www/pdftemplates/tick-icon.svg" />' : '')
                temp = temp.replace('{{isEngineoilwater}}', truckReport[i].isEngineoilwater == true ? '<img src="file:///android_asset/www/pdftemplates/tick-icon.svg" />' : '')
                temp = temp.replace('{{isTyres}}', truckReport[i].isTyres == true ? '<img src="file:///android_asset/www/pdftemplates/tick-icon.svg" />' : '')
                temp = temp.replace('{{isFireExtinguisher}}', truckReport[i].isFireExtinguisher == true ? '<img src="file:///android_asset/www/pdftemplates/tick-icon.svg" />' : '')
                temp = temp.replace('{{isFirstAidKit}}', truckReport[i].isFirstAidKit == true ? '<img src="file:///android_asset/www/pdftemplates/tick-icon.svg" />' : '')
                temp = temp.replace('{{isYardbroom}}', truckReport[i].isYardbroom == true ? '<img src="file:///android_asset/www/pdftemplates/tick-icon.svg" />' : '')
                temp = temp.replace('{{isPailofsoapywater_brush}}', truckReport[i].isPailofsoapywater_brush == true ? '<img src="file:///android_asset/www/pdftemplates/tick-icon.svg" />' : '')
                temp = temp.replace('{{isAmber}}', truckReport[i].isAmber == true ? '<img src="file:///android_asset/www/pdftemplates/tick-icon.svg" />' : '')
                temp = temp.replace('{{isAlllights}}', truckReport[i].isAlllights == true ? '<img src="file:///android_asset/www/pdftemplates/tick-icon.svg" />' : '')
                temp = temp.replace('{{isTruckMirrors}}', truckReport[i].isTruckMirrors == true ? '<img src="file:///android_asset/www/pdftemplates/tick-icon.svg" />' : '')
                temp = temp.replace('{{isWheel}}', truckReport[i].isWheel == true ? '<img src="file:///android_asset/www/pdftemplates/tick-icon.svg" />' : '')
                temp = temp.replace('{{isWaterCoolerFull}}', truckReport[i].isWaterCoolerFull == true ? '<img src="file:///android_asset/www/pdftemplates/tick-icon.svg" />' : '')
                temp = temp.replace('{{isNotes}}', truckReport[i].isNotes != null ? truckReport[i].isNotes : '')
                


                result.truckcontainer += temp;
            }
            return result;
        }

        function generateGearLeftData(gearLeftOnSiteRecord, gearleftcontainer_template, staff, createdOn, address, tmpReference, ClientContract) {
            let result = { truckcontainer: '' };
            let temptemplate = gearleftcontainer_template

            for (let i = 0; i < gearLeftOnSiteRecord.length; i++) {
               
                let temp = temptemplate;
                if (staff !== null && staff !== undefined) {
                    temp = temp.replace('{{STMSName}}', staff.fullName)
                    temp = temp.replace('{{STMSId}}', staff.stmsCertificationNo === null ? "" : staff.stmsCertificationNo)
                    temp = temp.replace('{{signiture}}', staff.signiture === null ? "" : staff.signiture === "" ? "" : (staff.isSignApp == true ? (RemoteUrls.auditproRootPath + "/UserData/images/" + staff.signiture) : (RemoteUrls.tmproRootPath + "/Uploads/User/" + staff.signiture)))
                } else {
                    temp = temp.replace('{{STMSName}}', '')
                    temp = temp.replace('{{STMSId}}', '')
                    temp = temp.replace('{{signiture}}', '')
                }
                temp = temp.replace('{{createdOn}}', createdOn != null ? $filter('date')(createdOn, 'dd-MM-yyyy') : '')
                temp = temp.replace('{{RoadName}}', address != null ? address : '')
                temp = temp.replace('{{TMPNO}}', tmpReference != null ? tmpReference : '')
                temp = temp.replace('{{ClientContract}}', ClientContract != null ? ClientContract : '')
                
                if (gearLeftOnSiteRecord[i].level1Items.length >= 10) {
                    temp = temp.replace('{{Level1lefttext_row1}}', gearLeftOnSiteRecord[i].level1Items[0].description)
                    temp = temp.replace('{{Level1leftqty_row1}}', gearLeftOnSiteRecord[i].level1Items[0].qty)
                    temp = temp.replace('{{Level1lefttext_row2}}', gearLeftOnSiteRecord[i].level1Items[2].description)
                    temp = temp.replace('{{Level1leftqty_row2}}', gearLeftOnSiteRecord[i].level1Items[2].qty)
                    temp = temp.replace('{{Level1lefttext_row3}}', gearLeftOnSiteRecord[i].level1Items[4].description)
                    temp = temp.replace('{{Level1leftqty_row3}}', gearLeftOnSiteRecord[i].level1Items[4].qty)
                    temp = temp.replace('{{Level1lefttext_row4}}', gearLeftOnSiteRecord[i].level1Items[6].description)
                    temp = temp.replace('{{Level1leftqty_row4}}', gearLeftOnSiteRecord[i].level1Items[6].qty)
                    temp = temp.replace('{{Level1lefttext_row5}}', gearLeftOnSiteRecord[i].level1Items[8].description)
                    temp = temp.replace('{{Level1leftqty_row5}}', gearLeftOnSiteRecord[i].level1Items[8].qty)

                    temp = temp.replace('{{Level1righttext_row1}}', gearLeftOnSiteRecord[i].level1Items[1].description)
                    temp = temp.replace('{{Level1rightqty_row1}}', gearLeftOnSiteRecord[i].level1Items[1].qty)
                    temp = temp.replace('{{Level1righttext_row2}}', gearLeftOnSiteRecord[i].level1Items[3].description)
                    temp = temp.replace('{{Level1rightqty_row2}}', gearLeftOnSiteRecord[i].level1Items[3].qty)
                    temp = temp.replace('{{Level1righttext_row3}}', gearLeftOnSiteRecord[i].level1Items[5].description)
                    temp = temp.replace('{{Level1rightqty_row3}}', gearLeftOnSiteRecord[i].level1Items[5].qty)
                    temp = temp.replace('{{Level1righttext_row4}}', gearLeftOnSiteRecord[i].level1Items[7].description)
                    temp = temp.replace('{{Level1rightqty_row4}}', gearLeftOnSiteRecord[i].level1Items[7].qty)
                    temp = temp.replace('{{Level1righttext_row5}}', gearLeftOnSiteRecord[i].level1Items[9].description)
                    temp = temp.replace('{{Level1rightqty_row5}}', gearLeftOnSiteRecord[i].level1Items[9].qty)

                    let Level1MoreRowData = '';
                    for (let y = 9; y < gearLeftOnSiteRecord[i].level1Items.length; y++){
                        Level1MoreRowData += '<tr> <td class="ppe-td1"> <div>';
                        Level1MoreRowData += gearLeftOnSiteRecord[i].level1Items.length-1 > y ? gearLeftOnSiteRecord[i].level1Items[y+1].description:'';
                        Level1MoreRowData += '</div></td ><td class="ppe-td2"><div class="text-center">';  
                        Level1MoreRowData += gearLeftOnSiteRecord[i].level1Items.length-1 > y ? gearLeftOnSiteRecord[i].level1Items[y + 1].qty : '';
                        Level1MoreRowData += '</div></td ><td class="ppe-td3"><div >';  
                        y = y + 1
                        if (gearLeftOnSiteRecord[i].level1Items.length - 1 > y) {
                            Level1MoreRowData += gearLeftOnSiteRecord[i].level1Items[y + 1].description;
                            Level1MoreRowData += '</div></td ><td class="ppe-td4"><div class="text-center">';
                            Level1MoreRowData += gearLeftOnSiteRecord[i].level1Items[y + 1].qty;
                            Level1MoreRowData += '</div></td ></tr>';

                        } else {
                            Level1MoreRowData += '</div></td ><td class="ppe-td4"><div class="text-center"></div></td ></tr>';
                        }                                            
                    }
                    temp = temp.replace('{{Level1MoreRow}}', Level1MoreRowData)

                } else {

                    temp = temp.replace('{{Level1lefttext_row1}}', gearLeftOnSiteRecord[i].level1Items.length >= 1 ? gearLeftOnSiteRecord[i].level1Items[0].description:'')
                    temp = temp.replace('{{Level1leftqty_row1}}', gearLeftOnSiteRecord[i].level1Items.length >= 1 ? gearLeftOnSiteRecord[i].level1Items[0].qty : '')                    

                    temp = temp.replace('{{Level1lefttext_row2}}', gearLeftOnSiteRecord[i].level1Items.length >= 3 ? gearLeftOnSiteRecord[i].level1Items[2].description : '')
                    temp = temp.replace('{{Level1leftqty_row2}}', gearLeftOnSiteRecord[i].level1Items.length >= 3?gearLeftOnSiteRecord[i].level1Items[2].qty:'')

                    temp = temp.replace('{{Level1lefttext_row3}}', gearLeftOnSiteRecord[i].level1Items.length >= 5 ? gearLeftOnSiteRecord[i].level1Items[4].description : '')
                    temp = temp.replace('{{Level1leftqty_row3}}', gearLeftOnSiteRecord[i].level1Items.length >= 5 ?gearLeftOnSiteRecord[i].level1Items[4].qty: '')
     
                    temp = temp.replace('{{Level1lefttext_row4}}', gearLeftOnSiteRecord[i].level1Items.length >= 7 ?gearLeftOnSiteRecord[i].level1Items[6].description: '')
                    temp = temp.replace('{{Level1leftqty_row4}}', gearLeftOnSiteRecord[i].level1Items.length >= 7 ?gearLeftOnSiteRecord[i].level1Items[6].qty: '')
                
                    temp = temp.replace('{{Level1lefttext_row5}}', gearLeftOnSiteRecord[i].level1Items.length >= 9 ?gearLeftOnSiteRecord[i].level1Items[8].description: '')
                    temp = temp.replace('{{Level1leftqty_row5}}', gearLeftOnSiteRecord[i].level1Items.length >= 9 ?gearLeftOnSiteRecord[i].level1Items[8].qty: '')
                  
                    temp = temp.replace('{{Level1righttext_row1}}', gearLeftOnSiteRecord[i].level1Items.length >= 2 ?gearLeftOnSiteRecord[i].level1Items[1].description: '')
                    temp = temp.replace('{{Level1rightqty_row1}}', gearLeftOnSiteRecord[i].level1Items.length >= 2 ?gearLeftOnSiteRecord[i].level1Items[1].qty: '')
                
                    temp = temp.replace('{{Level1righttext_row2}}', gearLeftOnSiteRecord[i].level1Items.length >= 4 ?gearLeftOnSiteRecord[i].level1Items[3].description: '')
                    temp = temp.replace('{{Level1rightqty_row2}}', gearLeftOnSiteRecord[i].level1Items.length >= 4 ?gearLeftOnSiteRecord[i].level1Items[3].qty: '')
                  
                    temp = temp.replace('{{Level1righttext_row3}}', gearLeftOnSiteRecord[i].level1Items.length >= 6 ?gearLeftOnSiteRecord[i].level1Items[5].description: '')
                    temp = temp.replace('{{Level1rightqty_row3}}', gearLeftOnSiteRecord[i].level1Items.length >= 6 ?gearLeftOnSiteRecord[i].level1Items[5].qty: '')
                  
                    temp = temp.replace('{{Level1righttext_row4}}', gearLeftOnSiteRecord[i].level1Items.length >= 8 ? gearLeftOnSiteRecord[i].level1Items[7].description: '')
                    temp = temp.replace('{{Level1rightqty_row4}}', gearLeftOnSiteRecord[i].level1Items.length >= 8 ? gearLeftOnSiteRecord[i].level1Items[7].qty: '')
                                     
                    temp = temp.replace('{{Level1righttext_row5}}', gearLeftOnSiteRecord[i].level1Items.length >= 10 ? gearLeftOnSiteRecord[i].level1Items[9].description: '')
                    temp = temp.replace('{{Level1rightqty_row5}}', gearLeftOnSiteRecord[i].level1Items.length >= 10 ?gearLeftOnSiteRecord[i].level1Items[9].qty: '')
                                     
                    temp = temp.replace('{{Level1MoreRow}}', '')                                                                                         
                }

                if (gearLeftOnSiteRecord[i].level2Items.length >= 10) {
                    temp = temp.replace('{{Level2lefttext_row1}}', gearLeftOnSiteRecord[i].level2Items[0].description)
                    temp = temp.replace('{{Level2leftqty_row1}}', gearLeftOnSiteRecord[i].level2Items[0].qty)
                    temp = temp.replace('{{Level2lefttext_row2}}', gearLeftOnSiteRecord[i].level2Items[2].description)
                    temp = temp.replace('{{Level2leftqty_row2}}', gearLeftOnSiteRecord[i].level2Items[2].qty)
                    temp = temp.replace('{{Level2lefttext_row3}}', gearLeftOnSiteRecord[i].level2Items[4].description)
                    temp = temp.replace('{{Level2leftqty_row3}}', gearLeftOnSiteRecord[i].level2Items[4].qty)
                    temp = temp.replace('{{Level2lefttext_row4}}', gearLeftOnSiteRecord[i].level2Items[6].description)
                    temp = temp.replace('{{Level2leftqty_row4}}', gearLeftOnSiteRecord[i].level2Items[6].qty)
                    temp = temp.replace('{{Level2lefttext_row5}}', gearLeftOnSiteRecord[i].level2Items[8].description)
                    temp = temp.replace('{{Level2leftqty_row5}}', gearLeftOnSiteRecord[i].level2Items[8].qty)

                    temp = temp.replace('{{Level2righttext_row1}}', gearLeftOnSiteRecord[i].level2Items[1].description)
                    temp = temp.replace('{{Level2rightqty_row1}}', gearLeftOnSiteRecord[i].level2Items[1].qty)
                    temp = temp.replace('{{Level2righttext_row2}}', gearLeftOnSiteRecord[i].level2Items[3].description)
                    temp = temp.replace('{{Level2rightqty_row2}}', gearLeftOnSiteRecord[i].level2Items[3].qty)
                    temp = temp.replace('{{Level2righttext_row3}}', gearLeftOnSiteRecord[i].level2Items[5].description)
                    temp = temp.replace('{{Level2rightqty_row3}}', gearLeftOnSiteRecord[i].level2Items[5].qty)
                    temp = temp.replace('{{Level2righttext_row4}}', gearLeftOnSiteRecord[i].level2Items[7].description)
                    temp = temp.replace('{{Level2rightqty_row4}}', gearLeftOnSiteRecord[i].level2Items[7].qty)
                    temp = temp.replace('{{Level2righttext_row5}}', gearLeftOnSiteRecord[i].level2Items[9].description)
                    temp = temp.replace('{{Level2rightqty_row5}}', gearLeftOnSiteRecord[i].level2Items[9].qty)

                    let Level2MoreRowData = '';
                    for (let y = 9; y < gearLeftOnSiteRecord[i].level2Items.length; y++) {
                        Level2MoreRowData += '<tr> <td class="ppe-td1"> <div>';
                        Level2MoreRowData += gearLeftOnSiteRecord[i].level2Items.length-1 > y ? gearLeftOnSiteRecord[i].level2Items[y + 1].description : '';
                        Level2MoreRowData += '</div></td ><td class="ppe-td2"><div class="text-center">';
                        Level2MoreRowData += gearLeftOnSiteRecord[i].level2Items.length-1 > y ? gearLeftOnSiteRecord[i].level2Items[y + 1].qty : '';
                        Level2MoreRowData += '</div></td ><td class="ppe-td3"><div >';
                        y = y + 1
                        if (gearLeftOnSiteRecord[i].level2Items.length - 1 > y) {
                            Level2MoreRowData += gearLeftOnSiteRecord[i].level2Items[y + 1].description;
                            Level2MoreRowData += '</div></td ><td class="ppe-td4"><div class="text-center">';
                            Level2MoreRowData += gearLeftOnSiteRecord[i].level2Items[y + 1].qty;
                            Level2MoreRowData += '</div></td ></tr>';

                        } else {
                            Level2MoreRowData += '</div></td ><td class="ppe-td4"><div class="text-center"></div></td ></tr>';
                        }
                    }
                    temp = temp.replace('{{Level2MoreRow}}', Level2MoreRowData)

                } else {

                    temp = temp.replace('{{Level2lefttext_row1}}', gearLeftOnSiteRecord[i].level2Items.length >= 1 ? gearLeftOnSiteRecord[i].level2Items[0].description : '')
                    temp = temp.replace('{{Level2leftqty_row1}}', gearLeftOnSiteRecord[i].level2Items.length >= 1 ? gearLeftOnSiteRecord[i].level2Items[0].qty : '')

                    temp = temp.replace('{{Level2lefttext_row2}}', gearLeftOnSiteRecord[i].level2Items.length >= 3 ? gearLeftOnSiteRecord[i].level2Items[2].description : '')
                    temp = temp.replace('{{Level2leftqty_row2}}', gearLeftOnSiteRecord[i].level2Items.length >= 3 ? gearLeftOnSiteRecord[i].level2Items[2].qty : '')

                    temp = temp.replace('{{Level2lefttext_row3}}', gearLeftOnSiteRecord[i].level2Items.length >= 5 ? gearLeftOnSiteRecord[i].level2Items[4].description : '')
                    temp = temp.replace('{{Level2leftqty_row3}}', gearLeftOnSiteRecord[i].level2Items.length >= 5 ? gearLeftOnSiteRecord[i].level2Items[4].qty : '')

                    temp = temp.replace('{{Level2lefttext_row4}}', gearLeftOnSiteRecord[i].level2Items.length >= 7 ? gearLeftOnSiteRecord[i].level2Items[6].description : '')
                    temp = temp.replace('{{Level2leftqty_row4}}', gearLeftOnSiteRecord[i].level2Items.length >= 7 ? gearLeftOnSiteRecord[i].level2Items[6].qty : '')

                    temp = temp.replace('{{Level2lefttext_row5}}', gearLeftOnSiteRecord[i].level2Items.length >= 9 ? gearLeftOnSiteRecord[i].level2Items[8].description : '')
                    temp = temp.replace('{{Level2leftqty_row5}}', gearLeftOnSiteRecord[i].level2Items.length >= 9 ? gearLeftOnSiteRecord[i].level2Items[8].qty : '')

                    temp = temp.replace('{{Level2righttext_row1}}', gearLeftOnSiteRecord[i].level2Items.length >= 2 ? gearLeftOnSiteRecord[i].level2Items[1].description : '')
                    temp = temp.replace('{{Level2rightqty_row1}}', gearLeftOnSiteRecord[i].level2Items.length >= 2 ? gearLeftOnSiteRecord[i].level2Items[1].qty : '')

                    temp = temp.replace('{{Level2righttext_row2}}', gearLeftOnSiteRecord[i].level2Items.length >= 4 ? gearLeftOnSiteRecord[i].level2Items[3].description : '')
                    temp = temp.replace('{{Level2rightqty_row2}}', gearLeftOnSiteRecord[i].level2Items.length >= 4 ? gearLeftOnSiteRecord[i].level2Items[3].qty : '')

                    temp = temp.replace('{{Level2righttext_row3}}', gearLeftOnSiteRecord[i].level2Items.length >= 6 ? gearLeftOnSiteRecord[i].level2Items[5].description : '')
                    temp = temp.replace('{{Level2rightqty_row3}}', gearLeftOnSiteRecord[i].level2Items.length >= 6 ? gearLeftOnSiteRecord[i].level2Items[5].qty : '')

                    temp = temp.replace('{{Level2righttext_row4}}', gearLeftOnSiteRecord[i].level2Items.length >= 8 ? gearLeftOnSiteRecord[i].level2Items[7].description : '')
                    temp = temp.replace('{{Level2rightqty_row4}}', gearLeftOnSiteRecord[i].level2Items.length >= 8 ? gearLeftOnSiteRecord[i].level2Items[7].qty : '')

                    temp = temp.replace('{{Level2righttext_row5}}', gearLeftOnSiteRecord[i].level2Items.length >= 10 ? gearLeftOnSiteRecord[i].level2Items[9].description : '')
                    temp = temp.replace('{{Level2rightqty_row5}}', gearLeftOnSiteRecord[i].level2Items.length >= 10 ? gearLeftOnSiteRecord[i].level2Items[9].qty : '')

                    temp = temp.replace('{{Level2MoreRow}}', '')
                }
                if (gearLeftOnSiteRecord[i].accessoriesItems.length >= 10) {
                    temp = temp.replace('{{Accessorieslefttext_row1}}', gearLeftOnSiteRecord[i].accessoriesItems[0].description)
                    temp = temp.replace('{{Accessoriesleftqty_row1}}', gearLeftOnSiteRecord[i].accessoriesItems[0].qty)
                    temp = temp.replace('{{Accessorieslefttext_row2}}', gearLeftOnSiteRecord[i].accessoriesItems[2].description)
                    temp = temp.replace('{{Accessoriesleftqty_row2}}', gearLeftOnSiteRecord[i].accessoriesItems[2].qty)
                    temp = temp.replace('{{Accessorieslefttext_row3}}', gearLeftOnSiteRecord[i].accessoriesItems[4].description)
                    temp = temp.replace('{{Accessoriesleftqty_row3}}', gearLeftOnSiteRecord[i].accessoriesItems[4].qty)
                    temp = temp.replace('{{Accessorieslefttext_row4}}', gearLeftOnSiteRecord[i].accessoriesItems[6].description)
                    temp = temp.replace('{{Accessoriesleftqty_row4}}', gearLeftOnSiteRecord[i].accessoriesItems[6].qty)
                    temp = temp.replace('{{Accessorieslefttext_row5}}', gearLeftOnSiteRecord[i].accessoriesItems[8].description)
                    temp = temp.replace('{{Accessoriesleftqty_row5}}', gearLeftOnSiteRecord[i].accessoriesItems[8].qty)

                    temp = temp.replace('{{Accessoriesrighttext_row1}}', gearLeftOnSiteRecord[i].accessoriesItems[1].description)
                    temp = temp.replace('{{Accessoriesrightqty_row1}}', gearLeftOnSiteRecord[i].accessoriesItems[1].qty)
                    temp = temp.replace('{{Accessoriesrighttext_row2}}', gearLeftOnSiteRecord[i].accessoriesItems[3].description)
                    temp = temp.replace('{{Accessoriesrightqty_row2}}', gearLeftOnSiteRecord[i].accessoriesItems[3].qty)
                    temp = temp.replace('{{Accessoriesrighttext_row3}}', gearLeftOnSiteRecord[i].accessoriesItems[5].description)
                    temp = temp.replace('{{Accessoriesrightqty_row3}}', gearLeftOnSiteRecord[i].accessoriesItems[5].qty)
                    temp = temp.replace('{{Accessoriesrighttext_row4}}', gearLeftOnSiteRecord[i].accessoriesItems[7].description)
                    temp = temp.replace('{{Accessoriesrightqty_row4}}', gearLeftOnSiteRecord[i].accessoriesItems[7].qty)
                    temp = temp.replace('{{Accessoriesrighttext_row5}}', gearLeftOnSiteRecord[i].accessoriesItems[9].description)
                    temp = temp.replace('{{Accessoriesrightqty_row5}}', gearLeftOnSiteRecord[i].accessoriesItems[9].qty)

                    let AccessoriesMoreRowData = '';
                    for (let y = 9; y < gearLeftOnSiteRecord[i].accessoriesItems.length; y++) {
                        AccessoriesMoreRowData += '<tr> <td class="ppe-td1"> <div>';
                        AccessoriesMoreRowData += gearLeftOnSiteRecord[i].accessoriesItems.length - 1 > y ? gearLeftOnSiteRecord[i].accessoriesItems[y + 1].description : '';
                        AccessoriesMoreRowData += '</div></td ><td class="ppe-td2"><div class="text-center">';
                        AccessoriesMoreRowData += gearLeftOnSiteRecord[i].accessoriesItems.length - 1 > y ? gearLeftOnSiteRecord[i].accessoriesItems[y + 1].qty : '';
                        AccessoriesMoreRowData += '</div></td ><td class="ppe-td3"><div >';
                        y = y + 1
                        if (gearLeftOnSiteRecord[i].accessoriesItems.length - 1 > y) {
                            AccessoriesMoreRowData += gearLeftOnSiteRecord[i].accessoriesItems[y + 1].description;
                            AccessoriesMoreRowData += '</div></td ><td class="ppe-td4"><div class="text-center">';
                            AccessoriesMoreRowData += gearLeftOnSiteRecord[i].accessoriesItems[y + 1].qty;
                            AccessoriesMoreRowData += '</div></td ></tr>';

                        } else {
                            AccessoriesMoreRowData += '</div></td ><td class="ppe-td4"><div class="text-center"></div></td ></tr>';
                        }
                    }
                    temp = temp.replace('{{AccessoriesMoreRow}}', AccessoriesMoreRowData)

                } else {

                    temp = temp.replace('{{Accessorieslefttext_row1}}', gearLeftOnSiteRecord[i].accessoriesItems.length >= 1 ? gearLeftOnSiteRecord[i].accessoriesItems[0].description : '')
                    temp = temp.replace('{{Accessoriesleftqty_row1}}', gearLeftOnSiteRecord[i].accessoriesItems.length >= 1 ? gearLeftOnSiteRecord[i].accessoriesItems[0].qty : '')

                    temp = temp.replace('{{Accessorieslefttext_row2}}', gearLeftOnSiteRecord[i].accessoriesItems.length >= 3 ? gearLeftOnSiteRecord[i].accessoriesItems[2].description : '')
                    temp = temp.replace('{{Accessoriesleftqty_row2}}', gearLeftOnSiteRecord[i].accessoriesItems.length >= 3 ? gearLeftOnSiteRecord[i].accessoriesItems[2].qty : '')

                    temp = temp.replace('{{Accessorieslefttext_row3}}', gearLeftOnSiteRecord[i].accessoriesItems.length >= 5 ? gearLeftOnSiteRecord[i].accessoriesItems[4].description : '')
                    temp = temp.replace('{{Accessoriesleftqty_row3}}', gearLeftOnSiteRecord[i].accessoriesItems.length >= 5 ? gearLeftOnSiteRecord[i].accessoriesItems[4].qty : '')

                    temp = temp.replace('{{Accessorieslefttext_row4}}', gearLeftOnSiteRecord[i].accessoriesItems.length >= 7 ? gearLeftOnSiteRecord[i].accessoriesItems[6].description : '')
                    temp = temp.replace('{{Accessoriesleftqty_row4}}', gearLeftOnSiteRecord[i].accessoriesItems.length >= 7 ? gearLeftOnSiteRecord[i].accessoriesItems[6].qty : '')

                    temp = temp.replace('{{Accessorieslefttext_row5}}', gearLeftOnSiteRecord[i].accessoriesItems.length >= 9 ? gearLeftOnSiteRecord[i].accessoriesItems[8].description : '')
                    temp = temp.replace('{{Accessoriesleftqty_row5}}', gearLeftOnSiteRecord[i].accessoriesItems.length >= 9 ? gearLeftOnSiteRecord[i].accessoriesItems[8].qty : '')

                    temp = temp.replace('{{Accessoriesrighttext_row1}}', gearLeftOnSiteRecord[i].accessoriesItems.length >= 2 ? gearLeftOnSiteRecord[i].accessoriesItems[1].description : '')
                    temp = temp.replace('{{Accessoriesrightqty_row1}}', gearLeftOnSiteRecord[i].accessoriesItems.length >= 2 ? gearLeftOnSiteRecord[i].accessoriesItems[1].qty : '')

                    temp = temp.replace('{{Accessoriesrighttext_row2}}', gearLeftOnSiteRecord[i].accessoriesItems.length >= 4 ? gearLeftOnSiteRecord[i].accessoriesItems[3].description : '')
                    temp = temp.replace('{{Accessoriesrightqty_row2}}', gearLeftOnSiteRecord[i].accessoriesItems.length >= 4 ? gearLeftOnSiteRecord[i].accessoriesItems[3].qty : '')

                    temp = temp.replace('{{Accessoriesrighttext_row3}}', gearLeftOnSiteRecord[i].accessoriesItems.length >= 6 ? gearLeftOnSiteRecord[i].accessoriesItems[5].description : '')
                    temp = temp.replace('{{Accessoriesrightqty_row3}}', gearLeftOnSiteRecord[i].accessoriesItems.length >= 6 ? gearLeftOnSiteRecord[i].accessoriesItems[5].qty : '')

                    temp = temp.replace('{{Accessoriesrighttext_row4}}', gearLeftOnSiteRecord[i].accessoriesItems.length >= 8 ? gearLeftOnSiteRecord[i].accessoriesItems[7].description : '')
                    temp = temp.replace('{{Accessoriesrightqty_row4}}', gearLeftOnSiteRecord[i].accessoriesItems.length >= 8 ? gearLeftOnSiteRecord[i].accessoriesItems[7].qty : '')

                    temp = temp.replace('{{Accessoriesrighttext_row5}}', gearLeftOnSiteRecord[i].accessoriesItems.length >= 10 ? gearLeftOnSiteRecord[i].accessoriesItems[9].description : '')
                    temp = temp.replace('{{Accessoriesrightqty_row5}}', gearLeftOnSiteRecord[i].accessoriesItems.length >= 10 ? gearLeftOnSiteRecord[i].accessoriesItems[9].qty : '')

                    temp = temp.replace('{{AccessoriesMoreRow}}', '')
                }

                result.truckcontainer += temp;
            }
            return result;
        }

        function saveReportFile(data){
            let defer = $q.defer();

            window.resolveLocalFileSystemURL(cordova.file.externalDataDirectory, function(dirEntry){
                //console.log(dirEntry);
                dirEntry.getFile('onsiterecordreportoutput.html', {create: true, exclusive: false}, function(fileEntry){
                    fileEntry.createWriter(function(fileWriter){
                        fileWriter.onwriteend = function(){
                            //console.log('generated report done');
                            defer.resolve(fileEntry.nativeURL);
                        };
                        fileWriter.onerror = function(error){
                            //console.log('something went wrong while writing file');
                            defer.reject(error);
                        };
                        fileWriter.write(data);
                    });
                });
            }, function(error){
                console.log(error);
            });

            return defer.promise;
        }

        function generatePdfByUrl(filePath){
            //console.log(filePath);
            window.resolveLocalFileSystemURL(filePath, function(fileEntry){
                //console.log(fileEntry);
                pdf.fromURL(fileEntry.nativeURL, {
                    documentsize: 'a4',
                    landscape: 'portrait',
                    type: 'share'
                });
            }, function(error){
                //console.log(error);
            });

        }

        function generatePdfByData(data){
            pdf.fromData(data, {
                documentsize: 'a4',
                landscape: 'portrait',
                type: 'share',
                filename: 'onsiterecord.pdf'
            });
        }

        /**
         * Convert a base64 string in a Blob according to the data and contentType.
         *
         * @param b64Data {String} Pure base64 string without contentType
         * @param contentType {String} the content type of the file i.e (application/pdf - text/plain)
         * @param sliceSize {Int} SliceSize to process the byteCharacters
         * @see http://stackoverflow.com/questions/16245767/creating-a-blob-from-a-base64-string-in-javascript
         * @return Blob
         */
        function b64toBlob(b64Data, contentType, sliceSize) {
                contentType = contentType || '';
                sliceSize = sliceSize || 512;

                var byteCharacters = atob(b64Data);
                var byteArrays = [];

                for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                    var slice = byteCharacters.slice(offset, offset + sliceSize);

                    var byteNumbers = new Array(slice.length);
                    for (var i = 0; i < slice.length; i++) {
                        byteNumbers[i] = slice.charCodeAt(i);
                    }

                    var byteArray = new Uint8Array(byteNumbers);

                    byteArrays.push(byteArray);
                }

            var blob = new Blob(byteArrays, {type: contentType});
            return blob;
        }

        /**
        * Create a PDF file according to its database64 content only.
        *
        * @param folderpath {String} The folder where the file will be created
        * @param filename {String} The name of the file that will be created
        * @param content {Base64 String} Important : The content can't contain the following string (data:application/pdf;base64). Only the base64 string is expected.
        */
        function savebase64AsPDF(folderpath,filename,content,contentType){
            // Convert the base64 string in a Blob
            var DataBlob = b64toBlob(content,contentType);

            console.log("Starting to write the file :3");

            window.resolveLocalFileSystemURL(folderpath, function(dir) {
                console.log("Access to the directory granted succesfully");
                dir.getFile(filename, {create:true, exclusive: false}, function(file) {
                    console.log(file);
                    console.log("File created succesfully.");
                    file.createWriter(function(fileWriter) {
                        fileWriter.onwriteend = function(){
                            console.log(file);
                            console.log('write succeeded');
                            //cordova.InAppBrowser.open(file.nativeURL, '_blank');

                            var options = {
                                showButtons: 0, //0: no buttons; 1: ok button, 2: ok and cancel button
                                cancel: "Close", //text for cancel button
                                ok: "OK" //text for ok button
                            };
                            $timeout(function () {
                                console.log('pdf file upload: ' + new Date());
                                UploadFile(file.nativeURL).then(function (result) {

                                    JobsSvc.updateSiteRecordPDFLog(jobId, { createdOn: moment().utc(), pdfPath: file.nativeURL }).then(function (response) {

                                    }, function (error) {
                                        console.log(error);
                                    }).finally(function () {
                                        $ionicLoading.hide();
                                    });

                                    //console.log('media upload done: ' + new Date());
                                }, function (error) {
                                    callback(error, false);
                                });
                            }, 1000);

                            var url = file.nativeURL.replace('file://', '');
                            PDFViewer.showPDF(url, options, function (result) {
                                console.log(result);
                                //result == '0' -> click on cancel, DONE or backpress button
                                //result == '1' -> click on OK button
                            });
                        }
                        fileWriter.onerror = function(error){
                            console.log(error);
                            console.log(JSON.stringify(error));
                        };
                        console.log("Writing content to file");
                        fileWriter.write(DataBlob);
                    });
                }, function(error){
                    console.log(error);
                });
            });
        }

        function UploadFile(path) {
            var defer = $q.defer();
            let token = AccountSvc.getAccessTokenV2();
            var tokenConfig = { headers: { 'Authorization': 'Bearer ' + token } };
            let tryCnt = 1;
            if (typeof path !== "string")
                defer.reject("path must be string");

            function upload(path) {
                var ft = new FileTransfer();
                var ftOptions = {};
                ftOptions.fileName = path.substr(path.lastIndexOf('/') + 1);
                ftOptions.headers = tokenConfig.headers;
                async.retry({ times: 3, interval: 5000 }, function (retryCallback) {
                    //console.log('upload try: ' + tryCnt);
                    ft.upload(path, RemoteUrls.mediaFilesUrl, function (result) {
                        //defer.resolve(result);
                        tryCnt++;
                        retryCallback(null, null);
                    }, function (error) {
                        //defer.reject(error);
                        tryCnt++;
                        retryCallback(error, null);
                    }, ftOptions);
                }).catch(function (error) {
                    console.log(error);
                    console.log(JSON.stringify(error));
                }).finally(function () {
                    defer.resolve(true);
                });

                // ft.upload(path, RemoteUrls.mediaFilesUrl, function (result) {
                //     defer.resolve(result);
                // }, function (error) {
                //     defer.reject(error);
                // }, ftOptions);
            }
            $timeout(function () {
                upload(path);
            }, 10000, false);
            return defer.promise;
        }
function truckContainerTemplate(){
            let defer = $q.defer();
            window.resolveLocalFileSystemURL('file:///android_asset/www/pdftemplates/TruckReport.html', function(fileEntry){
                fileEntry.file(function(file){
                    var reader = new FileReader();
                    reader.onloadend = function(){
                        defer.resolve(this.result);
                    };
                    reader.onerror = function(error){
                        defer.reject(error);
                    };
                    reader.readAsText(file);
                });
            }, function(error){
                alert('Something went wrong.');
            });
            return defer.promise;
        }

        function gearleftContainerTemplate() {
            let defer = $q.defer();
            window.resolveLocalFileSystemURL('file:///android_asset/www/pdftemplates/GearLeftOnsiteReport.html', function (fileEntry) {
                fileEntry.file(function (file) {
                    var reader = new FileReader();
                    reader.onloadend = function () {
                        defer.resolve(this.result);
                    };
                    reader.onerror = function (error) {
                        defer.reject(error);
                    };
                    reader.readAsText(file);
                });
            }, function (error) {
                alert('Something went wrong.');
            });
            return defer.promise;
        }

        $q.all({onsiterecord_template: readOnsiteRecordTemplate(), tsl_template: readTslTemplate(), moretslcontainer_template: readMoreTslContainerTemplate(), tsltablecontainer_template: readTslTableContainerTemplate(),
            truckcontainer_template: truckContainerTemplate(), gearleftcontainer_template: gearleftContainerTemplate()}).then(function(templates){
            /*var testTsls = [];
            for (let index = 0; index < 30; index++) {
                testTsls.push(tsls[0]);
            }*/
            var data = generateReportData(siteChecks);
            let tslData = generateTslData(tsls, templates.tsl_template, templates.tsltablecontainer_template, templates.moretslcontainer_template);
            let tslReport = generateTslTruckData(truckReport, templates.truckcontainer_template, staff, createdOn);
            let gearleftReport = generateGearLeftData(gearLeftOnSiteRecord, templates.gearleftcontainer_template, staff, createdOn, address, tmpReference, ClientContract);
            templates.onsiterecord_template = templates.onsiterecord_template.replace('{{tmpReference}}', tmpReference ? tmpReference : '');
            templates.onsiterecord_template = templates.onsiterecord_template.replace('{{todaysDate}}', $filter('date')(new Date(), 'dd-MM-yyyy'));
            templates.onsiterecord_template = templates.onsiterecord_template.replace('{{address}}', address ? address.replace(',New Zealand', '') : '');
            templates.onsiterecord_template = templates.onsiterecord_template.replace('{{rps}}', rps ? rps : '');
            templates.onsiterecord_template = templates.onsiterecord_template.replace('{{onsitecontact}}', onsitecontact ? onsitecontact : '');
            templates.onsiterecord_template = templates.onsiterecord_template.replace('{{onsitecontactsignature}}', onsitecontactsignature ? onsitecontactsignature : '');
            templates.onsiterecord_template = templates.onsiterecord_template.replace('{{pageContent}}', data);
            templates.onsiterecord_template = templates.onsiterecord_template.replace('{{truckReport}}', tslReport.truckcontainer);
            templates.onsiterecord_template = templates.onsiterecord_template.replace('{{gearleftReport}}', gearleftReport.truckcontainer);
            templates.onsiterecord_template = templates.onsiterecord_template.replace('{{tsls}}', tslData.tsls);
            templates.onsiterecord_template = templates.onsiterecord_template.replace('{{moreTsls}}', tslData.moreTsls);

            if (staff !== null && staff !== undefined) {
                templates.onsiterecord_template = templates.onsiterecord_template.replace('{{stmsfullName}}', staff.fullName);
                templates.onsiterecord_template = templates.onsiterecord_template.replace('{{stmsCertificationNo}}', staff.stmsCertificationNo === null ? "" : staff.stmsCertificationNo);
                templates.onsiterecord_template = templates.onsiterecord_template.replace('{{expiryDate}}', staff.expiryDate === null ? "" : $filter('date')(staff.expiryDate, 'dd-MM-yyyy'));
                templates.onsiterecord_template = templates.onsiterecord_template.replace('{{stmsTime}}',  $filter('date')(new Date(), 'hh:mm a'));
                templates.onsiterecord_template = templates.onsiterecord_template.replace('{{signiture}}', staff.signiture === null ? "" : staff.signiture === "" ? "" : ( staff.isSignApp == true ? (RemoteUrls.auditproRootPath + "/UserData/images/" + staff.signiture) : (RemoteUrls.tmproRootPath + "/Uploads/User/" + staff.signiture)));
            } else {
                templates.onsiterecord_template = templates.onsiterecord_template.replace('{{stmsfullName}}', "");
                templates.onsiterecord_template = templates.onsiterecord_template.replace('{{stmsCertificationNo}}', "");
                templates.onsiterecord_template = templates.onsiterecord_template.replace('{{expiryDate}}', "");
                templates.onsiterecord_template = templates.onsiterecord_template.replace('{{stmsTime}}', "");
                templates.onsiterecord_template = templates.onsiterecord_template.replace('{{signiture}}', "");
            }

            var options = {
                documentSize: 'A4',
                type: 'base64'
            };

            pdf.fromData(templates.onsiterecord_template , options)
            .then(function(base64){               
                // To define the type of the Blob
                var contentType = "application/pdf";
                    
                // if cordova.file is not available use instead :
                // var folderpath = "file:///storage/emulated/0/Download/";
                var folderpath = cordova.file.externalRootDirectory + "Download/"; //you can select other folders
                savebase64AsPDF(folderpath, (new ObjectId()).toString() +'_onsiterecord.pdf', base64, contentType);
            })  
            .catch((err)=>console.log(err));
        }, function(error){
            console.log(error);
            alert('something went wrong');
        });
        /*
        readTemplateFile().then(function(template){
            let tslData = generateTslData();
            var data = generateReportData(siteChecks);
            template = template.replace('{{tmpReference}}', tmpReference);
            template = template.replace('{{pageContent}}', data);
            //generatePdfByData(template);            
            var options = {
                documentSize: 'A4',
                type: 'base64'                
            };

            pdf.fromData(template , options)
            .then(function(base64){               
                // To define the type of the Blob
                var contentType = "application/pdf";
                    
                // if cordova.file is not available use instead :
                // var folderpath = "file:///storage/emulated/0/Download/";
                var folderpath = cordova.file.externalRootDirectory + "Download/"; //you can select other folders
                savebase64AsPDF(folderpath, 'onsiterecord.pdf', base64, contentType);          
            })  
            .catch((err)=>console.log(err));
        }, function(error){
            console.log(error);
            alert('something went wrong');
        });
        */        
    };

    return service;
}]);