mainApp.factory('RemoteSyncSvc',['$q', '$resource', 'DBSvc', '$rootScope', '$timeout', 'RemoteSyncHandler', function($q, $resource, DBSvc, $rootScope, $timeout, RemoteSyncHandler){    
    let service = {};
    let isOnline = false;
    let isRunning = false;
    let syncTimeout = 5000;

    $rootScope.$on('online', function(event){
        console.log('phone online');
        //isOnline = true;
        service.start();
    });
    $rootScope.$on('offline', function(event){
        console.log('phone offline');        
        service.stop();
    });

    function init(){
        let networkstate = navigator.connection.type;
        isOnline = networkstate !== Connection.NONE;
        if(isOnline && !isRunning)
            service.start();
    }

    function send(){
        if(isOnline && isRunning)
            DBSvc.getNextQData().then(function(qData){
                if(qData != null){                    
                    RemoteSyncHandler.execute(qData).then(function(response){
                        DBSvc.removeQData(qData).then(function(remResponse){
                            if(remResponse.ok){
                                $timeout(function(){
                                    send();
                                }, syncTimeout);
                            }
                        });
                    }, function(error){
                        //retry in 5 secs
                        $timeout(function(){
                            send();
                        }, syncTimeout);
                    });                    
                }
                else{
                    service.stop();
                    //isRunning = false;
                }
            }, function(error){
                //retry in 5 secs
                $timeout(function(){
                    send();
                }, syncTimeout);
            });
    }

    service.start = function(){
        //check if internet connect is available & there is data from Q                
        //console.log('RemoteSyncSvc started');
        let networkstate = navigator.connection.type;
        isOnline = networkstate !== Connection.NONE;
        if(isOnline && !isRunning){
            isRunning = true;
            send();
        }
    };

    service.stop = function(){
        isOnline = false;
        isRunning = false;
    };

    service.add = function(data){
        //console.log('RemoteSyncSvc.Add()');
        let defer = $q.defer();
        DBSvc.addToSyncQ(data).then(function(response){                        
            service.start();
            defer.resolve(true);
        }, function(error){
            defer.reject(error);
        });

        return defer.promise;
    };

    service.test = function(){
        console.log('xxxx test xxxxx');
    };

    service.init = function(){
        init();
    }
    //init();

    return service;
}]);