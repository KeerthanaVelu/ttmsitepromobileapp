mainApp.factory('AccountSvc', ['$q', 'ApiSvc', '$resource', '$http', 'RCASvc', 'ContractorSvc', 'AssetOwnerSvc', 'DBSvc', 'StaffSvc',  function ($q, ApiSvc, $resource, $http, RCASvc, ContractorSvc, AssetOwnerSvc, DBSvc, StaffSvc) {
    var resource = $resource(ApiSvc.loginUrl, {}, {
        'getToken': { method: 'POST', isArray: false, headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }        
    });
    var accountToken = {};
    var service = {};
    var parties = {};
    var siteprodb = null;
    var branches =[];

    function getParties(tenantId) {
        console.log('getParties(): 625');
        var defer = $q.defer();
        var partiesResource = $resource(ApiSvc.partiesUrl, {}, {
            'getParties': { method: 'GET', isArray: false, headers: { 'Authorization': 'Bearer ' + accountToken.access_token }, url: ApiSvc.partiesUrl, params: { tenantId: '@tenantId' } }
        });        

        partiesResource.getParties({ tenantId: accountToken.tenantId }, null, function (result) {
            RCASvc.set(result.rcas);
            ContractorSvc.set(result.contractors);
            AssetOwnerSvc.set(result.assetOwners);
            StaffSvc.setStaff(result.staff);
            branches = result.branches;
            trucks = result.trucks;
            console.log('638: partiesResource.getParties() success callback');
            defer.resolve(result);
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    }

    function relogin(email, password) {
        var credentials = "grant_type=password&username=" + email + "&password=" + password;
        var defer = $q.defer();

        $http.post(ApiSvc.loginUrl, credentials, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' }, timeout: 60000 }).then(function (response) {
            //after successful login save token data to db
            console.log('AccountSvc.login');
            //console.log(response);
            accountToken = response.data;
            DBSvc.saveToken(accountToken).then(function (result) {
                defer.resolve(result);
            }, function (error) {
                defer.reject(error);
            });
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    }

    function login(email, password) {
        var credentials = "grant_type=password&username=" + email + "&password=" + password;
        var defer = $q.defer();

        $http.post(ApiSvc.loginUrl, credentials, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' }, timeout: 60000 }).then(function (response) {
            //after successful login save token data to db
            defer.resolve(response.data);
            /*
            accountToken = response.data;
            getParties(accountToken.tenantId).then(function (result) {
                console.log('getParties');
                //console.log(result);
                DBSvc.saveTokenParties(accountToken, result, result.staff).then(function (result) {
                        console.log('683: login() -> httt.post() -> DBSvc.saveTokenParties()');
                        defer.resolve(result);
                    }, function (error) {
                        defer.reject(error);
                    });
            }, function (error) {
                console.log(error);
                defer.reject(error);
            });
            */
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    }

    service.init = function (accountDetails) {
        RCASvc.set(accountDetails.parties.rcas);
        ContractorSvc.set(accountDetails.parties.contractors);
        AssetOwnerSvc.set(accountDetails.parties.assetOwners);
        StaffSvc.setStaff(accountDetails.staff);
        accountToken = accountDetails.token;
        branches = accountDetails.parties.branches;        
    };

    service.login = function (email, password) {
        var defer = $q.defer();
        login(email, password).then(function (result) {
            defer.resolve(result);
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    };

    service.relogin = function (email, password) {
        var defer = $q.defer();
        relogin(email, password).then(function (result) {
            defer.resolve(result);
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    };

    service.getAccessToken = function () {        
        return accountToken.access_token;
    };

    service.getTenantId = function () {
        return accountToken.tenantId;
    };

    service.getBranchId = function () {
        return accountToken.branchId;
    };

    service.getBranches = function () {
        return branches;
    };

    service.getUserId = function () {
        return accountToken.id;
    };

    service.getUserInfo = function () {
        return { fullName: accountToken.fullName, email: accountToken.userName };
    };

    service.forgotPassword = function (email) {
        var resource = $resource('/token', {}, {
            'forgotPassword': { method: 'POST', url: ApiSvc.forgotPassword + '?email=' + email }
        });
        return resource.forgotPassword().$promise;
    };

    service.hasSeedData = function(){
      return DBSvc.hasSeedData();  
    };

    service.save = function(account){
        return DBSvc.saveAccountDetails(account);
    };

    service.setHasSeedData = function(hasSeedData){
        return DBSvc.setHasSeedData(hasSeedData);
    };

    let account;
    service.getAccessTokenV2 = function(){
        return account.access_token;
    };
    service.getTenantIdV2 = function(){
        return account.tenantId;
    };
    service.getBranchIdV2 = function(){
        return account.branchId;
    };
    service.getUserIdV2 = function(){
        return account.id;
    }    

    service.initV2 = function(){
        console.log('accountsvc.initV2')
        let defer = $q.defer();
        DBSvc.getAccountDetails().then(function(response){
            console.log(response);
            account = response;
            defer.resolve(true);
        }, function(error){
            console.log(error);
            defer.reject(error);
        });
        return defer.promise;
    };

    service.getBranchesV2 = function(){
        return DBSvc.getTenantBranches();
    };

    return service;
}]);