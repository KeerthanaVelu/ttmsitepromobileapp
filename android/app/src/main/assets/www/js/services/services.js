﻿mainApp.factory('ApiSvc', function () {
    console.log('ApiSvc startup');
    var service = {};        
    
    service.loginUrl = 'http://192.168.0.175:8086/token';
    service.partiesUrl = 'http://192.168.0.175:8086/api/parties/:tenantId';
    service.jobsUrl = 'http://192.168.0.175:8086/api/jobs';
    service.addActivityUrl = 'http://192.168.0.175:8086/api/jobs/:JobId/addactivity';
    service.updateActivityUrl = 'http://192.168.0.175:8086/api/jobs/:JobId/updateactivity';
    service.signalrHubs = 'http://192.168.0.175:8086/signalr/hubs';
    service.apiRootUrl = 'http://192.168.0.175:8086/api';
    service.addStaffMobile = 'http://192.168.0.175:8086/api/tenants/addstaffmobile';
    service.uploadFile = 'http://192.168.0.175:8086/api/upload/file';
    service.completeJobUrl = 'http://192.168.0.175:8086/api/jobs/complete/:jobId';
    service.updateTeamMembersUrl = 'http://192.168.0.175:8086/api/jobs/:jobId/updateteammembers';
    service.addTimeLogsUrl = 'http://192.168.0.175:8086/api/jobs/addtimelogs';
    service.cancelActivity = 'http://192.168.0.175:8086/api/jobs/:jobId/cancelActivity';
    service.updateGeneralNotes = 'http://192.168.0.175:8086/api/jobs/:jobId/generalnotes';
    service.saveAudit = 'http://192.168.0.175:8086/api/trucks/saveaudit';
    service.teammembertimelog = 'http://192.168.0.175:8086/api/jobs/:jobId/teammembertimelog';
    service.updatetrucks = 'http://192.168.0.175:8086/api/jobs/:jobId/updatetrucks';
    service.updategeneralinfo = 'http://192.168.0.175:8086/api/jobs/:jobId/updategeneralinfo';
    service.truckauditfileupload = 'http://test.auditpro.co.nz/api/truckaudit/file?key=2154e2eb-a6e5-4c30-af64-a84e81e52834';
    service.trucks = 'http://192.168.0.175:8086/api/trucks/:tenantId';
    service.tenantInfo = 'http://192.168.0.175:8086/api/tenants/:tenantId';
    service.tenantStmss = 'http://192.168.0.175:8086/api/tenants/:tenantid/branch/:branchid/users';
    service.mobileJobTransfer = 'http://192.168.0.175:8086/api/jobs/mobilejobtransfer';
    service.forgotPassword = 'http://192.168.0.175:8086/home/forgotpassword';
    service.partiesDataSeed = 'http://192.168.0.175:8086/api/parties/:tenantId/dataseed';
    service.branchesDataSeed = 'http://192.168.0.175:8086/api/tenants/:tenantId/branchesdataseed';
    service.trucksDataSeed = 'http://192.168.0.175:8086/api/trucks/:tenantId/dataseed';
    service.staffDataSeed = 'http://192.168.0.175:8086/api/staff/:tenantId/dataseed';
    service.getJob = 'http://192.168.0.175:8086/api/jobs/:id';
    return service;
});

mainApp.factory('ContractorSvc',['$linq', 'RCASvc', function ($linq, RCASvc) {
    var contractors = [];
    var service = {};
    service.GetAll = function () {
        return contractors;
    };

    service.GetAllByRCA = function (rcaId) {
        var rca = RCASvc.GetById(rcaId);
        var contractors = service.GetAll();        
        var matchingContractors = $linq.Enumerable().From(contractors).Where(function (x) {
            var contractorBranchRegions = $linq.Enumerable().From(x.branches).SelectMany('s => s.regions').ToArray();
            return $linq.Enumerable().From(contractorBranchRegions).Intersect(rca.regions).Any() || (x.branches == null || x.branches.length == 0);
        }).ToArray();

        return matchingContractors;
    };

    service.GetById = function (id) {
        return $linq.Enumerable().From(contractors).FirstOrDefault(null, 'f => f.id == "' + id + '"');
    };

    service.set = function (contractorsList) {
        contractors = contractorsList;
    };

    return service;
}]);
mainApp.factory('WebApiQSvc', ['$linq', '$resource', '$timeout', 'ApiSvc', '$q', '$rootScope', 'DBSvc', '$cordovaFileTransfer', function ($linq, $resource, $timeout, ApiSvc, $q, $rootScope, DBSvc, $cordovaFileTransfer) {
    var sendQ = [];
    var service = {};
    var accessToken = null;
    var serviceRunning = false;
    service.setAccessToken = function (token) {
        service.accessToken = token;
    };

    //save the q in db
    //get the last data
    //send to server
    //on success
    //delete data from db by rowid
    service.Send = function (data) {
        //var defer = $q.defer();
        console.log('WebApiQSvc.Send');
        //console.log(data);
        //var exist = dbsvc.getexistingdata(data.id);
        //if(not exist)
        //dbsvc.savedata(data);
        //if(service not running)
        //turn on service running flag
        //run q service
        //console.log(data);
        DBSvc.saveQData(data).then(function (saveResult) {
            //runDataService();            
            if (!serviceRunning) {
                serviceRunning = true;
                runDataService();
            }            
        });               
    };
    service.SendFirst = function (data) {
        console.log('WebApiQSvc.SendFirst');
        var defer = $q.defer();
        DBSvc.saveQToFirst(data).then(function (result) {
            if (!serviceRunning) {
                serviceRunning = true;
                runDataService();
            }
            defer.resolve(true);
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });
        return defer.promise;
    };
    service.stopDataService = function () {
        serviceRunning = false;
    };
    service.startDataService = function () {
        serviceRunning = true;
        runDataService();
    };
    service.runDataService = function () {
        console.log('runDataService');
        runDataService();
    };
    
    function runDataService() {
        DBSvc.getNextQData().then(function (data) {            
            console.log(serviceRunning);
            if (data && serviceRunning) {                
                sendData(data).then(function () {
                    DBSvc.deleteQData(data.qid).then(function (result) {
                        //console.log(result);
                        return $timeout(runDataService, timeoutDelay);
                    }, function (error) {
                        console.log(error);
                        return $timeout(runDataService, timeoutDelay);
                    });
                }, function (error) {
                    console.log(error);
                    return $timeout(runDataService, timeoutDelay);
                });
            }
            else {
                serviceRunning = false;
                return;
            }
        }, function (error) {
            console.log(error);
        });
    }  

    function sendData(data) {
        var defer = $q.defer();
        console.log('sendData');        
        var networkState = navigator.connection.type;
        if (networkState === Connection.NONE) {            
            return $q.reject(false);
        }

        if (data){
            if (data.isJob && data.complete) {
                resource.completeJob(data, function (response, headers) {
                    defer.resolve(true);
                }, function (error) {
                    console.log(error);
                    defer.reject(error);
                });
            }
            else if (data.isJob) {                                
                resource.addJob(data, function (response, headers) {                    
                    defer.resolve(true);
                }, function (error) {
                    console.log(error);
                    defer.reject(error);
                });
            }
            else if (data.cancelActivity) {
                resource.cancelActivity(data, { activityId: data.activityId }, function (result) {
                    defer.resolve(true);
                }, function (error) {
                    console.log(error);
                    defer.reject(error);
                });
            }
            else if (data.isTruckAudit) {
                resource.saveAudit(data, function (result) {
                    //console.log(result);
                    DBSvc.updateTruckAuditId(data.id, result.id).then(function () {
                        $rootScope.$broadcast('truckAuditSavedOnServer', { newId: result.id, oldId: data.id });
                        defer.resolve(true);
                    });                    
                }, function (error) {
                    console.log(error);
                    defer.reject(error);
                });
            }
            else if (data.hasOwnProperty('activityId')) {
                if (data.mode == 0) {
                    if (data.activityId == 6) {
                        for (var i = 0; i < data.siteCheckItems.length; i++) {
                            if (data.siteCheckItems[i].photos.length > 0) {
                                for (var x = 0; x < data.siteCheckItems[i].photos.length; x++) {
                                    data.siteCheckItems[i].photos[x].path = '';
                                }
                            }
                        }
                    }
                    console.log('addActivity');
                    resource.addActivity({ jobId: data.jobId }, data, function (response, headers) {
                        defer.resolve(true);
                    }, function (error) {
                        console.log(error);
                        if (error.status == 404 && error.statusText == 'JOB_NOT_FOUND' || error.data == 'JOB_NOT_FOUND') {
                            console.log('start grab job');
                            //grab associated job
                            //add to queue
                            DBSvc.getJobById(data.jobId).then(function (jobresult) {
                                if (jobresult != null) {
                                    if (!jobresult.hasOwnProperty('isJob'))
                                        jobresult.isJob = true;
                                    service.SendFirst(jobresult).then(function () {
                                        defer.reject({ message: 'JOB_NOT_FOUND:Re-upload job' });
                                    }, function (error) {
                                        defer.reject(error);
                                    });
                                }
                                else
                                    defer.reject(error);
                            }, function (error) {
                                defer.reject(error);
                            });                            
                        }
                        else
                            defer.reject(error);
                    });
                }
                else if (data.mode == 1) {
                    console.log('updateActivity');
                    resource.updateActivity({ jobId: data.jobId }, data, function (response, headers) {
                        defer.resolve(true);
                    }, function (error) {
                        console.log(error);

                        if (error.status == 404 && error.statusText == 'JOB_NOT_FOUND' || error.data == 'JOB_NOT_FOUND') {
                            console.log('start grab job');
                            //grab associated job
                            //add to queue
                            DBSvc.getJobById(data.jobId).then(function (jobresult) {
                                if (jobresult != null) {
                                    if (!jobresult.hasOwnProperty('isJob'))
                                        jobresult.isJob = true;
                                    service.SendFirst(jobresult).then(function () {
                                        defer.reject({ message: 'JOB_NOT_FOUND:Re-upload job' });
                                    }, function (error) {
                                        defer.reject(error);
                                    });
                                }
                                else
                                    defer.reject(error);
                            }, function (error) {
                                defer.reject(error);
                            });                            
                        }
                        else
                            defer.reject(error);
                    });
                }

            }
            else if (data.isStaff) {
                resource.addStaff(data, function (result) {
                    //broadcast new staff member id/tempid
                    $rootScope.$broadcast('newStaffMemberAdded', { tempId: data.id, newId: result.id });
                    defer.resolve(true);
                }, function (error) {
                    console.log(error);
                    defer.reject(error);
                });
            }
            else if (data.isStaffUpdate) {
                resource.addStaff(data, function (result) {
                    //broadcast new staff member id/tempid
                    defer.resolve(true);
                }, function (error) {
                    console.log(error);
                    defer.reject(error);
                });
            }
            else if (data.isFileUpload && data.isFileUpload == true) {
                $cordovaFileTransfer.upload(ApiSvc.uploadFile, data.path, { fileName: getFileName(data.path) }).then(function (result) {
                    defer.resolve(true);
                }, function (error) {
                    console.log(error);
                    defer.reject(error);
                });
            }
            else if (data.isTruckAuditFileUpload && data.isTruckAuditFileUpload == true) {
                var apiPath = ApiSvc.truckauditfileupload + '&userid=' + accessToken.id;
                $cordovaFileTransfer.upload(apiPath, data.path, { fileName: getFileName(data.path) }).then(function (result) {
                    defer.resolve(true);
                }, function (error) {
                    console.log(error);
                    defer.reject(error);
                });
            }
            else if (data.isTeamMembers) {
                //console.log(data);
                for (var i = 0; i < data.team.length; i++) {
                    data.team[i].id = data.team[i].staffMemberId;
                }
                resource.updateTeamMembers({ jobId: data.jobId }, data.team, function (apiresult) {
                    console.log(apiresult);
                    defer.resolve(true);
                }, function (error) {
                    console.log(error);
                    defer.reject(error);
                });
            }
            else if (data.isTimeLogs) {
                resource.addTimeLogs(data.timeLogs, function (result) {
                    defer.resolve(true);
                }, function (error) {
                    console.log(error);
                    defer.reject(error);
                });
            }
            else if (data.isGeneralNotes) {
                resource.updateGeneralNotes({ jobId: data.jobId }, data, function (result) {
                    defer.resolve(true);
                }, function (error) {
                    console.log(error);
                    if (error.status == 404 && error.statusText == 'JOB_NOT_FOUND' || error.data == 'JOB_NOT_FOUND') {
                        console.log('start grab job');
                        //grab associated job
                        //add to queue
                        DBSvc.getJobById(data.jobId).then(function (jobresult) {
                            if (jobresult != null) {
                                if (!jobresult.hasOwnProperty('isJob'))
                                    jobresult.isJob = true;
                                service.SendFirst(jobresult).then(function () {
                                    defer.reject({ message: 'JOB_NOT_FOUND:Re-upload job' });
                                }, function (error) {
                                    defer.reject(error);
                                });
                            }
                            else
                                defer.reject(error);
                        }, function (error) {
                            defer.reject(error);
                        });                        
                    }
                    else
                        defer.reject(error);
                });
            }
            else if (data.isTeamMemberTimelog) {
                resource.teammembertimelog({ jobId: data.jobId }, data, function (result) {
                    defer.resolve(true);
                }, function (error) {
                    console.log(error);
                    defer.reject(error);
                });
            }
            else if (data.isTrucksUpdate) {
                resource.updatetrucks({ jobId: data.jobId }, data.truckIds, function (result) {
                    defer.resolve(true);
                }, function (error) {
                    console.log(error);
                    defer.reject(error);
                });
            }
            else if (data.isJobGeneralInfo) {
                resource.updategeneralinfo({ jobId: data.jobId }, data.job, function (result) {
                    defer.resolve(true);
                }, function (error) {
                    console.log(error);
                    defer.reject(error);
                });
            }
        }
        else
            defer.resolve(true);

        return defer.promise;
    }

    var resource = null;
    //var timeoutDelay = 5000;
    var timeoutDelay = 30000;
    var sendJobCount = 0;
    
    service.init = function (token) {
        accessToken = token;        
        resource = $resource(ApiSvc.apiRootUrl, null, {
            'addJob': { method: 'POST', url: ApiSvc.jobsUrl, headers: { 'Authorization': 'Bearer ' + token.access_token } },
            'addActivity': { method: 'POST', url: ApiSvc.addActivityUrl, headers: { 'Authorization': 'Bearer ' + token.access_token }, params: { JobId: '@jobId' } },
            'updateActivity': { method: 'PUT', url: ApiSvc.updateActivityUrl, headers: { 'Authorization': 'Bearer ' + token.access_token }, params: { JobId: '@jobId' } },
            'addStaff': { method: 'POST', url: ApiSvc.addStaffMobile, headers: { 'Authorization': 'Bearer ' + token.access_token } },
            'updateStaff': { method: 'POST', url: ApiSvc.addStaffMobile, headers: { 'Authorization': 'Bearer ' + token.access_token } },
            'completeJob' : {method: 'POST', url: ApiSvc.completeJobUrl, headers: { 'Authorization': 'Bearer ' + token.access_token }, params: { jobId: '@jobId' } },
            'updateTeamMembers' : { method: 'POST', url: ApiSvc.updateTeamMembersUrl, headers: { 'Authorization': 'Bearer ' + token.access_token } },
            'addTimeLogs' : { method: 'POST', url: ApiSvc.addTimeLogsUrl, headers: { 'Authorization': 'Bearer ' + token.access_token } },
            'cancelActivity': { method: 'POST', url: ApiSvc.cancelActivity, headers: { 'Authorization': 'Bearer ' + token.access_token }, params: { jobId: '@jobId' }},
            'updateGeneralNotes': { method: 'POST', url: ApiSvc.updateGeneralNotes, headers: { 'Authorization': 'Bearer ' + token.access_token }, params: { jobId: '@jobId' } },
            'saveAudit': { method: 'POST', url: ApiSvc.saveAudit, isArray: false, headers: { 'Authorization': 'Bearer ' + token.access_token } },
            'teammembertimelog': { method: 'POST', url: ApiSvc.teammembertimelog, headers: { 'Authorization': 'Bearer ' + token.access_token }, params: { jobId: '@jobId' } },
            'updatetrucks': { method: 'POST', url: ApiSvc.updatetrucks, headers: { 'Authorization': 'Bearer ' + token.access_token }, params: { jobId: '@jobId' } },
            'updategeneralinfo': { method: 'POST', url: ApiSvc.updategeneralinfo, headers: { 'Authorization': 'Bearer ' + token.access_token }, params: { jobId: '@jobId' } },
        });
    };

    service.getAccessToken = function () {
        return accessToken.access_token;
    };    

    return service;
}]);

mainApp.factory('TSLSvc', ['$ionicModal', function ($ionicModal) {
    var service = {};
    
    service.AddTSL = function (scope) {
        $ionicModal.fromTemplateUrl('views/addtsl.html', {
            scope: scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            service.modal = modal;
            service.modal.show();
        });
    };
    service.Cancel = function () {
        service.modal.hide();
    };

    return service;
}]);

mainApp.factory('ClosureTypeSvc', ['$ionicModal', '$q', '$rootScope', function ($ionicModal, $q, $rootScope) {
    var service = {};
    var eventDone;
    service.GetAll = function () {
        return angular.copy(ClosureTypes);
    };
    service.AddClosureTypes = function (existingClosures) {
        var defer = $q.defer();
        $ionicModal.fromTemplateUrl('views/act-closuretype.html', {
            //scope: scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            service.modal = modal;
            service.modal.show();

            eventDone = $rootScope.$on('closuretypedone', function (event, data) {
                eventDone();
                defer.resolve(data);
            });
        });

        return defer.promise;
    };
    service.EditClosureTypes = function (scope) {
        $ionicModal.fromTemplateUrl('views/act-closuretype.html', {
            scope: scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            service.EditMode = true;
            service.modal = modal;
            service.modal.show();
        });
    };
    service.Close = function () {
        service.modal.hide();
    };

    return service;
}]);



mainApp.factory('UploadSvc', ['$cordovaFileTransfer', '$q', 'ApiSvc', '$rootScope', function ($cordovaFileTransfer, $q, ApiSvc, $rootScope) {
    var service = {};
    service.uploadSiteCheckPhotos = function (paths, activity) {
        var uploads = [];
        for (var i = 0; i < paths.length; i++) {
            uploads.push($cordovaFileTransfer.upload(ApiSvc.uploadPhoto, paths[i], {fileName: getFileName(paths[i])}));
        }

        $q.all(uploads).then(function (results) {
            //console.log(results);
            $rootScope.$broadcast('uploadSiteCheckPhotosDone', activity);
        }, function(errors){
            console.log(errors);
        });
    };

    return service;
}]);

mainApp.factory('LocationSvc', ['$q', '$ionicPlatform', '$cordovaGeolocation', '$http', function ($q, $ionicPlatform, $cordovaGeolocation, $http) {
    var service = {};        
    function getCurrentPosition(posOptions) {
        if(!posOptions){
            posOptions = { timeout: 5000, enableHighAccuracy: true };
        }

        var ctr = 1;
        function getPosition() {
            $cordovaGeolocation.getCurrentPosition(posOptions).then(function (result) {
                ctr = 1;
                defer.resolve(result);
            }, function (error) {
                if (ctr <= 5) {
                    ctr++;
                    getPosition();
                }
                else {
                    ctr = 1;
                    defer.reject(error);
                }
            });
        }

        var defer = $q.defer();
        //posOptions.timeout = 5000;
        $ionicPlatform.ready(function () {
            getPosition();
        });
        return defer.promise;  
    }
    service.getCurrentPosition = function (posOptions) {
        var defer = $q.defer();
        getCurrentPosition(posOptions).then(function (result) {
            defer.resolve(result);
        }, function (error) {            
            defer.reject(error);
        });

        return defer.promise;
    };

    service.getCurrentAddress = function(){
        let defer = $q.defer();

        getCurrentPosition().then(function(position){
            var geocodeUrl = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' + position.coords.latitude + ',' + position.coords.longitude + '&key=AIzaSyDA4WKyjVRE3arwia2FPRf5xG_x6wOHAdE';
            $http.get(geocodeUrl).then(function (locationResult) {
                console.log(locationResult);
                if(locationResult.data.results.length > 0)
                    defer.resolve(locationResult.data.results[0].formatted_address);
                else
                    defer.resolve(null);
            }, function(error){
                console.log(error);
                defer.reject(error);
            });
        });

        return defer.promise;
    };

    return service; 
}]);

mainApp.factory('VoiceToTextSvc', ['$q', '$ionicPopup', function ($q, $ionicPopup) {
    var service = {};
    function hasPermission() {
        console.log('VoiceToTextSvc:hasPermission()');
        var defer = $q.defer();

        window.plugins.speechRecognition.hasPermission(function (permitted) {
            console.log('VoiceToTextSvc:hasPermission()-> success callback');
            console.log(permitted);
            if (!permitted) {
                window.plugins.speechRecognition.requestPermission(function (granted) {
                    console.log('speechRecognition.requestPermission -> success callback')
                    console.log(granted);
                    defer.resolve(true);
                }, function (error) {
                    console.log('speechRecognition.requestPermission -> failure callback');
                    console.log(error);
                    defer.reject(false);
                });
            }
            else
                defer.resolve(permitted);
        }, function (error) {
            console.log('VoiceToTextSvc:hasPermission() -> failure callback');
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    }
    
    function playStartTone() {
        var defer = $q.defer();
        window.resolveLocalFileSystemURL(cordova.file.applicationDirectory + '/www/audio/startRecord.wav', function (entry) {
            console.log(entry);
            var startTone = new Media(entry.nativeURL, function () {
                //defer.resolve(true);
                console.log('startRecord.wav success');
                startTone.release();
                defer.resolve(true);
            }, function (error) {
                console.log(error);
                switch (error.code) {
                    case MediaError.MEDIA_ERR_ABORTED:
                        console.log('MediaError.MEDIA_ERR_ABORTED');
                        break;
                    case MediaError.MEDIA_ERR_NETWORK:
                        console.log('MediaError.MEDIA_ERR_NETWORK');
                        break;
                    case MediaError.MEDIA_ERR_DECODE:
                        console.log('MediaError.MEDIA_ERR_DECODE');
                        break;
                    case MediaError.MEDIA_ERR_NONE_SUPPORTED:
                        console.log('MediaError.MEDIA_ERR_NONE_SUPPORTED');
                        break;
                }
                defer.reject(error);
            });
            startTone.play();
        }, function (error) {
            console.log(error);
            defer.reject(error);
        });
        return defer.promise;        
    }
    //show prompt
    //play tone
    service.Capture = function () {                        
        console.log('VoiceToText.Capture()');
        var defer = $q.defer();
        var settings = {
            language: "en-NZ",
            showPopup: false
        };
        window.plugins.speechRecognition.isRecognitionAvailable(function (available) {
            if (available) {
                console.log('speechRecognition available!');

                hasPermission().then(function (isPermitted) {
                    console.log('speechRecognition isPermitted: ' + isPermitted);
                    if (isPermitted) {
                        var ttsClass = '';
                        if (device.platform === 'Android')
                            ttsClass = 'ttsAndroid';

                        var confirm = $ionicPopup.confirm({ cssClass: ttsClass, title: 'Speech recognition', template: "<div style='text-align:center;'><ion-spinner icon='circles'></ion-spinner><div>Speak now.</div></div>", okText: 'Done' });
                        confirm.then(function (result) {
                            window.plugins.speechRecognition.stopListening(function () {
                            }, function (error) {
                                console.log(error);
                            });
                        });
                        
                        window.plugins.speechRecognition.startListening(function (results) {
                            console.log(results);
                            confirm.close();
                            defer.resolve(results);                            
                        }, function (error) {
                            console.log(error);
                            confirm.close();
                            defer.reject(error);
                        }, settings);
                    }
                    else {
                        console.log('speechRecognition not permitted');

                    }
                }, function (error) {
                    console.log('hasPermission() -> failure callback');
                    console.log(error);
                    defer.reject(error);
                });
            }
            console.log('speechRecognition not available');
        }, function (error) {
            //console.log('Speech Recognition not available.');
            console.log(error);
            defer.reject('Speech Recognition not available.');
        });

        return defer.promise;
        
    };

    return service;
}]);

mainApp.factory('TempSvc', ['$q', function($q){
    let service = {};
    return service;    
}]);