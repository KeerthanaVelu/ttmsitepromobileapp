mainApp.factory('VersionSvc', ['$http', '$q', '$resource', 'RemoteUrls', 'AccountSvc', function($http, $q, $resource, RemoteUrls, AccountSvc){
    let service = {};
    service.getLatest = function(){
        let defer = $q.defer();
        $http.get(RemoteUrls.latestVersion, {headers: { 'Authorization': 'Bearer ' + AccountSvc.getAccessTokenV2() }}).then(function(result){
            console.log('[13]');
            console.log(result);
            defer.resolve(result.data);    
        }, function(error){
            console.log(error);
            defer.resolve(null);
        });
        return defer.promise;
    };

    service.currentVersion = '1.0.2022052714';

    return service;
}]);