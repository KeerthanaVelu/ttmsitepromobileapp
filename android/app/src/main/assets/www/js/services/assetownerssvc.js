//#region AssetOwnerSvc
mainApp.factory('AssetOwnerSvc', ['$linq', 'RCASvc', 'DBSvc', function ($linq, RCASvc, DBSvc) {
    var assetOwners = [];
    var service = {};
    service.GetAll = function () {
        return assetOwners;
    };

    service.GetAllByRCA = function (rcaId) {
        var rca = RCASvc.GetById(rcaId);
        var assetOwners = service.GetAll();
        var matchingAssetOwners = $linq.Enumerable().From(assetOwners).Where(function (x) {
            var assetOwnerBranchRegions = $linq.Enumerable().From(x.branches).SelectMany('s => s.regions').ToArray();
            return $linq.Enumerable().From(assetOwnerBranchRegions).Intersect(rca.regions).Any() || (x.branches == null || x.branches.length == 0);
        }).ToArray();

        return matchingAssetOwners;
    };

    service.set = function (assetOwnersList) {
        assetOwners = assetOwnersList;
    };

    service.saveSeedData = function(assetOwners){
        assetOwners.forEach(item => {
            item._id = objectTypes.assetowners + item.id;
        });
    };

    return service;
}]);
//#endregion

mainApp.factory('AssetOwnersSvc', ['$q', '$linq', 'DBSvc', function($q, $linq, DBSvc){
    let service = {};
    service.getAllByRca = function(rcaId){
        let defer = $q.defer();
        DBSvc.getRcaById(rcaId).then(function(rca){
            //console.log(rca);
            if(rca){
                DBSvc.getAllAssetOwners().then(function(assetOwners){
                    let matchingAssetOwners = $linq.Enumerable().From(assetOwners).Where(function (x) {
                        let assetOwnerBranchRegions = $linq.Enumerable().From(x.branches).SelectMany('s => s.regions').ToArray();
                        return $linq.Enumerable().From(assetOwnerBranchRegions).Intersect(rca.regions).Any() || (x.branches == null || x.branches.length == 0);
                    }).ToArray();
                    defer.resolve(matchingAssetOwners);
                }, function(error){ defer.reject(error);});
            }
            else
                defer.resolve([]);
        });

        return defer.promise;        
    };

    return service;
}]);