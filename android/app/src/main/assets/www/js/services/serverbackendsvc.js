mainApp.factory('ServerBackendSvc',['$q', '$resource', 'DBSvc', 'ApiSvc', 'RemoteUrls', function($q, $resource, DBSvc, ApiSvc, RemoteUrls){
    console.log('ServerBackendSvc first line');
    var service = {};
    //sync should be all or nothing?
    var getSeedData = function(tenantId, token){
        console.log('getSeedData() initialized');
        var defer = $q.defer();

        function getResource(url, isArray){
            console.log('get ' + url);
            var resource = $resource(url, {}, {
                'getSeedData': { method: 'GET', isArray: isArray, headers: { 'Authorization': 'Bearer ' + token }, url: url, params: { tenantId: '@tenantId' } }
            });

            return resource.getSeedData({tenantId: tenantId}).$promise;
        }

        var seedsTasks = {
            parties: function(callback){
                console.log(Date());
                getResource(RemoteUrls.partiesDataSeed, false).then(function(response){
                    console.log('parties get seed done');
                    console.log(Date());
                    callback(null, response);
                }, function(error){
                    callback(error, 'parties error');
                });
            },
            trucks: function(callback){
                getResource(RemoteUrls.trucksDataSeed, true).then(function(response){
                    console.log('trucks get seed done');
                    callback(null, response);
                }, function(error){
                    callback(error, 'trucks error');
                });
            },            
            branches: function(callback){
                getResource(RemoteUrls.branchesDataSeed, true).then(function(response){
                    console.log('branches get seed done');
                    callback(null, response);
                }, function(error){
                    callback(error, 'branches error');
                });
            },
            staff: function(callback){
                getResource(RemoteUrls.staffDataSeedV2, true).then(function(response){
                    console.log('staff get seed done');
                    callback(null, response);
                }, function(error){
                    callback(error, 'staff error');
                });
            },
            agencies: function(callback){
                getResource(RemoteUrls.agenciesDataSeed, true).then(function(response){
                    console.log('agencies get seed done');
                    callback(null, response);
                }, function(error){
                    callback(error, 'agencies error');
                });
            },
            agenciesStaff: function(callback){
                getResource(RemoteUrls.agenciesStaffListDataSeed, true).then(function(response){
                    console.log('agencies staff list get seed done');
                    callback(null, response);
                }, function(error){
                    callback(error, 'agencies error');
                });
            }
        };

        async.retry({times:10, interval: 5000}, function(retryCallback){
            async.series(seedsTasks, function(error, results){
                if(error)
                    retryCallback(error, null);
                else
                    retryCallback(null, results);
            });
        }).then(function(response){
            defer.resolve(response);
        }, function(error){
            defer.reject(error);
        });
                
        return defer.promise;
    };

    service.seedData = function(){        
        var defer = $q.defer();
        //get account token
        //get industry parties
        ///rcas then save to db
        ///assetowners then save to db
        ///contractors then save to db
        ///branches then save to db
        ///staff then save to db
        ///trucks then save to db
        var token = DBSvc.getAccessToken();
        var tenantId = DBSvc.getTenantId();
        getSeedData(tenantId, token).then(function(results){
            //console.log('ok');
            console.log(results);
            results.parties.assetOwners.forEach(item => {
                item._id = objectTypes.assetowner + item.id;
            });
            results.parties.rcas.forEach(item => {
                item._id = objectTypes.rca + item.id;
            });
            results.parties.contractors.forEach(item =>{
                item._id = objectTypes.contractor + item.id;
            });
            results.branches.forEach(branch => {
                branch._id = objectTypes.tenantbranch + branch.id;
            });
            results.trucks.forEach(truck => {
                truck._id = objectTypes.truck + truck.id;
            });
            results.staff.forEach(staff =>{
                staff._id = objectTypes.staff + staff.id;
            });
            results.agencies.forEach(agency => {
                agency._id = objectTypes.agency + agency.id;
            });
            results.agenciesStaff.forEach(agencyStaff => {
                agencyStaff._id = objectTypes.agencystaff + agencyStaff.id;
            });
            var parties = results.parties.assetOwners.concat(results.parties.contractors, results.parties.rcas);
            var data = parties.concat(results.branches, results.trucks, results.staff, results.agencies, results.agenciesStaff);
            DBSvc.save(data).then(function(saveResponse){
                console.log('parties saved to pouchdb');
                defer.resolve(saveResponse);
            });
        }, function(error){
            console.log(error);
            defer.reject(error);
        });
        
        return defer.promise;
    };

    return service;
}]);