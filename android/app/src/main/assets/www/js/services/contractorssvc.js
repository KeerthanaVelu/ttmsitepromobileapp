mainApp.factory('ContractorsSvc',['DBSvc', '$linq', '$q', function(DBSvc, $linq, $q){
    let service = {};
    service.getAllByRca = function(rcaId){
        var defer = $q.defer();
        DBSvc.getRcaById(rcaId).then(function(rca){
            console.log(rca);
            if(rca){
                DBSvc.getAllContractors().then(function(contractors){
                    var matchingContractors = $linq.Enumerable().From(contractors).Where(function (x) {
                        var contractorBranchRegions = $linq.Enumerable().From(x.branches).SelectMany('s => s.regions').ToArray();
                        return $linq.Enumerable().From(contractorBranchRegions).Intersect(rca.regions).Any() || (x.branches == null || x.branches.length == 0);
                    }).ToArray();
                    defer.resolve(matchingContractors);
                }, function(error){ defer.reject(error);});
            }
            else
                defer.resolve([]);
        });

        return defer.promise;
    };

    return service;
}]);