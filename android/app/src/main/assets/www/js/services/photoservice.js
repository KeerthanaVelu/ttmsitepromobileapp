﻿mainApp.factory('PhotoSvc', ['$ionicPopup', '$cordovaCamera', '$q', '$filter', '$cordovaFile', '$ionicPlatform', function ($ionicPopup, $cordovaCamera, $q, $filter, $cordovaFile, $ionicPlatform) {
    var service = {};

    function getPhotoFromCamera(options){
        var defer = $q.defer();
        navigator.camera.getPicture(function (imgResult) {
            console.log(imgResult);
            window.resolveLocalFileSystemURL(imgResult, function (resolvedUrl) {
                console.log(resolvedUrl);                                
                var filename = $filter('date')(new Date(), 'ddMMyyyyHHmmss');
                var newFilename = filename + '_' + getFileName(resolvedUrl.nativeURL);
                $cordovaFile.copyFile(getDirectory(resolvedUrl.nativeURL), getFileName(resolvedUrl.nativeURL), cordova.file.dataDirectory, newFilename).then(function (copySuccess) {
                    console.log(copySuccess);
                    var photo = { path: null, created: new Date() };
                    photo.path = copySuccess.nativeURL;
                    photo.fileName = newFilename;
                    defer.resolve(photo);
                }, function (error) {
                    console.log(error);
                    defer.reject(error);
                });        
            }, function(error){
                console.log(error);
            });                            
        }, function (error) {
            console.log(error);
            defer.reject(error);
        }, options);

        return defer.promise;
    }

    service.getPhotoFromCamera = function(){
        var options = {
            quality: 75,
            destinationType: Camera.DestinationType.FILE_URI,
            sourceType: Camera.PictureSourceType.CAMERA,
            encodingType: Camera.EncodingType.JPEG,
            correctOrientation: true,
            saveToPhotoAlbum: true
        };
        return getPhotoFromCamera(options);
    };

    service.getPhoto = function () {
        var defer = $q.defer();
        $ionicPlatform.ready(function () {
            var photoSourcePopup = $ionicPopup.show({
                templateUrl: 'views/photosource.html',
                title: 'Choose source',
                //scope: service,
                buttons: [
                    { text: 'Cancel' },
                    {
                        text: '<b>OK</b>',
                        type: 'button-positive',
                        onTap: function (e) {
                            return this.scope.photoSource;
                        }
                    }
                ]
            });
            photoSourcePopup.then(function (source) {
                if (source) {
                    var options = {
                        quality: 75,
                        destinationType: Camera.DestinationType.FILE_URI,
                        sourceType: Camera.PictureSourceType.CAMERA,
                        encodingType: Camera.EncodingType.JPEG,
                        correctOrientation: true,
                        saveToPhotoAlbum: true
                    };

                    if (source == 'photolibrary') {
                        options.sourceType = Camera.PictureSourceType.PHOTOLIBRARY;
                        $cordovaCamera.getPicture(options).then(function (photoresult) {
                            //console.log(result);
                            window.resolveLocalFileSystemURL(photoresult, function (resolvedUrl) {
                                //console.log(resolvedUrl);
                                //var filename = $filter('date')(new Date(), 'ddMMyyyyHHmmss');                                
                                //newFilename = filename + '_' + resolvedUrl.name;
                                var newFilename = (new ObjectId()).toString() + '.' + resolvedUrl.name.split('.').pop();
                                $cordovaFile.copyFile(getDirectory(resolvedUrl.nativeURL), resolvedUrl.name, cordova.file.dataDirectory, newFilename).then(function (success) {
                                    //console.log(success);
                                    var photo = { path: null, created: new Date() };
                                    photo.path = success.nativeURL;
                                    photo.fileName = newFilename;
                                    //$scope.fields.photos.push(photo);
                                    $cordovaCamera.cleanup().then(function () { });
                                    defer.resolve(photo);
                                }, function (error) {
                                    console.log(error);
                                    defer.reject(error);
                                });
                            });
                        }, function (error) {
                            console.log(error);
                            defer.reject(error);
                        });
                    }
                    else {                        
                        if(device.platform == 'Android'){
                            var permissions = ['android.permission.WRITE_EXTERNAL_STORAGE','android.permission.READ_EXTERNAL_STORAGE'];
                            var Permission = window.plugins.Permission;
                            Permission.has(permissions, function(results) {
                                if (!results[permissions[0]] && !results[permissions[0]]) {
                                    Permission.request(permissions, function(results) {
                                        if (results[permissions[0]] && results[permissions[1]]) {
                                            // permission is granted
                                            getPhotoFromCamera(options).then(function(photo){
                                                defer.resolve(photo);
                                            }, function(error){ 
                                                defer.reject(error);
                                            });
                                        }
                                    }, function(error){ 
                                        console.log(error); 
                                        defer.reject(error);
                                    });
                                }
                                else
                                    getPhotoFromCamera(options).then(function(photo){
                                        defer.resolve(photo);
                                    }, function(error){ 
                                        defer.reject(error);
                                    });
                            }, function(error){defer.reject(error);});
                        }
                        else{
                            getPhotoFromCamera(options).then(function(photo){
                                defer.resolve(photo);
                            }, function(error){ 
                                defer.reject(error);
                            });
                        }
                                      
                    }
                }
            }, function (error) {
                console.log(error);
                defer.reject(error);
            });

        }, function (error) {
            console.log(error);
            defer.reject(error);
        });

        return defer.promise;
    };

    return service;
}]);