mainApp.filter('activityTemplate', ['$filter', function ($filter) {
    return function (activity) {        
        switch (activity.activityId) {
            case 1:
                var template = '<strong>Travel to Site</strong><div>Departure Time <span class="pull-right">' + $filter('date')(activity.startTime, 'hh:mm a dd-MM-yyyy') + '</span></div>';
                template += '<div>Arrival Time <span class="pull-right">' + (activity.endTime == null ? 'Ongoing' : $filter('date')(activity.endTime, 'hh:mm a dd-MM-yyyy')) + '</span></div>';
                return template;            
            case 2:
                var template = '<strong>Site Deployment</strong><div>Start Time <span class="pull-right">' + $filter('date')(activity.startTime, 'hh:mm a dd-MM-yyyy') + '</span></div>';
                template += '<div>End Time <span class="pull-right">' + (activity.endTime == null ? 'Ongoing' : $filter('date')(activity.endTime, 'hh:mm a dd-MM-yyyy')) + '</span></div>';
                return template;
            case 3:
                var template = '<strong>Site Hazard Assessment</strong><div class="wrap-ellipse">' + (activity.Description == null ? '' : activity.Description) + '</span></div>';
                template += '<div>Start Time <span class="pull-right">' + $filter('date')(activity.startTime, 'hh:mm a dd-MM-yyyy') + '</span></div>';
                template += '<div>End Time <span class="pull-right">' + (activity.endTime == null ? 'Ongoing' : $filter('date')(activity.endTime, 'hh:mm a dd-MM-yyyy')) + '</span></div>';
                return template;
            case 4:
                var template = '<strong>Monitoring - Not Active</strong><div>Start Time <span class="pull-right">' + $filter('date')(activity.startTime, 'hh:mm a dd-MM-yyyy') + '</span></div>';
                template += '<div>End Time <span class="pull-right">' + (activity.endTime == null ? 'Ongoing' : $filter('date')(activity.endTime, 'hh:mm a dd-MM-yyyy')) + '</span></div>';
                return template;
            case 5:
                var template = '<strong>Work Space Active</strong><div>Start Time <span class="pull-right">' + (activity.startTime != null ? $filter('date')(activity.startTime, 'hh:mm a dd-MM-yyyy') : '') + '</span></div>';
                template += '<div>End Time <span class="pull-right">' + (activity.endTime == null ? 'Ongoing' : $filter('date')(activity.endTime, 'hh:mm a dd-MM-yyyy')) + '</span></div>';
                return template;
            case 6:
                var template = '<strong>Site Check</strong><div>Time <span class="pull-right">' + $filter('date')(activity.startTime, 'hh:mm a dd-MM-yyyy') + '</span></div>';
                return template;
            case 7:
                var template = '<strong>Site Amendment</strong><div>Start Time <span class="pull-right">' + $filter('date')(activity.startTime, 'hh:mm a dd-MM-yyyy') + '</span></div>';
                template += '<div>End Time <span class="pull-right">' + (activity.endTime == null ? 'Ongoing' : $filter('date')(activity.endTime, 'hh:mm a dd-MM-yyyy')) + '</span></div>';
                return template;
            case 8:
                var template = '<strong>Site Retrieval</strong><div>Start Time <span class="pull-right">' + $filter('date')(activity.startTime, 'hh:mm a dd-MM-yyyy') + '</span></div>';
                template += '<div>End Time <span class="pull-right">' + (activity.endTime == null ? 'Ongoing' : $filter('date')(activity.endTime, 'hh:mm a dd-MM-yyyy')) + '</span></div>';
                return template;            
            case 9:
                var template = '<strong>Travel to Depot</strong><div>Departure Time <span class="pull-right">' + $filter('date')(activity.startTime, 'hh:mm a dd-MM-yyyy') + '</span></div>';
                template += '<div>Arrival Time <span class="pull-right">' + (activity.endTime == null ? 'Ongoing' : $filter('date')(activity.endTime, 'hh:mm a dd-MM-yyyy')) + '</span></div>';
                return template;
            case 10:
                var template = '<strong>Truck Audit</strong><div>Time <span class="pull-right">' + $filter('date')(activity.startTime, 'hh:mm a dd-MM-yyyy') + '</span></div>';
                return template;
            case 11:
                var template = '<strong>Pre-start Prep</strong><div>Start Time <span class="pull-right">' + $filter('date')(activity.startTime, 'hh:mm a dd-MM-yyyy') + '</span></div>';
                template += '<div>End Time <span class="pull-right">' + (activity.endTime == null ? 'Ongoing' : $filter('date')(activity.endTime, 'hh:mm a dd-MM-yyyy')) + '</span></div>';
                return template;
            case 12:
                var template = '<strong>Travel to Next Job</strong><div>Departure Time <span class="pull-right">' + $filter('date')(activity.startTime, 'hh:mm a dd-MM-yyyy') + '</span></div>';
                template += '<div>Arrival Time <span class="pull-right">' + (activity.endTime == null ? 'Ongoing' : $filter('date')(activity.endTime, 'hh:mm a dd-MM-yyyy')) + '</span></div>';
                return template;
            case 13:
                var template = '<strong>End of Shift</strong><div>Time <span class="pull-right">' + $filter('date')(activity.startTime, 'hh:mm a dd-MM-yyyy') + '</span></div>';
                return template;
        }
    };
}]);