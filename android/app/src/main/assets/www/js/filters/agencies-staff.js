mainApp.filter('filterAgenciesStaff', ['$linq', function($linq){
    return function(staff, agencyId){
        if(agencyId == null || agencyId.length == 0)
            return [];
        
        return $linq.Enumerable().From(staff).Where('w => w.agencyId == "' + agencyId + '"').ToArray();
    }
}]);