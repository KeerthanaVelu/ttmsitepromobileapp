mainApp.filter('selectBranch', ['$linq', function ($linq) {
    return function (branches, query) {
        if (query == null || query.length == 0)
            return branches;

        return $linq.Enumerable().From(branches).Where('w => w.branchId == "' + query + '"').ToArray();
    };
}]);