mainApp.filter('activityTemplate', ['$filter', function ($filter) {
    return function (activity) {        
        switch (activity.activityType) {
            case ActivityTypeEnum.TravelToSite:// 1:
            case ActivityTypeEnum.TravelToDepot:// 9:
            case ActivityTypeEnum.TravelToNextJob:// 12:
            case ActivityTypeEnum.SiteHazardAssessment:            
                var template = '<strong>' + ActivityTypeEnum.props[activity.activityType].name + '</strong><div>Departure Time <span class="pull-right">' + $filter('date')(activity.startTime, 'hh:mm a dd-MM-yyyy') + '</span></div>';
                template += '<div>Arrival Time <span class="pull-right">' + (activity.isCancelled ? 'Cancelled' : (activity.endTime == null ? 'Ongoing' : $filter('date')(activity.endTime, 'hh:mm a dd-MM-yyyy'))) + '</span></div>';
                return template;            
            case ActivityTypeEnum.SiteDeployment:// 2:
            case ActivityTypeEnum.SiteAmendmentRecord:// 7            
                var template = '<strong>' + ActivityTypeEnum.props[activity.activityType].name + '</strong><div>Start Time <span class="pull-right">' + $filter('date')(activity.startTime, 'hh:mm a dd-MM-yyyy') + '</span></div>';
                template += '<div>End Time <span class="pull-right">' + (activity.isCancelled ? 'Cancelled' : (activity.endTime == null ? 'Ongoing' : $filter('date')(activity.endTime, 'hh:mm a dd-MM-yyyy'))) + '</span></div>';
                return template;
            case 3:
                var template = '<strong>Site Hazard Assessment</strong><div class="wrap-ellipse">' + (activity.Description == null ? '' : activity.Description) + '</span></div>';
                template += '<div>Start Time <span class="pull-right">' + $filter('date')(activity.startTime, 'hh:mm a dd-MM-yyyy') + '</span></div>';
                template += '<div>End Time <span class="pull-right">' + (activity.endTime == null ? 'Ongoing' : $filter('date')(activity.endTime, 'hh:mm a dd-MM-yyyy')) + '</span></div>';
                return template;
            case 4:
                var template = '<strong>Monitoring - Not Active</strong><div>Start Time <span class="pull-right">' + $filter('date')(activity.startTime, 'hh:mm a dd-MM-yyyy') + '</span></div>';
                template += '<div>End Time <span class="pull-right">' + (activity.endTime == null ? 'Ongoing' : $filter('date')(activity.endTime, 'hh:mm a dd-MM-yyyy')) + '</span></div>';
                return template;
            case ActivityTypeEnum.WorkSpaceActive:
                var template = '<strong>Work Space Active</strong><div>Start Time <span class="pull-right">' + (activity.startTime != null ? $filter('date')(activity.startTime, 'hh:mm a dd-MM-yyyy') : '') + '</span></div>';
                template += '<div>End Time <span class="pull-right">' + (activity.isCancelled ? 'Cancelled' : (activity.endTime == null ? 'Ongoing' : $filter('date')(activity.endTime, 'hh:mm a dd-MM-yyyy'))) + '</span></div>';
                return template;
            case ActivityTypeEnum.OnSiteRecord: //6:
                var template = '<strong>On-Site Record</strong><div>Time <span class="pull-right">' + $filter('date')(activity.startTime, 'hh:mm a dd-MM-yyyy') + '</span></div>';
                return template;
            case 7:
                var template = '<strong>Site Amendment</strong><div>Start Time <span class="pull-right">' + $filter('date')(activity.startTime, 'hh:mm a dd-MM-yyyy') + '</span></div>';
                template += '<div>End Time <span class="pull-right">' + (activity.endTime == null ? 'Ongoing' : $filter('date')(activity.endTime, 'hh:mm a dd-MM-yyyy')) + '</span></div>';
                return template;
            case 8:
                var template = '<strong>Site Retrieval</strong><div>Start Time <span class="pull-right">' + $filter('date')(activity.startTime, 'hh:mm a dd-MM-yyyy') + '</span></div>';
                template += '<div>End Time <span class="pull-right">' + (activity.endTime == null ? 'Ongoing' : $filter('date')(activity.endTime, 'hh:mm a dd-MM-yyyy')) + '</span></div>';
                return template;            
            // case 9:
            //     var template = '<strong>Travel to Depot</strong><div>Departure Time <span class="pull-right">' + $filter('date')(activity.startTime, 'hh:mm a dd-MM-yyyy') + '</span></div>';
            //     template += '<div>Arrival Time <span class="pull-right">' + (activity.endTime == null ? 'Ongoing' : $filter('date')(activity.endTime, 'hh:mm a dd-MM-yyyy')) + '</span></div>';
            //     return template;
            case ActivityTypeEnum.PreStartTruckCheck:
                var template = '<strong>'+ ActivityTypeEnum.props[activity.activityType].name +'</strong><div>Time <span class="pull-right">' + (!activity.endTime ? 'Cancelled' : $filter('date')(activity.startTime, 'hh:mm a dd-MM-yyyy')) + '</span></div>';
                return template;
            case ActivityTypeEnum.TruckPrecheckRecord:
                var template = '<strong>' + ActivityTypeEnum.props[activity.activityType].name + '</strong><div>Time <span class="pull-right">' + ($filter('date')(activity.startTime, 'hh:mm a dd-MM-yyyy')) + '</span></div>';
                return template;
            case ActivityTypeEnum.GearLeftOnSiteRecord:
                var template = '<strong>' + ActivityTypeEnum.props[activity.activityType].name + '</strong><div>Time <span class="pull-right">' + ($filter('date')(activity.startTime, 'hh:mm a dd-MM-yyyy')) + '</span></div>';
                return template;
            case ActivityTypeEnum.TMPOnSiteAssessment:
                var template = '<strong>TMP On-Site Assessment</strong><div>Time <span class="pull-right">' + $filter('date')(activity.startTime, 'hh:mm a dd-MM-yyyy') + '</span></div>';
                return template;
            case 11:
                var template = '<strong>Pre-start Prep</strong><div>Start Time <span class="pull-right">' + $filter('date')(activity.startTime, 'hh:mm a dd-MM-yyyy') + '</span></div>';
                template += '<div>End Time <span class="pull-right">' + (activity.endTime == null ? 'Ongoing' : $filter('date')(activity.endTime, 'hh:mm a dd-MM-yyyy')) + '</span></div>';
                return template;
            // case 12:
            //     var template = '<strong>Travel to Next Job</strong><div>Departure Time <span class="pull-right">' + $filter('date')(activity.startTime, 'hh:mm a dd-MM-yyyy') + '</span></div>';
            //     template += '<div>Arrival Time <span class="pull-right">' + (activity.endTime == null ? 'Ongoing' : $filter('date')(activity.endTime, 'hh:mm a dd-MM-yyyy')) + '</span></div>';
            //     return template;
            case ActivityTypeEnum.EndOfShift:
                var template = '<strong>End of Shift</strong><div>Time <span class="pull-right">' + $filter('date')(activity.endTime, 'hh:mm a dd-MM-yyyy') + '</span></div>';
                return template;
        }
    };
}]);