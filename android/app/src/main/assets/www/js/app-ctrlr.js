mainApp.controller('AppCtrlr', ['$scope', '$state', '$ionicLoading', 'DBSvc', 'AccountSvc', 'WebApiQSvc', 'JobsSvc', 'TrucksSvc', 'SyncService', 'ServerBackendSvc', 'RemoteSyncSvc', '$ionicPlatform',
 function($scope, $state, $ionicLoading, DBSvc, AccountSvc, WebApiQSvc, JobsSvc, TrucksSvc, SyncService, ServerBackendSvc,RemoteSyncSvc, $ionicPlatform){
    console.log('AppCtrlr');
    
    function init(){
        console.log('AppCtrlr -> init');
        $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>'});                
        AccountSvc.hasSeedData().then(function(hasSeedData){
            $ionicLoading.hide();
            if(hasSeedData){
                AccountSvc.initV2().then(function(response){
                    RemoteSyncSvc.init();
                    JobsSvc.initSignalr();
                    JobsSvc.initStopBreakTimeTimer().then(function(){
                        $state.go('app.jobs');
                    });
                }, function(error){
                    console.log(error);
                    $state.go('app.login');
                });
            }
            else
                $state.go('app.login');
        });
        /*
        DBSvc.getAccount().then(function (result) {
            $ionicLoading.hide();
            if (result == null)
                $state.go('app.login');
            else {
                AccountSvc.init(result);
                WebApiQSvc.init(result.token);
                WebApiQSvc.startDataService();
                JobsSvc.init(result.jobs);                    
                TrucksSvc.init(result.trucks);
                TruckAuditSvc.init(result.trucks);
                SyncService.start();                
                $state.go('app.jobs');
            }            
        }, function(error){
            $ionicLoading.hide();
            alert('Something went wrong.');
        });
        */
        //$state.go('app.jobs');
    }

    $scope.$on('$viewContentLoaded', function(event){        
        init();
    });

    $scope.$on('unathorizedaccess', function (event, data) {
        console.log(data);
        alert('Unauthorized: Please login again.');
        //WebApiQSvc.stopDataService();
        RemoteSyncSvc.stop();
        $state.go('app.login', { mode: 'relogin' });
    });

    $scope.$on('loginsuccess', function(event, data){
        console.log("$scope.$on('loginsuccess')");
        //console.log(data);
        AccountSvc.save(data).then(function(response){
            AccountSvc.hasSeedData().then(function(hasSeedData){
                //console.log(results);
                if(!hasSeedData){
                    $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner><div>Syncing server...</div>'});
                    ServerBackendSvc.seedData().then(function(response){     
                        AccountSvc.setHasSeedData(true).then(function(response){                            
                            AccountSvc.initV2().then(function(response){
                                JobsSvc.initSignalr();
                                $ionicLoading.hide();
                                $state.go('app.jobs');
                            }, function(error){
                                console.log(error);
                                $ionicLoading.hide();
                                alert('Something went wrong.');
                            });
                        }, function(error){
                            console.log(error);
                            $ionicLoading.hide();
                            alert('Something went wrong.');
                        });
                    }, function(error){
                        $ionicLoading.hide();
                        alert('Something went wrong.');
                    });
                }
                else{
                    $state.go('app.jobs');
                }
            }, function(error){
                console.log(error);
            });
        }, function(error){
            console.log(error);
        });
    });

    //init();
}]);